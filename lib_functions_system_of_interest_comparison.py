#Library to read OpenModelica or Dymola generated mat files
import DyMat

#For debugging
import pdb
#Use below as a breakpoint as needed
#pdb.set_trace()

#For using "map" element wize addition
from operator import add

def buildModelRunSimExtractData(point_design_name, scenario_name, scenario_descriptions, cost_scenario_descriptions, scenario_type, OMPython, os, simulation_length, modelica_variable_of_interest, extract_data_type, stretch_goal, simulation_results, new_simulation, component_name, dymola, system_of_interest_name, testbed_package_dot_notation, testbed_package_slash_notation, alternative_for_replacement_package_dot_notation, alternative_for_replacement, component_for_replacement_package_dot_notation, component_for_replacement, path_library_top_dir):
    print "Starting: ScenarioProcess: " + scenario_name
    #Setup the directory for the scenario
    OMPython.execute("mkdir(\""+scenario_name+"\")")
    OMPython.execute("cd(\""+scenario_name+"\")")
    os.chdir(scenario_name)
    path_for_dymola_results = os.getcwd()
    print OMPython.execute("cd()")
    if (point_design_name != "Ideal"):
        #Only do simulations if this is not the ideal design
        print "Copying: " + "SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Scenario_"+scenario_name+",\"Auto_"+scenario_name+"_"+point_design_name
        print OMPython.execute("copyClass(SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Scenario_"+scenario_name+",\"Auto_"+scenario_name+"_"+point_design_name+"\",SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto)")
        print "Setting new model source file to: " + path_library_top_dir+"/"+testbed_package_slash_notation+"/ReadyTestBeds/Auto/Auto_"+scenario_name+"_"+point_design_name+".mo"
        print OMPython.execute("setSourceFile(SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+", \""+path_library_top_dir+"/"+testbed_package_slash_notation+"/ReadyTestBeds/Auto/Auto_"+scenario_name+"_"+point_design_name+".mo\")")
        print "Saving the new file: "
        print OMPython.execute("save(SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+")")
        #Insert the alternate point design into the test bed
        filename = path_library_top_dir+"\\"+testbed_package_slash_notation+"\ReadyTestBeds\Auto\\Auto_"+scenario_name+"_"+point_design_name+".mo"
        with open(filename, 'r') as f:
          text = f.read()
        if alternative_for_replacement_package_dot_notation + "." + alternative_for_replacement not in text:
            print "Cannot find"+alternative_for_replacement_package_dot_notation + "." + alternative_for_replacement+"in the provided testbed. Exiting"
            sys.exit()
        else:
            text = text.replace(alternative_for_replacement_package_dot_notation + "." + alternative_for_replacement, alternative_for_replacement_package_dot_notation + "." + point_design_name)
        if component_for_replacement != "No_component_variation":
            #Real component for variation
            if component_for_replacement_package_dot_notation + "." + component_for_replacement not in text:
                print "Cannot find" + component_for_replacement_package_dot_notation + "." + component_for_replacement + "in the provided testbed. Exiting"
                sys.exit()
            else:
                text = text.replace(component_for_replacement_package_dot_notation + "." + component_for_replacement, component_for_replacement_package_dot_notation + "." + component_name)
        with open(filename, 'w') as f:
          f.write(text)
        print "Reload the model after modification: "
        print OMPython.execute("reloadClass(SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+")")
        #Simulate
        #In the past had needed to return back to the library. This is left incase this bug occurs again.
        #OMPython.execute("cd(\"../../../L1_Assess_L2/ReadyTestBeds/ForAutomatedReplacement/\")")
        #os.chdir("..\..\..\L1_Assess_L2\ReadyTestBeds\ForAutomatedReplacement\\")
        if new_simulation:
            dymola.openModel(os.path.abspath(path_library_top_dir+"\\"+testbed_package_slash_notation+"\ReadyTestBeds\Auto\\Auto_"+scenario_name+"_"+point_design_name+".mo"))
            result_file = path_for_dymola_results + "/dsres"
            print "Simulating: " + "dymola.simulateModel(\"SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+", stopTime="+str(simulation_length)+",resultFile="+result_file+")"
            dymola.simulateModel("SolarBoatAutomation."+testbed_package_dot_notation+".ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name, stopTime=simulation_length,resultFile=result_file)
            #dymola.savelog(path_for_dymola_results + "/log.txt")

            #Old for OpenModelica
            #print "Simulating: " + "simulate(SolarBoatAutomation.L1_Assess_L2.ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+",stopTime="+simulation_length_string+",outputFormat=\"mat\")"
            #print OMPython.execute("simulate(SolarBoatAutomation.L1_Assess_L2.ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+",stopTime="+simulation_length_string+",outputFormat=\"mat\")")
        else:
            print "No simulation requested"
    #Extract the results from the mat files
    simulated_performance = 0
    if (point_design_name != "Ideal"):
        #Only extract data from files if this is not the ideal design
        #Extract the value of the variable to interest from simulation results
        print "Loading .mat file: " + "dsres.mat"
        print OMPython.execute("cd()")
        mat_file = DyMat.DyMatFile("dsres.mat")

        #Old for OPenModelica
        #print "Loading .mat file: " + "SolarBoatAutomation.L1_Assess_L2.ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+"_res.mat"
        #print OMPython.execute("cd()")
        #mat_file = DyMat.DyMatFile("SolarBoatAutomation.L1_Assess_L2.ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+"_res.mat")

        #TODO pull out the time at which this occurs to plot on the graph
        if (extract_data_type == "max"):
            simulated_performance = mat_file.data(modelica_variable_of_interest).max()
        elif (extract_data_type == "min"):
            simulated_performance = mat_file.data(modelica_variable_of_interest).min()
        elif (extract_data_type == "mean"):
            simulated_performance = mat_file.data(modelica_variable_of_interest).mean()
        else:
            print "WARNING: Unrecognised extract_data_type:" +extract_data_type+ " for " +scenario_name+"_"+point_design_name
    else:
        #Ideal point design so set to the stretch goal
        simulated_performance = stretch_goal
    print modelica_variable_of_interest + " = " + str(simulated_performance) + " Data processing: " + extract_data_type
    #Process the results for the scenario
    unweighted_value = 0
    weighted_value = 0
    if scenario_type == "scenario":
        #Store the performance result in the structure
        simulation_results.scenario_results_dict[scenario_name,point_design_name].setSimulatedPerformance(simulated_performance)
        #Compute value from performance
        [unweighted_value, weighted_value] = scenario_descriptions.scenario_description_dict[scenario_name].computeValueFromPerformance(simulated_performance)
        #Store value in the approriate class
        simulation_results.scenario_results_dict[scenario_name,point_design_name].storeComputedValue(unweighted_value, weighted_value)
    elif scenario_type == "cost":
        #Store the performance result in the structure
        simulation_results.cost_scenario_results_dict[scenario_name,point_design_name].setSimulatedPerformance(simulated_performance)
        #TODO Don't need this #Compute value from performance
        [unweighted_value, weighted_value] = cost_scenario_descriptions.scenario_description_dict[scenario_name].computeValueFromPerformance(simulated_performance)
        #TODO Don't need this #Store value in the approriate class
        simulation_results.cost_scenario_results_dict[scenario_name,point_design_name].storeComputedValue(unweighted_value, weighted_value)
    else :
        print "scenario_type: " + scenario_type + " is not supported"
    print "unweighted_value = " + str(unweighted_value) + "   weighted_value = " + str(weighted_value)
    print "Completed: ScenarioProcess: " + scenario_name
    print OMPython.execute("cd(\"..\")")
    os.chdir("..")

def runAllVisualization(point_design_descriptions, scenario_descriptions, simulation_results, cost_scenario_descriptions, component_name, system_of_interest_name, cost_scenario_name):
    #For plots and charts
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import matplotlib.ticker as mtick
    #For sorting data
    import operator

    #TODO These massive functions are not ideal as they make it not easy to see the code at the bottom of them
    def runScenarioVisulization(point_design_descriptions, simulation_results, scenario_descriptions, component_name, system_of_interest_name):
        print "Starting visulization of: " + scenario_name

        #Get the results
        simulated_performance_dict = dict()
        unweighted_value_dict = dict()
        for point_design_name in point_design_descriptions.getPointDesignNameList():
            simulated_performance_dict[point_design_name] = simulation_results.getResultsForScenarioAndPointDesignPair(scenario_name,point_design_name).getSimulatedPerformance()
            unweighted_value_dict[point_design_name] = simulation_results.getResultsForScenarioAndPointDesignPair(scenario_name,point_design_name).getUnweightedValue()
        print "simulated_performance_dict: " + str(simulated_performance_dict)
        modelica_variable_of_interest = scenario_descriptions.GetModelicaVariableOfInterest(scenario_name)
        modelica_variable_of_interest_units = scenario_descriptions.GetModelicaVariableOfInterestUnits(scenario_name)
        stretch_goal = scenario_descriptions.getStretchGoal(scenario_name)
        minimum_acceptable_performance = scenario_descriptions.getMinimumAcceptablePerformace(scenario_name)
        value_performance_slope_direction = scenario_descriptions.getValuePerformaceSlopeDirection(scenario_name)
        extract_data_type = scenario_descriptions.getExtractDataType(scenario_name)

        #Sort data (by unweighted value)
        unweighted_value_dict_larger_value_first = sorted(unweighted_value_dict.items(), key=operator.itemgetter(1), reverse=True)
        print "unweighted_value_dict_larger_value_first: " + str(unweighted_value_dict_larger_value_first)
        point_design_names_list_unweighted_value_ordered = list()
        unweighted_value_list_ordered = list()
        for name_and_performance in unweighted_value_dict_larger_value_first:
            point_design_names_list_unweighted_value_ordered.append(name_and_performance[0])
            unweighted_value_list_ordered.append(name_and_performance[1])

        #Sort data (performance values by by unweighted value)
        performance_list_unweighted_value_ordered = list()
        for point_design_name in point_design_names_list_unweighted_value_ordered:
            performance_list_unweighted_value_ordered.append(simulated_performance_dict[point_design_name])
        print "performance_list_unweighted_value_ordered: " + str(performance_list_unweighted_value_ordered)

        #Get a plot area
        fig_senario = plt.figure(figsize=(12,8))
        fig_senario.suptitle(component_name+"  "+scenario_name+": "+modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+").\nPerformance taken as "+extract_data_type+" of the simulation data",fontsize='small',fontweight='bold')

        #Simulation graph
        ax4 = fig_senario.add_subplot(2,2,1)
        plt.title("Simulation (Plot 1)", fontsize='small')
        point_design_performance_plots = list()
        legend_text_list = list()
        for point_design_name in point_design_descriptions.getPointDesignNameList():
            legend_text_list.append(point_design_name)
            if (point_design_name != "Ideal"):
                #Use simulation data
                mat_file = DyMat.DyMatFile(point_design_name+"/"+scenario_name+"/dsres.mat")
                #Old open modelica
                #mat_file = DyMat.DyMatFile(point_design_name+"/"+scenario_name+"/SolarBoatAutomation.L1_Assess_L2.ReadyTestBeds.Auto.Auto_"+scenario_name+"_"+point_design_name+"_res.mat")
                performance_plot = ax4.plot(mat_file.abscissa(modelica_variable_of_interest)[0],mat_file.data(modelica_variable_of_interest))
                point_design_performance_plots.append(performance_plot[0])
            elif (point_design_name == "Ideal"):
                #Use stretch_goal
                stretch_goal = scenario_descriptions.getStretchGoal(scenario_name)
                performance_plot = ax4.plot([0,scenario_descriptions.getSimulationLength(scenario_name)],[stretch_goal,stretch_goal],linewidth=2.5,linestyle="--")
                point_design_performance_plots.append(performance_plot[0])
                #Use minimum acceptable performance
                minimum_acceptable_performance = scenario_descriptions.getMinimumAcceptablePerformace(scenario_name)
                performance_plot_minium_acceptable = ax4.plot([0,scenario_descriptions.getSimulationLength(scenario_name)],[minimum_acceptable_performance,minimum_acceptable_performance],linewidth=2.5,linestyle="--")
                point_design_performance_plots.append(performance_plot_minium_acceptable[0])
                legend_text_list.append("Minimum acceptable performance")

        plt.legend(point_design_performance_plots, legend_text_list, fontsize='x-small', loc="best")
        plt.ylabel(modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='x-small')
        plt.yticks(fontsize='x-small')
        plt.xticks(fontsize='x-small')
        plt.xlabel("Time (s)", fontsize='x-small')
        plt.axhline(0, color='black')
        plt.grid(b=True, which='major', color='k', linestyle='-')
        plt.grid(b=True, which='minor', color='k', linestyle='-')

        #Performance vs. design name bar chart
        ax2 = fig_senario.add_subplot(2,2,2)
        plt.title("Performance used in value function (Plot 2)", fontsize='small')
        p3 = ax2.bar(ind, performance_list_unweighted_value_ordered, width, color='g')
        plt.ylabel(modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='x-small')
        plt.xticks(ind+width/2., point_design_names_list_unweighted_value_ordered, fontsize=5)
        plt.yticks(fontsize='x-small')
        plt.axhline(0, color='black')

        #Value function (Unweighted value vs. performance measure)
        ax1 = fig_senario.add_subplot(2,2,3)
        plt.title("Value function (Plot 3)", fontsize='small')
        p1 = ax1.plot([minimum_acceptable_performance,stretch_goal],[0,1])
        p2 = ax1.plot(performance_list_unweighted_value_ordered,unweighted_value_list_ordered,'kx')
        ax1.tick_params(axis='both', which='major', labelsize='x-small')
        for i, name in enumerate(point_design_names_list_unweighted_value_ordered):
            ax1.annotate(name,(performance_list_unweighted_value_ordered[i],unweighted_value_list_ordered[i]), xytext = (60, -20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'), fontsize='xx-small')
        plt.ylabel("Unweighted value", fontsize='x-small')
        plt.xlabel("Performance: "+modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='x-small')
        plt.grid(b=True, which='major', color='k', linestyle='-')
        plt.grid(b=True, which='minor', color='k', linestyle='-')

        #Unweighted value vs. design name bar chart
        ax3 = fig_senario.add_subplot(2,2,4)
        plt.title("Unweighted value computed from performance (Plot 4)", fontsize='small')
        p4 = ax3.bar(ind, unweighted_value_list_ordered, width, color='g')
        plt.ylabel("Unweighted value", fontsize='x-small')
        plt.xticks(ind+width/2., point_design_names_list_unweighted_value_ordered, fontsize=5)
        plt.yticks(np.arange(0,1.1,0.1), fontsize='x-small')
        plt.axhline(0, color='black')

        #Make everything visable
        plt.tight_layout()
        plt.subplots_adjust(top=0.92)
        #Save the figure
        fig_senario.savefig(scenario_name+".png")






        #Get race results plot area
        fig_race_results = plt.figure(figsize=(12,8))

        #Simulation graph

        #Performance vs. design name bar chart
        ax5 = fig_race_results.add_subplot(1,1,1)
        plt.title("Boat predicted time to complete race", fontsize='x-large')
        p5 = ax5.bar(ind, performance_list_unweighted_value_ordered, width, color='g')
        plt.ylabel(modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='x-large')
        plt.xlabel("Design alternative name", fontsize='x-large')
        plt.xticks(ind+width/2., point_design_names_list_unweighted_value_ordered, fontsize='medium')
        plt.yticks(fontsize='small')
        plt.axhline(0, color='black')

        #Make everything visable
        plt.tight_layout()
        #Save the figure
        fig_race_results.savefig("race_results.png")







    def runCombinedScenariosVisulization(point_design_descriptions, simulation_results, cost_scenario_descriptions, cost_scenario_name):
        print "Starting visulization of all scenarios combined"
        #Get the total weighted value
        total_weighted_value_dict = dict()
        for point_design_name in point_design_descriptions.getPointDesignNameList():
            total_weighted_value_dict[point_design_name] = simulation_results.getTotalWeightedValueForPointDesign(point_design_name)
        #Sort data by total weighted value
        total_weighted_value_dict_larger_value_first = sorted(total_weighted_value_dict.items(), key=operator.itemgetter(1), reverse=True)
        print "total_weighted_value_dict_larger_value_first: "
        print total_weighted_value_dict_larger_value_first
        point_design_names_list_value_ordered = list()
        total_weighted_value_list = list()
        total_weighted_value_with_zero_value_list = list()
        for name_and_weighted_value in total_weighted_value_dict_larger_value_first:
            point_design_names_list_value_ordered.append(name_and_weighted_value[0])
            total_weighted_value_list.append(name_and_weighted_value[1])
            if simulation_results.pointDesignFailedAScenario(name_and_weighted_value[0]):
                # One scenario failed so set the total value to zero
                total_weighted_value_with_zero_value_list.append(0)
            else:
                # All scenarios passed
                total_weighted_value_with_zero_value_list.append(name_and_weighted_value[1])

        #Make a diagram area
        fig_all_senarios = plt.figure(figsize=(12,8))

        #Bar chart of total value for each design
        ax1 = fig_all_senarios.add_subplot(3,1,1)
        plt.title(component_name+"  Weighted value comparison")
        p1 = ax1.bar(ind, total_weighted_value_with_zero_value_list, width, color='r')
        plt.ylabel("Total weighted value", fontsize='x-small')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='x-small')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='x-small')
        plt.axhline(0, color='black')

        #Bar chart of stacked weighted value vs point design bar chart (ranked, value breakdown)
        ax2 = fig_all_senarios.add_subplot(3,1,2)
        #Get the data
        weighted_value_dict = simulation_results.getWeightedValueDictionary()
        scenario_name_list = scenario_descriptions.getScenarioNameList()
        bar_plot_list = list()
        #Process the data
        #For each scenario. Make a list of the weighted values (for each alternative)
        previous_scenario_weighted_value_list = [0] * len(point_design_names_list_value_ordered)
        scenario_number = 0
        for scenario_name in scenario_name_list:
            scenario_weighted_value_list = list()
            for point_design_name in point_design_names_list_value_ordered:
                scenario_weighted_value_list.append(weighted_value_dict[scenario_name,point_design_name])
            #pdb.set_trace()
            #Make a list of plots varying the colour of each one
            #Auto generate a colour
            colour = cm.jet(1.*scenario_number/len(scenario_name_list))
            plot = ax2.bar(ind, scenario_weighted_value_list, width, color=colour, bottom=previous_scenario_weighted_value_list)
            bar_plot_list.append(plot[0])
            previous_scenario_weighted_value_list = map(add, scenario_weighted_value_list, previous_scenario_weighted_value_list)
            scenario_number = scenario_number + 1
        #Insert the legend
        legend_text_list = [scenario_name + ": Weight = " + str(scenario_descriptions.getWeight(scenario_name)) for scenario_name in scenario_name_list]
        plt.legend(bar_plot_list, legend_text_list, fontsize='x-small', loc="best")
        #Setup the axes
        plt.ylabel("Weighted value", fontsize='x-small')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='x-small')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='x-small')
        plt.axhline(0, color='black')

        #Cost vs performance scatter chart
        #Get the cost data
        cost_money_dict = dict()
        for point_design_name in point_design_descriptions.getPointDesignNameList():
            cost_money_dict[point_design_name] = simulation_results.getResultsForCostScenarioAndPointDesignPair(cost_scenario_name,point_design_name).getSimulatedPerformance()
        print "cost_money_dict: " + str(cost_money_dict)
        #Sort the data (cost values by unweighted value)
        cost_dict_unweighted_value_ordered = list()
        for point_design_name in point_design_names_list_value_ordered:
            cost_dict_unweighted_value_ordered.append(cost_money_dict[point_design_name])
        print "cost_dict_unweighted_value_ordered: " + str(cost_dict_unweighted_value_ordered)
        # Labels for the axis
        modelica_variable_of_interest = cost_scenario_descriptions.GetModelicaVariableOfInterest(cost_scenario_name)
        modelica_variable_of_interest_units = cost_scenario_descriptions.GetModelicaVariableOfInterestUnits(cost_scenario_name)
        #Make the plot
        ax3 = fig_all_senarios.add_subplot(3,1,3)
        plt.scatter(cost_dict_unweighted_value_ordered, total_weighted_value_list, marker='x')
        #Plot the label names
        for i, name in enumerate(point_design_names_list_value_ordered):
            ax3.annotate(name,(cost_dict_unweighted_value_ordered[i],total_weighted_value_list[i]), xytext = (60, -20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'), fontsize='xx-small')
        minimum_acceptable_performance = cost_scenario_descriptions.getMinimumAcceptablePerformace(cost_scenario_name)
        cost_limit_p = ax3.plot([minimum_acceptable_performance,minimum_acceptable_performance],[0,1],linewidth=2.5,linestyle="--")
        plt.legend([cost_limit_p[0]], ["budget limit"], fontsize='x-small', loc="best")
        plt.ylabel("Weighted value", fontsize='x-small')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='x-small')
        plt.xlabel(modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='x-small')
        plt.xticks(fontsize='x-small')
        plt.grid(b=True, which='major', color='k', linestyle='-')

        #Make everything visable
        plt.tight_layout()
        #Save the figure
        fig_all_senarios.savefig("_FinalResult.png")



        #Make a diagram area
        fig_all_senarios_bar_contribution = plt.figure(figsize=(12,8))

        #Bar chart of stacked weighted value vs point design bar chart (ranked, value breakdown)
        ax2 = fig_all_senarios_bar_contribution.add_subplot(1,1,1)
        #Get the data
        weighted_value_dict = simulation_results.getWeightedValueDictionary()
        scenario_name_list = scenario_descriptions.getScenarioNameList()
        bar_plot_list = list()
        #Process the data
        #For each scenario. Make a list of the weighted values (for each alternative)
        previous_scenario_weighted_value_list = [0] * len(point_design_names_list_value_ordered)
        scenario_number = 0
        for scenario_name in scenario_name_list:
            scenario_weighted_value_list = list()
            for point_design_name in point_design_names_list_value_ordered:
                scenario_weighted_value_list.append(weighted_value_dict[scenario_name,point_design_name])
            #pdb.set_trace()
            #Make a list of plots varying the colour of each one
            #Auto generate a colour
            colour = cm.jet(1.*scenario_number/len(scenario_name_list))
            plot = ax2.bar(ind, scenario_weighted_value_list, width, color=colour, bottom=previous_scenario_weighted_value_list)
            bar_plot_list.append(plot[0])
            previous_scenario_weighted_value_list = map(add, scenario_weighted_value_list, previous_scenario_weighted_value_list)
            scenario_number = scenario_number + 1
        #Insert the legend
        legend_text_list = [scenario_name + ": Weight = " + str(scenario_descriptions.getWeight(scenario_name)) for scenario_name in scenario_name_list]
        plt.legend(bar_plot_list, legend_text_list, fontsize='large', loc="best")
        #Setup the axes
        plt.ylabel("Weighted value", fontsize='medium')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.axhline(0, color='black')

        #Make everything visable
        plt.tight_layout()
        #Save the figure
        fig_all_senarios_bar_contribution.savefig("_FinalResult-LargeBreakdown.png")



        #Make a diagram area
        just_bars = plt.figure(figsize=(12,8))

        #Bar chart of stacked weighted value vs point design bar chart (ranked, value breakdown)
        ax3 = just_bars.add_subplot(2,1,1)
        p1 = ax3.bar(ind, total_weighted_value_with_zero_value_list, width, color='r')
        plt.ylabel("Total weighted value", fontsize='medium')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.axhline(0, color='black')


        #Bar chart of stacked weighted value vs point design bar chart (ranked, value breakdown)
        ax4 = just_bars.add_subplot(2,1,2)
        #Get the data
        weighted_value_dict = simulation_results.getWeightedValueDictionary()
        scenario_name_list = scenario_descriptions.getScenarioNameList()
        bar_plot_list = list()
        #Process the data
        #For each scenario. Make a list of the weighted values (for each alternative)
        previous_scenario_weighted_value_list = [0] * len(point_design_names_list_value_ordered)
        scenario_number = 0
        for scenario_name in scenario_name_list:
            scenario_weighted_value_list = list()
            for point_design_name in point_design_names_list_value_ordered:
                scenario_weighted_value_list.append(weighted_value_dict[scenario_name,point_design_name])
            #pdb.set_trace()
            #Make a list of plots varying the colour of each one
            #Auto generate a colour
            colour = cm.jet(1.*scenario_number/len(scenario_name_list))
            plot = ax4.bar(ind, scenario_weighted_value_list, width, color=colour, bottom=previous_scenario_weighted_value_list)
            bar_plot_list.append(plot[0])
            previous_scenario_weighted_value_list = map(add, scenario_weighted_value_list, previous_scenario_weighted_value_list)
            scenario_number = scenario_number + 1
        #Insert the legend
        legend_text_list = [scenario_name + ": Weight = " + str(scenario_descriptions.getWeight(scenario_name)) for scenario_name in scenario_name_list]
        plt.legend(bar_plot_list, legend_text_list, fontsize='large', loc="best")
        #Setup the axes
        plt.ylabel("Weighted value", fontsize='medium')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.axhline(0, color='black')

        #Make everything visable
        plt.tight_layout()
        #Save the figure
        just_bars.savefig("_FinalResult-double.png")


        #Make a diagram area
        fig_all_senarios_big = plt.figure(figsize=(12,12))

        #Bar chart of total value for each design
        ax1 = fig_all_senarios_big.add_subplot(3,1,1)
        plt.title(component_name+"  Weighted value comparison")
        p1 = ax1.bar(ind, total_weighted_value_with_zero_value_list, width, color='r')
        plt.ylabel("Total weighted value", fontsize='medium')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.axhline(0, color='black')

        #Bar chart of stacked weighted value vs point design bar chart (ranked, value breakdown)
        ax2 = fig_all_senarios_big.add_subplot(3,1,2)
        #Get the data
        weighted_value_dict = simulation_results.getWeightedValueDictionary()
        scenario_name_list = scenario_descriptions.getScenarioNameList()
        bar_plot_list = list()
        #Process the data
        #For each scenario. Make a list of the weighted values (for each alternative)
        previous_scenario_weighted_value_list = [0] * len(point_design_names_list_value_ordered)
        scenario_number = 0
        for scenario_name in scenario_name_list:
            scenario_weighted_value_list = list()
            for point_design_name in point_design_names_list_value_ordered:
                scenario_weighted_value_list.append(weighted_value_dict[scenario_name,point_design_name])
            #pdb.set_trace()
            #Make a list of plots varying the colour of each one
            #Auto generate a colour
            colour = cm.jet(1.*scenario_number/len(scenario_name_list))
            plot = ax2.bar(ind, scenario_weighted_value_list, width, color=colour, bottom=previous_scenario_weighted_value_list)
            bar_plot_list.append(plot[0])
            previous_scenario_weighted_value_list = map(add, scenario_weighted_value_list, previous_scenario_weighted_value_list)
            scenario_number = scenario_number + 1
        #Insert the legend
        legend_text_list = [scenario_name + ": Weight = " + str(scenario_descriptions.getWeight(scenario_name)) for scenario_name in scenario_name_list]
        plt.legend(bar_plot_list, legend_text_list, fontsize='medium', loc="best")
        #Setup the axes
        plt.ylabel("Weighted value", fontsize='medium')
        plt.xticks(ind+width/2., point_design_names_list_value_ordered, fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.axhline(0, color='black')

        #Cost vs performance scatter chart
        #Get the cost data
        cost_money_dict = dict()
        for point_design_name in point_design_descriptions.getPointDesignNameList():
            cost_money_dict[point_design_name] = simulation_results.getResultsForCostScenarioAndPointDesignPair(cost_scenario_name,point_design_name).getSimulatedPerformance()
        print "cost_money_dict: " + str(cost_money_dict)
        #Sort the data (cost values by unweighted value)
        cost_dict_unweighted_value_ordered = list()
        for point_design_name in point_design_names_list_value_ordered:
            cost_dict_unweighted_value_ordered.append(cost_money_dict[point_design_name])
        print "cost_dict_unweighted_value_ordered: " + str(cost_dict_unweighted_value_ordered)
        # Labels for the axis
        modelica_variable_of_interest = cost_scenario_descriptions.GetModelicaVariableOfInterest(cost_scenario_name)
        modelica_variable_of_interest_units = cost_scenario_descriptions.GetModelicaVariableOfInterestUnits(cost_scenario_name)
        #Make the plot
        ax3 = fig_all_senarios_big.add_subplot(3,1,3)
        plt.scatter(cost_dict_unweighted_value_ordered, total_weighted_value_list, marker='x')
        #Plot the label names
        for i, name in enumerate(point_design_names_list_value_ordered):
            ax3.annotate(name,(cost_dict_unweighted_value_ordered[i],total_weighted_value_list[i]), xytext = (60, -20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'), fontsize='medium')
        minimum_acceptable_performance = cost_scenario_descriptions.getMinimumAcceptablePerformace(cost_scenario_name)
        cost_limit_p = ax3.plot([minimum_acceptable_performance,minimum_acceptable_performance],[0,1],linewidth=2.5,linestyle="--")
        plt.legend([cost_limit_p[0]], ["budget limit"], fontsize='medium', loc="best")
        plt.ylabel("Weighted value", fontsize='medium')
        plt.yticks(np.arange(0,1.1,0.1), fontsize='medium')
        plt.xlabel(modelica_variable_of_interest+" ("+modelica_variable_of_interest_units+")", fontsize='medium')
        plt.xticks(fontsize='medium')
        plt.grid(b=True, which='major', color='k', linestyle='-')

        #Make everything visable
        plt.tight_layout()
        #Save the figure
        fig_all_senarios_big.savefig("_FinalResult-big.png")


    #Setup the sizes based on how many point designs there are
    number_point_designs = len(point_design_descriptions.getPointDesignNameList())
    # the x locations for the groups
    ind = np.arange(number_point_designs) #TODO Make this based on something more worthwhile
    # the width of the bars: can also be len(x) sequence
    width = 0.35 #TODO Make this based on the number of alternatives

    #For each scenario create a set of figures
    for scenario_name in scenario_descriptions.getScenarioNameList():
        runScenarioVisulization(point_design_descriptions, simulation_results, scenario_descriptions, component_name,system_of_interest_name)
    #For the combined data of all the scenarios make a set of figures
    runCombinedScenariosVisulization(point_design_descriptions, simulation_results, cost_scenario_descriptions, cost_scenario_name)