Created by Joshua Sutherland as part of Master's thesis at The University of Tokyo in 2016.
No use, modification, copying or transfering without permission.

Dependencies required:
    Dymat: https://bitbucket.org/jraedler/dymat/overview
    OpenModelica: https://openmodelica.org/
    JModelica: http://www.jmodelica.org/