#Define the lowest data structure to store the score from the simulation

#For integer division
from __future__ import division

#For debugging
import pdb
#Use below as a breakpoint as needed
#pdb.set_trace()

class SimulationResult:
    "Location to store the results of the simulation result"
    def setSimulatedPerformance(self, simulated_performance):
        #Set the performance from the Modelica simulation
        self.simulated_performance = simulated_performance
    def storeComputedValue(self, unweighted_value, weighted_value):
        #Store the computed value for the simulation
        self.unweighted_value = unweighted_value
        self.weighted_value = weighted_value
    def getUnweightedValue(self):
        #Get the unweighted value for scenario simulation run
        return self.unweighted_value
    def getWeightedValue(self):
        #Get the weighted value for scenario simulation run
        return self.weighted_value
    def getSimulatedPerformance(self):
        #Get the simulated performance for the scenario simulation run
        return self.simulated_performance

class SimulationResults:
    "Class containing all of the simulation results"
    scenario_results_dict = dict()
    cost_scenario_results_dict = dict()
    scenario_names_list = list()
    point_designs_name_list = list()
    cost_scenario_names_list = list()
    #def __init__(self, scenario_names_list, point_designs_name_list):
    def __init__(self, scenario_names_list, point_designs_name_list, cost_scenario_names_list):
        self.scenario_names_list = scenario_names_list
        self.point_designs_name_list = point_designs_name_list
        self.cost_scenario_names_list = cost_scenario_names_list
        #Create results structure for all scenarios and point designs
        for scenario_name in scenario_names_list:
            for point_design_name in point_designs_name_list:
                self.scenario_results_dict[scenario_name,point_design_name] = SimulationResult()
        #Create results structure for all cost_scenarios and point designs
        for cost_scenario_name in cost_scenario_names_list:
            for point_design_name in point_designs_name_list:
                self.cost_scenario_results_dict[cost_scenario_name,point_design_name] = SimulationResult()
    def getTotalWeightedValueForPointDesign(self, point_design):
        total_weighted_value = 0
        for scenario_name in self.scenario_names_list:
            total_weighted_value = total_weighted_value + self.scenario_results_dict[scenario_name, point_design].getWeightedValue()
        return total_weighted_value
    def pointDesignFailedAScenario(self, point_design):
        for scenario_name in self.scenario_names_list:
            scenario_value = self.scenario_results_dict[scenario_name, point_design].getWeightedValue()
            if scenario_value <= 0:
                # One senario failed so exit
                return True
        # All scenarios passed so exit
        return False
    def getResultsForScenarioAndPointDesignPair(self, scenario, point_design):
        return self.scenario_results_dict[scenario, point_design]
    def getResultsForCostScenarioAndPointDesignPair(self, cost_scenario, point_design):
        return self.cost_scenario_results_dict[cost_scenario, point_design]
    def getWeightedValueDictionary(self):
        weighted_value_dict = dict()
        for simulation_key, simulation_result in self.scenario_results_dict.iteritems():
            weighted_value_dict[simulation_key] = simulation_result.getWeightedValue()
        return weighted_value_dict

#Define location for each point design
class PointDesignDescription:
    "Location to store details of a point designs"
    pass#Nothing in this class currently so need to include this keyword

class PointDesignDescriptions:
    "Containing all the point design details"
    def __init__(self, point_designs_dict):
        self.point_designs_dict = point_designs_dict
    def getPointDesignNameList(self):
        point_design_names = list()
        for point_design_name in self.point_designs_dict:
            point_design_names.append(point_design_name)
        return point_design_names
    def getNumberPointDesigns(self):
        return len(self.point_designs_dict)

#Define location for each scenario
class ScenarioDescription:
    "Location to store details of a scenario"
    def __init__(self, modelica_variable_of_interest, modelica_variable_of_interest_units, value_performance_slope_direction, minimum_acceptable_performance, stretch_goal, weight, simulation_length, extract_data_type):
        self.modelica_variable_of_interest = modelica_variable_of_interest
        self.modelica_variable_of_interest_units = modelica_variable_of_interest_units
        self.minimum_acceptable_performance = minimum_acceptable_performance
        self.stretch_goal = stretch_goal
        self.weight = weight
        self.simulation_length = simulation_length
        self.extract_data_type = extract_data_type
        #For the linear value curve
        self.m = 1/(stretch_goal-minimum_acceptable_performance)
        self.c = -self.m*minimum_acceptable_performance
        self.value_performance_slope_direction = value_performance_slope_direction
    def computeValueFromPerformance(self, simulated_performance):
        #Assumes a linear increase in value
        if self.value_performance_slope_direction == "positive":
            if (simulated_performance <= self.minimum_acceptable_performance):
                #Below minimum_acceptable_performance so no value
                unweighted_value = 0
            elif (simulated_performance >= self.stretch_goal):
                #At or above the stretch goal
                unweighted_value = 1
            else:
                unweighted_value = (simulated_performance*self.m) + self.c
            return [unweighted_value, unweighted_value*self.weight]
        elif self.value_performance_slope_direction == "negative":
            if (simulated_performance >= self.minimum_acceptable_performance):
                #Above minimum_acceptable_performance so no value
                unweighted_value = 0
            elif (simulated_performance <= self.stretch_goal):
                #At or below the stretch goal
                unweighted_value = 1
            else:
                unweighted_value = (simulated_performance*self.m) + self.c
            return [unweighted_value, unweighted_value*self.weight]
    def getMinimumAcceptablePerformace(self):
        return self.minimum_acceptable_performance
    def getStretchGoal(self):
        return self.stretch_goal
    def setWeight(self, weight):
        self.weight = weight

class ScenarioDescriptions:
    "Containing all the scenario details"
    scenario_description_dict = dict()
    def __init__(self,scenario_description_dict):
        self.scenario_description_dict = scenario_description_dict
    def getScenarioNameList(self):
        scenario_names = list()
        for scenario_name in self.scenario_description_dict:
            scenario_names.append(scenario_name)
        return scenario_names
    def GetModelicaVariableOfInterest(self, scenario_name):
        return self.scenario_description_dict[scenario_name].modelica_variable_of_interest
    def GetModelicaVariableOfInterestUnits(self, scenario_name):
        return self.scenario_description_dict[scenario_name].modelica_variable_of_interest_units
    def getStretchGoal(self, scenario_name):
        return self.scenario_description_dict[scenario_name].stretch_goal
    def getMinimumAcceptablePerformace(self, scenario_name):
        return self.scenario_description_dict[scenario_name].minimum_acceptable_performance
    def getValuePerformaceSlopeDirection(self, scenario_name):
        return self.scenario_description_dict[scenario_name].value_performance_slope_direction
    def getExtractDataType(self, scenario_name):
        return self.scenario_description_dict[scenario_name].extract_data_type
    def getWeight(self, scenario_name):
        return self.scenario_description_dict[scenario_name].weight
    def getSimulationLength(self, scenario_name):
        return self.scenario_description_dict[scenario_name].simulation_length
    def setEqualWeights(self):
        num_scenarios = len(self.scenario_description_dict)
        weight = 1/num_scenarios
        for scenario_name, scenario_description in self.scenario_description_dict.iteritems():
            scenario_description.setWeight(weight)
    def getNumberScenarios(self):
        return len(self.scenario_description_dict)