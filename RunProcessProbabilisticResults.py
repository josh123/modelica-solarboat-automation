#Creates charts showing the simulation results

#For file creation and moving around directories
import os

#Move to the folder containing the data we would like to use
os.chdir("SolarBoatAutomation/_Results-MonteCarlo")
os.chdir("Test/No_component_variation")

#For each simulation load and process the data
import numpy as np

#Load the data
#csv1 = np.genfromtxt("ConstantSun_MonoDisplacementHullSB_A/multOut.csv",delimiter=";",skip_header=1) # A bit slow

# SB1 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_LM_13_200mm", stopTime=100, method="dassl", resultFile="Auto_StraightLineAvSun_A_LM_13_200mm");
csv2 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_200mm/multOut.csv",delimiter=";",skip_header=1) # Fast (0.7)

# SB2 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_LM_13_220mm", method="dassl", resultFile="Auto_StraightLineAvSun_A_LM_13_220mm");
csv9 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_220mm/multOut.csv",delimiter=";",skip_header=1) #

# SB3 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_HM_3_220mm", method="dassl", resultFile="Auto_StraightLineAvSun_A_HM_3_220mm");
csv8 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mm/multOut.csv",delimiter=";",skip_header=1) #0.55

# SB4 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_HM_160mm", method="dassl", resultFile="Auto_StraightLineAvSun_A_HM_160mm");
csv4 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_160mm/multOut.csv",delimiter=";",skip_header=1) #Upper mid (0.45)

# SB5 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_HM_3_220mm_SP50f_DH", stopTime=100, method="dassl", resultFile="Auto_StraightLineAvSun_A_HM_3_220mm_SP50f_DH");
csv6 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mm_SP50f_DH/multOut.csv",delimiter=";",skip_header=1) #Dual hull KTH

# SB6 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_HM_3_160mm", method="dassl", resultFile="Auto_StraightLineAvSun_A_HM_3_160mm");
csv3 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_160mm/multOut.csv",delimiter=";",skip_header=1) #Most slow (0.2)

# SB7 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_HM_220mm", method="dassl", resultFile="Auto_StraightLineAvSun_A_HM_220mm");
csv7 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_220mm/multOut.csv",delimiter=";",skip_header=1) #0.2

# SB8 100 MC Trials
# simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto.Auto_StraightLineAvSun_A_LM_13_200mm_SP50f", stopTime=100, method="dassl", resultFile="Auto_StraightLineAvSun_A_LM_13_200mm_SP50f");
## csv8 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_200mm_SP50f/multOut.csv",delimiter=";",skip_header=1) # Fast (0.7)

csv8_10 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mmTrials_10/multOut.csv",delimiter=";",skip_header=1) #10 trials of a previous design
csv8_500 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mmTrials_500/multOut.csv",delimiter=";",skip_header=1) #500 trials of a previous design
#csv5 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_200mm_SP50f/multOut.csv",delimiter=";",skip_header=1) #Fast KTH panels --> Strange. Does not go constant (0.7)

#Extract the probability of winning
legend_text_list = list()
#legend_text_list.append("MonoDisplacementHullSB_A")
#probability_of_winning1 = csv1[:,2]
legend_text_list.append("SB1 100 MC Trials")
probability_of_winning2 = csv2[:,2]
legend_text_list.append("SB2 100 MC Trials")
probability_of_winning9 = csv9[:,2]
legend_text_list.append("SB3 100 MC Trials")
probability_of_winning8 = csv8[:,2]
legend_text_list.append("SB4 100 MC Trials")
probability_of_winning4 = csv4[:,2]
legend_text_list.append("SB5 100 MC Trials")
probability_of_winning6 = csv6[:,2]
legend_text_list.append("SB6 100 MC Trials")
probability_of_winning3 = csv3[:,2]
legend_text_list.append("SB7 100 MC Trials")
probability_of_winning7 = csv7[:,2]
legend_text_list.append("SB3 10 MC Trials")
probability_of_winning8_10 = csv8_10[:,2]
legend_text_list.append("SB3 500 MC Trials")
probability_of_winning8_500 = csv8_500[:,2]

#legend_text_list.append("A_LM_13_200mm_SP50f")
#probability_of_winning5 = csv5[:,2]






#Process the data for a chart (based on the following but I normalize to 1: http://stackoverflow.com/questions/15408371/cumulative-distribution-plots-python)
#sorted_probability_of_winning1 = np.sort(probability_of_winning1)
#x_values1 = np.concatenate([sorted_probability_of_winning1, sorted_probability_of_winning1[[-1]]])
#y_values1 = (np.arange((sorted_probability_of_winning1.size+1))).astype(float)/(probability_of_winning1.size)

sorted_probability_of_winning2 = np.sort(probability_of_winning2)
x_values2 = np.concatenate([sorted_probability_of_winning2, sorted_probability_of_winning2[[-1]]])
y_values2 = (np.arange((sorted_probability_of_winning2.size+1))).astype(float)/probability_of_winning2.size

sorted_probability_of_winning3 = np.sort(probability_of_winning3)
x_values3 = np.concatenate([sorted_probability_of_winning3, sorted_probability_of_winning3[[-1]]])
y_values3 = (np.arange((sorted_probability_of_winning3.size+1))).astype(float)/probability_of_winning3.size

sorted_probability_of_winning4 = np.sort(probability_of_winning4)
x_values4 = np.concatenate([sorted_probability_of_winning4, sorted_probability_of_winning4[[-1]]])
y_values4 = (np.arange((sorted_probability_of_winning4.size+1))).astype(float)/probability_of_winning4.size

#sorted_probability_of_winning5 = np.sort(probability_of_winning5)
#x_values5 = np.concatenate([sorted_probability_of_winning5, sorted_probability_of_winning5[[-1]]])
#y_values5 = (np.arange((sorted_probability_of_winning5.size+1))).astype(float)/probability_of_winning5.size

sorted_probability_of_winning6 = np.sort(probability_of_winning6)
x_values6 = np.concatenate([sorted_probability_of_winning6, sorted_probability_of_winning6[[-1]]])
y_values6 = (np.arange((sorted_probability_of_winning6.size+1))).astype(float)/probability_of_winning6.size

sorted_probability_of_winning7 = np.sort(probability_of_winning7)
x_values7 = np.concatenate([sorted_probability_of_winning7, sorted_probability_of_winning7[[-1]]])
y_values7 = (np.arange((sorted_probability_of_winning7.size+1))).astype(float)/probability_of_winning7.size

sorted_probability_of_winning8 = np.sort(probability_of_winning8)
x_values8 = np.concatenate([sorted_probability_of_winning8, sorted_probability_of_winning8[[-1]]])
y_values8 = (np.arange((sorted_probability_of_winning8.size+1))).astype(float)/probability_of_winning8.size


sorted_probability_of_winning9 = np.sort(probability_of_winning9)
x_values9 = np.concatenate([sorted_probability_of_winning9, sorted_probability_of_winning9[[-1]]])
y_values9 = (np.arange((sorted_probability_of_winning9.size+1))).astype(float)/probability_of_winning9.size

sorted_probability_of_winning8_10 = np.sort(probability_of_winning8_10)
x_values8_10 = np.concatenate([sorted_probability_of_winning8_10, sorted_probability_of_winning8_10[[-1]]])
y_values8_10 = (np.arange((sorted_probability_of_winning8_10.size+1))).astype(float)/probability_of_winning8_10.size

sorted_probability_of_winning8_500 = np.sort(probability_of_winning8_500)
x_values8_500 = np.concatenate([sorted_probability_of_winning8_500, sorted_probability_of_winning8_500[[-1]]])
y_values8_500 = (np.arange((sorted_probability_of_winning8_500.size+1))).astype(float)/probability_of_winning8_500.size


#Plot the chart
import matplotlib.pyplot as plt
fig1 = plt.figure()
ax1 = fig1.add_subplot(111,ylabel="Probability Density",xlabel="Probability of Winning")

plots = list()
#plot1 = ax1.plot(x_values1, y_values1)
plot2 = ax1.plot(x_values2, y_values2, linewidth=2.5)
plot9 = ax1.plot(x_values9, y_values9, linewidth=2.5)
plot8 = ax1.plot(x_values8, y_values8, linewidth=2.5)
plot4 = ax1.plot(x_values4, y_values4, linewidth=2.5)
plot6 = ax1.plot(x_values6, y_values6, linewidth=2.5, linestyle=":")
plot3 = ax1.plot(x_values3, y_values3, linewidth=2.5)
plot7 = ax1.plot(x_values7, y_values7, linewidth=2.5)
plot8_10 = ax1.plot(x_values8_10, y_values8_10, linewidth=2.5, linestyle="--", color='k')
plot8_500 = ax1.plot(x_values8_500, y_values8_500, linewidth=2.5, linestyle="--", color='b')
#plot5 = ax1.plot(x_values5, y_values5)

#plots.append(plot1[0])
plots.append(plot2[0])
plots.append(plot9[0])
plots.append(plot8[0])
plots.append(plot4[0])
plots.append(plot6[0])
plots.append(plot3[0])
plots.append(plot7[0])
plots.append(plot8_10[0])
plots.append(plot8_500[0])
#plots.append(plot5[0])

ax1.legend(plots, legend_text_list, loc="best",fontsize='small')
ax1.axis([0,1,0,1])
ax1.minorticks_on()
#ax1.set_xticks(numpy.arange(0,1,0.1))
#ax1.set_yticks(numpy.arange(0,1.,0.1))
ax1.grid(b=True, which='major', color='k', linestyle='--')
ax1.grid(b=True, which='minor', color='k', linestyle=':')
fig1.savefig("ProbabilisitcComparison-ProbabilityOfWinning.png")


#Load the data
#csv1 = np.genfromtxt("ConstantSun_MonoDisplacementHullSB_A/multOut.csv",delimiter=";",skip_header=1) # A bit slow
csv2 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_200mm/multOut.csv",delimiter=";",skip_header=1) # Fast (0.7)
csv9 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_220mm/multOut.csv",delimiter=";",skip_header=1) #
csv8 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mm/multOut.csv",delimiter=";",skip_header=1) #0.55
csv4 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_160mm/multOut.csv",delimiter=";",skip_header=1) #Upper mid (0.45)
csv6 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mm_SP50f_DH/multOut.csv",delimiter=";",skip_header=1) #Dual hull KTH
csv3 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_160mm/multOut.csv",delimiter=";",skip_header=1) #Most slow (0.2)
csv7 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_220mm/multOut.csv",delimiter=";",skip_header=1) #0.2
csv8_10 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mmTrials_10/multOut.csv",delimiter=";",skip_header=1) #10 Trial run of a previous design
csv8_500 = np.genfromtxt("Auto_StraightLineAvSun_A_HM_3_220mmTrials_500/multOut.csv",delimiter=";",skip_header=1) #500 Trial run of a previous design
#csv5 = np.genfromtxt("Auto_StraightLineAvSun_A_LM_13_200mm_SP50f/multOut.csv",delimiter=";",skip_header=1) #Fast KTH panels --> Strange. Does not go constant (0.7)


#Do it again for time to complete race

#Extract the probability of winning
legend_text_list = list()
#legend_text_list.append("MonoDisplacementHullSB_A")
#probability_of_winning1 = csv1[:,2]
legend_text_list.append("SB1 100 MC Trials")
probability_of_winning2 = csv2[:,1]
legend_text_list.append("SB2 100 MC Trials")
probability_of_winning9 = csv9[:,1]
legend_text_list.append("SB3 100 MC Trials")
probability_of_winning8 = csv8[:,1]
legend_text_list.append("SB4 100 MC Trials")
probability_of_winning4 = csv4[:,1]
legend_text_list.append("SB5 100 MC Trials")
probability_of_winning6 = csv6[:,1]
legend_text_list.append("SB6 100 MC Trials")
probability_of_winning3 = csv3[:,1]
legend_text_list.append("SB7 100 MC Trials")
probability_of_winning7 = csv7[:,1]
legend_text_list.append("SB3 10 MC Trials")
probability_of_winning8_10 = csv8_10[:,1]
legend_text_list.append("SB3 500 MC Trials")
probability_of_winning8_500 = csv8_500[:,1]
#legend_text_list.append("A_LM_13_200mm_SP50f")
#probability_of_winning5 = csv5[:,1]


#Process the data for a chart (based on the following but I normalize to 1: http://stackoverflow.com/questions/15408371/cumulative-distribution-plots-python)
#sorted_probability_of_winning1 = np.sort(probability_of_winning1)
#x_values1 = np.concatenate([sorted_probability_of_winning1, sorted_probability_of_winning1[[-1]]])
#y_values1 = (np.arange((sorted_probability_of_winning1.size+1))).astype(float)/(probability_of_winning1.size)

sorted_probability_of_winning2 = np.sort(probability_of_winning2)
x_values2 = np.concatenate([sorted_probability_of_winning2, sorted_probability_of_winning2[[-1]]])
y_values2 = (np.arange((sorted_probability_of_winning2.size+1))).astype(float)/probability_of_winning2.size

sorted_probability_of_winning3 = np.sort(probability_of_winning3)
x_values3 = np.concatenate([sorted_probability_of_winning3, sorted_probability_of_winning3[[-1]]])
y_values3 = (np.arange((sorted_probability_of_winning3.size+1))).astype(float)/probability_of_winning3.size

sorted_probability_of_winning4 = np.sort(probability_of_winning4)
x_values4 = np.concatenate([sorted_probability_of_winning4, sorted_probability_of_winning4[[-1]]])
y_values4 = (np.arange((sorted_probability_of_winning4.size+1))).astype(float)/probability_of_winning4.size

#sorted_probability_of_winning5 = np.sort(probability_of_winning5)
#x_values5 = np.concatenate([sorted_probability_of_winning5, sorted_probability_of_winning5[[-1]]])
#y_values5 = (np.arange((sorted_probability_of_winning5.size+1))).astype(float)/probability_of_winning5.size

sorted_probability_of_winning6 = np.sort(probability_of_winning6)
x_values6 = np.concatenate([sorted_probability_of_winning6, sorted_probability_of_winning6[[-1]]])
y_values6 = (np.arange((sorted_probability_of_winning6.size+1))).astype(float)/probability_of_winning6.size

sorted_probability_of_winning7 = np.sort(probability_of_winning7)
x_values7 = np.concatenate([sorted_probability_of_winning7, sorted_probability_of_winning7[[-1]]])
y_values7 = (np.arange((sorted_probability_of_winning7.size+1))).astype(float)/probability_of_winning7.size

sorted_probability_of_winning8 = np.sort(probability_of_winning8)
x_values8 = np.concatenate([sorted_probability_of_winning8, sorted_probability_of_winning8[[-1]]])
y_values8 = (np.arange((sorted_probability_of_winning8.size+1))).astype(float)/probability_of_winning8.size

sorted_probability_of_winning9 = np.sort(probability_of_winning9)
x_values9 = np.concatenate([sorted_probability_of_winning9, sorted_probability_of_winning9[[-1]]])
y_values9 = (np.arange((sorted_probability_of_winning9.size+1))).astype(float)/probability_of_winning9.size

sorted_probability_of_winning8_10 = np.sort(probability_of_winning8_10)
x_values8_10 = np.concatenate([sorted_probability_of_winning8_10, sorted_probability_of_winning8_10[[-1]]])
y_values8_10 = (np.arange((sorted_probability_of_winning8_10.size+1))).astype(float)/probability_of_winning8_10.size

sorted_probability_of_winning8_500 = np.sort(probability_of_winning8_500)
x_values8_500 = np.concatenate([sorted_probability_of_winning8_500, sorted_probability_of_winning8_500[[-1]]])
y_values8_500 = (np.arange((sorted_probability_of_winning8_500.size+1))).astype(float)/probability_of_winning8_500.size

#Plot the chart
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, ylabel="Probability Density", xlabel="Predicted time to complete race (hours)")


plots = list()
#plot1 = ax2.plot(x_values1, y_values1)
plot2 = ax2.plot(x_values2, y_values2, linewidth=2.5)
plot9 = ax2.plot(x_values9, y_values9, linewidth=2.5)
plot8 = ax2.plot(x_values8, y_values8, linewidth=2.5)
plot4 = ax2.plot(x_values4, y_values4, linewidth=2.5)
plot6 = ax2.plot(x_values6, y_values6, linewidth=2.5, linestyle=":")
plot3 = ax2.plot(x_values3, y_values3, linewidth=2.5)
plot7 = ax2.plot(x_values7, y_values7, linewidth=2.5)
plot8_10 = ax2.plot(x_values8_10, y_values8_10, linewidth=2.5, linestyle="--", color='k')
plot8_500 = ax2.plot(x_values8_500, y_values8_500, linewidth=2.5, linestyle="--", color='b')
#plot5 = ax2.plot(x_values5, y_values5)


#plots.append(plot1[0])
plots.append(plot2[0])
plots.append(plot9[0])
plots.append(plot8[0])
plots.append(plot4[0])
plots.append(plot6[0])
plots.append(plot3[0])
plots.append(plot7[0])
plots.append(plot8_10[0])
plots.append(plot8_500[0])
#plots.append(plot5[0])




ax2.legend(plots, legend_text_list, loc="best",fontsize='small')
ax2.axis([1,5,0,1])
ax2.minorticks_on()
#ax2.set_xticks(numpy.arange(0,1,0.1))
#ax2.set_yticks(numpy.arange(0,1.,0.1))
ax2.grid(b=True, which='major', color='k', linestyle='--')
ax2.grid(b=True, which='minor', color='k', linestyle=':')

fig2.savefig("ProbabilisitcComparison-TimeToCompleteRace.png")