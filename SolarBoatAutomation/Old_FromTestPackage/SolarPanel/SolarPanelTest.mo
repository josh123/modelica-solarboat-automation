within SolarBoatAutomation.Old_FromTestPackage.SolarPanel;
model SolarPanelTest
  Modelica.Electrical.Analog.Basic.Ground ground1
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-64,-68},{-44,-48}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{0,-68},{20,-48}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-28,-68},{-8,-48}})));
  replaceable
    SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial.Motor_IdealGear_Prop
    partial_ThrusterMultiBody constrainedby
    SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    annotation (Placement(transformation(extent={{38,4},{58,24}})));
  AcausalSolarPanel acausalSolarPanel
    annotation (Placement(transformation(extent={{-42,68},{-22,88}})));
equation
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{0,-58},{-8,-58}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-44,-58},{-28,-58}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TestRig.frame_b,partial_ThrusterMultiBody. frame_a) annotation (Line(
      points={{20,-58},{30,-58},{30,14.2},{38,14.2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_ThrusterMultiBody.p, ground1.p) annotation (Line(
      points={{42,24},{42,30},{-70,30}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(acausalSolarPanel.p, ground1.p) annotation (Line(
      points={{-42,78},{-70,78},{-70,30}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(acausalSolarPanel.n, partial_ThrusterMultiBody.n) annotation (Line(
      points={{-22,78},{52,78},{52,23.8}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end SolarPanelTest;
