within SolarBoatAutomation.Old_FromTestPackage.SolarPanel;
model PowerControl "SolarPanelAtMPP"
  extends Modelica.Electrical.Analog.Interfaces.OnePort;
  parameter Modelica.SIunits.Power input_power = 150;

equation
  if (i == 0) then
    input_power = 0;
  else
    input_power = i * v;
  end if;
end PowerControl;
