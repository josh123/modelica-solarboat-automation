within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model VoltageSweep_FT_12_Config
  extends Modelica.Icons.Example;
  Modelica.SIunits.Power power = abs(currentSensor.i) * abs(voltageSensor.v);
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-90,-20},{-76,-6}})));
  Modelica.Blocks.Sources.Constant G(k=1000)
    annotation (Placement(transformation(extent={{-90,4},{-76,18}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{28,-82},{48,-62}})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(duration=1, V=2*21.8)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={116,-4})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{70,12},{90,32}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={94,-6})));
  FT136SE_12 fT136SE_12_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-52,2})));
  FT136SE_12 fT136SE_12_2 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-52,-36})));
  Modelica.Blocks.Sources.Constant G1(
                                     k=1000)
    annotation (Placement(transformation(extent={{-92,-44},{-78,-30}})));
  Modelica.Blocks.Sources.Constant T1(
                                     k=298)
    annotation (Placement(transformation(extent={{-92,-68},{-78,-54}})));
  Modelica.Blocks.Sources.Constant T2(
                                     k=298)
    annotation (Placement(transformation(extent={{-32,-20},{-18,-6}})));
  Modelica.Blocks.Sources.Constant G2(
                                     k=1000)
    annotation (Placement(transformation(extent={{-32,4},{-18,18}})));
  FT136SE_12 fT136SE_12_3 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,0})));
  FT136SE_12 fT136SE_12_4 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,-38})));
  Modelica.Blocks.Sources.Constant G3(
                                     k=1000)
    annotation (Placement(transformation(extent={{-32,-40},{-18,-26}})));
  Modelica.Blocks.Sources.Constant T3(
                                     k=298)
    annotation (Placement(transformation(extent={{-32,-60},{-18,-46}})));
  Modelica.Blocks.Sources.Constant T4(
                                     k=298)
    annotation (Placement(transformation(extent={{20,-18},{34,-4}})));
  Modelica.Blocks.Sources.Constant G4(
                                     k=1000)
    annotation (Placement(transformation(extent={{20,6},{34,20}})));
  FT136SE_12 fT136SE_12_5 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={58,2})));
  FT136SE_12 fT136SE_12_6 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={58,-36})));
  Modelica.Blocks.Sources.Constant G5(
                                     k=1000)
    annotation (Placement(transformation(extent={{20,-38},{34,-24}})));
  Modelica.Blocks.Sources.Constant T5(
                                     k=298)
    annotation (Placement(transformation(extent={{20,-58},{34,-44}})));
equation
  connect(rampVoltage.n, ground.p) annotation (Line(
      points={{116,-14},{116,-62},{38,-62}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, rampVoltage.p) annotation (Line(
      points={{90,22},{116,22},{116,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.p, rampVoltage.p) annotation (Line(
      points={{94,4},{94,22},{116,22},{116,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.n, ground.p) annotation (Line(
      points={{94,-16},{94,-62},{38,-62}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G.y, fT136SE_12_1.G) annotation (Line(
      points={{-75.3,11},{-69.65,11},{-69.65,7.8},{-62.8,7.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, fT136SE_12_1.T) annotation (Line(
      points={{-75.3,-13},{-69.65,-13},{-69.65,-4},{-62.8,-4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fT136SE_12_1.p, currentSensor.p) annotation (Line(
      points={{-52,12},{-52,22},{70,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_1.n, fT136SE_12_2.p) annotation (Line(
      points={{-52,-8},{-52,-26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, fT136SE_12_2.n) annotation (Line(
      points={{38,-62},{-52,-62},{-52,-46}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G1.y, fT136SE_12_2.G) annotation (Line(
      points={{-77.3,-37},{-70.65,-37},{-70.65,-30.2},{-62.8,-30.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1.y, fT136SE_12_2.T) annotation (Line(
      points={{-77.3,-61},{-77.3,-52},{-72,-52},{-62.8,-52},{-62.8,-48},{-62.8,
          -42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G2.y, fT136SE_12_3.G) annotation (Line(
      points={{-17.3,11},{-11.65,11},{-11.65,5.8},{-4.8,5.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T2.y, fT136SE_12_3.T) annotation (Line(
      points={{-17.3,-13},{-11.65,-13},{-11.65,-6},{-4.8,-6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fT136SE_12_3.p, currentSensor.p) annotation (Line(
      points={{6,10},{6,22},{70,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_3.n, fT136SE_12_4.p) annotation (Line(
      points={{6,-10},{6,-28}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, fT136SE_12_4.n) annotation (Line(
      points={{38,-62},{6,-62},{6,-48}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G3.y, fT136SE_12_4.G) annotation (Line(
      points={{-17.3,-33},{-12.65,-33},{-12.65,-32.2},{-4.8,-32.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3.y, fT136SE_12_4.T) annotation (Line(
      points={{-17.3,-53},{-17.3,-46},{-8,-46},{-4,-46},{-4.8,-46},{-4.8,-44}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(fT136SE_12_1.p, currentSensor.p) annotation (Line(
      points={{-52,12},{-52,22},{70,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, fT136SE_12_2.n) annotation (Line(
      points={{38,-62},{-52,-62},{-52,-46}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G4.y, fT136SE_12_5.G) annotation (Line(
      points={{34.7,13},{40.35,13},{40.35,7.8},{47.2,7.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4.y, fT136SE_12_5.T) annotation (Line(
      points={{34.7,-11},{40.35,-11},{40.35,-4},{47.2,-4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fT136SE_12_5.p, currentSensor.p) annotation (Line(
      points={{58,12},{58,22},{70,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_5.n, fT136SE_12_6.p) annotation (Line(
      points={{58,-8},{58,-26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, fT136SE_12_6.n) annotation (Line(
      points={{38,-62},{58,-62},{58,-46}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G5.y, fT136SE_12_6.G) annotation (Line(
      points={{34.7,-31},{39.35,-31},{39.35,-30.2},{47.2,-30.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5.y, fT136SE_12_6.T) annotation (Line(
      points={{34.7,-51},{34.7,-44},{47.2,-44},{47.2,-42}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end VoltageSweep_FT_12_Config;
