within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model VoltageSweep
  extends Modelica.Icons.Example;
  Modelica.SIunits.Power power = abs(currentSensor.i) * abs(voltageSensor.v);
  BP_BP_7185 bP_BP_7185_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-48,2})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-90,-20},{-76,-6}})));
  Modelica.Blocks.Sources.Constant G(k=1000)
    annotation (Placement(transformation(extent={{-90,4},{-76,18}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-44},{-38,-24}})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(duration=1, V=44.8)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={30,0})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-32,10},{-12,30}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,-2})));
equation
  connect(G.y,bP_BP_7185_1. G) annotation (Line(
      points={{-75.3,11},{-67.65,11},{-67.65,7.8},{-58.8,7.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y,bP_BP_7185_1. T) annotation (Line(
      points={{-75.3,-13},{-67.65,-13},{-67.65,-4},{-58.8,-4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(bP_BP_7185_1.n, ground.p) annotation (Line(
      points={{-48,-8},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(rampVoltage.n, ground.p) annotation (Line(
      points={{30,-10},{30,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.p, bP_BP_7185_1.p) annotation (Line(
      points={{-32,20},{-48,20},{-48,12}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, rampVoltage.p) annotation (Line(
      points={{-12,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.p, rampVoltage.p) annotation (Line(
      points={{6,8},{6,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.n, ground.p) annotation (Line(
      points={{6,-12},{6,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end VoltageSweep;
