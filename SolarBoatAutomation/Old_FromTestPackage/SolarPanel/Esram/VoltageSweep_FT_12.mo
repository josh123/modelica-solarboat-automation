within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model VoltageSweep_FT_12
  extends Modelica.Icons.Example;
  Modelica.SIunits.Power power = abs(currentSensor.i) * abs(voltageSensor.v);
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-90,-20},{-76,-6}})));
  Modelica.Blocks.Sources.Constant G(k=1000)
    annotation (Placement(transformation(extent={{-90,4},{-76,18}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-44},{-38,-24}})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(duration=1, V=21.8)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={30,0})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-32,10},{-12,30}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,-2})));
  FT136SE_12 fT136SE_12_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-52,2})));
equation
  connect(rampVoltage.n, ground.p) annotation (Line(
      points={{30,-10},{30,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, rampVoltage.p) annotation (Line(
      points={{-12,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.p, rampVoltage.p) annotation (Line(
      points={{6,8},{6,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.n, ground.p) annotation (Line(
      points={{6,-12},{6,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G.y, fT136SE_12_1.G) annotation (Line(
      points={{-75.3,11},{-69.65,11},{-69.65,7.8},{-62.8,7.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, fT136SE_12_1.T) annotation (Line(
      points={{-75.3,-13},{-69.65,-13},{-69.65,-4},{-62.8,-4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fT136SE_12_1.p, currentSensor.p) annotation (Line(
      points={{-52,12},{-52,20},{-32,20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, fT136SE_12_1.n) annotation (Line(
      points={{-48,-24},{-52,-24},{-52,-8}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end VoltageSweep_FT_12;
