within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model MPPTWithDutyRatioCommand
  extends Modelica.Icons.Example;
  BP_BP_7185 bP_BP_7185_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-54,16})));
  Modelica.Blocks.Sources.Constant G(k=1000)
    annotation (Placement(transformation(extent={{-96,18},{-82,32}})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-96,-6},{-82,8}})));
  L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Components.dP_PandO
    dP_PandO1 annotation (Placement(transformation(extent={{-16,-30},{4,-10}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={2,16})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-64,-22},{-44,-2}})));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor(C=3.3e-6) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={24,14})));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L=100e-6)
    annotation (Placement(transformation(extent={{30,20},{50,40}})));
  Modelica.Electrical.Analog.Ideal.IdealClosingSwitch idealClosingSwitch
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={56,14})));
  Modelica.Electrical.Analog.Ideal.IdealDiode idealDiode
    annotation (Placement(transformation(extent={{62,20},{82,40}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=-60)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={94,14})));
  L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Components.PWM pWM
    annotation (Placement(transformation(extent={{22,-30},{42,-10}})));
equation

  connect(G.y, bP_BP_7185_1.G) annotation (Line(
      points={{-81.3,25},{-73.65,25},{-73.65,21.8},{-64.8,21.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, bP_BP_7185_1.T) annotation (Line(
      points={{-81.3,1},{-73.65,1},{-73.65,10},{-64.8,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(bP_BP_7185_1.p, currentSensor.p) annotation (Line(
      points={{-54,26},{-54,30},{-40,30}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, voltageSensor.p) annotation (Line(
      points={{-20,30},{2,30},{2,26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.v, dP_PandO1.v) annotation (Line(
      points={{-8,16},{-22,16},{-22,-14},{-16,-14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(currentSensor.i, dP_PandO1.i) annotation (Line(
      points={{-30,20},{-30,-25.6},{-16,-25.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(bP_BP_7185_1.n, voltageSensor.n) annotation (Line(
      points={{-54,6},{-54,-2},{2,-2},{2,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(bP_BP_7185_1.n, ground.p) annotation (Line(
      points={{-54,6},{-54,-2}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(capacitor.p, voltageSensor.p) annotation (Line(
      points={{24,24},{24,30},{2,30},{2,26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(capacitor.n, voltageSensor.n) annotation (Line(
      points={{24,4},{24,-2},{2,-2},{2,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(inductor.p, voltageSensor.p) annotation (Line(
      points={{30,30},{2,30},{2,26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(inductor.n, idealClosingSwitch.n) annotation (Line(
      points={{50,30},{56,30},{56,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealClosingSwitch.p, voltageSensor.n) annotation (Line(
      points={{56,4},{56,-2},{2,-2},{2,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealDiode.p, idealClosingSwitch.n) annotation (Line(
      points={{62,30},{56,30},{56,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealDiode.n, constantVoltage.p) annotation (Line(
      points={{82,30},{94,30},{94,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.n, voltageSensor.n) annotation (Line(
      points={{94,4},{94,-2},{2,-2},{2,6}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(dP_PandO1.y, pWM.d) annotation (Line(
      points={{5,-20},{20,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pWM.q, idealClosingSwitch.control) annotation (Line(
      points={{43,-20},{46,-20},{46,14},{49,14}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end MPPTWithDutyRatioCommand;
