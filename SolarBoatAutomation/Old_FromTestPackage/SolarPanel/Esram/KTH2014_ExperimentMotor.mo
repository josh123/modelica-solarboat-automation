within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model KTH2014_ExperimentMotor
  extends TestBed(redeclare
      SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial.Motor_IdealGear_Prop
      partial_ThrusterMultiBody);
  extends Modelica.Icons.Example;
end KTH2014_ExperimentMotor;
