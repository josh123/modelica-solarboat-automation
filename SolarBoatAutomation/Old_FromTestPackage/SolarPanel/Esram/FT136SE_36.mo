within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model FT136SE_36
  //Based on: Esram, T. (2010). Modeling and control of an alternating-current photovoltaic module. University of Illinois at Urbana-Champaign. Retrieved from https://www.ideals.illinois.edu/handle/2142/15501
  extends Modelica.Electrical.Analog.Interfaces.OnePort;

  // Constants
protected
  parameter Real T_STC(unit="K") = 298 "STC temperature";
  parameter Real G_STC(unit="W/m^2") = 1000 "STC insolation";
  parameter Real k(unit="J/K") = 1.38065e-23 "Boltzmann constant";
  parameter Real q(unit="C") = 1.602e-19 "electron charge";

  // Datasheet Parameters
  Real V_oc(unit="V") = 21.8 "module open-circuit voltage at STC";
  Real I_sc(unit="A") = 2.7 "module short-circuit current at STC";
  // V_mpp(unit="V") = 17.8 "module voltage at MPP at STC";
  // I_mpp(unit="A") = 2.52 "module current at MPP at STC";
  parameter Real k_i(unit="%/K") = 0.065 "temperature coefficient of I_sc";
  parameter Real k_v(unit="V/K") = -0.16 "temperature coefficient of V_oc";
  parameter Real N=12 "number of cells";

  // Extracted Parameters (using MATLAB)
  parameter Real n_d=1.2271 "diode quality (ideality) factor";
  parameter Real R_s(unit="ohm") = 0.3393 "module series resistance";
  parameter Real R_sh(unit="ohm") = 1.4704e3 "module shunt resistance";

  // Breakdown Parameters
  parameter Real a(unit="1/ohm") = 2.3e-3
    "fraction of ohmic current in avalanche breakdown";
  parameter Real V_br(unit="V") = -18 "breakdown voltage of one cell";
  parameter Real m=2 "exponent for avalanche breakdown";

  // Variables
  Real I(unit="A") "module current";
  Real V(unit="V") "module voltage";

  // Insolation/Temperature-Dependent Variables
  Real V_T(unit="V") "temperature-dependent junction thermal voltage";
  Real I_sc_T(unit="A") "temperature-dependent short-circuit current";
  Real V_oc_T(unit="V") "temperature-dependent open-circuit voltage";
  Real I_s_T(unit="A") "temperature-dependent dark saturation current";
  Real I_ph_T(unit="A") "temperature-dependent photo-generated current";

  Real I_sc_GT(unit="A")
    "insolation/temperature-dependent short-circuit current";
  Real V_oc_GT(unit="V")
    "insolation/temperature-dependent open-circuit voltage";
  Real I_ph_star(unit="A")
    "insolation/temperature-dependent photo-generated current";
  Real I_ph_GT(unit="A")
    "insolation/temperature-dependent photo-generated current";
  Real I_s_GT(unit="A")
    "insolation/temperature-dependent dark saturation current";

public
  Modelica.Blocks.Interfaces.RealInput G annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=-90,
        origin={-58,-108})));
  Modelica.Blocks.Interfaces.RealInput T annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=-90,
        origin={60,-108})));
equation
  // Temperature dependence:
  V_T = k*T/q;
  I_sc_T = I_sc*(1 + k_i*(T - T_STC)/100);
  V_oc_T = V_oc + k_v*(T - T_STC);
  I_s_T = (I_sc_T - (V_oc_T - I_sc_T*R_s)/R_sh)*exp(-
    V_oc_T/(n_d*N*V_T));
  I_ph_T = I_s_T*exp(V_oc_T/(n_d*N*V_T)) + V_oc_T/R_sh;

  // Followed by insolation dependence:
  I_sc_GT = I_sc_T*G/G_STC;
  I_ph_star = I_ph_T*G/G_STC;
  V_oc_GT = ln((I_ph_star*R_sh - V_oc_GT)/(I_s_T*R_sh))*n_d*N*V_T;

  // Using all of the above:
  I_s_GT = (I_sc_GT - (V_oc_GT - I_sc_GT*R_s)/R_sh)*exp(-
    V_oc_GT/(n_d*N*V_T));
  I_ph_GT = I_s_GT*exp(V_oc_GT/(n_d*N*V_T)) + V_oc_GT/R_sh;

  // Module single-diode equation, including breakdown:
  I = I_ph_GT - I_s_GT*(exp((V + I*R_s)/(n_d*N*V_T)) - 1) - ((V +
    I*R_s)/R_sh)*(1 + a*(1 - (V + I*R_s)/(N*V_br))^(-m));

  // External pins assignment; current goes out of positive pin!
  i = -I;
  v = V;

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FT136SE_36;
