within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
partial model TestBed
//  Modelica.SIunits.Power supplied_power;
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-90,0},{-70,20}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-74,-78},{-54,-58}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{-10,-78},{10,-58}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-38,-78},{-18,-58}})));
  replaceable
    SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    partial_ThrusterMultiBody
    annotation (Placement(transformation(extent={{28,-6},{48,14}})));
  Modelica.Blocks.Sources.Constant G(k=1000)
    annotation (Placement(transformation(extent={{-64,50},{-50,64}})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-64,26},{-50,40}})));
  SolarPanel.Esram.FT136SE_12_6 fT136SE_12_6_1
    annotation (Placement(transformation(extent={{-34,70},{-14,90}})));
equation
//  supplied_power = abs(constantVoltage.v) * abs(constantVoltage.i);
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{-10,-68},{-18,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-54,-68},{-38,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TestRig.frame_b, partial_ThrusterMultiBody.frame_a) annotation (Line(
      points={{10,-68},{20,-68},{20,-5.8},{38,-5.8}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_ThrusterMultiBody.p, ground.p) annotation (Line(
      points={{32,14},{32,20},{-80,20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_6_1.p, ground.p) annotation (Line(
      points={{-34,80},{-80,80},{-80,20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_6_1.n, partial_ThrusterMultiBody.n) annotation (Line(
      points={{-14,80},{42,80},{42,13.8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G.y, fT136SE_12_6_1.G) annotation (Line(
      points={{-49.3,57},{-29.8,57},{-29.8,69.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, fT136SE_12_6_1.T) annotation (Line(
      points={{-49.3,33},{-18,33},{-18,69.2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end TestBed;
