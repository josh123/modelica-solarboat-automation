within SolarBoatAutomation.Old_FromTestPackage.SolarPanel.Esram;
model FT136SE_12_6
  extends Modelica.Electrical.Analog.Interfaces.TwoPin;

public
  Modelica.Blocks.Interfaces.RealInput G annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=-90,
        origin={-58,-108})));
  Modelica.Blocks.Interfaces.RealInput T annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=-90,
        origin={60,-108})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={2,-30})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={100,26})));
  FT136SE_12 fT136SE_12_7 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-22,36})));
  FT136SE_12 fT136SE_12_8 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={14,36})));
  FT136SE_12 fT136SE_12_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-22,72})));
  FT136SE_12 fT136SE_12_2 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={14,72})));
  FT136SE_12 fT136SE_12_3 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-22,110})));
  FT136SE_12 fT136SE_12_4 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={14,110})));
equation

  connect(voltageSensor.n, n) annotation (Line(
      points={{12,-30},{100,-30},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.p, p) annotation (Line(
      points={{-8,-30},{-100,-30},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(n, currentSensor.n) annotation (Line(
      points={{100,0},{100,16}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_7.n, fT136SE_12_8.p) annotation (Line(
      points={{-12,36},{4,36}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_8.n, currentSensor.p) annotation (Line(
      points={{24,36},{100,36}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(p, fT136SE_12_7.p) annotation (Line(
      points={{-100,0},{-100,36},{-32,36}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_1.n, fT136SE_12_2.p) annotation (Line(
      points={{-12,72},{4,72}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_2.n, currentSensor.p) annotation (Line(
      points={{24,72},{100,72},{100,36}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_1.p, p) annotation (Line(
      points={{-32,72},{-100,72},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_3.n, fT136SE_12_4.p) annotation (Line(
      points={{-12,110},{4,110}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_4.n, currentSensor.p) annotation (Line(
      points={{24,110},{100,110},{100,36}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(fT136SE_12_3.p, p) annotation (Line(
      points={{-32,110},{-100,110},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(G, fT136SE_12_7.G) annotation (Line(
      points={{-58,-108},{-58,16},{-27.8,16},{-27.8,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G, fT136SE_12_8.G) annotation (Line(
      points={{-58,-108},{-58,16},{8.2,16},{8.2,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G, fT136SE_12_1.G) annotation (Line(
      points={{-58,-108},{-58,52},{-27.8,52},{-27.8,61.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G, fT136SE_12_2.G) annotation (Line(
      points={{-58,-108},{-58,52},{8.2,52},{8.2,61.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G, fT136SE_12_3.G) annotation (Line(
      points={{-58,-108},{-58,90},{-27.8,90},{-27.8,99.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(G, fT136SE_12_4.G) annotation (Line(
      points={{-58,-108},{-58,90},{8.2,90},{8.2,99.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_8.T) annotation (Line(
      points={{60,-108},{60,14},{20,14},{20,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_7.T) annotation (Line(
      points={{60,-108},{60,14},{-16,14},{-16,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_2.T) annotation (Line(
      points={{60,-108},{60,50},{20,50},{20,61.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_1.T) annotation (Line(
      points={{60,-108},{60,50},{-16,50},{-16,61.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_4.T) annotation (Line(
      points={{60,-108},{60,86},{20,86},{20,99.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T, fT136SE_12_3.T) annotation (Line(
      points={{60,-108},{60,86},{-16,86},{-16,99.2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FT136SE_12_6;
