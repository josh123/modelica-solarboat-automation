within SolarBoatAutomation.Old_FromTestPackage.SolarPanel;
model AcausalSolarPanel
  extends Modelica.Electrical.Analog.Interfaces.TwoPin;
  PowerControl powerControl
    annotation (Placement(transformation(extent={{26,-10},{46,10}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=30)
    annotation (Placement(transformation(extent={{-48,-10},{-28,10}})));
  Modelica.Electrical.Analog.Basic.Resistor resistor
    annotation (Placement(transformation(extent={{-50,64},{-30,84}})));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch idealOpeningSwitch
    annotation (Placement(transformation(extent={{-12,34},{8,54}})));
  Modelica.Electrical.Analog.Ideal.IdealClosingSwitch idealClosingSwitch
    annotation (Placement(transformation(extent={{38,38},{58,58}})));
  Modelica.Electrical.Analog.Ideal.IdealDiode idealDiode
    annotation (Placement(transformation(extent={{-74,22},{-54,42}})));
equation
  connect(powerControl.n, n) annotation (Line(
      points={{46,0},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.p, p) annotation (Line(
      points={{-48,0},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.n, powerControl.p) annotation (Line(
      points={{-28,0},{26,0}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end AcausalSolarPanel;
