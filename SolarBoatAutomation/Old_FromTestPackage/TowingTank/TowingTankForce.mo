within SolarBoatAutomation.Old_FromTestPackage.TowingTank;
model TowingTankForce
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-90,50},{-70,70}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{8,50},{28,70}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-20,50},{0,70}})));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force
    annotation (Placement(transformation(extent={{6,-62},{26,-42}})));
  Modelica.Blocks.Sources.RealExpression x(y=10)
    annotation (Placement(transformation(extent={{-50,-40},{-30,-20}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-50,-58},{-30,-38}})));
  Modelica.Blocks.Sources.RealExpression z(y=0)
    annotation (Placement(transformation(extent={{-50,-74},{-30,-54}})));
  Modelica.Mechanics.MultiBody.Parts.PointMass pointMass(m=10)
    annotation (Placement(transformation(extent={{64,50},{84,70}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(useAxisFlange=false,
      n={1,0,0})
    annotation (Placement(transformation(extent={{-58,50},{-38,70}})));
equation
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{8,60},{0,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x.y, force.force[1]) annotation (Line(
      points={{-29,-30},{-10,-30},{-10,-53.3333},{4,-53.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, force.force[2]) annotation (Line(
      points={{-29,-48},{-8,-48},{-8,-52},{4,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, force.force[3]) annotation (Line(
      points={{-29,-64},{-10,-64},{-10,-50.6667},{4,-50.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(world.frame_b, prismatic.frame_a) annotation (Line(
      points={{-70,60},{-58,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic.frame_b, cutForceAndTorque.frame_a) annotation (Line(
      points={{-38,60},{-20,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(pointMass.frame_a, TestRig.frame_b) annotation (Line(
      points={{74,60},{28,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(force.frame_b, TestRig.frame_b) annotation (Line(
      points={{26,-52},{32,-52},{32,60},{28,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end TowingTankForce;
