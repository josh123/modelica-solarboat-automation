within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Thrusters.Old;
model Thurster2014B_MultiBody
  Modelica.Mechanics.MultiBody.Forces.WorldTorque torque(resolveInFrame=
        Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.world)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=0)
    annotation (Placement(transformation(extent={{-88,-6},{-68,14}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-88,-26},{-68,-6}})));
  Modelica.Blocks.Sources.RealExpression z(y=100000)
    annotation (Placement(transformation(extent={{-88,-46},{-68,-26}})));
  inner Modelica.Mechanics.MultiBody.World world
    annotation (Placement(transformation(extent={{-76,-90},{-56,-70}})));
  Modelica.Mechanics.MultiBody.Parts.BodyBox boxBody1(r={0.5,0,0}, width=0.06,
    density=0.001)
    annotation (Placement(transformation(extent={{56,-10},{76,10}},rotation=0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(useAxisFlange=false)
    annotation (Placement(transformation(extent={{4,-10},{24,10}})));
equation
  connect(realExpression.y, torque.torque[1]) annotation (Line(
      points={{-67,4},{-52,4},{-52,-1.33333},{-42,-1.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, torque.torque[2]) annotation (Line(
      points={{-67,-16},{-54,-16},{-54,0},{-42,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, torque.torque[3]) annotation (Line(
      points={{-67,-36},{-54,-36},{-54,1.33333},{-42,1.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(torque.frame_b, revolute.frame_a) annotation (Line(
      points={{-20,0},{4,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(revolute.frame_b, boxBody1.frame_a) annotation (Line(
      points={{24,0},{56,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b, revolute.frame_a) annotation (Line(
      points={{-56,-80},{-14,-80},{-14,0},{4,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Thurster2014B_MultiBody;
