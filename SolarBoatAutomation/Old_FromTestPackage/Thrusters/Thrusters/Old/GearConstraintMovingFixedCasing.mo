within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Thrusters.Old;
model GearConstraintMovingFixedCasing
  extends Modelica.Icons.Example;
  Modelica.Mechanics.MultiBody.Joints.GearConstraint gearConstraint(
    ratio=10,
    phi_b(fixed=true),
    w_b(fixed=true)) annotation (Placement(transformation(extent={{34,40},{54,
            60}}, rotation=0)));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-74,-4},{-54,16}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder cyl1(
    diameter=0.1,
    color={0,128,0},
    r={0.4,0,0})
    annotation (Placement(transformation(extent={{2,40},{22,60}}, rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder cyl2(r={0.4,0,0}, diameter=
        0.2) annotation (Placement(transformation(extent={{70,40},{90,60}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Forces.Torque torque1 annotation (Placement(
        transformation(extent={{-26,40},{-6,60}}, rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.Fixed fixed annotation (Placement(
        transformation(extent={{-52,-100},{-32,-80}}, rotation=0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia1(
    J=cyl1.I[1, 1],
    a(fixed=false),
    phi(fixed=true, start=0),
    w(fixed=true, start=0)) annotation (Placement(transformation(extent={{-24,
            -50},{-4,-30}}, rotation=0)));
  Modelica.Mechanics.Rotational.Components.IdealGear idealGear(ratio=10,
      useSupport=true) annotation (Placement(transformation(extent={{8,-50},{28,
            -30}}, rotation=0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia2(J=cyl2.I[1, 1])
    annotation (Placement(transformation(extent={{40,-50},{60,-30}}, rotation=0)));
  Modelica.Mechanics.Rotational.Sources.Torque torque2(useSupport=true)
    annotation (Placement(transformation(extent={{-52,-50},{-32,-30}}, rotation=
           0)));
  Modelica.Mechanics.MultiBody.Parts.Mounting1D mounting1D annotation (
      Placement(transformation(extent={{-24,-80},{-4,-60}}, rotation=0)));
  Modelica.Blocks.Sources.RealExpression x(y=1000)
    annotation (Placement(transformation(extent={{-110,52},{-90,72}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-110,32},{-90,52}})));
  Modelica.Blocks.Sources.RealExpression z(y=0)
    annotation (Placement(transformation(extent={{-110,12},{-90,32}})));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force(resolveInFrame=Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.frame_b)
    annotation (Placement(transformation(extent={{56,72},{76,92}})));
  Modelica.Mechanics.MultiBody.Parts.Body casing(m=0, r_CM={0,0,-0.1})
    annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=180,
        origin={32,-2})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteAngularVelocity
    absoluteAngularVelocity
    annotation (Placement(transformation(extent={{26,92},{46,112}})));
  Modelica.Mechanics.MultiBody.Forces.Torque torque3 annotation (Placement(
        transformation(extent={{-32,-8},{-12,12}}, rotation=0)));
  Modelica.Blocks.Sources.RealExpression x_reverse(y=-2250)
    annotation (Placement(transformation(extent={{-108,-18},{-88,2}})));
equation
  connect(cyl1.frame_b, gearConstraint.frame_a) annotation (Line(
      points={{22,50},{34,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gearConstraint.frame_b, cyl2.frame_a) annotation (Line(
      points={{54,50},{70,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(torque1.frame_b, cyl1.frame_a) annotation (Line(
      points={{-6,50},{2,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(inertia1.flange_b, idealGear.flange_a) annotation (Line(
      points={{-4,-40},{8,-40}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(idealGear.flange_b, inertia2.flange_a) annotation (Line(
      points={{28,-40},{40,-40}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(torque2.flange, inertia1.flange_a) annotation (Line(
      points={{-32,-40},{-24,-40}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(mounting1D.flange_b, idealGear.support) annotation (Line(
      points={{-4,-70},{18,-70},{18,-50}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(mounting1D.flange_b, torque2.support) annotation (Line(
      points={{-4,-70},{0,-70},{0,-58},{-42,-58},{-42,-50}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(fixed.frame_b, mounting1D.frame_a) annotation (Line(
      points={{-32,-90},{-14,-90},{-14,-80}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x.y, torque1.torque[1]) annotation (Line(
      points={{-89,62},{-56,62},{-56,63.3333},{-22,63.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, torque1.torque[2]) annotation (Line(
      points={{-89,42},{-56,42},{-56,62},{-22,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, torque1.torque[3]) annotation (Line(
      points={{-89,22},{-74,22},{-74,66},{-22,66},{-22,60.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(x.y, torque2.tau) annotation (Line(
      points={{-89,62},{-82,62},{-82,-36},{-54,-36},{-54,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(force.frame_b, cyl2.frame_b) annotation (Line(
      points={{76,82},{88,82},{88,70},{96,70},{96,50},{90,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x.y, force.force[1]) annotation (Line(
      points={{-89,62},{-88,62},{-88,84},{54,84},{54,80.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, force.force[2]) annotation (Line(
      points={{-89,42},{-86,42},{-86,56},{-82,56},{-82,82},{54,82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, force.force[3]) annotation (Line(
      points={{-89,22},{-86,22},{-86,30},{-80,30},{-80,83.3333},{54,83.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(casing.frame_a, gearConstraint.bearing) annotation (Line(
      points={{40,-2},{44,-2},{44,40}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteAngularVelocity.frame_a, gearConstraint.frame_a) annotation (
      Line(
      points={{26,102},{26,50},{34,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gearConstraint.bearing, torque1.frame_a) annotation (Line(
      points={{44,40},{36,40},{36,24},{-30,24},{-30,50},{-26,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(torque3.frame_a, world.frame_b) annotation (Line(
      points={{-32,2},{-44,2},{-44,6},{-54,6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(torque3.frame_b, torque1.frame_a) annotation (Line(
      points={{-12,2},{-6,2},{-6,24},{-30,24},{-30,50},{-26,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(z.y, torque3.torque[3]) annotation (Line(
      points={{-89,22},{-28,22},{-28,12.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, torque3.torque[2]) annotation (Line(
      points={{-89,42},{-64,42},{-64,30},{-34,30},{-34,14},{-28,14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(x_reverse.y, torque3.torque[1]) annotation (Line(
      points={{-87,-8},{-64,-8},{-64,-14},{-38,-14},{-38,18},{-28,18},{-28,
          15.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (experiment(StopTime=5), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end GearConstraintMovingFixedCasing;
