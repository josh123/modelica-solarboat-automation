within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Thrusters.Old;
model CutForce
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-64,6},{-44,26}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder cyl2(r={0.4,0,0}, diameter=
        0.2) annotation (Placement(transformation(extent={{80,50},{100,70}},
          rotation=0)));
  Modelica.Blocks.Sources.RealExpression x(y=1000)
    annotation (Placement(transformation(extent={{-100,62},{-80,82}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-100,42},{-80,62}})));
  Modelica.Blocks.Sources.RealExpression z(y=0)
    annotation (Placement(transformation(extent={{-100,22},{-80,42}})));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force(resolveInFrame=Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.frame_b)
    annotation (Placement(transformation(extent={{66,82},{86,102}})));
  Modelica.Mechanics.MultiBody.Parts.Body casing(r_CM={0,0,-0.1}, m=10)
    annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=90,
        origin={52,68})));
  Modelica.Mechanics.MultiBody.Sensors.CutForce cutForce(resolveInFrame=
        Modelica.Mechanics.MultiBody.Types.ResolveInFrameA.world)
    annotation (Placement(transformation(extent={{-8,2},{12,22}})));
equation
  connect(force.frame_b, cyl2.frame_b) annotation (Line(
      points={{86,92},{98,92},{98,80},{106,80},{106,60},{100,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x.y, force.force[1]) annotation (Line(
      points={{-79,72},{-78,72},{-78,94},{64,94},{64,90.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, force.force[2]) annotation (Line(
      points={{-79,52},{-76,52},{-76,66},{-72,66},{-72,92},{64,92}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, force.force[3]) annotation (Line(
      points={{-79,32},{-76,32},{-76,40},{-70,40},{-70,93.3333},{64,93.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(casing.frame_a, cyl2.frame_a) annotation (Line(
      points={{52,60},{80,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cutForce.frame_a, world.frame_b) annotation (Line(
      points={{-8,12},{-26,12},{-26,16},{-44,16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cutForce.frame_b, cyl2.frame_a) annotation (Line(
      points={{12,12},{42,12},{42,14},{64,14},{64,60},{80,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end CutForce;
