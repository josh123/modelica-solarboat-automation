within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Thrusters.Old;
model Thruster2014B_1DRotation
  Modelica.Mechanics.Rotational.Components.IdealPlanetary idealPlanetary(ratio=
        6/1) annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Mechanics.Rotational.Sources.Speed speed
    annotation (Placement(transformation(extent={{-54,-8},{-34,12}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=10000*((2*3.14)/60))
    annotation (Placement(transformation(extent={{-96,-8},{-76,12}})));
  Modelica.Mechanics.Rotational.Components.Inertia inertia
    annotation (Placement(transformation(extent={{26,-10},{46,10}})));
  Modelica.Mechanics.Rotational.Components.Fixed fixed
    annotation (Placement(transformation(extent={{-22,32},{-2,12}})));
equation
  connect(speed.flange, idealPlanetary.sun) annotation (Line(
      points={{-34,2},{-26,2},{-26,0},{-8,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(realExpression.y, speed.w_ref) annotation (Line(
      points={{-75,2},{-56,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(idealPlanetary.ring, inertia.flange_a) annotation (Line(
      points={{12,0},{26,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(idealPlanetary.carrier, fixed.flange) annotation (Line(
      points={{-8,4},{-12,4},{-12,22}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Thruster2014B_1DRotation;
