within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old;
model PropellerNVariation
  Old.ExpermentalFunction.PropellerFixedN propellerFixedN
    annotation (Placement(transformation(extent={{2,-8},{22,12}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=1,
    offset=0.1,
    height=99.9)
    annotation (Placement(transformation(extent={{-50,-6},{-30,14}})));
equation
  connect(ramp.y, propellerFixedN.u) annotation (Line(
      points={{-29,4},{-14,4},{-14,2},{1.8,2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PropellerNVariation;
