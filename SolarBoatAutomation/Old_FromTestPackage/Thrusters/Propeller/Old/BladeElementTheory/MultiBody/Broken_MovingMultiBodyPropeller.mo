within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.MultiBody;
model Broken_MovingMultiBodyPropeller
  extends Modelica.Icons.Example;
  inner Modelica.Mechanics.MultiBody.World world(driveTrainMechanics3D=true, g=
        0) annotation (Placement(transformation(extent={{-72,-14},{-52,6}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Forces.Torque torque1 annotation (Placement(
        transformation(extent={{-26,40},{-6,60}}, rotation=0)));
  Modelica.Blocks.Sources.RealExpression x(y=0.05)
    annotation (Placement(transformation(extent={{-110,52},{-90,72}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-110,32},{-90,52}})));
  Modelica.Blocks.Sources.RealExpression z(y=0)
    annotation (Placement(transformation(extent={{-110,12},{-90,32}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteAngularVelocity
    absoluteAngularVelocity
    annotation (Placement(transformation(extent={{40,88},{60,108}})));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder cyl1(
    diameter=0.1,
    color={0,128,0},
    r={0.4,0,0},
    density=1,
    w_0_fixed=false)
    annotation (Placement(transformation(extent={{10,40},{30,60}},rotation=0)));
  PropellerMultiBody propellerMultiBody
    annotation (Placement(transformation(extent={{76,40},{96,60}})));
  Modelica.Mechanics.MultiBody.Joints.GearConstraint gearConstraint(
    phi_b(fixed=true),
    w_b(fixed=true),
    ratio=10)        annotation (Placement(transformation(extent={{44,40},{64,
            60}}, rotation=0)));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteAngularVelocity
    absoluteAngularVelocity1
    annotation (Placement(transformation(extent={{74,88},{94,108}})));
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=0.1, r_CM={0,100000,0})
    annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=180,
        origin={2,4})));
equation
  connect(x.y, torque1.torque[1]) annotation (Line(
      points={{-89,62},{-56,62},{-56,63.3333},{-22,63.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, torque1.torque[2]) annotation (Line(
      points={{-89,42},{-56,42},{-56,62},{-22,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, torque1.torque[3]) annotation (Line(
      points={{-89,22},{-74,22},{-74,66},{-22,66},{-22,60.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(torque1.frame_b, cyl1.frame_a) annotation (Line(
      points={{-6,50},{10,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cyl1.frame_b, gearConstraint.frame_a) annotation (Line(
      points={{30,50},{44,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(propellerMultiBody.frame_a, gearConstraint.frame_b) annotation (Line(
      points={{76.2,50.4},{70,50.4},{70,50},{64,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteAngularVelocity.frame_a, gearConstraint.frame_a) annotation (
      Line(
      points={{40,98},{38,98},{38,50},{44,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteAngularVelocity1.frame_a, gearConstraint.frame_b) annotation (
     Line(
      points={{74,98},{70,98},{70,50},{64,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(torque1.frame_a, gearConstraint.bearing) annotation (Line(
      points={{-26,50},{-38,50},{-38,20},{54,20},{54,40}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(lumpedMass.frame_a, gearConstraint.bearing) annotation (Line(
      points={{10,4},{12,4},{12,20},{54,20},{54,40}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=5), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end Broken_MovingMultiBodyPropeller;
