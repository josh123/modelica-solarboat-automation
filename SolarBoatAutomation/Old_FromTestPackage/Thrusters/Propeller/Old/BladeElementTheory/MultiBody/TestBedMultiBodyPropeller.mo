within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.MultiBody;
model TestBedMultiBodyPropeller
  extends Modelica.Icons.Example;
  inner Modelica.Mechanics.MultiBody.World world(driveTrainMechanics3D=true, g=
        0) annotation (Placement(transformation(extent={{-98,-74},{-78,-54}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Forces.Torque torque1 annotation (Placement(
        transformation(extent={{-14,40},{6,60}},  rotation=0)));
  Modelica.Blocks.Sources.RealExpression x(y=0.05)
    annotation (Placement(transformation(extent={{-110,52},{-90,72}})));
  Modelica.Blocks.Sources.RealExpression y(y=0)
    annotation (Placement(transformation(extent={{-110,32},{-90,52}})));
  Modelica.Blocks.Sources.RealExpression z(y=0)
    annotation (Placement(transformation(extent={{-110,12},{-90,32}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteAngularVelocity
    absoluteAngularVelocity
    annotation (Placement(transformation(extent={{40,76},{60,96}})));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder cyl1(
    diameter=0.1,
    color={0,128,0},
    r={0.4,0,0},
    density=1,
    w_0_fixed=false)
    annotation (Placement(transformation(extent={{14,40},{34,60}},rotation=0)));
  PropellerMultiBody propellerMultiBody
    annotation (Placement(transformation(extent={{74,40},{94,60}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-72,-34},{-52,-14}})));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{-42,-10},{-22,10}})));
  Modelica.Mechanics.MultiBody.Joints.GearConstraint gearConstraint(
    phi_b(fixed=true),
    w_b(fixed=true),
    ratio=10)        annotation (Placement(transformation(extent={{44,40},{64,
            60}}, rotation=0)));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteAngularVelocity
    absoluteAngularVelocity1
    annotation (Placement(transformation(extent={{76,76},{96,96}})));
equation
  connect(x.y, torque1.torque[1]) annotation (Line(
      points={{-89,62},{-56,62},{-56,63.3333},{-10,63.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y.y, torque1.torque[2]) annotation (Line(
      points={{-89,42},{-56,42},{-56,62},{-10,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z.y, torque1.torque[3]) annotation (Line(
      points={{-89,22},{-74,22},{-74,66},{-10,66},{-10,60.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(torque1.frame_b, cyl1.frame_a) annotation (Line(
      points={{6,50},{14,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b, cutForceAndTorque.frame_a) annotation (Line(
      points={{-78,-64},{-74,-64},{-74,-24},{-72,-24}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cutForceAndTorque.frame_b, TestRig.frame_a) annotation (Line(
      points={{-52,-24},{-46,-24},{-46,0},{-42,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TestRig.frame_b, torque1.frame_a) annotation (Line(
      points={{-22,0},{-20,0},{-20,50},{-14,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(propellerMultiBody.frame_a, gearConstraint.frame_b) annotation (Line(
      points={{74.2,50.4},{70,50.4},{70,50},{64,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cyl1.frame_b, gearConstraint.frame_a) annotation (Line(
      points={{34,50},{44,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteAngularVelocity.frame_a, gearConstraint.frame_a) annotation (
      Line(
      points={{40,86},{40,50},{44,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteAngularVelocity1.frame_a, gearConstraint.frame_b) annotation (
     Line(
      points={{76,86},{70,86},{70,50},{64,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gearConstraint.bearing, torque1.frame_a) annotation (Line(
      points={{54,40},{54,6},{-20,6},{-20,50},{-14,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=5), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end TestBedMultiBodyPropeller;
