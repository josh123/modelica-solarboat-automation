within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.MultiBody;
model PropellerMultiBody

  parameter Modelica.SIunits.Length dia = 0.2;
  parameter Modelica.SIunits.Length pitch = 0.286;
  parameter Modelica.SIunits.Density water_density = 1000;
  parameter Modelica.SIunits.Length chord = 0.1 "Propeller chord length";
  parameter Real num_blades = 2 "Number of blades on the propller";
  parameter Modelica.SIunits.Density rho = 1000 "Density of the fluid";
  parameter Real num_elements = 10
    "Number of elements to devide each propeller blade into";

  Modelica.SIunits.AngularVelocity w
    "Angular velocity of flange with respect to support (= der(phi))";
  Modelica.SIunits.Torque torque_generated
    "Accelerating torque acting at flange (= -flange.tau)";
  Real advance_coefficient_j;
  Real torque_coefficient_kq;
  Real thrust_coefficient_kt;
  Modelica.SIunits.Force thrust_generated;
  Modelica.SIunits.Efficiency efficency;
  Modelica.SIunits.Velocity v
    "Velocity of the propeller moving through the water";
  Modelica.SIunits.Length rad "Propeller radius";

  Modelica.Mechanics.MultiBody.Forces.WorldForce force
    annotation (Placement(transformation(extent={{14,34},{-6,54}})));
  Modelica.Mechanics.MultiBody.Forces.WorldTorque torque
    annotation (Placement(transformation(extent={{12,-24},{-8,-4}})));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a
    annotation (Placement(transformation(extent={{-114,-12},{-82,20}})));
  Modelica.Blocks.Sources.RealExpression x_force(y=sign(w)*thrust_generated)
    annotation (Placement(transformation(extent={{84,34},{48,54}})));
  Modelica.Blocks.Sources.RealExpression y_force(y=0)
    annotation (Placement(transformation(extent={{84,14},{64,34}})));
  Modelica.Blocks.Sources.RealExpression z_force(y=0)
    annotation (Placement(transformation(extent={{84,-6},{64,14}})));
  Modelica.Blocks.Sources.RealExpression x_torque(y=-sign(w)*torque_generated)
    annotation (Placement(transformation(extent={{82,-26},{46,-6}})));
  Modelica.Blocks.Sources.RealExpression y_torque(y=0)
    annotation (Placement(transformation(extent={{82,-46},{62,-26}})));
  Modelica.Blocks.Sources.RealExpression z_torque(y=0)
    annotation (Placement(transformation(extent={{82,-66},{62,-46}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{-68,72},{-48,92}})));
equation
  w = frame_a.R.w[1];
  v = absoluteVelocity.v[1];
  rad = dia/2;

  connect(torque.frame_b, frame_a) annotation (Line(
      points={{-8,-14},{-54,-14},{-54,4},{-98,4}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(force.frame_b, frame_a) annotation (Line(
      points={{-6,44},{-56,44},{-56,4},{-98,4}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x_force.y, force.force[1]) annotation (Line(
      points={{46.2,44},{40,44},{40,42.6667},{16,42.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y_force.y, force.force[2]) annotation (Line(
      points={{63,24},{40,24},{40,44},{16,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z_force.y, force.force[3]) annotation (Line(
      points={{63,4},{40,4},{40,45.3333},{16,45.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(x_torque.y, torque.torque[1]) annotation (Line(
      points={{44.2,-16},{38,-16},{38,-15.3333},{14,-15.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y_torque.y, torque.torque[2]) annotation (Line(
      points={{61,-36},{38,-36},{38,-14},{14,-14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z_torque.y, torque.torque[3]) annotation (Line(
      points={{61,-56},{38,-56},{38,-12.6667},{14,-12.6667}},
      color={0,0,127},
      smooth=Smooth.None));

//algorithm
  (thrust_generated,torque_generated,advance_coefficient_j,
    thrust_coefficient_kt,torque_coefficient_kq,efficency) =
    SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.getPropellerThrustAndTorque(
    v,
    rad,
    chord,
    num_blades,
    w,
    rho,
    num_elements,
    pitch);

  connect(absoluteVelocity.frame_a, frame_a) annotation (Line(
      points={{-68,82},{-74,82},{-74,4},{-98,4}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PropellerMultiBody;
