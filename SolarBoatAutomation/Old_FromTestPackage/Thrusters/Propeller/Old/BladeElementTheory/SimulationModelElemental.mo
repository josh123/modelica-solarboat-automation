within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory;
model SimulationModelElemental
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Velocity v = 1
    "Velocity of the propeller moving through the water";
  parameter Modelica.SIunits.Length r = 0.08
    "Radius of elemental propeller strip from hub";
  parameter Modelica.SIunits.Length chord = 0.1 "Propeller chord length";
  parameter Real num_blades = 2 "Number of blades on the propller";
  parameter Modelica.SIunits.AngularVelocity w = 219.9115
    "Angular velcity of the propeller";
  parameter Modelica.SIunits.Density rho = 1.2250 "Density of the fluid";
  parameter Modelica.SIunits.Angle theta = 1.1050
    "Local blade element setting angle";
  parameter Modelica.SIunits.Length d_r = 0.072
    "Elemental propeller strip length";
  parameter Real a_inital = 0.1 "Inital guess value for axial inflow factor";
  parameter Real b_inital = 0.01 "Inital guess value for angular inflow factor";
  Modelica.SIunits.Force d_thrust
    "Thrust contribution of the elemental propeller strip";
  Modelica.SIunits.Torque d_torque
    "Torque contribution of the elemental propeller strip";
  Boolean iteration_failed
    "Indicates if the iteration scheme failed to converge";
  Real num_iterations "Number of iterations to converge to result";
  Real a "Value for axial inflow factor of previous iteration";
  Real b "Value for angular inflow factor of previous iteration";
algorithm
  (d_thrust,d_torque,iteration_failed,num_iterations,a,b) :=
    SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.getElementalThrustAndTorque(
    v,
    r,
    chord,
    num_blades,
    w,
    rho,
    theta,
    d_r,
    a_inital,
    b_inital);
end SimulationModelElemental;
