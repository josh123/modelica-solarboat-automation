within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.Rotational;
model PropellerWithFlange
//Somewhat based on "open test.xlsx"
  extends Modelica.Mechanics.Rotational.Interfaces.PartialTorque;

  parameter Modelica.SIunits.Length dia = 0.2;
  parameter Modelica.SIunits.Length pitch = 0.286;
  parameter Modelica.SIunits.Density rho = 1000;
  parameter Modelica.SIunits.Length chord = 0.1 "Propeller chord length";
  parameter Real num_blades = 2 "Number of blades on the propller";
  parameter Real num_elements = 10
    "Number of elements to devide each propeller blade into";

  Modelica.SIunits.AngularVelocity w
    "Angular velocity of flange with respect to support (= der(phi))";
  Modelica.SIunits.Torque tau
    "Accelerating torque acting at flange (= -flange.tau)";
  Real angular_velocity_rotaion_per_second_n
    "Angular velocity in the units of rotations per second";
  Modelica.SIunits.Velocity advance_velocity_va;
  Real advance_coefficient_j;
  Real torque_coefficient_kq;
  //Modelica.SIunits.Torque torque;
  Real thrust_coefficient_kt;
  Modelica.SIunits.Force thrust_generated;
  Modelica.SIunits.Efficiency efficency;
  Modelica.SIunits.Velocity v
    "Velocity of the propeller moving through the water";
  Modelica.SIunits.Length rad "Propeller radius";

equation
  w = der(phi);
  tau = -flange.tau;
  rad = dia/2;
  angular_velocity_rotaion_per_second_n = w/(2*Modelica.Constants.pi);
  v = pitch * angular_velocity_rotaion_per_second_n; //Assume no slip

algorithm
  (thrust_generated,tau,advance_coefficient_j,thrust_coefficient_kt,
    torque_coefficient_kq,efficency) :=
    SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.getPropellerThrustAndTorque(
    v,
    rad,
    chord,
    num_blades,
    w,
    rho,
    num_elements,
    pitch);

  annotation (
    Icon(
      coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100.0,-100.0},{100.0,100.0}},
        initialScale=0.1),
      graphics={
        Line(
          points={{-100.0,-100.0},{-80.0,-98.0},{-60.0,-92.0},
            {-40.0,-82.0},{-20.0,-68.0},{0.0,-50.0},{20.0,-28.0},
            {40.0,-2.0},{60.0,28.0},{80.0,62.0},{100.0,100.0}},
          color={0,0,127}, smooth=Smooth.Bezier)}),
    Documentation(info="<HTML>
<p>
Model of torque, quadratic dependent on angular velocity of flange.<br>
Parameter TorqueDirection chooses whether direction of torque is the same in both directions of rotation or not.
</p>
</HTML>"));
end PropellerWithFlange;
