within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory;
function getPropellerThrustAndTorque
  input Modelica.SIunits.Velocity v_advance
    "Velocity of the propeller moving through the water";
  input Modelica.SIunits.Length rad "Propeller radius";
  input Modelica.SIunits.Length chord "Propeller chord length";
  input Real num_blades "Number of blades on the propller";
  input Modelica.SIunits.AngularVelocity w "Angular velocity of the propeller";
  input Modelica.SIunits.Density rho "Density of the fluid";
  input Real num_elements
    "Number of elements to devide each propeller blade into";
  input Modelica.SIunits.Length pitch "Propeller pitch";
  output Modelica.SIunits.Force thrust "Thrust of the propeller";
  output Modelica.SIunits.Torque torque "Torque of the propeller";
  output Real advance_coefficient_j;
  output Real thrust_coefficient_kt;
  output Real torque_coefficient_kq;
  output Modelica.SIunits.Efficiency efficency;
protected
  Modelica.SIunits.Length d_r "Elemental propeller strip length";
  Modelica.SIunits.Length element_radius "Radius where this element is";
  Real a_inital "Inital guess value for axial inflow factor";
  Real b_inital "Inital guess value for angular inflow factor";
  Modelica.SIunits.Force d_thrust
    "Thrust contribution of the elemental propeller strip";
  Modelica.SIunits.Torque d_torque
    "Torque contribution of the elemental propeller strip";
  Boolean iteration_failed
    "Indicates if the iteration scheme failed to converge";
  Real num_iterations "Number of iterations to converge to result";
  Modelica.SIunits.Angle theta "Local blade element setting angle";
  Real a "Value for axial inflow factor of previous iteration";
  Real b "Value for angular inflow factor of previous iteration";
  Real angular_velocity_rotaion_per_second_n
    "Angular velocity in the units of rotations per second";
  Modelica.SIunits.Velocity v "Assumed velocity of water past the propeller";
algorithm
  angular_velocity_rotaion_per_second_n := w/(2*Modelica.Constants.pi);
  //Compute the size of each element
  d_r := rad / num_elements;
  //For each element compute its thrust and torque contribution
  if (w > 0) then
    if (v_advance == 0) then
      //Propeller is not moving so must compute a velocity based on no slip
      v := angular_velocity_rotaion_per_second_n * pitch;
    else
      v := v_advance;
    end if;
    advance_coefficient_j := v/(angular_velocity_rotaion_per_second_n*(2*rad));
    for element in 1:num_elements loop
      element_radius := element * d_r;
      theta := Modelica.Math.atan(pitch / (2 * Modelica.Constants.pi * element_radius));
      if w == 0 then
        a := 0;
        b := 0;
      else
        a := 0.1;
        b := 0.01;
      end if;
      (d_thrust, d_torque, iteration_failed, num_iterations, a, b) := getElementalThrustAndTorque(v, element_radius, chord, num_blades, w, rho, theta, d_r, a, b);
      thrust := thrust + d_thrust;
      torque := torque + d_torque;
    end for;
    if ((thrust <= 0) or (torque <= 0)) then
      thrust := 0;
      torque := 0;
      thrust_coefficient_kt := 0;
      torque_coefficient_kq := 0;
      efficency := 0;
    else
      thrust_coefficient_kt := thrust/(rho*(angular_velocity_rotaion_per_second_n^2)*((rad*2)^4));
      torque_coefficient_kq := torque/(rho*(angular_velocity_rotaion_per_second_n^2)*((rad*2)^5));
      efficency := (advance_coefficient_j*thrust_coefficient_kt)/(2*Modelica.Constants.pi*torque_coefficient_kq);
    end if;
  else
    // There is no angular speed so no thrust etc
    thrust := 0;
    torque := 0;
    advance_coefficient_j := 0;
    thrust_coefficient_kt := 0;
    torque_coefficient_kq := 0;
    efficency := 0;
  end if;

end getPropellerThrustAndTorque;
