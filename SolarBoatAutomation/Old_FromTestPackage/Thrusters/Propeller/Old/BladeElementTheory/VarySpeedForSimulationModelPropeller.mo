within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory;
model VarySpeedForSimulationModelPropeller
  extends Modelica.Icons.Example;
  SimulationModelPropeller simulationModelPropeller
    annotation (Placement(transformation(extent={{16,-10},{36,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=60,
    duration=1,
    offset=0)
    annotation (Placement(transformation(extent={{-56,-10},{-36,10}})));
  Modelica.Blocks.Sources.Constant const(k=1)
    annotation (Placement(transformation(extent={{-62,38},{-42,58}})));
equation

  connect(simulationModelPropeller.u, ramp.y) annotation (Line(
      points={{15.8,0},{-35,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics));
end VarySpeedForSimulationModelPropeller;
