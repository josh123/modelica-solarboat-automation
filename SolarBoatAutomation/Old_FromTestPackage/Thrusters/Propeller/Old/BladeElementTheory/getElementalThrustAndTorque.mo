within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory;
function getElementalThrustAndTorque
  //TODO JCBS - Does not like negative velocity
  //TODO JCBS - Force and torque is coming out negative
  input Modelica.SIunits.Velocity v
    "Velocity of the propeller moving through the water";
  input Modelica.SIunits.Length r
    "Radius of elemental propeller strip from hub";
  input Modelica.SIunits.Length chord "Propeller chord length";
  input Real num_blades "Number of blades on the propller";
  input Modelica.SIunits.AngularVelocity w "Angular velocity of the propeller";
  input Modelica.SIunits.Density rho "Density of the fluid";
  input Modelica.SIunits.Angle theta "Local blade element setting angle";
  input Modelica.SIunits.Length d_r "Elemental propeller strip length";
  input Real a_inital "Inital guess value for axial inflow factor";
  input Real b_inital "Inital guess value for angular inflow factor";
  output Modelica.SIunits.Force d_thrust
    "Thrust contribution of the elemental propeller strip";
  output Modelica.SIunits.Torque d_torque
    "Torque contribution of the elemental propeller strip";
  output Boolean iteration_failed
    "Indicates if the iteration scheme failed to converge";
  output Real num_iterations "Number of iterations to converge to result";
  output Real a "Value for axial inflow factor of previous iteration";
  output Real b "Value for angular inflow factor of previous iteration";
protected
  Boolean finished "Indicates if iteration loop is finished";
  Modelica.SIunits.Velocity v1 "Velocity of fluid local to the propeller";
  Modelica.SIunits.Velocity v0
    "Velocity of fluid axial to the propeller (orthogonal component of v1)";
  Modelica.SIunits.Velocity v2
    "Velocity of fluid in the plane of the propeller (orthogonal component of v1)";
  Modelica.SIunits.Angle phi
    "Angle between the component of fluid velocity axial to the propeller and in the plane of the propller";
  Modelica.SIunits.Angle alpha "Angle between the blade and the fluid flow";
  Modelica.SIunits.Angle cl "Blade lift coefficient";
  Modelica.SIunits.Angle cd "Blade drag coefficient";
  Real a_temp
    "Value for axial inflow factor computed from current elemental thrust";
  Real b_temp
    "Value for angular inflow factor computed from current elemental torque";
  Real a_new
    "Value for axial inflow factor avaeraged from the previous iteration and new value";
  Real b_new
    "Value for angular inflow factor avaeraged from the previous iteration and new value";
algorithm
  num_iterations := 0;
  finished := false;
  // For the inital a and b values
  a := a_inital;
  b := b_inital;
  while finished == false loop
    num_iterations := num_iterations + 1;
    //compute axial velocity
    v0 := v * (1 + a);
    //compute disk plane velocity
    v2 := w * r * (1 - b);
    //flow angle
    phi := Modelica.Math.atan2(v0, v2);
    //blade angle of attack compared to fluid flow
    alpha := theta - phi;
//    if (alpha < 0) then
//      alpha := 0.05;  //3 degrees
//    end if;
    //lift coefficient
    cl := 2 * Modelica.Constants.pi * alpha;
    //drag coefficient
    cd := 0.008 - 0.003 * cl + 0.01 * cl ^ 2;
    //local velocity at blade
    v1 := sqrt(v0 ^ 2 + v2 ^ 2);
    //thrust grading
    d_thrust := 0.5 * rho * v1 ^ 2 * num_blades * chord * (cl * Modelica.Math.cos(phi) - cd * Modelica.Math.sin(phi)) * d_r;
    //torque grading
    d_torque := 0.5 * rho * v1 ^ 2 * num_blades * chord * r * (cd * Modelica.Math.cos(phi) + cl * Modelica.Math.sin(phi)) * d_r;
    //Compute new a and b values based on momentum check
    a_temp := d_thrust / (4.0 * Modelica.Constants.pi * r * rho * v ^ 2 * (1 + a) * d_r);
    //stabilise iteration
    b_temp := d_torque / (4.0 * Modelica.Constants.pi * r ^ 3 * rho * v * (1 + a) * w * d_r);
    a_new := 0.5 * (a + a_temp);
    b_new := 0.5 * (b + b_temp);
    //check for convergence
    if abs(a_new - a) < 1.0e-5 then
      if abs(b_new - b) < 1.0e-5 then
        iteration_failed := false;
        finished := true;
      end if;
    end if;
    a := a_new;
    b := b_new;
    //check to see if iteration stuck
    if num_iterations > 500 then
      iteration_failed := true;
      finished := true;
    end if;
  end while;

end getElementalThrustAndTorque;
