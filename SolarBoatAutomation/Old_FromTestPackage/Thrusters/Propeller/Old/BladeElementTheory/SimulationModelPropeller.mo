within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory;
model SimulationModelPropeller
  Modelica.SIunits.Velocity v
    "Velocity of the propeller moving through the water";
  parameter Modelica.SIunits.Length dia = 1.6 "Propeller diameter";
  parameter Modelica.SIunits.Length rad = dia/2 "Propeller radius";
  parameter Modelica.SIunits.Length chord = 0.1 "Propeller chord length";
  parameter Real num_blades = 2 "Number of blades on the propller";
  parameter Real n = 2100/60
    "Angular velocity in the units of rotations per second";
//  parameter Real n = 1 "Angular velocity in the units of rotations per second";
  parameter Modelica.SIunits.AngularVelocity w = n*2*Modelica.Constants.pi
    "Angular velocity of the propeller";
  parameter Modelica.SIunits.Density rho = 1.225 "Density of the fluid";
  parameter Real num_elements = 10
    "Number of elements to devide each propeller blade into";
  parameter Modelica.SIunits.Length pitch = 1 "Propeller pitch";
  Modelica.SIunits.Force thrust "Thrust of the propeller";
  Modelica.SIunits.Torque torque "Torque of the propeller";
  Real torque_coefficient_kq;
  Real thrust_coefficient_kt;
  Real advance_coefficient_j;
  Modelica.SIunits.Efficiency efficency;
  Modelica.Blocks.Interfaces.RealInput u
    annotation (Placement(transformation(extent={{-122,-20},{-82,20}})));
equation
  v = u;
algorithm
  (thrust,torque,advance_coefficient_j,thrust_coefficient_kt,
    torque_coefficient_kq,efficency) :=
    SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.BladeElementTheory.getPropellerThrustAndTorque(
    v,
    rad,
    chord,
    num_blades,
    w,
    rho,
    num_elements,
    pitch);
end SimulationModelPropeller;
