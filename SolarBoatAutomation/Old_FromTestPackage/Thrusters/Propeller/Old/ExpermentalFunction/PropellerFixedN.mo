within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.Old.ExpermentalFunction;
model PropellerFixedN
  //Somewhat based on "open test.xlsx"
  parameter Modelica.SIunits.Length diameter = 0.2;
  parameter Modelica.SIunits.Length pitch = 0.05;
  parameter Modelica.SIunits.Density water_density = 1000;
  Real angular_velocity_rotaion_per_second_n = u;

  Modelica.SIunits.Velocity advance_velocity_va;
  Real advance_coefficient_j;
  Modelica.SIunits.AngularVelocity angular_velocity_omega;
  Real torque_coefficient_kq;
  Modelica.SIunits.Torque torque;
  Real thrust_coefficient_kt;
  Modelica.SIunits.Force thrust;
  Modelica.SIunits.Efficiency efficency;

  Modelica.Blocks.Interfaces.RealInput u
    annotation (Placement(transformation(extent={{-122,-20},{-82,20}})));
equation
  angular_velocity_rotaion_per_second_n = angular_velocity_omega/(2*Modelica.Constants.pi);
  advance_coefficient_j = advance_velocity_va/(angular_velocity_rotaion_per_second_n*diameter);
  advance_velocity_va = pitch * angular_velocity_rotaion_per_second_n;
  torque_coefficient_kq = ((0.0092*(advance_coefficient_j^3))-(0.1178*(advance_coefficient_j^2))+(0.0023*(advance_coefficient_j))+0.3064)/10;
  torque = torque_coefficient_kq*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^5);
  thrust_coefficient_kt = (-0.0893*(advance_coefficient_j^3))+(0.0792*(advance_coefficient_j^2))-(0.0492*(advance_coefficient_j))+0.2229;
  thrust = thrust_coefficient_kt*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^4);
  efficency = (-0.7573*(advance_coefficient_j^3))+(1.2609*(advance_coefficient_j^2))+(0.3022*(advance_coefficient_j))+0.0042;

end PropellerFixedN;
