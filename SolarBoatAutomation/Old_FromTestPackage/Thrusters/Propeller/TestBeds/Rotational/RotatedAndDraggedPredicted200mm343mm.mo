within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.TestBeds.Rotational;
model RotatedAndDraggedPredicted200mm343mm
  extends Modelica.Icons.Example;
  extends Partial_RotatedAndDraggedPropellerWithFlange(redeclare
      L4_SubsystemComponents.RotationToThrust.Rotational.Complete.Predicted200mm343mm
      partial_PropellerRotational);
end RotatedAndDraggedPredicted200mm343mm;
