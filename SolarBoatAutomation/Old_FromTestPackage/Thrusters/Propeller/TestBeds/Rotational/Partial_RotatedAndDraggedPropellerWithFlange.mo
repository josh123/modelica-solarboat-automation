within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.TestBeds.Rotational;
model Partial_RotatedAndDraggedPropellerWithFlange
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(w_fixed=7*2
        *3.14)
              annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
  replaceable
    L4_SubsystemComponents.RotationToThrust.Rotational.Partial.PropellerRotational
    partial_PropellerRotational
    annotation (Placement(transformation(extent={{56,-10},{36,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=1,
    offset=0,
    height=2.5)
    annotation (Placement(transformation(extent={{38,26},{58,46}})));
equation
  connect(constantSpeed.flange, partial_PropellerRotational.flange) annotation (
     Line(
      points={{-70,0},{36,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(ramp.y, partial_PropellerRotational.set_velocity) annotation (Line(
      points={{59,36},{72,36},{72,4.2},{56.2,4.2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Partial_RotatedAndDraggedPropellerWithFlange;
