within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Propeller.TestBeds.Rotational;
model RotatedAndDraggedPredicted160mm135mm
  extends Modelica.Icons.Example;
  extends Partial_RotatedAndDraggedPropellerWithFlange(redeclare
      L4_SubsystemComponents.RotationToThrust.Rotational.Complete.Predicted160mm135mm
      partial_PropellerRotational);
end RotatedAndDraggedPredicted160mm135mm;
