within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Motor.Old;
model ConnectedMotor
  Old.Turnigy_L3040A_480G turnigy_L3040A_480G(inertia=99999999)
    annotation (Placement(transformation(extent={{-12,-2},{8,18}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=22.5)
    annotation (Placement(transformation(extent={{-70,14},{-50,34}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-46,-46},{-26,-26}})));
equation
  connect(constantVoltage.n, turnigy_L3040A_480G.pin_ep) annotation (Line(
      points={{-50,24},{-32,24},{-32,14.8},{-12,14.8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(turnigy_L3040A_480G.pin_en, ground.p) annotation (Line(
      points={{-12,2.8},{-22,2.8},{-22,-14},{-36,-14},{-36,-26}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, constantVoltage.p) annotation (Line(
      points={{-36,-26},{-62,-26},{-62,-20},{-84,-20},{-84,24},{-70,24}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end ConnectedMotor;
