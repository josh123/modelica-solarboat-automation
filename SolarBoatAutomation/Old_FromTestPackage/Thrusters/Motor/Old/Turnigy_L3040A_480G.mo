within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Motor.Old;
model Turnigy_L3040A_480G

  parameter Modelica.SIunits.MomentOfInertia inertia = 1;

  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange "Shaft"
    annotation (Placement(transformation(extent={{90,-2},{110,18}},
          rotation=0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertiaRotor(final J=inertia)
    annotation (Placement(transformation(
        origin={80,8},
        extent={{10,10},{-10,-10}},
        rotation=180)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_ep
    "Positive excitation pin"
    annotation (Placement(transformation(extent={{-110,78},{-90,58}},
          rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_en
    "Negative excitation pin"
    annotation (Placement(transformation(extent={{-90,-42},{-110,-62}},
          rotation=0)));
  Modelica.Electrical.Analog.Basic.EMF emf(k=0.02) annotation(Placement(visible = true, transformation(origin={40,8},
                                                                                                    extent = {{-10,-10},{10,10}}, rotation = 0)));
equation
  connect(inertiaRotor.flange_b,flange) annotation (Line(points={{90,8},{92,8},{
          100,8}},                                         color={0,0,0}));
  connect(inertiaRotor.flange_a, emf.flange) annotation (Line(
      points={{70,8},{50,8}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(pin_en, emf.n) annotation (Line(
      points={{-100,-52},{-28,-52},{-28,-48},{40,-48},{40,-2}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(pin_ep, emf.p) annotation (Line(
      points={{-100,68},{40,68},{40,18}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Turnigy_L3040A_480G;
