within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Motor.Old;
model MaxonEC_max_30_60W_Solar
  //http://www.maxonmotor.com/maxon/view/category/motor?target=filter&filterCategory=ecmax
  import Modelica.Constants.pi;

  Modelica.Electrical.Machines.BasicMachines.DCMachines.DC_PermanentMagnet dcpm(
    VaNominal=12,
    IaNominal=4.72,
    Ra=0.447,
    La=0.049,
    Jr=2.19e-06,
    wNominal=690.10318623856,
    useThermalPort=false)
    annotation (Placement(transformation(extent={{-32,-12},{-12,8}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-2},{-38,18}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor multRotationalSensor
    annotation (Placement(transformation(extent={{0,-12},{20,8}})));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia1(J=0.001)
    annotation (Placement(transformation(extent={{40,-12},{60,8}},
          rotation=0)));
  Modelica_EnergyStorages.Sources.Chargers.CCCV cCCV(
    ExternalControl=false,
    Vmax=30,
    Imax=5) annotation (Placement(transformation(extent={{-78,40},{-58,60}})));
  Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=true)
    annotation (Placement(transformation(extent={{-104,-22},{-84,-2}})));
  Modelica.Mechanics.Rotational.Sources.QuadraticSpeedDependentTorque
    loadTorque1(
      useSupport=false,
    TorqueDirection=false,
    tau_nominal=-63.66e-3,
    w_nominal=6590*2*pi/60)
                annotation (Placement(transformation(extent={{96,-38},{76,-18}},
                   rotation=0)));
equation
  connect(ground.p, dcpm.pin_an) annotation (Line(
      points={{-48,18},{-28,18},{-28,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(dcpm.flange, multRotationalSensor.flange_a) annotation (Line(
      points={{-12,-2},{0,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(multRotationalSensor.flange_b, loadInertia1.flange_a) annotation (
      Line(
      points={{20,-2},{40,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(cCCV.pin_n, ground.p) annotation (Line(
      points={{-68,40},{-68,18},{-48,18}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(cCCV.pin_p, dcpm.pin_ap) annotation (Line(
      points={{-68,60},{-68,72},{-16,72},{-16,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(cCCV.on, booleanExpression.y) annotation (Line(
      points={{-77,50},{-80,50},{-80,-12},{-83,-12}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end MaxonEC_max_30_60W_Solar;
