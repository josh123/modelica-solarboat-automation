within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Motor.Old;
model MaxonEC_max_60W_24V_272763
  //http://www.maxonmotor.com/maxon/view/category/motor?target=filter&filterCategory=ecmax
  import Modelica.Constants.pi;

  Modelica.Electrical.Machines.BasicMachines.DCMachines.DC_PermanentMagnet dcpm(
    Jr=2.19e-06,
    useThermalPort=false,
    VaNominal=24,
    IaNominal=2.66,
    Ra=1.27,
    La=0.143e-3,
    useSupport=false,
    wNominal=841.94683116206)
    annotation (Placement(transformation(extent={{-32,-12},{-12,8}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=24)
    annotation (Placement(transformation(extent={{-82,58},{-62,78}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-2},{-38,18}})));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia1(J=
        100000000000000)
    annotation (Placement(transformation(extent={{40,-12},{60,8}},
          rotation=0)));
  SolarBoatAutomation.Helpers.Sensors.RotationSensor_MultiUnits
    multiSensor_MultiUnits
    annotation (Placement(transformation(extent={{8,-12},{28,8}})));
equation
  connect(ground.p, constantVoltage.p) annotation (Line(
      points={{-48,18},{-74,18},{-74,24},{-96,24},{-96,68},{-82,68}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.n, dcpm.pin_ap) annotation (Line(
      points={{-62,68},{-38,68},{-38,66},{-16,66},{-16,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, dcpm.pin_an) annotation (Line(
      points={{-48,18},{-28,18},{-28,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(dcpm.flange, multiSensor_MultiUnits.flange_a) annotation (Line(
      points={{-12,-2},{8,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(loadInertia1.flange_a, multiSensor_MultiUnits.flange_b) annotation (
      Line(
      points={{40,-2},{28,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end MaxonEC_max_60W_24V_272763;
