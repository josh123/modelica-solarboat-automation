within SolarBoatAutomation.Old_FromTestPackage.Thrusters.Motor.Old;
model MaxonEC_max_30_60W_Loaded
  //http://www.maxonmotor.com/maxon/view/category/motor?target=filter&filterCategory=ecmax
  import Modelica.Constants.pi;

  Modelica.Electrical.Machines.BasicMachines.DCMachines.DC_PermanentMagnet dcpm(
    VaNominal=12,
    IaNominal=4.72,
    Ra=0.447,
    Jr=2.19e-06,
    useThermalPort=false,
    wNominal=690.10318623856,
    La=0.049e-3)
    annotation (Placement(transformation(extent={{-32,-12},{-12,8}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=12)
    annotation (Placement(transformation(extent={{-82,58},{-62,78}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-2},{-38,18}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor multRotationalSensor
    annotation (Placement(transformation(extent={{0,-12},{20,8}})));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia1(J=0.001)
    annotation (Placement(transformation(extent={{40,-12},{60,8}},
          rotation=0)));
  Modelica.Mechanics.Rotational.Sources.QuadraticSpeedDependentTorque
    loadTorque1(
      useSupport=false,
    TorqueDirection=false,
    tau_nominal=-63.66e-3,
    w_nominal=6590*2*pi/60)
                annotation (Placement(transformation(extent={{90,-12},{70,8}},
                   rotation=0)));
equation
  connect(ground.p, constantVoltage.p) annotation (Line(
      points={{-48,18},{-74,18},{-74,24},{-96,24},{-96,68},{-82,68}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.n, dcpm.pin_ap) annotation (Line(
      points={{-62,68},{-38,68},{-38,66},{-16,66},{-16,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, dcpm.pin_an) annotation (Line(
      points={{-48,18},{-28,18},{-28,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(dcpm.flange, multRotationalSensor.flange_a) annotation (Line(
      points={{-12,-2},{0,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(loadInertia1.flange_b,loadTorque1. flange)
    annotation (Line(points={{60,-2},{70,-2}},   color={0,0,0}));
  connect(multRotationalSensor.flange_b, loadInertia1.flange_a) annotation (
      Line(
      points={{20,-2},{40,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end MaxonEC_max_30_60W_Loaded;
