within SolarBoatAutomation.L2_Assess_L3.SolarToElectrical.Examples;
model SubSystems_Array_VoltageSweep_STC
  extends Modelica.Icons.Example;
  extends PartialModels.Partial_SubSystems_VoltageSweep(redeclare
      L1_Assess_L2.Components.Solar.CompletedModels.Constant.STC
      partialSolarInsolation, redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical);
end SubSystems_Array_VoltageSweep_STC;
