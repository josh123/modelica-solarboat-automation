within SolarBoatAutomation.L2_Assess_L3.SolarToElectrical.PartialModels;
partial model Partial_MPPT

  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-82,-88},{-62,-68}},
          rotation=0)));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-46,-88},{-12,-68}})));
  replaceable
    L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance
    partialSolarInsolation
    annotation (Placement(transformation(extent={{-94,66},{-74,86}})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-64,70},{-50,84}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=-60)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={64,-2})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-46,-30},{-26,-10}})));
  replaceable L3_Subsystems.SolarToElectrical.A.Partial.SysArch_PanelMPPT
    sysArch_PanelMPPT
    annotation (Placement(transformation(extent={{-18,-8},{2,12}})));
equation
  connect(world.frame_b,visualEnvironment. frame_a) annotation (Line(
      points={{-62,-78},{-46,-78}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(sysArch_PanelMPPT.p, ground.p) annotation (Line(
      points={{-18,2},{-36,2},{-36,-10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partialSolarInsolation.solarInsolation, sysArch_PanelMPPT.solarInsolation)
    annotation (Line(
      points={{-84,65.2},{-84,28},{-12.9,28},{-12.9,12.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, sysArch_PanelMPPT.temp_kelvin) annotation (Line(
      points={{-49.3,77},{-2.1,77},{-2.1,12.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sysArch_PanelMPPT.n, constantVoltage.p) annotation (Line(
      points={{2,2.2},{14,2.2},{14,8},{42,8},{42,22},{64,22},{64,8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(constantVoltage.n, ground.p) annotation (Line(
      points={{64,-12},{64,-26},{-26,-26},{-26,-10},{-36,-10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(sysArch_PanelMPPT.frame_a, visualEnvironment.frame_a) annotation (
      Line(
      points={{-8,-7.8},{-8,-50},{-52,-50},{-52,-78},{-46,-78}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Partial_MPPT;
