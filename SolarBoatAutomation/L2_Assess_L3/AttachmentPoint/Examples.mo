within SolarBoatAutomation.L2_Assess_L3.AttachmentPoint;
package Examples
  extends Modelica.Icons.ExamplesPackage;
  model CompileCheck
    extends Modelica.Icons.Example;
    L3_Subsystems.AttachmentPoint.Complete.AttachmentPoint attachmentPoint
      annotation (Placement(transformation(extent={{-30,-24},{28,24}})));
    inner Modelica.Mechanics.MultiBody.World world(n={0,0,1})
      annotation (Placement(transformation(extent={{-62,-64},{-42,-44}})));
    Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=1)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-42,22})));
  equation

    connect(attachmentPoint.frame_a, lumpedMass.frame_a) annotation (Line(
        points={{-1,0},{-16,0},{-16,22},{-32,22}},
        color={95,95,95},
        thickness=0.5,
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{
              -100,-100},{100,100}}), graphics));
  end CompileCheck;
end Examples;
