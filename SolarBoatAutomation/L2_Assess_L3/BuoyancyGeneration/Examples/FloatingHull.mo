within SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.Examples;
model FloatingHull
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_FloatingHull(redeclare
      L1_Assess_L2.Components.Payload.Components.CompletedModels.Payload15kg
      partialPayload, redeclare
      L3_Subsystems.BuoyancyGeneration.Complete.MonoDisplacementHull2014
      partialBuoyancyGeneration);
end FloatingHull;
