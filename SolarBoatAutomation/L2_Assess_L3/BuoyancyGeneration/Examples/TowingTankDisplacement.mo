within SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.Examples;
model TowingTankDisplacement
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_TowingTank(
    redeclare
      L4_SubsystemComponents.BuoyancyGeneration.Complete.DisplacementHull2014
      partialBuoyancyGeneration,
    redeclare
      L1_Assess_L2.Components.Payload.Components.CompletedModels.Payload15kg
      partialPayload,
    ramp(height=3));
end TowingTankDisplacement;
