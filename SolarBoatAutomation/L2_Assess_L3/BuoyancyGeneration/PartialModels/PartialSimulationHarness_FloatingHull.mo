within SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.PartialModels;
partial model PartialSimulationHarness_FloatingHull
  replaceable
    L3_Subsystems.BuoyancyGeneration.Partial.PartialBuoyancyGeneration
    partialBuoyancyGeneration
    annotation (Placement(transformation(extent={{4,12},{24,32}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-92,42},{-72,62}},
          rotation=0)));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-56,-26},{-22,-6}})));
  replaceable L1_Assess_L2.Components.Payload.PartialModels.PartialPayload
    partialPayload
    annotation (Placement(transformation(extent={{-4,48},{28,70}})));
equation
  connect(world.frame_b, visualEnvironment.frame_a) annotation (Line(
      points={{-72,52},{-62,52},{-62,-16},{-56,-16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partialBuoyancyGeneration.frame_a, partialPayload.frame_a)
    annotation (Line(
      points={{14,12.2},{14,6},{-2,6},{-2,42},{12,42},{12,48.22}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialSimulationHarness_FloatingHull;
