within SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.PartialModels;
partial model PartialSimulationHarness_TowingTank
  replaceable
    L3_Subsystems.BuoyancyGeneration.Partial.PartialBuoyancyGeneration
    partialBuoyancyGeneration
    annotation (Placement(transformation(extent={{72,24},{92,44}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-92,42},{-72,62}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{2,10},{22,30}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n={1,0,0},
      useAxisFlange=true)
    annotation (Placement(transformation(extent={{-60,42},{-40,62}})));
  Modelica.Mechanics.Translational.Sources.Speed speed
    annotation (Placement(transformation(extent={{-68,76},{-48,96}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{28,62},{48,82}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-14,40})));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-56,-26},{-22,-6}})));
  replaceable L1_Assess_L2.Components.Payload.PartialModels.PartialPayload
    partialPayload
    annotation (Placement(transformation(extent={{64,60},{96,82}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=3.5,
    duration=10,
    startTime=20)
    annotation (Placement(transformation(extent={{-98,76},{-78,96}})));
equation
  connect(world.frame_b, prismatic.frame_a) annotation (Line(
      points={{-72,52},{-60,52}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(speed.flange, prismatic.axis) annotation (Line(
      points={{-48,86},{-42,86},{-42,58}},
      color={0,127,0},
      smooth=Smooth.None));
  connect(cutForceAndTorque.frame_b, absoluteVelocity.frame_a) annotation (Line(
      points={{22,20},{28,20},{28,72}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partialBuoyancyGeneration.frame_a, absoluteVelocity.frame_a)
    annotation (Line(
      points={{82,24.2},{60,24.2},{60,26},{28,26},{28,72}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_a, prismatic.frame_b) annotation (Line(
      points={{-14,50},{-14,52},{-40,52}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_b, cutForceAndTorque.frame_a) annotation (Line(
      points={{-14,30},{-14,20},{2,20}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(visualEnvironment.frame_a, prismatic.frame_a) annotation (Line(
      points={{-56,-16},{-64,-16},{-64,52},{-60,52}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partialPayload.frame_a, absoluteVelocity.frame_a) annotation (Line(
      points={{80,60.22},{74,60.22},{74,56},{68,56},{68,24},{60,24.2},{60,26},{
          28,26},{28,72}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(speed.v_ref, ramp.y) annotation (Line(
      points={{-70,86},{-77,86}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialSimulationHarness_TowingTank;
