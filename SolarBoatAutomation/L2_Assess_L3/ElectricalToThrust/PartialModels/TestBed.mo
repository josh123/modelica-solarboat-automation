within SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.PartialModels;
partial model TestBed
  import SolarBoatAutomation;
  Modelica.SIunits.Power supplied_power;
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-90,0},{-70,20}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-74,-78},{-54,-58}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{-10,-78},{10,-58}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-38,-78},{-18,-58}})));
  replaceable
    SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    partial_ThrusterMultiBody
    annotation (Placement(transformation(extent={{28,-6},{48,14}})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(      duration=10, V=18.5)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-54,76})));
equation
  supplied_power = abs(rampVoltage.v) * abs(rampVoltage.i);
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{-10,-68},{-18,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-54,-68},{-38,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TestRig.frame_b, partial_ThrusterMultiBody.frame_a) annotation (Line(
      points={{10,-68},{20,-68},{20,-5.8},{38,-5.8}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_ThrusterMultiBody.p, ground.p) annotation (Line(
      points={{32,14},{32,20},{-80,20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(rampVoltage.n, partial_ThrusterMultiBody.n) annotation (Line(
      points={{-44,76},{42,76},{42,13.8}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(rampVoltage.p, ground.p) annotation (Line(
      points={{-64,76},{-80,76},{-80,20}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end TestBed;
