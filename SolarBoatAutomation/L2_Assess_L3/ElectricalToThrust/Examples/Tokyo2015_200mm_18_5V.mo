within SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples;
model Tokyo2015_200mm_18_5V
  extends PartialModels.TestBed(redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2015_200mm
      partial_ThrusterMultiBody);
  extends Modelica.Icons.Example;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Tokyo2015_200mm_18_5V;
