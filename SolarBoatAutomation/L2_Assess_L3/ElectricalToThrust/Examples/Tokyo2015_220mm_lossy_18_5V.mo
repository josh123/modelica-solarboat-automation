within SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples;
model Tokyo2015_220mm_lossy_18_5V
  extends PartialModels.TestBed(redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2015_220mm_lossy
      partial_ThrusterMultiBody);
  extends Modelica.Icons.Example;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Tokyo2015_220mm_lossy_18_5V;
