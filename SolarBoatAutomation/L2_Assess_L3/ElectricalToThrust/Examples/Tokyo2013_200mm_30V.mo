within SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples;
model Tokyo2013_200mm_30V
  extends PartialModels.TestBed(redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2013_200mm
      partial_ThrusterMultiBody, rampVoltage(V=30));
  extends Modelica.Icons.Example;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Tokyo2013_200mm_30V;
