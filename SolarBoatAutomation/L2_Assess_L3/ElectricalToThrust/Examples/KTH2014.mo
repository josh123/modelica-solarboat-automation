within SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples;
model KTH2014
  import SolarBoatAutomation;
  extends Modelica.Icons.Example;
  extends
    SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.PartialModels.TestBed(
      redeclare
      SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete.KTH2014_160mm
      partial_ThrusterMultiBody);
end KTH2014;
