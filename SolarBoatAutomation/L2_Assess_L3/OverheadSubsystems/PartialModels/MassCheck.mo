within SolarBoatAutomation.L2_Assess_L3.OverheadSubsystems.PartialModels;
partial model MassCheck
  replaceable
    L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents_StructAndElectrical
    partialOverheadComponents_StructAndElectrical
    annotation (Placement(transformation(extent={{-52,20},{56,56}})));
  inner Modelica.Mechanics.MultiBody.World world(n={0,0,1})
    annotation (Placement(transformation(extent={{-72,-74},{-52,-54}})));
end MassCheck;
