within SolarBoatAutomation.L2_Assess_L3.OverheadSubsystems.Examples;
model Mass2014A
  extends Modelica.Icons.Example;
  extends PartialModels.MassCheck(redeclare
      L3_Subsystems.OverheadSubsystems.Complete.OverheadComponents2014A
      partialOverheadComponents_StructAndElectrical);
end Mass2014A;
