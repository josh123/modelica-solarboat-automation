within SolarBoatAutomation.L1_Assess_L2.Powertrain.PartialModels;
partial model SimulationHarness

  replaceable Components.Solar.CompletedModels.Constant.SunBestEver
    solarInsolation constrainedby
    Components.Solar.PartialModels.PartialSolarIrradiance
    annotation (Placement(transformation(extent={{-18,68},{18,98}})));
  inner Modelica.Mechanics.MultiBody.World world(n={0,0,1})
    annotation (Placement(transformation(extent={{-92,-42},{-72,-22}})));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-44,-42},{-10,-22}})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{32,70},{46,84}})));
  replaceable
    L2_SystemsOfInterest.Powertrain.PartialModels.A.Powertrain_Architecture01_A
    powertrain
    annotation (Placement(transformation(extent={{24,-6},{68,18}})));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{-22,-4},{-2,16}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-50,-4},{-30,16}})));
equation

  connect(world.frame_b, visualEnvironment.frame_a) annotation (Line(
      points={{-72,-32},{-44,-32}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarInsolation.solarInsolation, powertrain.solarInsolation)
    annotation (Line(
      points={{0,66.8},{34.12,66.8},{34.12,16.56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T.y, powertrain.temperature) annotation (Line(
      points={{46.7,77},{54,77},{54,48},{53.92,48},{53.92,16.56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{-22,6},{-30,6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(cutForceAndTorque.frame_a, visualEnvironment.frame_a) annotation (
      Line(
      points={{-50,6},{-64,6},{-64,-32},{-44,-32}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(powertrain.Attachment, TestRig.frame_b) annotation (
      Line(
      points={{24,6.24},{12,6.24},{12,6},{-2,6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=100),
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics={Text(
          extent={{-60,-80},{64,-38}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>A common architecture to simulate boats traveling in a straight line.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSimulationHarness_StraightLine-OPM.png\"/></p>
</html>"));
end SimulationHarness;
