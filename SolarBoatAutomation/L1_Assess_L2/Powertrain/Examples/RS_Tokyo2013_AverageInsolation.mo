within SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples;
model RS_Tokyo2013_AverageInsolation
  extends Modelica.Icons.Example;
  extends ReadyTestBeds.Auto.Scenario_AverageInsolation_Thrust(redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2013 powertrain);
end RS_Tokyo2013_AverageInsolation;
