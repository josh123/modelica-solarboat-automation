within SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples;
model RS_KTH2014_AverageInsolation
  extends Modelica.Icons.Example;
  extends ReadyTestBeds.Auto.Scenario_AverageInsolation_Thrust(redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_KTH2014 powertrain);
end RS_KTH2014_AverageInsolation;
