within SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples;
model KTH2014_Sweden_3Px2S_Array_SP50f_L_LOXX_AverageInsolation
  extends Modelica.Icons.Example;
  extends ReadyTestBeds.Auto.Scenario_AverageInsolation_Thrust(redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.KTH2014_SP50f
      powertrain);
end KTH2014_Sweden_3Px2S_Array_SP50f_L_LOXX_AverageInsolation;
