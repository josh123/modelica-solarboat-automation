within SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples;
model Direct_220mm_AverageInsolation
  extends Modelica.Icons.Example;
  extends ReadyTestBeds.Auto.Scenario_AverageInsolation_Thrust(redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Direct_Turnigy_220mm
      powertrain);
end Direct_220mm_AverageInsolation;
