within SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples;
model Tokyo2015_AverageInsolation
  extends Modelica.Icons.Example;
  extends ReadyTestBeds.Auto.Scenario_AverageInsolation_Thrust(redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2015 powertrain);
end Tokyo2015_AverageInsolation;
