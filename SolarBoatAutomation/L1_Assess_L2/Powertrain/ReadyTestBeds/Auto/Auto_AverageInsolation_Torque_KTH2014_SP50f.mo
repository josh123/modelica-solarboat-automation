within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;


model Auto_AverageInsolation_Torque_KTH2014_SP50f
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.KTH2014_SP50f                                                                                                     powertrain);
end Auto_AverageInsolation_Torque_KTH2014_SP50f;
