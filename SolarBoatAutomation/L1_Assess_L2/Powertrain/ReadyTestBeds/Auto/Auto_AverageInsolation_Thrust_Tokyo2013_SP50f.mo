within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;


model Auto_AverageInsolation_Thrust_Tokyo2013_SP50f
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Tokyo2013_SP50f                                                                                                     powertrain);
end Auto_AverageInsolation_Thrust_Tokyo2013_SP50f;
