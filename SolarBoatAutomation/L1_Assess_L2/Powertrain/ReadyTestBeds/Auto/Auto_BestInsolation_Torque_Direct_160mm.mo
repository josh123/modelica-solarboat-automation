within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_BestInsolation_Torque_Direct_160mm
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Direct_160mm                                                                                                     powertrain);
end Auto_BestInsolation_Torque_Direct_160mm;

