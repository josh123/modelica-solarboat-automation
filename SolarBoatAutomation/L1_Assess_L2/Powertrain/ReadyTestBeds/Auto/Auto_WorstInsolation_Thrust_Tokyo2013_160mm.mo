within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_WorstInsolation_Thrust_Tokyo2013_160mm
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunWorstEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Tokyo2013_160mm                                                                                                     powertrain);
end Auto_WorstInsolation_Thrust_Tokyo2013_160mm;

