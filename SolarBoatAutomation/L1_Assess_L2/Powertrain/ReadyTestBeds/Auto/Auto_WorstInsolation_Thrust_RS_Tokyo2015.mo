within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_WorstInsolation_Thrust_RS_Tokyo2015
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunWorstEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2015                                                                                                     powertrain);
end Auto_WorstInsolation_Thrust_RS_Tokyo2015;

