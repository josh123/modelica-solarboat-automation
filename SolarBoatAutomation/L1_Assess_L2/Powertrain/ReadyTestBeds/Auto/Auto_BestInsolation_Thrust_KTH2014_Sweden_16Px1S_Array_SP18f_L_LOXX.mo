within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_BestInsolation_Thrust_KTH2014_Sweden_16Px1S_Array_SP18f_L_LOXX
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.KTH2014_Sweden_16Px1S_Array_SP18f_L_LOXX
                                                                                                          powertrain);
end Auto_BestInsolation_Thrust_KTH2014_Sweden_16Px1S_Array_SP18f_L_LOXX;

