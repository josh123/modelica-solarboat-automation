within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_AverageInsolation_Torque_RS_KTH2014
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_KTH2014                                                                                                     powertrain);
end Auto_AverageInsolation_Torque_RS_KTH2014;

