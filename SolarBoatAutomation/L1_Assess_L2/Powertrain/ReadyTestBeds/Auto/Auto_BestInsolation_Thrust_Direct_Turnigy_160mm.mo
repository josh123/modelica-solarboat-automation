within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_BestInsolation_Thrust_Direct_Turnigy_160mm
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Direct_Turnigy_160mm                                                                                                     powertrain);
end Auto_BestInsolation_Thrust_Direct_Turnigy_160mm;

