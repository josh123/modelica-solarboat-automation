within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Scenario_AverageInsolation_Thrust
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage solarInsolation,
      redeclare L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2015
      powertrain);
end Scenario_AverageInsolation_Thrust;
