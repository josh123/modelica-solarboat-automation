within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_AverageInsolation_Thrust_RS_Tokyo2015
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2015                                                                                                     powertrain);
end Auto_AverageInsolation_Thrust_RS_Tokyo2015;

