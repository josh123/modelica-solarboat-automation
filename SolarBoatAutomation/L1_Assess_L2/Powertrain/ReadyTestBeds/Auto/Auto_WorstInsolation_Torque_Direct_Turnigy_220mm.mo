within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Auto_WorstInsolation_Torque_Direct_Turnigy_220mm
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunWorstEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Direct_Turnigy_220mm                                                                                                     powertrain);
end Auto_WorstInsolation_Torque_Direct_Turnigy_220mm;

