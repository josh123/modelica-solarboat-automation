within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;


model Auto_BestInsolation_Torque_KTH2014_SP50f
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.KTH2014_SP50f                                                                                                     powertrain);
end Auto_BestInsolation_Torque_KTH2014_SP50f;
