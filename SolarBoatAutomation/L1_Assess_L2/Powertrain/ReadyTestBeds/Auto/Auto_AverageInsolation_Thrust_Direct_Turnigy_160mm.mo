within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;


model Auto_AverageInsolation_Thrust_Direct_Turnigy_160mm
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                               solarInsolation, redeclare
      L2_SystemsOfInterest.Powertrain.CompletedModels.A.Direct_Turnigy_160mm                                                                                                     powertrain);
end Auto_AverageInsolation_Thrust_Direct_Turnigy_160mm;
