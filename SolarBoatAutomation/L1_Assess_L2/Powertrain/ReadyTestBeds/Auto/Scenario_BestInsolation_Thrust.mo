within SolarBoatAutomation.L1_Assess_L2.Powertrain.ReadyTestBeds.Auto;
model Scenario_BestInsolation_Thrust
  extends PartialModels.SimulationHarness(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver solarInsolation,
      redeclare L2_SystemsOfInterest.Powertrain.CompletedModels.A.RS_Tokyo2015
      powertrain);
end Scenario_BestInsolation_Thrust;
