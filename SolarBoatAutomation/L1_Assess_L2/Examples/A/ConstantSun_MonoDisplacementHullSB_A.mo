within SolarBoatAutomation.L1_Assess_L2.Examples.A;
model ConstantSun_MonoDisplacementHullSB_A
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage solarInsolation,
    redeclare L2_SolarBoat.CompletedModels.A.MonoDisplacementHullSB_A solarBoat,
    redeclare Components.Payload.Components.CompletedModels.Payload15kg
      partialPayload);
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-80,-48},{-60,-28}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n={1,0,0},
      useAxisFlange=false)
    annotation (Placement(transformation(extent={{-128,-16},{-108,4}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{-42,-48},{-22,-28}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-96,-18})));
equation
  connect(cutForceAndTorque.frame_b,absoluteVelocity. frame_a) annotation (Line(
      points={{-60,-38},{-42,-38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_a,prismatic. frame_b) annotation (Line(
      points={{-96,-8},{-96,-6},{-108,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-96,-28},{-96,-38},{-80,-38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarBoat.payloadAttachment, absoluteVelocity.frame_a) annotation (
      Line(
      points={{-27.42,2.36},{-50,2.36},{-50,-38},{-42,-38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-128,-6},{-132,-6},{-132,-54},{-52,-54},{-52,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=100), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end ConstantSun_MonoDisplacementHullSB_A;
