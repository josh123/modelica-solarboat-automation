within SolarBoatAutomation.L1_Assess_L2.Examples.C;
model ConstantSunDualHull
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage solarInsolation,
      redeclare L2_SolarBoat.CompletedModels.C.DualHullSB solarBoat);
  annotation (experiment(StopTime=100));
end ConstantSunDualHull;
