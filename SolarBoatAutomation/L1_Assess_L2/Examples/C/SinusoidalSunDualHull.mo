within SolarBoatAutomation.L1_Assess_L2.Examples.C;
model SinusoidalSunDualHull
  extends PartialModels.PartialSimulationHarness_StraightLine(
                                                 redeclare
      Components.Solar.CompletedModels.Sinusoid.SinusoidalSun solarInsolation,
      redeclare L2_SolarBoat.CompletedModels.C.DualHullSB solarBoat);
  extends Modelica.Icons.Example;
  annotation (experiment(StopTime=100));
end SinusoidalSunDualHull;
