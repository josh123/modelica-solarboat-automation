within SolarBoatAutomation;
package L1_Assess_L2 "Package containing partial models for assessing the performance of alternative SolarBoats"


  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide a common way to assess various alternative SolarBoat designs.</p>
</html>"));
end L1_Assess_L2;
