within SolarBoatAutomation.L1_Assess_L2.PartialModels;
partial model PartialSimulationHarness_StraightLine
  "Simulates the SolarBoat traveling in a straight line starting at rest"
  Modelica.SIunits.Velocity x_velocity
    "x direction velocity in world frame of reference";
  Modelica.SIunits.Position z_top_of_hull
    "z position of the top of the hull in the world frame of reference";

  replaceable Components.Solar.CompletedModels.Constant.SunBestEver
    solarInsolation constrainedby
    Components.Solar.PartialModels.PartialSolarInsolation
    annotation (Placement(transformation(extent={{-18,68},{18,98}})));
  replaceable L2_SolarBoat.PartialModels.C.PartialSolarBoat solarBoat
    annotation (Placement(transformation(extent={{-28,-16},{30,20}})));
  inner Modelica.Mechanics.MultiBody.World world(n={0,0,1})
    annotation (Placement(transformation(extent={{-82,-84},{-62,-64}})));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-34,-84},{0,-64}})));
  replaceable Components.Payload.Components.CompletedModels.Payload15kg
                                                              partialPayload
    constrainedby Components.Payload.PartialModels.PartialPayload
    annotation (Placement(transformation(extent={{-82,36},{-42,56}})));
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{32,70},{46,84}})));
equation
  x_velocity = solarBoat.x_velocity;
  z_top_of_hull = solarBoat.z_top_of_hull;
  connect(world.frame_b, visualEnvironment.frame_a) annotation (Line(
      points={{-62,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarInsolation.solarInsolation, solarBoat.solarInsolation)
    annotation (Line(
      points={{0,66.8},{0,17.84},{-14.66,17.84}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(partialPayload.frame_a, solarBoat.payloadAttachment) annotation (Line(
      points={{-62,36.2},{-62,2.36},{-27.42,2.36}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(T.y, solarBoat.temperature) annotation (Line(
      points={{46.7,77},{56,77},{56,56},{11.44,56},{11.44,17.84}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (experiment(StopTime=100),
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics={Text(
          extent={{-60,-80},{64,-38}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>A common architecture to simulate boats traveling in a straight line.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSimulationHarness_StraightLine-OPM.png\"/></p>
</html>"));
end PartialSimulationHarness_StraightLine;
