within SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels;
partial model PartialSolarIrradiance_Sinusoid
  extends PartialSolarIrradiance;
  parameter Real amplitude(final unit="W.m-2") "Amplitude of sine wave";
  parameter Modelica.SIunits.Frequency freqHz "Frequency of sine wave";
  parameter Real average_irradiance(final unit="W.m-2")
    "Offset of output signal";
  Modelica.Blocks.Sources.Sine sine(
    amplitude=amplitude,
    offset=average_irradiance,
    freqHz=freqHz)
    annotation (Placement(transformation(extent={{-68,-18},{-32,18}})));
equation
  connect(solarIrradiance, solarIrradiance) annotation (Line(
      points={{0,-108},{-16,-108},{-16,-110},{-32,-110},{-32,-108},{0,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine.y, solarIrradiance) annotation (Line(
      points={{-30.2,0},{18,0},{18,-108},{0,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialSolarIrradiance_Sinusoid;
