within SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels;
partial model PartialSolarIrradiance_Constant
  extends PartialSolarIrradiance;
  parameter Real solar_irradiance_constant(final unit="W.m-2")
    "Constant solar irradiance from the sun.";
equation
  solarIrradiance = solar_irradiance_constant;
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Simulate solar irradiance which does not vary with time</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarInsolation-OPM.png\"/></p>
</html>"));
end PartialSolarIrradiance_Constant;
