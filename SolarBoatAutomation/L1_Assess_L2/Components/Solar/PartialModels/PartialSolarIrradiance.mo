within SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels;
partial model PartialSolarIrradiance

  Modelica.Blocks.Interfaces.RealOutput solarIrradiance( final unit="W.m-2")
    "The power per square metre delivered by the weather" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={0,-108})));
equation

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
          Text(
          extent={{-64,-100},{66,-50}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Simulate solar irradiance input</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarInsolation-OPM.png\"/></p>
</html>"));
end PartialSolarIrradiance;
