within SolarBoatAutomation.L1_Assess_L2.Components.Solar.CompletedModels.Sinusoid;
model SinusoidalSun
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance_Sinusoid(
                                          amplitude=100, freqHz=10, average_irradiance=0.3e3);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={
        Ellipse(
          extent={{-56,56},{50,-44}},
          lineColor={0,0,127},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-46,-6}},
          color={0,0,127},
          smooth=Smooth.None),
        Line(
          points={{-54,0},{-50,0},{-40,18},{-34,16},{-34,12},{-30,-2},{-24,-18},
              {-18,-20},{-16,-14},{-10,0},{-2,18},{12,8},{12,0},{16,-6},{18,-12},
              {20,-20},{28,-16},{30,-8},{32,0},{32,2},{32,10},{36,14},{40,18},{44,
              4},{48,2},{50,-4}},
          color={0,0,127},
          thickness=1,
          smooth=Smooth.None)}));
end SinusoidalSun;
