within SolarBoatAutomation.L1_Assess_L2.Components.Solar.CompletedModels.Sinusoid;
model SinusoidalSunHighFreq
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance_Sinusoid(
                                          amplitude=100, freqHz=100, average_irradiance=0.3e3);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                         graphics={
        Ellipse(
          extent={{-56,56},{50,-44}},
          lineColor={0,0,127},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-46,-6}},
          color={0,0,127},
          smooth=Smooth.None),
        Line(
          points={{-52,-4},{-44,26},{-42,-20},{-34,26},{-26,-26},{-18,26},{-12,
              -26},{-4,28},{4,-26},{12,28},{20,-26},{26,28},{30,-24},{40,28},{
              46,-2}},
          color={0,0,127},
          thickness=1,
          smooth=Smooth.None)}));
end SinusoidalSunHighFreq;
