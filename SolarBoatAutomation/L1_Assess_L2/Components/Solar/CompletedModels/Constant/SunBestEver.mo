within SolarBoatAutomation.L1_Assess_L2.Components.Solar.CompletedModels.Constant;
model SunBestEver
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance_Constant(
                                          solar_irradiance_constant=0.87e3);
  annotation (Icon(graphics={Ellipse(
          extent={{-58,52},{54,-46}},
          lineColor={255,255,0},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid)}), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Simulate solar irradiance which does not vary with time and is the best ever recorded</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/SunBestEver-OPM.png\"/></p>
</html>"));
end SunBestEver;
