within SolarBoatAutomation.L1_Assess_L2.Components.Solar.CompletedModels.Constant;
model NoSun
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance_Constant(
                                          solar_irradiance_constant=0);
  annotation (Icon(graphics={Ellipse(
          extent={{-54,54},{52,-36}},
          lineColor={255,255,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Simulate solar irradiance which does not vary with time and is zero</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/NoSun-OPM.png\"/></p>
</html>", revisions="<html>
</html>"));
end NoSun;
