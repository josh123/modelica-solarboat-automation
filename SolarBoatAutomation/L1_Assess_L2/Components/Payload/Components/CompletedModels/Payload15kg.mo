within SolarBoatAutomation.L1_Assess_L2.Components.Payload.Components.CompletedModels;
model Payload15kg
  extends PartialModels.PartialNonMovingPayload(mass = payload_2015_mass);
  parameter Modelica.SIunits.Mass payload_2015_mass = 15;
equation
  mass_computed = payload_2015_mass;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={Text(
          extent={{-54,24},{48,72}},
          lineColor={0,0,255},
          textString="2015 Payload")}));
end Payload15kg;
