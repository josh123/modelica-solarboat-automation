within SolarBoatAutomation.L1_Assess_L2.Components.Payload.Components.PartialModels;
partial model PartialNonMovingPayload
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L1_Assess_L2.Components.Payload.PartialModels.PartialPayload;
  parameter Modelica.SIunits.Mass mass;
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=mass)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-14,-12})));
equation
  connect(lumpedMass.frame_a, frame_a) annotation (Line(
      points={{-4,-12},{2,-12},{2,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialNonMovingPayload;
