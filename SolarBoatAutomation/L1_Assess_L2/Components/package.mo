within SolarBoatAutomation.L1_Assess_L2;
package Components "Package of components which are only used with the Simulation harness. SolarBoats are not defined here."


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide components which are only needed for simulation harnesses.</p>
</html>"));
end Components;
