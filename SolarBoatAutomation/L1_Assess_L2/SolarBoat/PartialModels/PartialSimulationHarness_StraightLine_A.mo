within SolarBoatAutomation.L1_Assess_L2.SolarBoat.PartialModels;
partial model PartialSimulationHarness_StraightLine_A
  extends PartialModels.PartialSimulationHarness_StraightLine;
  Modelica.Mechanics.MultiBody.Joints.Prismatic x_direction(n={1,0,0},
      useAxisFlange=false)
    annotation (Placement(transformation(extent={{-78,-6},{-58,14}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic z_direction(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-48,-22})));
equation
  connect(z_direction.frame_a, x_direction.frame_b) annotation (Line(
      points={{-48,-12},{-48,4},{-58,4}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x_direction.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-78,4},{-82,4},{-82,-48},{-52,-48},{-52,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(z_direction.frame_b, solarBoat.payloadAttachment) annotation (Line(
      points={{-48,-32},{-48,-40},{-34,-40},{-34,-1.08},{8.58,-1.08}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialSimulationHarness_StraightLine_A;
