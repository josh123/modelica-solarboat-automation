within SolarBoatAutomation.L1_Assess_L2.SolarBoat;
package PartialModels "Package of simulation harnesses which can have SolarBoats and weather conditions redeclared"
annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide a common architecture for which simulations can be run.</p>
</html>"));
end PartialModels;
