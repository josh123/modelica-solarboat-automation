within SolarBoatAutomation.L1_Assess_L2.SolarBoat.PartialModels;
partial model ScreenShot_FormalStructure
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Payload.Components.CompletedModels.Payload2015 partialPayload,
      redeclare Components.Solar.CompletedModels.Constant.SunBestEver
      solarInsolation);
  Modelica.Mechanics.MultiBody.Joints.Prismatic x_direction(n={1,0,0},
      useAxisFlange=false)
    annotation (Placement(transformation(extent={{-94,-6},{-74,14}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic z_direction(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-68,-24})));
equation
  connect(z_direction.frame_a, x_direction.frame_b) annotation (Line(
      points={{-68,-14},{-68,4},{-74,4}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(x_direction.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-94,4},{-98,4},{-98,-48},{-48,-48},{-48,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(z_direction.frame_b, solarBoat.payloadAttachment) annotation (Line(
      points={{-68,-34},{-68,-42},{-46,-42},{-46,0.36},{8.58,0.36}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end ScreenShot_FormalStructure;
