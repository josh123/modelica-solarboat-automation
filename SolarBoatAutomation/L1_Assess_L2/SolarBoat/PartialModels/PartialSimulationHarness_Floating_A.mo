within SolarBoatAutomation.L1_Assess_L2.SolarBoat.PartialModels;
partial model PartialSimulationHarness_Floating_A
  "Simulates the boat traveling in a straight line"
  extends PartialModels.PartialSimulationHarness_StraightLine;
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-60,-12})));
equation
  connect(prismatic1.frame_b, solarBoat.payloadAttachment) annotation (Line(
      points={{-60,-22},{-60,-28},{-48,-28},{-48,0.36},{8.58,0.36}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-60,-2},{-60,0},{-74,0},{-74,-40},{-46,-40},{-46,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));

  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialSimulationHarness_Floating_A;
