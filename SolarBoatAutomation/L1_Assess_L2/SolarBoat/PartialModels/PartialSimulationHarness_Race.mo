within SolarBoatAutomation.L1_Assess_L2.SolarBoat.PartialModels;
partial model PartialSimulationHarness_Race
  "Simulates the SolarBoat traveling in a straight line starting at rest"
  Real cost_money(unit="yen") "Predicted cost of the boat";
  Modelica.SIunits.Velocity x_velocity
    "x direction velocity in world frame of reference";
  Modelica.SIunits.Time time_complete_race_at_this_speed_seconds
    "Time in seconds to complete the race the current speed of the boat";
  parameter Modelica.SIunits.Length race_track_length = 20000
    "The length of the race track in meters";
  Modelica.SIunits.Conversions.NonSIunits.Time_hour
    predicted_time_to_complete_race "Time in hours to complete the race";
  Real probability_of_winning "The computed probability of winning the race";

  replaceable Components.Solar.CompletedModels.Constant.SunBestEver
    solarIrradiance constrainedby
    Components.Solar.PartialModels.PartialSolarIrradiance
    annotation (Placement(transformation(extent={{-10,64},{26,94}})));
  replaceable L2_SystemsOfInterest.SolarBoat.PartialModels.C.PartialSolarBoat
    solarBoat annotation (Placement(transformation(extent={{8,-18},{66,18}})));
  inner Modelica.Mechanics.MultiBody.World world(n={0,0,1})
    annotation (Placement(transformation(extent={{-82,-84},{-62,-64}})));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-34,-84},{0,-64}})));
  replaceable Components.Payload.Components.CompletedModels.Payload15kg
                                                              partialPayload
    constrainedby Components.Payload.PartialModels.PartialPayload
    annotation (Placement(transformation(extent={{-46,38},{-6,58}})));
  Modelica.Blocks.Sources.Constant ambientTemperature(k=299.75)
    annotation (Placement(transformation(extent={{36,70},{58,92}})));
  Modelica.Blocks.Sources.Constant waterVelocityX(k=0)
    annotation (Placement(transformation(extent={{-22,-30},{0,-8}})));
equation
  cost_money = solarBoat.cost_money;
  x_velocity = solarBoat.x_velocity;
  time_complete_race_at_this_speed_seconds = race_track_length/x_velocity;
  predicted_time_to_complete_race = time_complete_race_at_this_speed_seconds/(60*60);
  if predicted_time_to_complete_race <= 1 then
    probability_of_winning = 1;
  elseif predicted_time_to_complete_race >= 2.5 then
    probability_of_winning = 0;
  else
    probability_of_winning = (-(2/3)*predicted_time_to_complete_race)+(5/3);
  end if;

  connect(world.frame_b, visualEnvironment.frame_a) annotation (Line(
      points={{-62,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partialPayload.frame_a, solarBoat.payloadAttachment) annotation (Line(
      points={{-26,38.2},{-26,-1.08},{8.58,-1.08}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(ambientTemperature.y, solarBoat.temperature) annotation (Line(
      points={{59.1,81},{62,81},{62,56},{47.44,56},{47.44,15.84}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarIrradiance.solarInsolation, solarBoat.solarInsolation)
    annotation (Line(
      points={{8,62.8},{8,56},{21.34,56},{21.34,15.84}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarBoat.waterVelocityX, waterVelocityX.y) annotation (Line(
      points={{10.9,-11.16},{2,-11.16},{2,-19},{1.1,-19}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (experiment(StopTime=100),
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics={Text(
          extent={{-60,-80},{64,-38}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>A common architecture to simulate boats traveling in a straight line.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSimulationHarness_StraightLine-OPM.png\"/></p>
</html>"));
end PartialSimulationHarness_Race;
