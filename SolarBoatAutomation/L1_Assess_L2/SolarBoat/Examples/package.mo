within SolarBoatAutomation.L1_Assess_L2.SolarBoat;
package Examples "Package of SolarBoats in simulation harnesses which run for demonstrations"
  extends Modelica.Icons.ExamplesPackage;


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide multiple examples of SolarBoats being simulated under various conditions.</p>
</html>"));
end Examples;
