within SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C;
model ConstantNoSunDualHull
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.NoSun solarInsolation,
      redeclare L2_SystemsOfInterest.SolarBoat.CompletedModels.C.DualHullSB
      solarBoat);
  annotation (experiment(StopTime=100));
end ConstantNoSunDualHull;
