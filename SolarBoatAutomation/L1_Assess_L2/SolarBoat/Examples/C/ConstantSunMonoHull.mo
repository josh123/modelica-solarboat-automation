within SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C;
model ConstantSunMonoHull
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(
    redeclare Components.Solar.CompletedModels.Constant.SunAverage
      solarInsolation,
    redeclare L2_SystemsOfInterest.SolarBoat.CompletedModels.C.MonoHullSB
      solarBoat,
    redeclare Components.Payload.Components.CompletedModels.Payload2015
      partialPayload);
  annotation (experiment(StopTime=100));
end ConstantSunMonoHull;
