within SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A;
model Image_For_Presentations
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(
    redeclare Components.Payload.Components.CompletedModels.Payload15kg
      partialPayload,
    waterVelocityX(k=0),
    redeclare Components.Solar.CompletedModels.Constant.SunWorstEver
      solarIrradiance,
    redeclare Components.Solar.CompletedModels.Constant.SunWorstEver
      solarInsolation);
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-56,-50},{-36,-30}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n={1,0,0},
      useAxisFlange=false)
    annotation (Placement(transformation(extent={{-84,-12},{-64,8}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{-20,-58},{0,-38}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-62,-18})));
equation
  connect(cutForceAndTorque.frame_b,absoluteVelocity. frame_a) annotation (Line(
      points={{-36,-40},{-26,-40},{-26,-48},{-20,-48}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_a,prismatic. frame_b) annotation (Line(
      points={{-62,-8},{-62,-2},{-64,-2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-62,-28},{-62,-40},{-56,-40}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarBoat.payloadAttachment, absoluteVelocity.frame_a) annotation (
      Line(
      points={{8.58,0.36},{-26,0.36},{-26,-48},{-20,-48}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-84,-2},{-86,-2},{-86,-56},{-42,-56},{-42,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=100), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    __Dymola_Commands(executeCall=Design.Experimentation.MonteCarloAnalysis(
          Design.Internal.Records.ModelUncertainSetup(
            Model=
              "SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A.ConstantSun_MonoDisplacementHullSB_A",
            uncertainParameters={Design.Internal.Records.UncertainParameter(
              name="solarInsolation.solar_insolation_constant",
              minimum=260,
              maximum=870,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                610, 100)),Design.Internal.Records.UncertainParameter(
              name="T.k",
              minimum=294.25,
              maximum=306.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                299.75, 0.9)),Design.Internal.Records.UncertainParameter(
              name="waterVelocityX.k",
              minimum=-0.35,
              maximum=0.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                0, 0.5))},
            observedVariables={Design.Internal.Records.ObservedVariable(name=
              "x_velocity"),Design.Internal.Records.ObservedVariable(name=
              "predicted_time_to_complete_race"),
              Design.Internal.Records.ObservedVariable(name=
              "probability_of_winning"),
              Design.Internal.Records.ObservedVariable(name=
              "solarInsolation.solar_insolation_constant"),
              Design.Internal.Records.ObservedVariable(name="T.k"),
              Design.Internal.Records.ObservedVariable(name="waterVelocityX.k")},
            integrator=Design.Internal.Records.Integrator(startTime=0, stopTime=
               100)),
          totalNumberOfSamples=100,
          showAccumulated=false) "MonteCarlo-SolarBoat-NotAccumulated",
        executeCall=Design.Experimentation.MonteCarloAnalysis(
          Design.Internal.Records.ModelUncertainSetup(
            Model=
              "SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A.ConstantSun_MonoDisplacementHullSB_A",
            uncertainParameters={Design.Internal.Records.UncertainParameter(
              name="solarInsolation.solar_insolation_constant",
              minimum=260,
              maximum=870,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                610, 100)),Design.Internal.Records.UncertainParameter(
              name="T.k",
              minimum=294.25,
              maximum=306.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                299.75, 0.9)),Design.Internal.Records.UncertainParameter(
              name="waterVelocityX.k",
              minimum=-0.35,
              maximum=0.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                0, 0.5))},
            observedVariables={Design.Internal.Records.ObservedVariable(name=
              "x_velocity"),Design.Internal.Records.ObservedVariable(name=
              "predicted_time_to_complete_race"),
              Design.Internal.Records.ObservedVariable(name=
              "probability_of_winning"),
              Design.Internal.Records.ObservedVariable(name=
              "solarInsolation.solar_insolation_constant"),
              Design.Internal.Records.ObservedVariable(name="T.k"),
              Design.Internal.Records.ObservedVariable(name="waterVelocityX.k")},
            integrator=Design.Internal.Records.Integrator(startTime=0, stopTime=
               100)),
          totalNumberOfSamples=100,
          showAccumulated=true) "MonteCarlo-Solarboat-Accmulated"));
end Image_For_Presentations;
