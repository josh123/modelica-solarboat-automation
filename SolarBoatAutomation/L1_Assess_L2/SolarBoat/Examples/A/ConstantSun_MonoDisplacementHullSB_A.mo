within SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A;
model ConstantSun_MonoDisplacementHullSB_A
  extends Modelica.Icons.Example;
  extends PartialModels.PartialSimulationHarness_StraightLine(
    redeclare Components.Solar.CompletedModels.Constant.SunAverage
      solarIrradiance,
    redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.MonoDisplacementHullSB_A
      solarBoat,
    redeclare Components.Payload.Components.CompletedModels.Payload15kg
      partialPayload,
    waterVelocityX(k=0));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-62,-48},{-42,-28}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n={1,0,0},
      useAxisFlange=false)
    annotation (Placement(transformation(extent={{-94,-16},{-74,4}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{-24,-60},{-4,-40}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n={0,0,1},
      useAxisFlange=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-68,-16})));
equation
  connect(cutForceAndTorque.frame_b,absoluteVelocity. frame_a) annotation (Line(
      points={{-42,-38},{-38,-38},{-38,-50},{-24,-50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_a,prismatic. frame_b) annotation (Line(
      points={{-68,-6},{-74,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic1.frame_b,cutForceAndTorque. frame_a) annotation (Line(
      points={{-68,-26},{-68,-38},{-62,-38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarBoat.payloadAttachment, absoluteVelocity.frame_a) annotation (
      Line(
      points={{8.58,0.36},{-38,0.36},{-38,-50},{-24,-50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic.frame_a, visualEnvironment.frame_a) annotation (Line(
      points={{-94,-6},{-96,-6},{-96,-54},{-48,-54},{-48,-74},{-34,-74}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=100), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    __Dymola_Commands(executeCall=Design.Experimentation.MonteCarloAnalysis(
          Design.Internal.Records.ModelUncertainSetup(
            Model=
              "SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A.ConstantSun_MonoDisplacementHullSB_A",
            uncertainParameters={Design.Internal.Records.UncertainParameter(
              name="solarIrradiance.solar_irradiance_constant",
              minimum=260,
              maximum=870,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                610, 100)),Design.Internal.Records.UncertainParameter(
              name="T.k",
              minimum=294.25,
              maximum=306.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                299.75, 0.9)),Design.Internal.Records.UncertainParameter(
              name="waterVelocityX.k",
              minimum=-0.35,
              maximum=0.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                0, 0.5))},
            observedVariables={Design.Internal.Records.ObservedVariable(name=
              "x_velocity"),Design.Internal.Records.ObservedVariable(name=
              "predicted_time_to_complete_race"),
              Design.Internal.Records.ObservedVariable(name=
              "probability_of_winning"),
              Design.Internal.Records.ObservedVariable(name=
              "solarIrradiance.solar_irradiance_constant"),
              Design.Internal.Records.ObservedVariable(name="T.k"),
              Design.Internal.Records.ObservedVariable(name="waterVelocityX.k")},
            integrator=Design.Internal.Records.Integrator(startTime=0, stopTime=
               100)),
          totalNumberOfSamples=100,
          showAccumulated=false) "MonteCarlo-SolarBoat-NotAccumulated",
        executeCall=Design.Experimentation.MonteCarloAnalysis(
          Design.Internal.Records.ModelUncertainSetup(
            Model=
              "SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A.ConstantSun_MonoDisplacementHullSB_A",
            uncertainParameters={Design.Internal.Records.UncertainParameter(
              name="solarIrradiance.solar_irradiance_constant",
              minimum=260,
              maximum=870,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                610, 100)),Design.Internal.Records.UncertainParameter(
              name="T.k",
              minimum=294.25,
              maximum=306.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                299.75, 0.9)),Design.Internal.Records.UncertainParameter(
              name="waterVelocityX.k",
              minimum=-0.35,
              maximum=0.35,
              Distributions=
                Design.Experimentation.RandomNumber.Functions.setFunctionData.randomNormal(
                0, 0.5))},
            observedVariables={Design.Internal.Records.ObservedVariable(name=
              "x_velocity"),Design.Internal.Records.ObservedVariable(name=
              "predicted_time_to_complete_race"),
              Design.Internal.Records.ObservedVariable(name=
              "probability_of_winning"),
              Design.Internal.Records.ObservedVariable(name=
              "solarIrradiance.solar_irradiance_constant"),
              Design.Internal.Records.ObservedVariable(name="T.k"),
              Design.Internal.Records.ObservedVariable(name="waterVelocityX.k")},
            integrator=Design.Internal.Records.Integrator(startTime=0, stopTime=
               100)),
          totalNumberOfSamples=100,
          showAccumulated=true) "MonteCarlo-Solarboat-Accmulated"));
end ConstantSun_MonoDisplacementHullSB_A;
