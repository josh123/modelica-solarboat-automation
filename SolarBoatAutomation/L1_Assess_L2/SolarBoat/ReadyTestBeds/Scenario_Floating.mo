within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds;
model Scenario_Floating "Simulates the boat floating"
  extends PartialModels.PartialSimulationHarness_StraightLine(
    redeclare Components.Solar.CompletedModels.Constant.NoSun solarInsolation,
    redeclare Components.Payload.Components.CompletedModels.Payload2015
      partialPayload,
    redeclare L2_SystemsOfInterest.SolarBoat.CompletedModels.C.MonoHullSB
      solarBoat);
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Check that the boat does not move when there is no input power or disturbance.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantNoSun-OPM.png\"/></p>
</html>"));
end Scenario_Floating;
