within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;


model Auto_Cost_A_HM_220mm
  extends PartialModels.PartialSimulationHarness_StraightLine_A(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                                                     solarInsolation, redeclare
      Components.Payload.Components.CompletedModels.Payload2015                                                                                                     partialPayload, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.HM_220mm                                                                                                     solarBoat);
end Auto_Cost_A_HM_220mm;
