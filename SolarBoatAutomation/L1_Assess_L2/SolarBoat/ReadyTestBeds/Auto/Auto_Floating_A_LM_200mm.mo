within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Auto_Floating_A_LM_200mm
  "Simulates the boat traveling in a straight line"
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                                                   solarInsolation, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.LM_200mm                                                                                                     solarBoat, redeclare
      Components.Payload.Components.CompletedModels.Payload2015                                                                                                     partialPayload);
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n = {0, 0, 1}, useAxisFlange = false) annotation(Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = -90, origin = {-74, -10})));
equation
  connect(prismatic1.frame_b, solarBoat.payloadAttachment) annotation(Line(points = {{-74, -20}, {-74, -28}, {-48, -28}, {-48, 2}, {-27.42, 2.36}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  connect(prismatic1.frame_a, visualEnvironment.frame_a) annotation(Line(points = {{-74, 0}, {-74, 6}, {-92, 6}, {-92, -34}, {-48, -34}, {-48, -74}, {-34, -74}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  annotation(Documentation(info = "<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics));
end Auto_Floating_A_LM_200mm;

