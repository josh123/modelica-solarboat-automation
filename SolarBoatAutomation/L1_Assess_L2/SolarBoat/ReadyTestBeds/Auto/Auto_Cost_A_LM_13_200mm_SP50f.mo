within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;


model Auto_Cost_A_LM_13_200mm_SP50f
  extends PartialModels.PartialSimulationHarness_StraightLine_A(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage                                                                     solarInsolation, redeclare
      Components.Payload.Components.CompletedModels.Payload15kg                                                                                                     partialPayload, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.LM_13_200mm_SP50f                                                                                                     solarBoat);
end Auto_Cost_A_LM_13_200mm_SP50f;
