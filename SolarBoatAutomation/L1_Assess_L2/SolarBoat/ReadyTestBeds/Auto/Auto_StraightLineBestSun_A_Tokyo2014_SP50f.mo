within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Auto_StraightLineBestSun_A_Tokyo2014_SP50f
  "Simulates the boat traveling in a straight line"
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                                                   solarInsolation, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.Tokyo2014_SP50f                                                                                                     solarBoat, redeclare
      Components.Payload.Components.CompletedModels.Payload15kg                                                                                                     partialPayload);
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque annotation(Placement(transformation(extent = {{-70, -38}, {-50, -18}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n = {1, 0, 0}, useAxisFlange = false) annotation(Placement(transformation(extent = {{-118, -6}, {-98, 14}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity annotation(Placement(transformation(extent = {{-32, -38}, {-12, -18}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic1(n = {0, 0, 1}, useAxisFlange = false) annotation(Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = -90, origin = {-86, -8})));
equation
  connect(cutForceAndTorque.frame_b, absoluteVelocity.frame_a) annotation(Line(points = {{-50, -28}, {-32, -28}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  connect(prismatic1.frame_a, prismatic.frame_b) annotation(Line(points = {{-86, 2}, {-86, 4}, {-98, 4}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  connect(prismatic1.frame_b, cutForceAndTorque.frame_a) annotation(Line(points = {{-86, -18}, {-86, -28}, {-70, -28}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  connect(solarBoat.payloadAttachment, absoluteVelocity.frame_a) annotation(Line(points = {{-27.42, 2.36}, {-40, 2.36}, {-40, -28}, {-32, -28}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  connect(prismatic.frame_a, visualEnvironment.frame_a) annotation(Line(points = {{-118, 4}, {-122, 4}, {-122, -52}, {-48, -52}, {-48, -74}, {-34, -74}}, color = {95, 95, 95}, thickness = 0.5, smooth = Smooth.None));
  annotation(Documentation(info = "<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics));
end Auto_StraightLineBestSun_A_Tokyo2014_SP50f;

