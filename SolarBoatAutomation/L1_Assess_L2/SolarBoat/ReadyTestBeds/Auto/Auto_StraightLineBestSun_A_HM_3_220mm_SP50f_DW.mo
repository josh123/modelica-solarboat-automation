within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Auto_StraightLineBestSun_A_HM_3_220mm_SP50f_DW
  extends PartialModels.PartialSimulationHarness_StraightLine_A(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                                                     solarInsolation, redeclare
      Components.Payload.Components.CompletedModels.Payload15kg                                                                                                     partialPayload, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.HM_3_220mm_SP50f_DW                                                                                                     solarBoat);
end Auto_StraightLineBestSun_A_HM_3_220mm_SP50f_DW;

