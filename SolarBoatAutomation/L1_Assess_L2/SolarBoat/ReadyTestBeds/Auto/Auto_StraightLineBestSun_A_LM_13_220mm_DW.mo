within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Auto_StraightLineBestSun_A_LM_13_220mm_DW
  extends PartialModels.PartialSimulationHarness_StraightLine_A(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                                                     solarInsolation, redeclare
      Components.Payload.Components.CompletedModels.Payload15kg                                                                                                     partialPayload, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.LM_13_220mm_DW                                                                                                     solarBoat);
end Auto_StraightLineBestSun_A_LM_13_220mm_DW;

