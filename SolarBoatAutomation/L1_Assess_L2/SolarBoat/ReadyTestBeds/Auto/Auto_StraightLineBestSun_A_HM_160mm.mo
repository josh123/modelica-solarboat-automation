within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Auto_StraightLineBestSun_A_HM_160mm
  extends PartialModels.PartialSimulationHarness_StraightLine_A(redeclare
      Components.Solar.CompletedModels.Constant.SunBestEver                                                                     solarInsolation, redeclare
      Components.Payload.Components.CompletedModels.Payload2015                                                                                                     partialPayload, redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.HM_160mm                                                                                                     solarBoat);
end Auto_StraightLineBestSun_A_HM_160mm;

