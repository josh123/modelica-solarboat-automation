within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Scenario_Cost_A
  extends PartialModels.PartialSimulationHarness_StraightLine_A(
    redeclare Components.Solar.CompletedModels.Constant.SunAverage
      solarInsolation,
    redeclare Components.Payload.Components.CompletedModels.Payload2015
      partialPayload,
    redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.MonoDisplacementHullSB_A
      solarBoat);
end Scenario_Cost_A;
