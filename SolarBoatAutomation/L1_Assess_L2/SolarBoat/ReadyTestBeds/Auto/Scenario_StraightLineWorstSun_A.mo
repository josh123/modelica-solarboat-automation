within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds.Auto;
model Scenario_StraightLineWorstSun_A
  extends PartialModels.PartialSimulationHarness_StraightLine_A(
    redeclare Components.Solar.CompletedModels.Constant.SunWorstEver
      solarInsolation,
    redeclare Components.Payload.Components.CompletedModels.Payload2015
      partialPayload,
    redeclare
      L2_SystemsOfInterest.SolarBoat.CompletedModels.A.MonoDisplacementHullSB_A
      solarBoat);
end Scenario_StraightLineWorstSun_A;
