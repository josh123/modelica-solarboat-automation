within SolarBoatAutomation.L1_Assess_L2.SolarBoat.ReadyTestBeds;
model Scenario_StraightLine_ConstantMostLikelySun
  "Simulates the boat with constant sun of the best ever insolation"
  extends PartialModels.PartialSimulationHarness_StraightLine(redeclare
      Components.Solar.CompletedModels.Constant.SunAverage solarInsolation,
      redeclare Components.Payload.Components.CompletedModels.Payload2015
      partialPayload);
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Determine the maximum speed and cruising height</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/Scenario_StraightLine_ConstantBestEverSun-OPM.png\"/></p>
</html>"));
end Scenario_StraightLine_ConstantMostLikelySun;
