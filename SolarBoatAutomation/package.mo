within ;
package SolarBoatAutomation "Package to describe, build and simulate solar powered boats"


annotation (uses(Modelica(version="3.2.1")), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>This Modelica package attempts to provide models such that various different SolarBoat designs can be created.</p>
</html>"));
end SolarBoatAutomation;
