within SolarBoatAutomation.L3_Assess_L4.OverheadOtherStructural.Examples;
model MassCheck2014
  extends Modelica.Icons.Example;
  extends Partial.MassCheck(redeclare
      L4_SubsystemComponents.OverheadOtherStructural.Complete.OtherStructuralComponents2014A
      otherStructural);
end MassCheck2014;
