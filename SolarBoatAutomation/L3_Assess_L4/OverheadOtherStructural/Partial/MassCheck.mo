within SolarBoatAutomation.L3_Assess_L4.OverheadOtherStructural.Partial;
partial model MassCheck
  replaceable
    L4_SubsystemComponents.OverheadOtherStructural.Partial.OtherStructural
    otherStructural
    annotation (Placement(transformation(extent={{-18,20},{26,38}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-54,-66},{-34,-46}},
          rotation=0)));
end MassCheck;
