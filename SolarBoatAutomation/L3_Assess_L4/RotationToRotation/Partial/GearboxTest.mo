within SolarBoatAutomation.L3_Assess_L4.RotationToRotation.Partial;
partial model GearboxTest
  replaceable L4_SubsystemComponents.RotationToRotation.Partial.IdealGearbox
    idealGearbox
    annotation (Placement(transformation(extent={{0,-14},{36,14}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-64,-76},{-44,-56}},
          rotation=0)));
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(w_fixed=20)
    annotation (Placement(transformation(extent={{-62,-10},{-42,10}})));
  Helpers.Sensors.RotationSensor_MultiUnits rotationSensor_MultiUnits
    annotation (Placement(transformation(extent={{60,-8},{80,12}})));
equation
  connect(idealGearbox.flange_a, constantSpeed.flange) annotation (Line(
      points={{0,0.28},{-22,0.28},{-22,0},{-42,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(idealGearbox.flange_b, rotationSensor_MultiUnits.flange_a)
    annotation (Line(
      points={{36,0.28},{48,0.28},{48,2},{60,2}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end GearboxTest;
