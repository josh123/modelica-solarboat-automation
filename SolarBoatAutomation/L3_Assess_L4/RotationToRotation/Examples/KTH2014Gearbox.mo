within SolarBoatAutomation.L3_Assess_L4.RotationToRotation.Examples;
model KTH2014Gearbox
  extends Modelica.Icons.Example;
  extends Partial.GearboxTest(redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.KTH2014_6_1_Ideal
      idealGearbox);
end KTH2014Gearbox;
