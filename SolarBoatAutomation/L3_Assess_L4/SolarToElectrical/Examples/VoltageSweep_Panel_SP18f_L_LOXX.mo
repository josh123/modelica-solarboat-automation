within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples;
model VoltageSweep_Panel_SP18f_L_LOXX
  extends Modelica.Icons.Example;
  extends PartialModels.VoltageSweep_Panel(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Components.Complete.SP18f_L_LOXX
      parameterizedPanel);
end VoltageSweep_Panel_SP18f_L_LOXX;
