within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples;
model VoltageSweep_Array_1Px1S_Array_FT136SE
  extends Modelica.Icons.Example;
  extends PartialModels.VoltageSweep_Array(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.FT136SE_1Px1S
      solarToUnstableVoltage);
end VoltageSweep_Array_1Px1S_Array_FT136SE;
