within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples;
model VoltageSweep_Panel_FT136SE
  extends Modelica.Icons.Example;
  extends PartialModels.VoltageSweep_Panel(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Components.Complete.FT136SE
      parameterizedPanel);
end VoltageSweep_Panel_FT136SE;
