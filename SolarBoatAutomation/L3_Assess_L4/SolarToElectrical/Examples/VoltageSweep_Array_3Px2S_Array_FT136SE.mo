within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples;
model VoltageSweep_Array_3Px2S_Array_FT136SE
  extends Modelica.Icons.Example;
  extends PartialModels.VoltageSweep_Array(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.FT136SE_3Px2S
      solarToUnstableVoltage);
end VoltageSweep_Array_3Px2S_Array_FT136SE;
