within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples;
model VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX
  extends Modelica.Icons.Example;
  extends PartialModels.VoltageSweep_Array(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.SP18f_L_LOXX_1Px1S
      solarToUnstableVoltage);
end VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX;
