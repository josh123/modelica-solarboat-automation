within SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.PartialModels;
partial model VoltageSweep_Panel
  Modelica.SIunits.Power power = abs(currentSensor.i) * abs(voltageSensor.v);
  Modelica.Blocks.Sources.Constant T(k=298)
    annotation (Placement(transformation(extent={{-62,74},{-48,88}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-44},{-38,-24}})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(duration=1, V=-
        parameterizedPanel.partial_SolarPanelSpecSheet.V_oc)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={30,0})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-32,10},{-12,30}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,-2})));
  replaceable L1_Assess_L2.Components.Solar.CompletedModels.Constant.STC
    partialSolarInsolation constrainedby
    L1_Assess_L2.Components.Solar.PartialModels.PartialSolarIrradiance
    annotation (Placement(transformation(extent={{-92,70},{-72,90}})));
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-90,-78},{-70,-58}},
          rotation=0)));
  Helpers.Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-54,-78},{-20,-58}})));
  replaceable
    L4_SubsystemComponents.SolarToElectrical.A.Components.Partial.ParameterizedPanel
    parameterizedPanel
    annotation (Placement(transformation(extent={{-64,10},{-44,30}})));
equation
  connect(rampVoltage.n, ground.p) annotation (Line(
      points={{30,-10},{30,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, rampVoltage.p) annotation (Line(
      points={{-12,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.p, rampVoltage.p) annotation (Line(
      points={{6,8},{6,20},{30,20},{30,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.n, ground.p) annotation (Line(
      points={{6,-12},{6,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(world.frame_b,visualEnvironment. frame_a) annotation (Line(
      points={{-70,-68},{-54,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(currentSensor.p, parameterizedPanel.n) annotation (Line(
      points={{-32,20},{-44,20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(T.y, parameterizedPanel.temp_kelvin) annotation (Line(
      points={{-47.3,81},{-40,81},{-40,52},{-48.1,52},{-48.1,30.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(partialSolarInsolation.solarInsolation, parameterizedPanel.solarInsolation)
    annotation (Line(
      points={{-82,69.2},{-82,54},{-58.9,54},{-58.9,30.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel.frame_a, visualEnvironment.frame_a) annotation (
      Line(
      points={{-54,10.2},{-56,10.2},{-56,-12},{-64,-12},{-64,-68},{-54,-68}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel.p, ground.p) annotation (Line(
      points={{-64,20},{-78,20},{-78,-24},{-48,-24}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end VoltageSweep_Panel;
