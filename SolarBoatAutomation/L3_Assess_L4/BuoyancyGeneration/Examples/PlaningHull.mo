within SolarBoatAutomation.L3_Assess_L4.BuoyancyGeneration.Examples;
model PlaningHull
  extends Modelica.Icons.Example;
  extends Partial.FixedSpeedTowingTank(redeclare
      L4_SubsystemComponents.BuoyancyGeneration.Complete.PlaningHull2013
      rigidHull);
end PlaningHull;
