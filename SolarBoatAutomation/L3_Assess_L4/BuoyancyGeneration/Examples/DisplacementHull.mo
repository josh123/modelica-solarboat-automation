within SolarBoatAutomation.L3_Assess_L4.BuoyancyGeneration.Examples;
model DisplacementHull
  extends Modelica.Icons.Example;
  extends Partial.FixedSpeedTowingTank(redeclare
      L4_SubsystemComponents.BuoyancyGeneration.Complete.DisplacementHull2014
      rigidHull);
end DisplacementHull;
