within SolarBoatAutomation.L3_Assess_L4.BuoyancyGeneration.Partial;
partial model FixedSpeedTowingTank
  extends Helpers.TestBeds.Partial.FixedSpeedTowingTank;
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    rigidHull annotation (Placement(transformation(extent={{72,66},{92,86}})));
equation
  connect(rigidHull.frame_a, TestRig.frame_b) annotation (Line(
      points={{82,66.2},{82,60},{28,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FixedSpeedTowingTank;
