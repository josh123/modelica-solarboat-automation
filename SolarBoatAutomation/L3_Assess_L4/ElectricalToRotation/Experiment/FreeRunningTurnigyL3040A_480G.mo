within SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Experiment;
record FreeRunningTurnigyL3040A_480G
  //Corresponds to the 2015-06-11_1543_Battery free running test

  parameter Modelica.SIunits.Voltage voltage = 20.5;

end FreeRunningTurnigyL3040A_480G;
