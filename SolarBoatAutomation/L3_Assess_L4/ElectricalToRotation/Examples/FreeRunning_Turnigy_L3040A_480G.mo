within SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples;
model FreeRunning_Turnigy_L3040A_480G
  extends Modelica.Icons.Example;
  extends PartialModels.FreeRunning(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G
      partial_DCMotor, redeclare
      L4_SubsystemComponents.ElectricalToRotation.SpecSheets.Turnigy_L3040A_480G
      partial_DCMotorDesign,
    constantVoltage(V=freeRunningTurnigyL3040A_480G.voltage));
  Experiment.FreeRunningTurnigyL3040A_480G freeRunningTurnigyL3040A_480G
    annotation (Placement(transformation(extent={{6,64},{26,84}})));
end FreeRunning_Turnigy_L3040A_480G;
