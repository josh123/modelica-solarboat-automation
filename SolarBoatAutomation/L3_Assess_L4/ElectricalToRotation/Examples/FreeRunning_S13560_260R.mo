within SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples;
model FreeRunning_S13560_260R
  extends Modelica.Icons.Example;
  extends PartialModels.FreeRunning(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.S13560_260R
      partial_DCMotor, redeclare
      L4_SubsystemComponents.ElectricalToRotation.SpecSheets.S13560_260R
      partial_DCMotorDesign,
    constantVoltage(V=27));
end FreeRunning_S13560_260R;
