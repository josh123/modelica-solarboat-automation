within SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples;
model FreeRunning_MaxonEC_max_60W_24V_272763
  extends PartialModels.FreeRunning(redeclare
      L4_SubsystemComponents.ElectricalToRotation.SpecSheets.MaxonEC_max_60W_24V_272763
      partial_DCMotorDesign, redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.MaxonEC_max_60W_24V_272763
      partial_DCMotor);
  extends Modelica.Icons.Example;
end FreeRunning_MaxonEC_max_60W_24V_272763;
