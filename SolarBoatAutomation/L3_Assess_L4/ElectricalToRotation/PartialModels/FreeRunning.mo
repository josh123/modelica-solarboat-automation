within SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.PartialModels;
model FreeRunning
  Modelica.SIunits.Power power_consumption;
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=
        partial_DCMotorDesign.VaNominal)
    annotation (Placement(transformation(extent={{-82,58},{-62,78}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-58,-2},{-38,18}})));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia1(J=0.00000001)
    annotation (Placement(transformation(extent={{40,-12},{60,8}},
          rotation=0)));
  SolarBoatAutomation.Helpers.Sensors.RotationSensor_MultiUnits
    multiSensor_MultiUnits
    annotation (Placement(transformation(extent={{8,-12},{28,8}})));
  replaceable
    L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor
    partial_DCMotor
    annotation (Placement(transformation(extent={{-30,-10},{-10,10}})));
  replaceable
    L4_SubsystemComponents.ElectricalToRotation.SpecSheets.Partial_DCMotorDesign
    partial_DCMotorDesign
    annotation (Placement(transformation(extent={{48,52},{68,72}})));
  inner Modelica.Mechanics.MultiBody.World world(driveTrainMechanics3D=true, g=
        0) annotation (Placement(transformation(extent={{-90,-70},{-70,-50}},
          rotation=0)));
equation
  power_consumption = constantVoltage.V * constantVoltage.n.i;
  connect(ground.p, constantVoltage.p) annotation (Line(
      points={{-48,18},{-74,18},{-74,24},{-96,24},{-96,68},{-82,68}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(loadInertia1.flange_a, multiSensor_MultiUnits.flange_b) annotation (
      Line(
      points={{40,-2},{28,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(partial_DCMotor.flange, multiSensor_MultiUnits.flange_a) annotation (
      Line(
      points={{-10.2,0},{2,0},{2,-2},{8,-2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(constantVoltage.n, partial_DCMotor.pin_n) annotation (Line(
      points={{-62,68},{-26.8,68},{-26.8,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, partial_DCMotor.pin_p) annotation (Line(
      points={{-48,18},{-14.8,18},{-14.8,10}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FreeRunning;
