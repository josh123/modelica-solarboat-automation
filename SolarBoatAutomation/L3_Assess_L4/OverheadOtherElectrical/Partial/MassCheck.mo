within SolarBoatAutomation.L3_Assess_L4.OverheadOtherElectrical.Partial;
partial model MassCheck

  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-44,-56},{-24,-36}},
          rotation=0)));
  replaceable
    L4_SubsystemComponents.OverheadOtherElectrical.Partial.OtherElectrical
    otherElectrical
    annotation (Placement(transformation(extent={{-24,22},{14,44}})));
end MassCheck;
