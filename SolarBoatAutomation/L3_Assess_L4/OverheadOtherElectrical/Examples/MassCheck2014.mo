within SolarBoatAutomation.L3_Assess_L4.OverheadOtherElectrical.Examples;
model MassCheck2014
  extends Modelica.Icons.Example;
  extends Partial.MassCheck(redeclare
      L4_SubsystemComponents.OverheadOtherElectrical.Complete.OtherElectricalComponents2014A
      otherElectrical);
end MassCheck2014;
