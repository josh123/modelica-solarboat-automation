within SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples;
model TestBedPredicted200mm343mm
  extends Modelica.Icons.Example;
  extends PartialModels.TestBedMultiBodyPropeller(        redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted200mm343mm
      partial_PropellerMultiBody);
end TestBedPredicted200mm343mm;
