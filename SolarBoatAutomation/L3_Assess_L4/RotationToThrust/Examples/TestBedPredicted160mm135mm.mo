within SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples;
model TestBedPredicted160mm135mm
  extends Modelica.Icons.Example;
  extends PartialModels.TestBedMultiBodyPropeller(        redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted160mm135mm
      partial_PropellerMultiBody);
end TestBedPredicted160mm135mm;
