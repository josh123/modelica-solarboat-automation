within SolarBoatAutomation.L3_Assess_L4.RotationToThrust.PartialModels;
model TestBedMultiBodyPropeller
  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    g=0,
    n={0,0,1}) annotation (Placement(transformation(extent={{-74,-4},{-54,16}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{-10,-4},{10,16}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-38,-4},{-18,16}})));
  replaceable
    L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody
    partial_PropellerMultiBody
    annotation (Placement(transformation(extent={{68,60},{88,80}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=1,
    offset=0,
    height=0.5)
    annotation (Placement(transformation(extent={{-104,60},{-84,80}})));
  Modelica.Mechanics.Rotational.Sources.Torque torque
    annotation (Placement(transformation(extent={{-54,60},{-34,80}})));
  Modelica.Mechanics.Rotational.Components.IdealGear idealGear(ratio=10)
    annotation (Placement(transformation(extent={{-14,60},{6,80}})));
equation
  connect(TestRig.frame_a, cutForceAndTorque.frame_b) annotation (Line(
      points={{-10,6},{-18,6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b, cutForceAndTorque.frame_a) annotation (Line(
      points={{-54,6},{-38,6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(ramp.y, torque.tau) annotation (Line(
      points={{-83,70},{-56,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(torque.flange, idealGear.flange_a) annotation (Line(
      points={{-34,70},{-14,70}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(idealGear.flange_b, partial_PropellerMultiBody.flange) annotation (
      Line(
      points={{6,70},{68,70}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(TestRig.frame_b, partial_PropellerMultiBody.frame_a) annotation (Line(
      points={{10,6},{78,6},{78,60.2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (experiment(StopTime=5), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end TestBedMultiBodyPropeller;
