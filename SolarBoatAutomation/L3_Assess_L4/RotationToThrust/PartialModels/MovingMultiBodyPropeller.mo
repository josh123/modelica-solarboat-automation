within SolarBoatAutomation.L3_Assess_L4.RotationToThrust.PartialModels;
partial model MovingMultiBodyPropeller
  inner Modelica.Mechanics.MultiBody.World world(driveTrainMechanics3D=true, g=
        0) annotation (Placement(transformation(extent={{-72,-14},{-52,6}},
          rotation=0)));
  Modelica.Blocks.Sources.RealExpression x(y=0.5)
    annotation (Placement(transformation(extent={{-96,70},{-76,90}})));
  replaceable
    L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody
    partial_PropellerMultiBody
    annotation (Placement(transformation(extent={{56,70},{76,90}})));
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(r_CM={0,0.1,0}, m=0)
    annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=180,
        origin={-2,2})));
  Modelica.Mechanics.Rotational.Sources.Torque torque
    annotation (Placement(transformation(extent={{-62,70},{-42,90}})));
  Modelica.Mechanics.Rotational.Components.IdealGear idealGear(ratio=10)
    annotation (Placement(transformation(extent={{-22,70},{-2,90}})));
equation
  connect(torque.flange, idealGear.flange_a) annotation (Line(
      points={{-42,80},{-22,80}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(torque.tau, x.y) annotation (Line(
      points={{-64,80},{-75,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(partial_PropellerMultiBody.frame_a, lumpedMass.frame_a) annotation (
      Line(
      points={{66,70.2},{66,2},{6,2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(idealGear.flange_b, partial_PropellerMultiBody.flange) annotation (
      Line(
      points={{-2,80},{56,80}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (experiment(StopTime=5), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end MovingMultiBodyPropeller;
