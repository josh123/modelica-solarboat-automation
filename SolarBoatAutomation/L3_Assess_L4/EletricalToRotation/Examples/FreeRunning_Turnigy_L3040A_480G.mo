within SolarBoatAutomation.L3_Assess_L4.EletricalToRotation.Examples;
model FreeRunning_Turnigy_L3040A_480G
  extends Modelica.Icons.Example;
  extends PartialModels.FreeRunning(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G
      partial_DCMotor, redeclare
      L4_SubsystemComponents.ElectricalToRotation.SpecSheets.Turnigy_L3040A_480G
      partial_DCMotorDesign);
end FreeRunning_Turnigy_L3040A_480G;
