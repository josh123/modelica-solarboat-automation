within SolarBoatAutomation.L3_Assess_L4.EletricalToRotation.Examples;
model LockedRotor_MaxonEC_max_60W_24V_272763
  extends Modelica.Icons.Example;
  extends PartialModels.LockedRotor(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.MaxonEC_max_60W_24V_272763
      partial_DCMotor, redeclare
      L4_SubsystemComponents.ElectricalToRotation.SpecSheets.MaxonEC_max_60W_24V_272763
      partial_DCMotorDesign);
end LockedRotor_MaxonEC_max_60W_24V_272763;
