within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.A.Partial;
partial model SysArch_PanelMPPT
  extends SolarToElectrical;
//  parameter Modelica.SIunits.Voltage V_oc = solarToUnstableVoltage.total_V_oc
//    "Open circuit voltage of the sub system";
  replaceable
    L4_SubsystemComponents.SolarToElectrical.A.Partial.SolarToUnstableVoltage
    solarToUnstableVoltage
    annotation (Placement(transformation(extent={{-38,-8},{-18,12}})));
  replaceable
    L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Partial.MPPT
    partial_MPPT annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-6,-32})));
equation
  mass_computed = solarToUnstableVoltage.mass_computed + partial_MPPT.mass_computed;
  cost_money_computed = solarToUnstableVoltage.cost_money_computed + partial_MPPT.cost_money_computed;
  cost_manhours_computed = solarToUnstableVoltage.cost_manhours_computed + partial_MPPT.cost_manhours_computed;
  connect(solarToUnstableVoltage.frame_a, frame_a) annotation (Line(
      points={{-28,-7.8},{-28,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.solarInsolation, solarInsolation) annotation (
      Line(
      points={{-32.9,12.3},{-44,12.3},{-44,104}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.temp_kelvin, temp_kelvin) annotation (Line(
      points={{-22.1,12.1},{38,12.1},{38,106}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(partial_MPPT.n, solarToUnstableVoltage.p) annotation (Line(
      points={{-11,-22.4},{-44,-22.4},{-44,2},{-37.8,2}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.n, partial_MPPT.p) annotation (Line(
      points={{-18,2},{0,2},{0,-22.4}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_MPPT.n1, p) annotation (Line(
      points={{-11.2,-41.8},{-11.2,-54},{-78,-54},{-78,0},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_MPPT.p1, n) annotation (Line(
      points={{-0.2,-41.8},{-0.2,-56},{50,-56},{50,2},{100,2}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(graphics={
        Rectangle(
          extent={{-78,70},{-4,-4}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,70},{78,-4}},
          lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{16,60},{66,10}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="MPPT")}));
end SysArch_PanelMPPT;
