within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.A.Partial;
partial model SolarToElectrical
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2")
    "Input solar insolation"                                                                        annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-44,104}),                            iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=-90,
        origin={-49,103})));
  Modelica.Blocks.Interfaces.RealInput temp_kelvin annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={38,106}), iconTransformation(
        extent={{-13,-13},{13,13}},
        rotation=-90,
        origin={59,101})));
  Modelica.Electrical.Analog.Interfaces.PositivePin
              p
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{-110,-10},{-90,10}}, rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n "Negative pin" annotation (Placement(transformation(extent={{110,-8},
            {90,12}},          rotation=0)));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={Text(
          extent={{-72,88},{-16,60}},
          lineColor={0,0,255},
          textString="solarInsolation"),                                        Text(
          extent={{34,88},{86,62}},
          lineColor={0,0,255},
          textString="temp_kelvin")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide the common attributes for all subsystems which perform this function.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarToElectrical-OPM.png\"/></p>
</html>"));
end SolarToElectrical;
