within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.A.Complete;
model SP18f_L_LOXX_16Px1S_NoMPPT
  extends Partial.SysArch_PanelNoMPPT(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.SP18f_L_LOXX_16Px1S
      solarToUnstableVoltage);
  parameter Modelica.SIunits.Voltage total_V_oc = solarToUnstableVoltage.total_V_oc;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end SP18f_L_LOXX_16Px1S_NoMPPT;
