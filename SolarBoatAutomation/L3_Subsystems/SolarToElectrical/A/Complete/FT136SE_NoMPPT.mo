within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.A.Complete;
model FT136SE_NoMPPT
  extends Partial.SysArch_PanelNoMPPT(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.FT136SE_1Px1S
      solarToUnstableVoltage);
  parameter Modelica.SIunits.Voltage total_V_oc = solarToUnstableVoltage.total_V_oc;
end FT136SE_NoMPPT;
