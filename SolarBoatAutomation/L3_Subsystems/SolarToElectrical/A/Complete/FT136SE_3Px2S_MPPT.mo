within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.A.Complete;
model FT136SE_3Px2S_MPPT
  extends Partial.SysArch_PanelMPPT(redeclare
      L4_SubsystemComponents.SolarToElectrical.A.Complete.FT136SE_3Px2S
      solarToUnstableVoltage, redeclare
      L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Complete.MPPT
      partial_MPPT);
end FT136SE_3Px2S_MPPT;
