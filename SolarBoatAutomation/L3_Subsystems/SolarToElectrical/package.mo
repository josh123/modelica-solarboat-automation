within SolarBoatAutomation.L3_Subsystems;
package SolarToElectrical "Models which convert solar insolation to a stable voltage"


annotation (Documentation(info="<html>
<p>See partial models for a full description.</p>
</html>"));
end SolarToElectrical;
