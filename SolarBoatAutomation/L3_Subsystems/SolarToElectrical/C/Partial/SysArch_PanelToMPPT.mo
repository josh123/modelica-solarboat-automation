within SolarBoatAutomation.L3_Subsystems.SolarToElectrical.C.Partial;
model SysArch_PanelToMPPT "SolarPanel and MPPT type architecture"
  extends SolarToElectrical;
  replaceable
    L4_SubsystemComponents.SolarToElectrical.C.Partial.SolarToUnstableVoltage
    solarToUnstableVoltage
    annotation (Placement(transformation(extent={{-92,16},{-36,38}})));
  replaceable
    L4_SubsystemComponents.ElectricalToPeakPowerElectrical.C.Partial.PartialUnstableVoltageToStableVoltage
    unstableVoltageToStableVoltage
    annotation (Placement(transformation(extent={{-4,-20},{70,20}})));
equation
  mass_computed = solarToUnstableVoltage.mass_computed + unstableVoltageToStableVoltage.mass_computed;
  cost_money_computed = solarToUnstableVoltage.cost_money_computed + unstableVoltageToStableVoltage.cost_money_computed;
  cost_manhours_computed = solarToUnstableVoltage.cost_manhours_computed + unstableVoltageToStableVoltage.cost_manhours_computed;
  connect(solarInsolation, solarToUnstableVoltage.solarInsolation) annotation (
      Line(
      points={{-60,104},{-60,62},{-63.72,62},{-63.72,38.33}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.frame_a, frame_a) annotation (Line(
      points={{-64,16.22},{-64,-78},{0,-78},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(powerDelivered, unstableVoltageToStableVoltage.powerDelivered)
    annotation (Line(
      points={{100,0},{71.48,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.powerDeliveredBySolarArray,
    unstableVoltageToStableVoltage.powerFromSolarPanel) annotation (Line(
      points={{-33.76,27.44},{-26,27.44},{-26,-0.4},{-4.74,-0.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(unstableVoltageToStableVoltage.frame_a, frame_a) annotation (Line(
      points={{33,-19.6},{33,-78},{0,-78},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide the common attributes for all subsystems which perform this function and its implementation.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<h4>OPM-Before layout</h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarToElectrical_PanelToMPPT-OPM1.png\"/></p>
<h5>OPM-After layout</h5>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarToElectrical_PanelToMPPT-OPM2.png\"/></p>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>Subsystems assumed not to interact other than on the interfaces</li>
</ul>
<h4>Equations</h4>
<ul>
<li><code>mass </code>is the summation of the sub system masses</li>
<li><code>cost_money </code>is the summation of the sub system <code>cost_money</code></li>
<li><code>cost_manhours </code>is the summation of the sub system <code>cost_manhours</code></li>
</ul>
</html>"));
end SysArch_PanelToMPPT;
