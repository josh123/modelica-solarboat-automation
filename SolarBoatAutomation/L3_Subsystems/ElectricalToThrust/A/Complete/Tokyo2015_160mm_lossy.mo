within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2015_160mm_lossy
  extends Partial.Motor_LossyGear_Prop(
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2015_13_1_Lossy
      lossyGearbox,
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted160mm135mm
      partial_PropellerMultiBody);
  annotation (Icon(graphics={            Polygon(
          points={{60,70},{140,-50},{120,-70},{100,10},{80,70},{60,70}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),              Rectangle(
          extent={{64,14},{102,-2}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Rectangle(
          extent={{-98,56},{64,-44}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder)}));
end Tokyo2015_160mm_lossy;
