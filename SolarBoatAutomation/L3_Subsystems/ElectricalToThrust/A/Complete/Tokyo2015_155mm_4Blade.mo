within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2015_155mm_4Blade
  extends Partial.Motor_IdealGear_Prop(
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted155mm135mm4Blade
      partial_PropellerMultiBody,
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2015_13_1_Ideal
      idealGearbox);
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
                                         Rectangle(
          extent={{-96,62},{66,-38}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder), Rectangle(
          extent={{66,20},{104,4}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{62,76},{142,-44},{122,-64},{102,16},{82,76},{62,76}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{142,74},{62,-46},{82,-66},{102,14},{122,74},{142,74}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Tokyo2015_155mm_4Blade;
