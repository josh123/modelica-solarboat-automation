within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2015_200mm_lossy
  extends Partial.Motor_LossyGear_Prop(
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2015_13_1_Lossy
      lossyGearbox,
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted200mm343mm
      partial_PropellerMultiBody);
  annotation (Icon(graphics={            Polygon(
          points={{60,68},{140,-52},{120,-72},{100,8},{80,68},{60,68}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),              Rectangle(
          extent={{64,12},{102,-4}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Rectangle(
          extent={{-98,54},{64,-46}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder)}));
end Tokyo2015_200mm_lossy;
