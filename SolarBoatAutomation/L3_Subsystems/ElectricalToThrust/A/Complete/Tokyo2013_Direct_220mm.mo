within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2013_Direct_220mm
  extends Partial.Motor_Prop(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.S13560_260R
      partial_DCMotor, redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted220mm343mm
      partial_PropellerMultiBody);
end Tokyo2013_Direct_220mm;
