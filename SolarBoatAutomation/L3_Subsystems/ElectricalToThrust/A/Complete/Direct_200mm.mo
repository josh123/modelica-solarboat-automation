within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Direct_200mm
  extends Partial.Motor_Prop(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor, redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted200mm343mm
      partial_PropellerMultiBody);
end Direct_200mm;
