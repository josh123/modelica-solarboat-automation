within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2015_155mm_4Blade_lossy
  extends Partial.Motor_LossyGear_Prop(
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2015_13_1_Lossy
      lossyGearbox,
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted155mm135mm4Blade
      partial_PropellerMultiBody);
  annotation (Icon(graphics={            Rectangle(
          extent={{-98,60},{64,-40}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder), Rectangle(
          extent={{64,18},{102,2}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{60,74},{140,-46},{120,-66},{100,14},{80,74},{60,74}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{140,72},{60,-48},{80,-68},{100,12},{120,72},{140,72}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Tokyo2015_155mm_4Blade_lossy;
