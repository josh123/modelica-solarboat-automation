within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2015_220mm_lossy
  extends Partial.Motor_LossyGear_Prop(
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2015_13_1_Lossy
      lossyGearbox,
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted220mm343mm
      partial_PropellerMultiBody);
  annotation (Icon(graphics={            Polygon(
          points={{62,70},{142,-50},{122,-70},{102,10},{82,70},{62,70}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),              Rectangle(
          extent={{66,14},{104,-2}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Rectangle(
          extent={{-96,56},{66,-44}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder)}));
end Tokyo2015_220mm_lossy;
