within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Tokyo2013_220mm
  extends Partial.Motor_IdealGear_Prop(
    redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted220mm343mm
      partial_PropellerMultiBody,
    redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.S13560_260R
      partial_DCMotor,
    redeclare
      L4_SubsystemComponents.RotationToRotation.Complete.Tokyo2014_3_1_Ideal
      idealGearbox);
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
                                                       Rectangle(
          extent={{66,20},{104,4}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{62,76},{142,-44},{122,-64},{102,16},{82,76},{62,76}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Rectangle(
          extent={{-10,74},{66,-40}},
          lineColor={0,0,0},
          fillColor={175,175,175},
          fillPattern=FillPattern.HorizontalCylinder)}));
end Tokyo2013_220mm;
