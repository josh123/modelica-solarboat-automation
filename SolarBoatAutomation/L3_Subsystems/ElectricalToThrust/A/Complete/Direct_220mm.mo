within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Complete;
model Direct_220mm
  extends Partial.Motor_Prop(redeclare
      L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.Turnigy_L3040A_480G_Experiment
      partial_DCMotor, redeclare
      L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.Predicted220mm343mm
      partial_PropellerMultiBody);
end Direct_220mm;
