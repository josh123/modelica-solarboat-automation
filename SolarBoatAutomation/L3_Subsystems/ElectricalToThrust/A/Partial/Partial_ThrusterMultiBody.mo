within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial;
partial model Partial_ThrusterMultiBody
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.SIunits.Force thrust "x direction thrust";

  Modelica.Electrical.Analog.Interfaces.PositivePin
              p
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{-70,90},{-50,110}},  rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n "Negative pin" annotation (Placement(transformation(extent={{50,88},
            {30,108}},         rotation=0)));

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Partial_ThrusterMultiBody;
