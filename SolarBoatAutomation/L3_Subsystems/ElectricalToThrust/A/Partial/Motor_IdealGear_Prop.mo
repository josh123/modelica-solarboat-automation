within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial;
model Motor_IdealGear_Prop
  extends Partial.Partial_ThrusterMultiBody;
  Modelica.SIunits.Torque torque "torque on the propeller shaft";
  replaceable
    L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.MaxonEC_max_60W_24V_272763
    partial_DCMotor constrainedby
    L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor
    annotation (Placement(transformation(extent={{-64,-2},{-44,18}})));
  replaceable
    L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.OpenTest200mm343mm
    partial_PropellerMultiBody constrainedby
    L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody
    annotation (Placement(transformation(extent={{66,-2},{86,18}})));
  replaceable L4_SubsystemComponents.RotationToRotation.Partial.IdealGearbox
    idealGearbox
    annotation (Placement(transformation(extent={{2,-2},{22,18}})));
equation
  torque = partial_PropellerMultiBody.torque_generated;
  thrust = partial_PropellerMultiBody.thrust_generated;
  mass_computed = partial_DCMotor.mass_computed + partial_PropellerMultiBody.mass_computed + idealGearbox.mass_computed;
  cost_money_computed = partial_DCMotor.cost_money_computed + partial_PropellerMultiBody.cost_money_computed + idealGearbox.cost_money_computed;
  cost_manhours_computed = partial_DCMotor.cost_manhours_computed + partial_PropellerMultiBody.cost_manhours_computed + idealGearbox.cost_manhours_computed;
  connect(partial_DCMotor.pin_n, p) annotation (Line(
      points={{-60.8,18},{-60,18},{-60,100}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_DCMotor.pin_p, n) annotation (Line(
      points={{-48.8,18},{-48,18},{-48,60},{40,60},{40,98}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_DCMotor.frame_a, frame_a) annotation (Line(
      points={{-54,-1.8},{-52,-1.8},{-52,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(idealGearbox.frame_a, frame_a) annotation (Line(
      points={{12,-1.8},{12,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_PropellerMultiBody.frame_a, frame_a) annotation (Line(
      points={{76,-1.8},{76,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_DCMotor.flange, idealGearbox.flange_a) annotation (Line(
      points={{-44.2,8},{-28,8},{-28,8.2},{2,8.2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(idealGearbox.flange_b, partial_PropellerMultiBody.flange) annotation (
     Line(
      points={{22,8.2},{38,8.2},{38,8},{66,8}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Motor_IdealGear_Prop;
