within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.A.Partial;
model Motor_LossyGear_Prop
  extends Partial.Partial_ThrusterMultiBody;
  Modelica.SIunits.Torque torque "torque on the propeller shaft";
  replaceable
    L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete.MaxonEC_max_60W_24V_272763
    partial_DCMotor constrainedby
    L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor
    annotation (Placement(transformation(extent={{-64,-2},{-44,18}})));
  replaceable
    L4_SubsystemComponents.RotationToThrust.MultiBody.Complete.OpenTest200mm343mm
    partial_PropellerMultiBody constrainedby
    L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody
    annotation (Placement(transformation(extent={{60,-4},{80,16}})));
  replaceable L4_SubsystemComponents.RotationToRotation.Partial.LossyGearbox
    lossyGearbox
    annotation (Placement(transformation(extent={{-8,-2},{12,18}})));
equation
  torque = partial_PropellerMultiBody.torque_generated;
  thrust = partial_PropellerMultiBody.thrust_generated;
  mass_computed = partial_DCMotor.mass_computed + partial_PropellerMultiBody.mass_computed + lossyGearbox.mass_computed;
  cost_money_computed = partial_DCMotor.cost_money_computed + partial_PropellerMultiBody.cost_money_computed + lossyGearbox.cost_money_computed;
  cost_manhours_computed = partial_DCMotor.cost_manhours_computed + partial_PropellerMultiBody.cost_manhours_computed + lossyGearbox.cost_manhours_computed;
  connect(partial_DCMotor.pin_n, p) annotation (Line(
      points={{-60.8,18},{-60,18},{-60,100}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_DCMotor.pin_p, n) annotation (Line(
      points={{-48.8,18},{-48,18},{-48,60},{40,60},{40,98}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(partial_DCMotor.frame_a, frame_a) annotation (Line(
      points={{-54,-1.8},{-52,-1.8},{-52,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(lossyGearbox.frame_a, frame_a) annotation (Line(
      points={{2,-1.8},{2,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(partial_DCMotor.flange, lossyGearbox.flange_a) annotation (Line(
      points={{-44.2,8},{-26,8},{-26,8.2},{-8,8.2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(lossyGearbox.flange_b, partial_PropellerMultiBody.flange) annotation (
     Line(
      points={{12,8.2},{36,8.2},{36,6},{60,6}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(partial_PropellerMultiBody.frame_a, frame_a) annotation (Line(
      points={{70,-3.8},{70,-50},{0,-50},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Motor_LossyGear_Prop;
