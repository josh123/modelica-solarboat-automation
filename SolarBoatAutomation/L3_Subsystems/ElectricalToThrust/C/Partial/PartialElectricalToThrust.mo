within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.C.Partial;
partial model PartialElectricalToThrust
  "Provides all the common attributes of a electrical to thrust system"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput electricalPowerInput(final unit="W")
    "The power delivered by the MPPT" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={0,102}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-14,106})));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide the common attributes for all subsystems which perform this function.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialElectricalToThrust-OPM.png\"/></p>
</html>"),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics));
end PartialElectricalToThrust;
