within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.C.Partial;
model PartialElectricalToThrust_Function
  "Models electrical to thrust by way of a function"
  extends C.Partial.PartialElectricalToThrust;
  parameter Modelica.SIunits.Mass mass;
  Modelica.SIunits.Force thurstDelivered
    "Driving force delivered by the motor pod";
  Modelica.Mechanics.MultiBody.Forces.WorldForce forceNetWorld
    annotation (Placement(transformation(extent={{-18,-4},{2,16}})));
  Modelica.Blocks.Sources.RealExpression forceNetX(y=thurstDelivered)
    annotation (Placement(transformation(extent={{-74,6},{-40,26}})));
  Modelica.Blocks.Sources.RealExpression forceNetY(y=0)
    annotation (Placement(transformation(extent={{-74,-12},{-40,8}})));
  Modelica.Blocks.Sources.RealExpression forceNetZ(y=0)
    annotation (Placement(transformation(extent={{-74,-28},{-40,-8}})));
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=mass)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-60,-54})));
equation
  connect(forceNetX.y, forceNetWorld.force[1]) annotation (Line(
      points={{-38.3,16},{-28,16},{-28,4.66667},{-20,4.66667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(forceNetY.y, forceNetWorld.force[2]) annotation (Line(
      points={{-38.3,-2},{-32,-2},{-32,6},{-20,6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(forceNetZ.y, forceNetWorld.force[3]) annotation (Line(
      points={{-38.3,-18},{-26,-18},{-26,7.33333},{-20,7.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(forceNetWorld.frame_b, frame_a) annotation (Line(
      points={{2,6},{16,6},{16,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(frame_a,lumpedMass. frame_a) annotation (Line(
      points={{0,-98},{0,-54},{-50,-54}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Enable the modeling of thust systems which are only characterized by high level functions.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/SolarBoatComponents/Components/ElectricalToThrust/PartialModels/PartialElectricalToThrust-OPM.png\"/></p>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<p>Thrust is computed from a simple function.</p>
<h4><span style=\"color:#008000\">Known issues</span></h4>
<ul>
<li>Fluid effects have not been implemented.</li>
</ul>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>The thrust fuction should be a block with <b>input</b> and output</li>
</ul>
</html>"));
end PartialElectricalToThrust_Function;
