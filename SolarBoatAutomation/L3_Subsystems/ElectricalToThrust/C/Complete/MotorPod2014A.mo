within SolarBoatAutomation.L3_Subsystems.ElectricalToThrust.C.Complete;
model MotorPod2014A
  extends Partial.PartialElectricalToThrust_Function(      mass = motorpod_mass);
  parameter Modelica.SIunits.Mass motorpod_mass = 2
    "The mass of the motor pod in kg";
  parameter Real motor_pod_cost_money(unit="yen") = 0 "Cost";
  parameter Real motor_pod_cost_manhours(unit="hours") = 0 "Time to build";
equation
  mass_computed = mass;
  cost_money_computed = motor_pod_cost_money;
  cost_manhours_computed = motor_pod_cost_manhours;
  if electricalPowerInput <= 0 then //No power from MPPT so no thrust
    thurstDelivered = 0;
  else
    thurstDelivered = (electricalPowerInput * 0.1945) + 3.3929; //From 2014 experiments
  end if;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Ellipse(
          extent={{4,-18},{40,18}},
          lineColor={0,0,0},
          lineThickness=1,
          fillPattern=FillPattern.Sphere,
          fillColor={175,175,175}),
        Ellipse(
          extent={{-82,-18},{-46,18}},
          lineColor={0,0,0},
          lineThickness=1,
          fillPattern=FillPattern.Sphere,
          fillColor={175,175,175}),
        Rectangle(
          extent={{-62,18},{18,-18}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={175,175,175},
          fillPattern=FillPattern.HorizontalCylinder),
        Rectangle(
          extent={{-30,18},{2,74}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.VerticalCylinder,
          fillColor={0,0,0}),
        Rectangle(
          extent={{38,8},{58,-8}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={127,127,0}),
        Rectangle(
          extent={{58,4},{72,-2}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,0,0}),
        Polygon(
          points={{74,-2},{66,14},{66,20},{68,22},{70,24},{72,22},{76,22},{78,20},
              {78,16},{78,14},{74,-2}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          smooth=Smooth.None,
          fillColor={0,0,0}),
        Polygon(
          points={{74,2},{66,-14},{66,-20},{68,-22},{70,-24},{72,-22},{76,-22},{
              78,-20},{78,-16},{78,-14},{74,2}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          smooth=Smooth.None,
          fillColor={0,0,0})}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide a model of the motorpod used in 2014 SolarBoat race. This is based on experimental data not bottom up component driven.</p>
<h4><span style=\"color:#008000\">Real component</span></h4>
<p>The real component is a machined aluminum pod with Turnigy L3040A-480G motor inside, 1:5 gearbox and 120mm propeller.</p>
<p><img src=\"modelica://SolarBoatAutomation/Images/photo/MotorPod2014A-Photo.png\"/></p>
<h4><span style=\"color:#008000\">Modeling approch</span></h4>
<p>The thust is modeled from an experiment where by the power as input to the motorpod was varied and thrust measured. A linear fit is approximated for the data. See chart below. <b>Note this is for 1:6 KTH gearbox, but likely the data is not so different.</b></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/photo/MotorPod2014A-Data.png\"/></p>
<p>Propeller sizes are as follows:</p>
<ul>
<li>Copper Prop: 3 Blade 105mm diameter</li>
<li>CRFP Prop A: 2 Blade 150mm</li>
<li>CRFP Prop B: 2 Blade 130mm</li>
</ul>
<p><img src=\"modelica://SolarBoatAutomation/Images/photo/MotorPod2014A-Data2.jpg\"/></p>
<h4>What is modeled:</h4>
<ul>
<li>Thrust in X direction</li>
<li>Mass</li>
</ul>
<h5>What is not modeled:</h5>
<ul>
<li>Fluid effects (drag, buoyancy)</li>
</ul>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>Fluid effects</li>
</ul>
</html>"));
end MotorPod2014A;
