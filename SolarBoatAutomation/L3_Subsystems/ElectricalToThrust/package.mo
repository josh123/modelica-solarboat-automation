within SolarBoatAutomation.L3_Subsystems;
package ElectricalToThrust "Models which convert electrical power to thrust"


annotation (Documentation(info="<html>
<p>See partial models for a full description.</p>
</html>"));
end ElectricalToThrust;
