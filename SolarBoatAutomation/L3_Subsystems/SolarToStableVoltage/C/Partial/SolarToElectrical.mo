within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.C.Partial;
partial model SolarToElectrical
  "Most basic partial model for this subsystem. All should extend this"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2")
    "Input solar insolation"                                                                        annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-60,104}),                            iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=-90,
        origin={1,103})));
  Modelica.Blocks.Interfaces.RealOutput powerDelivered(final unit="W")
    "The power delivered by the solar conversion process"
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide the common attributes for all subsystems which perform this function.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarToElectrical-OPM.png\"/></p>
</html>"));
end SolarToElectrical;
