within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.C.Complete;
model SolarToElectrical2014A
  extends Partial.SysArch_PanelToMPPT(redeclare
      L4_SubsystemComponents.SolarToUnstableVoltage.C.Complete.SolarPanelArray2014A
      solarToUnstableVoltage, redeclare
      L4_SubsystemComponents.UnstableVoltageToStableVoltage.C.Complete.MPPT2014A
      unstableVoltageToStableVoltage);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={
        Rectangle(
          extent={{22,-16},{100,-46}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{10,-8},{22,-16},{22,-46},{10,-34},{10,-8}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{10,-8},{88,-8},{100,-16},{22,-16},{10,-8}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{24,-12},{24,-12}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{22,-10},{30,-16},{76,-16},{64,-10},{22,-10}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{34,-22},{34,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{44,-22},{44,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{54,-22},{54,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{64,-22},{64,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{72,-22},{72,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{82,-22},{82,-36}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Polygon(
          points={{-98,66},{-40,32},{0,62},{-54,86},{-98,66}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-98,0},{-28,-46},{12,-2},{-52,26},{-98,0}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-12,34},{60,0},{88,46},{30,66},{-12,34}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-30,40},{14,4},{68,-14},{96,30}},
          color={0,0,0},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{26,16},{14,4},{-2,4}},
          color={0,0,0},
          thickness=1,
          smooth=Smooth.None)}), Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Model the system used on boat 2014A. Providing cost, mass and power output characteristics.</p>
<p><img src=\"modelica://SolarBoatAutomation/Images/photo/SolarToElectrical2014A-Photo.png\"/></p>
</html>"));
end SolarToElectrical2014A;
