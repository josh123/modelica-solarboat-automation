within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.A.Partial;
partial model SysArch_PanelNoMPPT
  extends SolarToElectrical;
//  parameter Modelica.SIunits.Voltage V_oc = solarToUnstableVoltage.total_V_oc
//    "Open circuit voltage of the sub system";
  replaceable
    L4_SubsystemComponents.SolarToUnstableVoltage.A.Partial.SolarToUnstableVoltage
    solarToUnstableVoltage
    annotation (Placement(transformation(extent={{-36,-24},{24,28}})));
equation
  mass_computed = solarToUnstableVoltage.mass_computed;
  cost_money_computed = solarToUnstableVoltage.cost_money_computed;
  cost_manhours_computed = solarToUnstableVoltage.cost_manhours_computed;
  connect(solarToUnstableVoltage.p, p) annotation (Line(
      points={{-35.4,2},{-56,2},{-56,0},{-100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.n, n) annotation (Line(
      points={{24,2},{100,2}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.frame_a, frame_a) annotation (Line(
      points={{-6,-23.48},{-6,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.solarInsolation, solarInsolation) annotation (
      Line(
      points={{-20.7,28.78},{-44,28.78},{-44,104}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarToUnstableVoltage.temp_kelvin, temp_kelvin) annotation (Line(
      points={{11.7,28.26},{38,28.26},{38,106}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(graphics={
        Rectangle(
          extent={{-78,70},{-4,-4}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,70},{78,-4}},
          lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{16,60},{66,10}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="No MPPT")}));
end SysArch_PanelNoMPPT;
