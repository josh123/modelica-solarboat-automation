within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.A.Complete;
model BP_7185_NoMPPT
  extends Partial.SysArch_PanelNoMPPT(redeclare
      L4_SubsystemComponents.SolarToUnstableVoltage.A.Complete.FT136SE_1Px1S
      solarToUnstableVoltage);
  parameter Modelica.SIunits.Voltage total_V_oc = solarToUnstableVoltage.total_V_oc;
end BP_7185_NoMPPT;
