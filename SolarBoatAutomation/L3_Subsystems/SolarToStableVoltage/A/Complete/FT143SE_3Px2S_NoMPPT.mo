within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.A.Complete;
model FT143SE_3Px2S_NoMPPT
  extends Partial.SysArch_PanelNoMPPT(redeclare
      L4_SubsystemComponents.SolarToUnstableVoltage.A.Complete.FT136SE_3Px2S
      solarToUnstableVoltage);
  parameter Modelica.SIunits.Voltage total_V_oc = solarToUnstableVoltage.total_V_oc;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FT143SE_3Px2S_NoMPPT;
