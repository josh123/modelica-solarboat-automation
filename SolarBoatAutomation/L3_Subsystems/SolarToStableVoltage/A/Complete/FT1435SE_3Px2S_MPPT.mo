within SolarBoatAutomation.L3_Subsystems.SolarToStableVoltage.A.Complete;
model FT1435SE_3Px2S_MPPT
  extends Partial.SysArch_PanelMPPT(redeclare
      L4_SubsystemComponents.SolarToUnstableVoltage.A.Complete.FT136SE_3Px2S
      solarToUnstableVoltage, redeclare
      L4_SubsystemComponents.UnstableVoltageToStableVoltage.A.Complete.MPPT
      partial_MPPT);
end FT1435SE_3Px2S_MPPT;
