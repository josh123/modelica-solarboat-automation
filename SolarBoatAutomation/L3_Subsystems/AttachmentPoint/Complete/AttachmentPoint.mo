within SolarBoatAutomation.L3_Subsystems.AttachmentPoint.Complete;
model AttachmentPoint
  Modelica.SIunits.Velocity x_velocity "Speed of the SolarBoat";
  Modelica.SIunits.Position z_world_frame
    "z position in world frame of reference";
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a
    "To attach the component rigidly to other components"                                                       annotation (Placement(
        transformation(extent={{-16,-16},{16,16}}), iconTransformation(extent={{
            -16,-16},{16,16}})));
  Modelica.Mechanics.MultiBody.Parts.PointMass pointMass(m=0)
    annotation (Placement(transformation(extent={{38,-12},{66,16}})));
equation
  x_velocity = pointMass.v_0[1];
  z_world_frame = pointMass.r_0[3];
  connect(pointMass.frame_a, frame_a) annotation (Line(
      points={{52,2},{26,2},{26,0},{0,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={Text(
          extent={{-58,-76},{66,-38}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Enable multiple solarboat designs to have a common part to read dynamic attributes.</p>
</html>"));
end AttachmentPoint;
