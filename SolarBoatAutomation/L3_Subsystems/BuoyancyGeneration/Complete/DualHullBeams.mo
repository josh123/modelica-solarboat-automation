within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Complete;
model DualHullBeams
  extends Partial.PartialBuoyancyGeneration_RigidHull_DualBeams(redeclare
      L4_SubsystemComponents.BuoyancyGeneration._Old.BasicRigidHull
      portRigidHull, redeclare
      L4_SubsystemComponents.BuoyancyGeneration._Old.BasicRigidHull
      starboardRigidHull);
equation

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                         graphics={Polygon(
          points={{-78,12},{-36,-12},{80,-12},{86,12},{-78,12}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          smooth=Smooth.None,
          fillColor={0,0,0}),      Polygon(
          points={{-78,2},{-36,-22},{80,-22},{86,2},{-78,2}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          smooth=Smooth.None,
          fillColor={0,0,0})}),  Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics));
end DualHullBeams;
