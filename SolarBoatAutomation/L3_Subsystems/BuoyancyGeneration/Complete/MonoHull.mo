within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Complete;
model MonoHull
  //Simple single hull system
  extends Partial.PartialBuoyancyGeneration_RigidHull_Mono(redeclare
      L4_SubsystemComponents.BuoyancyGeneration._Old.BasicRigidHull rigidHull);
equation

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                         graphics={Polygon(
          points={{-78,12},{-36,-12},{80,-12},{86,12},{-78,12}},
          lineColor={175,175,175},
          lineThickness=1,
          fillPattern=FillPattern.HorizontalCylinder,
          smooth=Smooth.None,
          fillColor={0,0,0})}),  Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics));
end MonoHull;
