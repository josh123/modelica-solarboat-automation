within SolarBoatAutomation.L3_Subsystems;
package BuoyancyGeneration "Models of buoyancy providers"


annotation (Documentation(info="<html>
<p>See partial models for a full description.</p>
</html>"));
end BuoyancyGeneration;
