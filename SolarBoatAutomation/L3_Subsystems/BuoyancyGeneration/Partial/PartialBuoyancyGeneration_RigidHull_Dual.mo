within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Partial;
model PartialBuoyancyGeneration_RigidHull_Dual
  extends PartialBuoyancyGeneration;
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    portRigidHull
    annotation (Placement(transformation(extent={{-22,64},{-64,86}})));
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    starboardRigidHull
    annotation (Placement(transformation(extent={{-22,-70},{-62,-46}})));
equation
  mass_computed = portRigidHull.mass_computed + starboardRigidHull.mass_computed;
  cost_money_computed = portRigidHull.cost_money_computed + starboardRigidHull.cost_money_computed;
  cost_manhours_computed = portRigidHull.cost_manhours_computed + starboardRigidHull.cost_manhours_computed;
  drag_force = portRigidHull.drag_force + starboardRigidHull.drag_force;
  displaced_volume = portRigidHull.displaced_volume + starboardRigidHull.displaced_volume;
  buoyancy_force = portRigidHull.buoyancy_force + starboardRigidHull.buoyancy_force;
  z_top_of_hull = portRigidHull.z_top_of_hull;
  connect(portRigidHull.frame_a, frame_a) annotation (Line(
      points={{-43,64.22},{-43,10},{0,10},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(starboardRigidHull.frame_a, frame_a) annotation (Line(
      points={{-42,-69.76},{-42,-80},{-72,-80},{-72,-20},{-72,-20},{-72,10},{0,
          10},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(waterVelocityX, portRigidHull.waterVelocityX) annotation (Line(
      points={{-104,2},{-78,2},{-78,92},{-14,92},{-14,75.66},{-24.52,75.66}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(starboardRigidHull.waterVelocityX, portRigidHull.waterVelocityX)
    annotation (Line(
      points={{-24.4,-57.28},{-10,-57.28},{-10,-92},{-78,-92},{-78,92},{-14,92},
          {-14,75.66},{-24.52,75.66}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide an architecture for modeling buoyancy generation with a two rigid hulls</p>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<p>Two rigid hulls assumed to be in the same position such that mass is not distributed</p>
</html>"));
end PartialBuoyancyGeneration_RigidHull_Dual;
