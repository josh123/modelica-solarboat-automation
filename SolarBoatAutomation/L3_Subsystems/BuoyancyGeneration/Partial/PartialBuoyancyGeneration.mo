within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Partial;
partial model PartialBuoyancyGeneration
  //Provides all the common attributes of a buoyancy provider
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialInAFluidAttributes;
  Modelica.SIunits.Position z_top_of_hull
    "z position of the hull in the world frame of reference";
  Modelica.Blocks.Interfaces.RealInput waterVelocityX(final unit="m.s-1")
    "Water velocity in the x direction" annotation (Placement(transformation(
          extent={{-124,-18},{-84,22}}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-88,6})));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide the common attributes for all subsystems which perform this function.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialBuoyancyGeneration.png\"/></p>
</html>"),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics));
end PartialBuoyancyGeneration;
