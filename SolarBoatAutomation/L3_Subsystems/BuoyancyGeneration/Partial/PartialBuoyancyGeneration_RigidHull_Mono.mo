within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Partial;
model PartialBuoyancyGeneration_RigidHull_Mono
  //Models architectures with a single rigidhull
  extends PartialBuoyancyGeneration;
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    rigidHull annotation (Placement(transformation(extent={{-20,2},{18,28}})));
equation
  mass_computed = rigidHull.mass_computed;
  cost_money_computed = rigidHull.cost_money_computed;
  cost_manhours_computed = rigidHull.cost_manhours_computed;
  drag_force = rigidHull.drag_force;
  displaced_volume = rigidHull.displaced_volume;
  buoyancy_force = rigidHull.buoyancy_force;
  z_top_of_hull = rigidHull.z_top_of_hull;
  connect(rigidHull.frame_a, frame_a) annotation (Line(
      points={{-1,2.26},{-1,-46.87},{0,-46.87},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(waterVelocityX, rigidHull.waterVelocityX) annotation (Line(
      points={{-104,2},{-68,2},{-68,15.78},{-17.72,15.78}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide an architecture for modeling buoyancy generation with a single rigid hull</p>
</html>"));
end PartialBuoyancyGeneration_RigidHull_Mono;
