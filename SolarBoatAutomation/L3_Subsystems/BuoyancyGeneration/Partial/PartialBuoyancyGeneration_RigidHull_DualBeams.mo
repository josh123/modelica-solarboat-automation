within SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Partial;
model PartialBuoyancyGeneration_RigidHull_DualBeams
  //Models architectures with two rigidhulls
  extends PartialBuoyancyGeneration;
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    portRigidHull
    annotation (Placement(transformation(extent={{-22,64},{-64,86}})));
  Modelica.Mechanics.MultiBody.Parts.BodyBox port(
    density=0,
    r={0,0.25,0},
    width=0.2,
    height=0.2,
    color={255,0,0}) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-44,40})));
  Modelica.Mechanics.MultiBody.Parts.BodyBox starboard(
    density=0,
    r={0,0.25,0},
    width=0.2,
    height=0.2,
    color={0,180,0}) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-44,-18})));
  replaceable L4_SubsystemComponents.BuoyancyGeneration.Partial.RigidHull
    starboardRigidHull
    annotation (Placement(transformation(extent={{-22,-70},{-62,-46}})));
equation
  mass_computed = portRigidHull.mass_computed + starboardRigidHull.mass_computed;
  cost_money_computed = portRigidHull.cost_money_computed + starboardRigidHull.cost_money_computed;
  cost_manhours_computed = portRigidHull.cost_manhours_computed + starboardRigidHull.cost_manhours_computed;
  drag_force = portRigidHull.drag_force + starboardRigidHull.drag_force;
  displaced_volume = portRigidHull.displaced_volume + starboardRigidHull.displaced_volume;
  buoyancy_force = portRigidHull.buoyancy_force + starboardRigidHull.buoyancy_force;
  connect(portRigidHull.frame_a, port.frame_a) annotation (Line(
      points={{-43,64.22},{-43,55.11},{-44,55.11},{-44,50}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(port.frame_b, starboard.frame_a) annotation (Line(
      points={{-44,30},{-44,-8}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(frame_a, starboard.frame_a) annotation (Line(
      points={{0,-98},{0,8},{-44,8},{-44,-8}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(starboardRigidHull.frame_a, starboard.frame_b) annotation (Line(
      points={{-42,-69.76},{-42,-82},{-72,-82},{-72,-34},{-44,-34},{-44,-28}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(waterVelocityX, portRigidHull.waterVelocityX) annotation (Line(
      points={{-104,2},{-72,2},{-72,94},{-12,94},{-12,75.66},{-24.52,75.66}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(starboardRigidHull.waterVelocityX, portRigidHull.waterVelocityX)
    annotation (Line(
      points={{-24.4,-57.28},{-8,-57.28},{-8,-94},{-80,-94},{-80,-24},{-72,-24},
          {-72,94},{-12,94},{-12,75.66},{-24.52,75.66}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>Two rigid hulls are connected by two beams.</li>
<li>Beams have no mass.</li>
<li>Beams are introduced for visulization.</li>
<li>NOTE: Beams make this no longer a single point mass model. The two hulls are a distance appart from each other.</li>
</ul>
</html>"));
end PartialBuoyancyGeneration_RigidHull_DualBeams;
