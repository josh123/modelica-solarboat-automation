within SolarBoatAutomation;
package L3_Subsystems "Package of components used to define a SolarBoat"


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide multiple implementations of components which can be used to provide alternative SolarBoat designs.</p>
</html>"));
end L3_Subsystems;
