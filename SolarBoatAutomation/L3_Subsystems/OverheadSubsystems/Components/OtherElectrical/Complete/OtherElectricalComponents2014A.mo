within SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Components.OtherElectrical.Complete;
model OtherElectricalComponents2014A
  "SolarBoat 2014A electrical parts whose mass and cost has not been accounted for prevously"
  extends Partial.OtherElectrical(             mass = other_electrical_components_mass);
  parameter Modelica.SIunits.Mass other_electrical_components_mass = 0.5
    "Mass structural components not accounted for";
  parameter Real cost_money(unit="yen") = 1 "Cost";
  parameter Real cost_manhours(unit="hours") = 1 "Time to build";
equation
  mass_computed = other_electrical_components_mass;
  cost_money_computed = cost_money;
  cost_manhours_computed = cost_manhours;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                         graphics={
        Rectangle(
          extent={{-72,26},{-68,16}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-69.5,16.5},
          rotation=-90),
        Rectangle(
          extent={{-62,36},{-58,26}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-59.5,26.5},
          rotation=-90),
        Rectangle(
          extent={{-82,32},{-78,22}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-79.5,22.5},
          rotation=-90),
        Rectangle(
          extent={{-76,44},{-72,34}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-73.5,34.5},
          rotation=-90),
        Rectangle(
          extent={{-62,20},{-58,10}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-59.5,10.5},
          rotation=-90),
        Rectangle(
          extent={{-66,48},{-62,38}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-63.5,38.5},
          rotation=-90),
        Rectangle(
          extent={{-20,40},{58,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-32,48},{-20,40},{-20,10},{-32,22},{-32,48}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-32,48},{46,48},{58,40},{-20,40},{-32,48}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-18,44},{-18,44}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-20,46},{-12,40},{34,40},{22,46},{-20,46}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-8,34},{-8,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{2,34},{2,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{12,34},{12,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{22,34},{22,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{30,34},{30,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{40,34},{40,20}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Rectangle(
          extent={{12,14},{90,-16}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{0,22},{12,14},{12,-16},{0,-4},{0,22}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{0,22},{78,22},{90,14},{12,14},{0,22}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{14,18},{14,18}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{12,20},{20,14},{66,14},{54,20},{12,20}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{24,8},{24,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{34,8},{34,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{44,8},{44,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{54,8},{54,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{62,8},{62,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{72,8},{72,-6}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-88,-22},{-82,-16},{-74,-10},{-62,-8},{-14,-8},{4,0}},
          color={255,0,0},
          pattern=LinePattern.Dash,
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-86,-24},{-72,-14},{-60,-12},{-14,-12},{6,-2}},
          color={0,255,0},
          pattern=LinePattern.Dash,
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-24,30},{-36,26},{-40,18},{-30,10},{-10,4},{4,2}},
          color={0,0,255},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-24,32},{-36,28},{-40,20},{-30,12},{-10,6},{4,4}},
          color={0,0,255},
          thickness=1,
          smooth=Smooth.None)}), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>This the electrical components which are known to exist on the 2014A solarboat but are not modeled</li>
</ul>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>Breakdown more?</li>
<li>Cost is not real.</li>
</ul>
</html>"));
end OtherElectricalComponents2014A;
