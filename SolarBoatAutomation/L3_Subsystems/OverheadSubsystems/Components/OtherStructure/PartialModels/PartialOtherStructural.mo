within SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Components.OtherStructure.PartialModels;
partial model PartialOtherStructural
  "Provides the common attributes for other structural components"
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents;
  parameter Modelica.SIunits.Mass mass;
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=mass)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-16,-28})));
equation
  connect(lumpedMass.frame_a, frame_a) annotation (Line(
      points={{-6,-28},{0,-28},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialOtherStructural;
