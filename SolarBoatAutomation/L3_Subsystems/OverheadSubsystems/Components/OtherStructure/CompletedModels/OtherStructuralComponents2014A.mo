within SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Components.OtherStructure.CompletedModels;
model OtherStructuralComponents2014A
  "SolarBoat 2014A structual parts whose mass and cost has not been accounted for prevously"
  extends PartialModels.PartialOtherStructural(mass = other_structural_components_mass);
  parameter Modelica.SIunits.Mass other_structural_components_mass = 2
    "Mass structural components not accounted for";
  parameter Real cost_money(unit="yen") = 1 "Cost";
  parameter Real cost_manhours(unit="hours") = 1 "Time to build";
equation
  cost_manhours_computed = cost_manhours;
  cost_money_computed = cost_money;
  mass_computed = other_structural_components_mass;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={
        Polygon(
          points={{-66,76},{54,4},{66,16},{-52,86},{-66,76}},
          lineColor={175,175,175},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.CrossDiag),
        Rectangle(
          extent={{-68,2},{-64,-8}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-44,22},{-38,-6}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-65.5,-7.5},
          rotation=-90),
        Rectangle(
          extent={{-58,12},{-54,2}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-55.5,2.5},
          rotation=-90),
        Rectangle(
          extent={{-78,8},{-74,-2}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-75.5,-1.5},
          rotation=-90),
        Rectangle(
          extent={{-72,20},{-68,10}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-69.5,10.5},
          rotation=-90),
        Rectangle(
          extent={{-58,-4},{-54,-14}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-55.5,-13.5},
          rotation=-90),
        Rectangle(
          extent={{-62,24},{-58,14}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-1.5,3.5},{1.5,-3.5}},
          lineColor={175,175,175},
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          origin={-59.5,14.5},
          rotation=-90),
        Polygon(
          points={{-42,88},{78,16},{90,28},{-28,98},{-42,88}},
          lineColor={175,175,175},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.CrossDiag),
        Polygon(
          points={{-78,58},{42,-14},{54,-2},{-64,68},{-78,58}},
          lineColor={175,175,175},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.CrossDiag),
        Rectangle(
          extent={{-32,6},{-26,-22}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-18,6},{-12,-22}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-40,-6},{-34,-34}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-4,-4},{2,-32}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>This is the difference in mass from the items modeled in the origonal SolarBoat 2014A model and the real boat mass</li>
</ul>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>Breakdown more?</li>
<li>Cost is not real.</li>
</ul>
</html>"));
end OtherStructuralComponents2014A;
