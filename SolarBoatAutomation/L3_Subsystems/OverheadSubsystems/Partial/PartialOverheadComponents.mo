within SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Partial;
partial model PartialOverheadComponents
  //"Aims to model extra items (mainly their mass and cost) which would be missed in a simple simulation"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PartialOverheadComponents;
