within SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Partial;
partial model PartialOverheadComponents_StructAndElectrical
  //"Provides an architecture where the extra components are divided between structural and electrical"
  extends PartialOverheadComponents;
  replaceable
    L4_SubsystemComponents.OverheadOtherElectrical.Partial.OtherElectrical
    otherElectrical
    annotation (Placement(transformation(extent={{-58,2},{-24,32}})));
  replaceable
    L4_SubsystemComponents.OverheadOtherStructural.Partial.OtherStructural
    otherStructural
    annotation (Placement(transformation(extent={{44,2},{78,32}})));
equation
  mass_computed = otherStructural.mass_computed + otherElectrical.mass_computed;
  cost_money_computed = otherStructural.cost_money_computed + otherElectrical.cost_money_computed;
  cost_manhours_computed = otherStructural.cost_manhours_computed + otherElectrical.cost_manhours_computed;
  connect(otherElectrical.frame_a, frame_a) annotation (Line(
      points={{-41,2.3},{-41,-54},{0,-54},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(otherStructural.frame_a, frame_a) annotation (Line(
      points={{61,2.3},{61,-54},{0,-54},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<p>Mass and cost from the two sub components are added.</p>
</html>"));
end PartialOverheadComponents_StructAndElectrical;
