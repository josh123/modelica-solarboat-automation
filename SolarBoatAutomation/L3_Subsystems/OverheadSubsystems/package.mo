within SolarBoatAutomation.L3_Subsystems;
package OverheadSubsystems "Any other system which is not explicitly modeled is lumped here"


  annotation (Documentation(info="<html>
<p>See partial models for a full description.</p>
</html>"));
end OverheadSubsystems;
