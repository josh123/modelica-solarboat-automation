within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.PartialModels.A;
partial model Powertrain_Architecture01_A
  Modelica.SIunits.Force thrust "x direction thrust";
  Modelica.SIunits.Torque torque "torque on the propeller shaft";
  Modelica.SIunits.Mass mass "Total powertrain mass";
  Real cost_money(unit="yen") "Predicted cost of boat";
  Real cost_manhours(unit="hours") "Predicted time to build boat";

  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2") annotation (Placement(
        transformation(extent={{-32,60},{8,100}}),iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-54,88})));
  L3_Subsystems.AttachmentPoint.Complete.AttachmentPoint attachmentPoint
    annotation (Placement(transformation(extent={{-20,-16},{22,12}})));
  Modelica.Blocks.Interfaces.RealInput temperature(final unit="K") annotation (
      Placement(transformation(extent={{40,66},{80,106}}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={36,88})));
  replaceable L3_Subsystems.SolarToElectrical.A.Partial.SolarToElectrical
    solarToElectrical
    annotation (Placement(transformation(extent={{36,28},{82,50}})));
  replaceable
    L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    electricalToThrust constrainedby
    L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    annotation (Placement(transformation(extent={{44,-40},{92,-16}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-18,0},{2,20}})));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a Attachment
    annotation (Placement(transformation(extent={{-116,-14},{-84,18}})));
equation
  thrust = electricalToThrust.thrust;
  torque = electricalToThrust.torque;
  mass = solarToElectrical.mass_computed + electricalToThrust.mass_computed;
  cost_money = solarToElectrical.cost_money_computed + electricalToThrust.cost_money_computed;
  cost_manhours = solarToElectrical.cost_manhours_computed + electricalToThrust.cost_manhours_computed;
  connect(electricalToThrust.frame_a,attachmentPoint. frame_a) annotation (Line(
      points={{68,-39.76},{56,-39.76},{56,-38},{40,-38},{40,-2},{1,-2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarInsolation,solarToElectrical. solarInsolation) annotation (Line(
      points={{-12,80},{47.73,80},{47.73,50.33}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarToElectrical.frame_a,attachmentPoint. frame_a) annotation (Line(
      points={{59,28.22},{59,8},{18,8},{18,-2},{1,-2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToElectrical.n,electricalToThrust. p) annotation (Line(
      points={{82,39.22},{86,39.22},{86,0},{53.6,0},{53.6,-16}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(solarToElectrical.p,electricalToThrust. n) annotation (Line(
      points={{36,39},{30,39},{30,14},{77.6,14},{77.6,-16.24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p,electricalToThrust. n) annotation (Line(
      points={{-8,20},{30,20},{30,14},{77.6,14},{77.6,-16.24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(temperature,solarToElectrical. temp_kelvin) annotation (Line(
      points={{60,86},{86,86},{86,76},{72.57,76},{72.57,50.11}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(attachmentPoint.frame_a, Attachment) annotation (Line(
      points={{1,-2},{-48,-2},{-48,2},{-100,2}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Powertrain_Architecture01_A;
