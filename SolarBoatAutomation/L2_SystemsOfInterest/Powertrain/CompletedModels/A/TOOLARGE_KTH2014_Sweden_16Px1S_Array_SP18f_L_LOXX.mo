within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.CompletedModels.A;
model TOOLARGE_KTH2014_Sweden_16Px1S_Array_SP18f_L_LOXX
  extends PartialModels.A.Powertrain_Architecture01_A(redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.SP18f_L_LOXX_16Px1S_NoMPPT
      solarToElectrical, redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.KTH2014_160mm
      electricalToThrust);
end TOOLARGE_KTH2014_Sweden_16Px1S_Array_SP18f_L_LOXX;
