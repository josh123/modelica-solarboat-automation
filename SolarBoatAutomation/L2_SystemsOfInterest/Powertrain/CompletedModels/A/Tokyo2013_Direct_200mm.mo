within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.CompletedModels.A;
model Tokyo2013_Direct_200mm
  extends PartialModels.A.Powertrain_Architecture01_A(redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical, redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2013_Direct_200mm
      electricalToThrust);
end Tokyo2013_Direct_200mm;
