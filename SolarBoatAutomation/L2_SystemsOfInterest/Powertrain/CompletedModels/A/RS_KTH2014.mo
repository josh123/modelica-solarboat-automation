within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.CompletedModels.A;
model RS_KTH2014
  extends PartialModels.A.Powertrain_Architecture01_A(redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical, redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.KTH2014_160mm
      electricalToThrust);
end RS_KTH2014;
