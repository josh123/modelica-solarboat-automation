within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.CompletedModels.A;
model Direct_Turnigy_160mm
  extends PartialModels.A.Powertrain_Architecture01_A(redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical, redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Direct_160mm
      electricalToThrust);
end Direct_Turnigy_160mm;
