within SolarBoatAutomation.L2_SystemsOfInterest.Powertrain.CompletedModels.A;
model Tokyo2014_SP50f
  extends PartialModels.A.Powertrain_Architecture01_A(redeclare
      L3_Subsystems.SolarToElectrical.A.Complete.SP50f_L_LOXX_3Px2S_NoMPPT
      solarToElectrical, redeclare
      L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2014_160mm
      electricalToThrust);
end Tokyo2014_SP50f;
