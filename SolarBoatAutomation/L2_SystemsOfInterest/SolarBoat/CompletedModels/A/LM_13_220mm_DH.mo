within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.CompletedModels.A;
model LM_13_220mm_DH
  extends PartialModels.A.PartialSolarBoat_Architecture01_A(
    redeclare L3_Subsystems.OverheadSubsystems.Complete.OverheadComponents2015
      overheadComponents,
    redeclare L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical,
    redeclare
      L3_Subsystems.BuoyancyGeneration.Complete.DualDisplacementHull2014
      buoyancyGeneration,
    redeclare L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2015_220mm
      electricalToThrust);
  annotation (Icon(graphics={            Polygon(
          points={{-62,24},{-34,-4},{78,-4},{78,24},{-62,24}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end LM_13_220mm_DH;
