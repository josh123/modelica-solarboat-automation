within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.CompletedModels.A;
model HM_3_160mm
  extends PartialModels.A.PartialSolarBoat_Architecture01_A(
    redeclare L3_Subsystems.OverheadSubsystems.Complete.OverheadComponents2015
      overheadComponents,
    redeclare L3_Subsystems.SolarToElectrical.A.Complete.FT136SE_3Px2S_NoMPPT
      solarToElectrical,
    redeclare
      L3_Subsystems.BuoyancyGeneration.Complete.MonoDisplacementHull2014
      buoyancyGeneration,
    redeclare L3_Subsystems.ElectricalToThrust.A.Complete.Tokyo2013_160mm
      electricalToThrust);
  annotation (Icon(graphics={            Polygon(
          points={{-62,24},{-34,-4},{78,-4},{78,24},{-62,24}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end HM_3_160mm;
