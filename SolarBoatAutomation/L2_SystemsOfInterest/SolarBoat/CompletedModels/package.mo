within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat;
package CompletedModels "Package of complete SolarBoats"


  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Repository of SolarBoat alternative designs.</p>
</html>"));
end CompletedModels;
