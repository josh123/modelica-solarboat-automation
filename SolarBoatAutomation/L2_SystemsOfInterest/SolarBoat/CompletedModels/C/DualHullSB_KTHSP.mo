within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.CompletedModels.C;
model DualHullSB_KTHSP
  extends PartialModels.C.PartialSolarBoat_Architecture01(
    redeclare
      L3_Subsystems.SolarToElectrical.C.Complete.SolarToElectrical2014KTH
      solarToElectricalSubSystem,
    redeclare L3_Subsystems.ElectricalToThrust.C.Complete.MotorPod2014A
      electricalToThrust,
    redeclare L3_Subsystems.BuoyancyGeneration.Complete.DualHull
      buoyancyGeneration,
    redeclare L3_Subsystems.OverheadSubsystems.Complete.OverheadComponents2014A
      overheadComponents);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Polygon(
          points={{-60,20},{-32,-8},{80,-8},{80,20},{-60,20}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{-70,8},{-42,-20},{70,-20},{70,8},{-70,8}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end DualHullSB_KTHSP;
