within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.PartialModels.A;
partial model PartialSolarBoat_Architecture01_A
  extends C.PartialSolarBoat;
  replaceable
    L3_Subsystems.BuoyancyGeneration.Partial.PartialBuoyancyGeneration
    buoyancyGeneration
    annotation (Placement(transformation(extent={{-68,-86},{-2,-52}})));
  replaceable
    L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents
    overheadComponents
    annotation (Placement(transformation(extent={{-80,30},{-16,58}})));
  replaceable L3_Subsystems.SolarToElectrical.A.Partial.SolarToElectrical
    solarToElectrical
    annotation (Placement(transformation(extent={{36,24},{82,46}})));
  replaceable
    L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    electricalToThrust constrainedby
    L3_Subsystems.ElectricalToThrust.A.Partial.Partial_ThrusterMultiBody
    annotation (Placement(transformation(extent={{44,-44},{92,-20}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-18,-4},{2,16}})));
equation
  drag_force = buoyancyGeneration.drag_force;
  mass = buoyancyGeneration.mass_computed + solarToElectrical.mass_computed + electricalToThrust.mass_computed + overheadComponents.mass_computed;
  cost_money = buoyancyGeneration.cost_money_computed + solarToElectrical.cost_money_computed + electricalToThrust.cost_money_computed + overheadComponents.cost_money_computed;
  cost_manhours = buoyancyGeneration.cost_manhours_computed + solarToElectrical.cost_manhours_computed + electricalToThrust.cost_manhours_computed + overheadComponents.cost_manhours_computed;
  z_top_of_hull = buoyancyGeneration.z_top_of_hull;
  connect(overheadComponents.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{-48,30.28},{-62,30.28},{-62,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(buoyancyGeneration.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{-35,-85.66},{-35,-90},{22,-90},{22,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(electricalToThrust.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{68,-43.76},{56,-43.76},{56,-42},{40,-42},{40,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToElectrical.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{59,24.22},{59,4},{18,4},{18,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToElectrical.n, electricalToThrust.p) annotation (Line(
      points={{82,35.22},{86,35.22},{86,-4},{53.6,-4},{53.6,-20}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(solarToElectrical.p, electricalToThrust.n) annotation (Line(
      points={{36,35},{30,35},{30,10},{77.6,10},{77.6,-20.24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, electricalToThrust.n) annotation (Line(
      points={{-8,16},{30,16},{30,10},{77.6,10},{77.6,-20.24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(waterVelocityX, buoyancyGeneration.waterVelocityX) annotation (Line(
      points={{-106,-60},{-78,-60},{-78,-67.98},{-64.04,-67.98}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarIrradiance, solarToElectrical.solarInsolation) annotation (Line(
      points={{-40,104},{-40,70},{47.73,70},{47.73,46.33}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ambientTemperature, solarToElectrical.temp_kelvin) annotation (Line(
      points={{36,104},{36,76},{72.57,76},{72.57,46.11}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide an architecture which can have components replaced such that a model can be created.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<h4>OPM-Before layout</h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarBoat_Architecture01-OPM1.png\"/></p>
<h5>OPM-With layout</h5>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarBoat_Architecture01-OPM2.png\"/></p>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>Subsystems assumed not to interact other than on the interfaces</li>
<li>SolarBoat origin is used to make measurements (e.g. speed)</li>
<li>Attributes such as <code>mass, cost_money </code>and<code> cost_manhours </code>are sumations of the sub system attributes</li>
</ul>
<h4>Equations</h4>
<ul>
<li><code>mass </code>is the summation of the sub system masses</li>
<li><code>cost_money </code>is the summation of the sub system <code>cost_money</code></li>
<li><code>cost_manhours </code>is the summation of the sub system <code>cost_manhours</code></li>
<li><code>drag_force </code>is the summation of the sub systems with <code>drag_force</code></li>
</ul>
<h4><span style=\"color:#008000\">Known issues</span></h4>
<ul>
<li>There is no mention of the extra components which are in the partial model.</li>
</ul>
</html>"));
end PartialSolarBoat_Architecture01_A;
