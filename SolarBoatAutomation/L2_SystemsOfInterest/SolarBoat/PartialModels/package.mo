within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat;
package PartialModels "Package of SolarBoats which are compliant with the SimulationHarnesses"


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide a common architectures to build SolarBoat models.</p>
</html>"));
end PartialModels;
