within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.PartialModels.C;
partial model PartialSolarBoat
  "Provides all the common attributes to all SolarBoat designs"

  Modelica.Blocks.Interfaces.RealInput solarIrradiance(final unit="W.m-2") annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-40,104}),                        iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-54,88})));
  Modelica.SIunits.Mass mass "Total boat mass";
  Modelica.SIunits.Force drag_force "Force caused by moving through the water";
  Real cost_money(unit="yen") "Predicted cost of boat";
  Real cost_manhours(unit="hours") "Predicted time to build boat";
  Modelica.SIunits.Velocity x_velocity
    "x direction velocity in world frame of reference";
  Modelica.SIunits.Position z_top_of_hull
    "z position of the hull in the world frame of reference";
  L3_Subsystems.AttachmentPoint.Complete.AttachmentPoint attachmentPoint
    annotation (Placement(transformation(extent={{-20,-20},{22,8}})));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a payloadAttachment
    annotation (Placement(transformation(extent={{-114,-22},{-82,10}})));
  Modelica.Blocks.Interfaces.RealInput ambientTemperature(final unit="K")
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={36,104}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={36,88})));
  Modelica.Blocks.Interfaces.RealInput waterVelocityX(final unit="m.s-1")
    "Water velocity in the x direction" annotation (Placement(transformation(
          extent={{-126,-80},{-86,-40}}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-90,-62})));
equation
  x_velocity =  attachmentPoint.x_velocity;
  connect(payloadAttachment, attachmentPoint.frame_a) annotation (Line(
      points={{-98,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
                                          Rectangle(
          extent={{-100,0},{100,-100}},
          lineColor={0,0,0},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-50,-80},{50,-30}},
          lineColor={0,0,0},
          textString="%name")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Ensure all SolarBoat alternatives populate the same variables.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarBoat-OPM.png\"/></p>
</html>"));
end PartialSolarBoat;
