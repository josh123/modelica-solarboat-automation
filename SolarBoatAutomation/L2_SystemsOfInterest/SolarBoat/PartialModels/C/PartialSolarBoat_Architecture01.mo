within SolarBoatAutomation.L2_SystemsOfInterest.SolarBoat.PartialModels.C;
partial model PartialSolarBoat_Architecture01
  "An example architecture for SolarBoat"
  extends C.PartialSolarBoat;
  replaceable L3_Subsystems.SolarToElectrical.C.Partial.SolarToElectrical
    solarToElectricalSubSystem
    annotation (Placement(transformation(extent={{-18,8},{36,52}})));
  replaceable
    L3_Subsystems.ElectricalToThrust.C.Partial.PartialElectricalToThrust
    electricalToThrust
    annotation (Placement(transformation(extent={{42,-62},{92,-22}})));
  replaceable
    L3_Subsystems.BuoyancyGeneration.Partial.PartialBuoyancyGeneration
    buoyancyGeneration
    annotation (Placement(transformation(extent={{-68,-86},{-12,-54}})));
  replaceable
    L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents
    overheadComponents
    annotation (Placement(transformation(extent={{-80,30},{-40,62}})));
equation
  drag_force = buoyancyGeneration.drag_force;
  mass = buoyancyGeneration.mass_computed + solarToElectricalSubSystem.mass_computed + electricalToThrust.mass_computed + overheadComponents.mass_computed;
  cost_money = buoyancyGeneration.cost_money_computed + solarToElectricalSubSystem.cost_money_computed + electricalToThrust.cost_money_computed + overheadComponents.cost_money_computed;
  cost_manhours = buoyancyGeneration.cost_manhours_computed + solarToElectricalSubSystem.cost_manhours_computed + electricalToThrust.cost_manhours_computed + overheadComponents.cost_manhours_computed;
  z_top_of_hull = buoyancyGeneration.z_top_of_hull;
  connect(solarToElectricalSubSystem.frame_a, attachmentPoint.frame_a) annotation (
     Line(
      points={{9,8.44},{9,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarToElectricalSubSystem.powerDelivered, electricalToThrust.electricalPowerInput)
    annotation (Line(
      points={{36,30},{62,30},{62,-20.8},{63.5,-20.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(electricalToThrust.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{67,-61.6},{67,-68},{22,-68},{22,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(overheadComponents.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{-60,30.32},{-62,30.32},{-62,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(buoyancyGeneration.frame_a, attachmentPoint.frame_a) annotation (Line(
      points={{-40,-85.68},{-40,-90},{22,-90},{22,-6},{1,-6}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(waterVelocityX, buoyancyGeneration.waterVelocityX) annotation (Line(
      points={{-106,-60},{-78,-60},{-78,-69.04},{-64.64,-69.04}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarIrradiance, solarToElectricalSubSystem.solarInsolation)
    annotation (Line(
      points={{-40,104},{-40,72},{9.27,72},{9.27,52.66}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide an architecture which can have components replaced such that a model can be created.</p>
<h4><span style=\"color:#008000\">OPM</span></h4>
<h4>OPM-Before layout</h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarBoat_Architecture01-OPM1.png\"/></p>
<h5>OPM-With layout</h5>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarBoat_Architecture01-OPM2.png\"/></p>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>Subsystems assumed not to interact other than on the interfaces</li>
<li>SolarBoat origin is used to make measurements (e.g. speed)</li>
<li>Attributes such as <code>mass, cost_money </code>and<code> cost_manhours </code>are sumations of the sub system attributes</li>
</ul>
<h4>Equations</h4>
<ul>
<li><code>mass </code>is the summation of the sub system masses</li>
<li><code>cost_money </code>is the summation of the sub system <code>cost_money</code></li>
<li><code>cost_manhours </code>is the summation of the sub system <code>cost_manhours</code></li>
<li><code>drag_force </code>is the summation of the sub systems with <code>drag_force</code></li>
</ul>
<h4><span style=\"color:#008000\">Known issues</span></h4>
<ul>
<li>There is no mention of the extra components which are in the partial model.</li>
</ul>
</html>"));
end PartialSolarBoat_Architecture01;
