within SolarBoatAutomation.L2_SystemsOfInterest;
package SolarBoat "Package containing complete SolarBoats"


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>Provide multiple SolarBoat designs which are compliant with the same interface for the SimulationHarnesses.</p>
</html>"));
end SolarBoat;
