within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.SpecSheets;
partial record Partial_DisplacementHullDesign
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";

  parameter Modelica.SIunits.Distance length "Length of the main hull";
  parameter Modelica.SIunits.Distance width "Width of the main hull";
  parameter Modelica.SIunits.Distance height "Height of the main hull";

  parameter Modelica.SIunits.Distance standard_section_length
    "Length of standard section of hull";
  parameter Real standard_section_volume_reduction
    "Volume reduction from standard box for standard hull width";
  parameter Modelica.SIunits.Distance narrow_section_length
    "Length of narrowing hull";
  parameter Real narrow_section_volume_reduction
    "Volume reduction of narrowing section of hull";

  parameter Modelica.SIunits.Density density "Density of the main hull";

end Partial_DisplacementHullDesign;
