within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.SpecSheets;
record DisplacementHull2014
  extends Partial_DisplacementHullDesign(cost_money = 20000,
    cost_manhours = 1,
    length = 2.3,
    width = 0.17,
    height = 0.19,
    standard_section_length = 1.6,
    standard_section_volume_reduction = 0.7,
    narrow_section_length =  0.6,
    narrow_section_volume_reduction =  0.5*0.7,
    density = 27.9);
end DisplacementHull2014;
