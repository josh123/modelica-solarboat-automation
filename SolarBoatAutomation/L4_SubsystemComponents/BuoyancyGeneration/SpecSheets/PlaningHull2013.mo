within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.SpecSheets;
record PlaningHull2013
  extends Partial_PlaningHullDesign(cost_money = 1,
    cost_manhours = 1,
    length = 2.3,
    width =  0.17,
    height = 0.19,
    standard_section_length =  1.6,
    standard_section_volume_reduction =  0.7,
    narrow_section_length =  0.6,
    narrow_section_volume_reduction =  0.5*0.7,
    planing_section_length =  0.66,
    alpha_attack_angle = 2,
    density = 27.9);
end PlaningHull2013;
