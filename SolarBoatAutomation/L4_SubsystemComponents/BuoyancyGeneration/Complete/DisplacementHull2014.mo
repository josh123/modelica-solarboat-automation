within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.Complete;
model DisplacementHull2014
  extends Partial.DisplacementHull(redeclare SpecSheets.DisplacementHull2014
      partial_DisplacementHullDesign);
end DisplacementHull2014;
