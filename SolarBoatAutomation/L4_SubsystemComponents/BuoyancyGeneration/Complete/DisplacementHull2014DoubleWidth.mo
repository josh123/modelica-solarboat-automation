within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.Complete;
model DisplacementHull2014DoubleWidth
  extends Partial.DisplacementHull(redeclare
      SpecSheets.DisplacementHull2014DoubleWidth
      partial_DisplacementHullDesign);
end DisplacementHull2014DoubleWidth;
