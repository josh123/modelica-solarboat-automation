within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.PartialModels;
partial model PartialRigidHull
  //Provides all of the common attributes of a rigid hull
  //TODO - JCBS But back the assertion
  //assert(frame_a.r_0[3] < 1, "Main hull is under 1 metre of water"); //Check that the main hull has not sunk
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L3_Subsystems.BuoyancyGeneration.Partial.PartialBuoyancyGeneration;
end PartialRigidHull;
