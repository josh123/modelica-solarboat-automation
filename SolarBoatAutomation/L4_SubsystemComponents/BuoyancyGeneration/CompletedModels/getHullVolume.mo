within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.CompletedModels;
function getHullVolume
  "Returns the volume of the hull from hull bottom to the postion named water_level"
  input Modelica.SIunits.Distance water_level
    "Water level from the hull bottom";
  input Modelica.SIunits.Distance width "Width of the hull";
  input Modelica.SIunits.Distance standard_section_length
    "Length of standard section of hull";
  input Real standard_section_volume_reduction
    "Volume reduction from standard box for standard hull width";
  input Modelica.SIunits.Distance narrow_section_length
    "Length of narrowing hull";
  input Real narrow_section_volume_reduction
    "Volume reduction of narrowing section of hull";
  output Modelica.SIunits.Volume volume
    "Volume from hull bottom to water_level position";
algorithm
  volume :=standard_section_volume_reduction*standard_section_length*width*
    water_level + narrow_section_volume_reduction*narrow_section_length*width*
    water_level;
end getHullVolume;
