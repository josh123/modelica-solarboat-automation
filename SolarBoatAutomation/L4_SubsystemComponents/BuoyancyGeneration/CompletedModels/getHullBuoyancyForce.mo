within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.CompletedModels;
function getHullBuoyancyForce
  "Computes the buoyancy force of the hull based on displaced volume. Acts in negative z direction."
  // Assumes center of mass is at middle of the height of the main hull
  input Modelica.SIunits.Position centre_mass_z_position;
  input Modelica.SIunits.Length hull_height "Height of the main hull";
  input Modelica.SIunits.Distance length "Length of the main hull";
  input Modelica.SIunits.Distance width "Width of the main hull";
  input Modelica.SIunits.Distance standard_section_length
    "Length of standard section of hull";
  input Real standard_section_volume_reduction
    "Volume reduction from standard box for standard hull width";
  input Modelica.SIunits.Distance narrow_section_length
    "Length of narrowing hull";
  input Real narrow_section_volume_reduction
    "Volume reduction of narrowing section of hull";
  output Modelica.SIunits.Force buoyancy_force;
  output Modelica.SIunits.Volume displaced_volume;
  output Modelica.SIunits.Length water_level_from_hull_bottom;
algorithm
  if centre_mass_z_position > (hull_height/2) then //Fully submerged
    displaced_volume :=getHullVolume(
      hull_height,
      width,
      standard_section_length,
      narrow_section_volume_reduction,
      narrow_section_length,
      narrow_section_volume_reduction);
      water_level_from_hull_bottom :=centre_mass_z_position + hull_height/2;
  elseif centre_mass_z_position < -(hull_height/2) then //Fully out of water
    displaced_volume :=0;
    water_level_from_hull_bottom := 0;
  else //Somewhere in between
    displaced_volume :=getHullVolume(
      ((hull_height/2) + centre_mass_z_position),
      width,
      standard_section_length,
      narrow_section_volume_reduction,
      narrow_section_length,
      narrow_section_volume_reduction);
      water_level_from_hull_bottom :=centre_mass_z_position + hull_height/2;
  end if;
  buoyancy_force :=-(displaced_volume*Modelica.Constants.g_n*
    SolarBoatAutomation.Helpers.Constants.water_density);
end getHullBuoyancyForce;
