within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.CompletedModels;
function getDragCoefficentDisplacementForm
  output Real cd_drag_coefficent_form "Displacement hull form drag coefficent";
algorithm
  cd_drag_coefficent_form := 0.8e-3
    "Assumes is constant for all Reynolds number";
end getDragCoefficentDisplacementForm;
