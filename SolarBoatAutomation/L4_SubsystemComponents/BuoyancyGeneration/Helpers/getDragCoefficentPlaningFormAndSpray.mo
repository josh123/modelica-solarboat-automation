within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.Helpers;
function getDragCoefficentPlaningFormAndSpray
  output Real cd_drag_coefficent_form_and_spray
    "Planing hull form and spray drag coefficent";
algorithm
  cd_drag_coefficent_form_and_spray := 3.06e-4
    "Assumes they can be grouped together";
end getDragCoefficentPlaningFormAndSpray;
