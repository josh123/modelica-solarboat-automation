within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.Helpers;
function getLiftCoefficentPlaningHull
  input Real fn "Froude number for planing hulls";
  input Real a_aspect_ratio_planing_area;
  input Modelica.SIunits.Angle alpha_attack_angle "Angle of attack in degrees";
  input Modelica.SIunits.Velocity velocity;
  input Modelica.SIunits.Position hull_bottom_position;
  output Real cl_lift_coefficent "Lift coefficent for planing";
  output Real cd_drag_coefficent_lift_induced
    "Drag coefficent of lift induced drag";
  output Real fn_over_sqrt_a "Used to check equation is in suitable regime";
algorithm
  fn_over_sqrt_a :=fn/sqrt(a_aspect_ratio_planing_area);
  if (velocity == 0) or (fn_over_sqrt_a < 0.6) or (hull_bottom_position < 0) then
    cl_lift_coefficent :=0;
    cd_drag_coefficent_lift_induced :=0;
  else
    cl_lift_coefficent :=sqrt(a_aspect_ratio_planing_area)*(alpha_attack_angle^1.1)
      *(0.012 + (0.0055/(a_aspect_ratio_planing_area*(fn^2))));
    cd_drag_coefficent_lift_induced := alpha_attack_angle*cl_lift_coefficent;
  end if;
end getLiftCoefficentPlaningHull;
