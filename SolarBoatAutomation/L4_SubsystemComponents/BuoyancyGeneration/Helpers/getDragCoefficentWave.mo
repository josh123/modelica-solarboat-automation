within SolarBoatAutomation.L4_SubsystemComponents.BuoyancyGeneration.Helpers;
function getDragCoefficentWave
  input Modelica.SIunits.FroudeNumber fn;
  output Real cd_drag_coefficent_wave "Hull form and spray drag coefficent";
protected
  Real temp;
algorithm
  temp :=(0.1692*fn^6) - (0.7554*fn^5) + (1.277*fn^4) - (0.9998
    *fn^3) + (0.3416*fn^2) - (0.0317*fn) + 0.0008;
  if (temp < 0) or (fn >=1.4) then
    cd_drag_coefficent_wave :=0;
  else
    cd_drag_coefficent_wave := temp;
  end if;
end getDragCoefficentWave;
