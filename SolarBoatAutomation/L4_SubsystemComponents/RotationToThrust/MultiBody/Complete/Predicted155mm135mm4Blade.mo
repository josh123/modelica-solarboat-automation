within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.MultiBody.Complete;
model Predicted155mm135mm4Blade
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted155mm135mm4Blade
      partial_PropellerDesign);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Polygon(
          points={{-40,60},{40,-60},{20,-80},{0,0},{-20,60},{-40,60}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),Polygon(
          points={{-40,70},{40,-50},{20,-70},{0,10},{-20,70},{-40,70}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          origin={-10,0},
          rotation=-90)}));
end Predicted155mm135mm4Blade;
