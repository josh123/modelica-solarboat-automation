within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.MultiBody.Complete;
model Predicted220mm343mm
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.MultiBody.Partial.PropellerMultiBody(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted220mm343mm
      partial_PropellerDesign);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Polygon(
          points={{-40,60},{40,-60},{20,-80},{0,0},{-20,60},{-40,60}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Predicted220mm343mm;
