within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Rotational.Complete;
model Predicted200mm343mm
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Rotational.Partial.PropellerRotational(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted200mm343mm
      partial_PropellerDesign);
end Predicted200mm343mm;
