within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Rotational.Complete;
model Predicted160mm135mm
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Rotational.Partial.PropellerRotational(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted160mm135mm
      partial_PropellerDesign);
end Predicted160mm135mm;
