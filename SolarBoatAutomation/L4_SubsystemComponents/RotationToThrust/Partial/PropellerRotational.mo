within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Partial;
partial model PropellerRotational
  extends Modelica.Mechanics.Rotational.Interfaces.PartialTorque;

  parameter Modelica.SIunits.Length diameter = partial_PropellerDesign.diameter;
  parameter Modelica.SIunits.Length pitch = partial_PropellerDesign.pitch;
  parameter Modelica.SIunits.Density water_density = partial_PropellerDesign.water_density;

  parameter Real torque_coefficient_kq_4_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_4_term_coefficient;
  parameter Real torque_coefficient_kq_3_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_3_term_coefficient;
  parameter Real torque_coefficient_kq_2_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_2_term_coefficient;
  parameter Real torque_coefficient_kq_1_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_1_term_coefficient;
  parameter Real torque_coefficient_kq_0_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_0_term_coefficient;

  parameter Real thrust_coefficient_kq_4_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_4_term_coefficient;
  parameter Real thrust_coefficient_kq_3_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_3_term_coefficient;
  parameter Real thrust_coefficient_kq_2_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_2_term_coefficient;
  parameter Real thrust_coefficient_kq_1_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_1_term_coefficient;
  parameter Real thrust_coefficient_kq_0_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_0_term_coefficient;

  parameter Real efficency_4_term_coefficient = partial_PropellerDesign.efficency_4_term_coefficient;
  parameter Real efficency_3_term_coefficient = partial_PropellerDesign.efficency_3_term_coefficient;
  parameter Real efficency_2_term_coefficient = partial_PropellerDesign.efficency_2_term_coefficient;
  parameter Real efficency_1_term_coefficient = partial_PropellerDesign.efficency_1_term_coefficient;
  parameter Real efficency_0_term_coefficient = partial_PropellerDesign.efficency_0_term_coefficient;

  Modelica.SIunits.AngularVelocity w
    "Angular velocity of flange with respect to support (= der(phi))";
  Modelica.SIunits.Torque tau
    "Accelerating torque acting at flange (= -flange.tau)";
  Real angular_velocity_rotaion_per_second_n
    "Angular velocity in the units of rotations per second";
  Modelica.SIunits.Velocity advance_velocity_va;
  Real advance_coefficient_j;
  Real torque_coefficient_kq;
  Real thrust_coefficient_kt;
  Modelica.SIunits.Force thrust;
  Modelica.SIunits.Efficiency efficency;
  Modelica.SIunits.Power power_consumption;

  replaceable SpecSheets.Partial_PropellerDesign partial_PropellerDesign
    annotation (Placement(transformation(extent={{40,58},{60,78}})));
  Modelica.Blocks.Interfaces.RealInput set_velocity
    annotation (Placement(transformation(extent={{-122,22},{-82,62}})));
equation
  w = der(phi);
  tau = -flange.tau;
  angular_velocity_rotaion_per_second_n = w/(2*Modelica.Constants.pi);
  advance_velocity_va =  set_velocity;
  power_consumption = tau * w;
  if (angular_velocity_rotaion_per_second_n == 0) then
    advance_coefficient_j = 0;
    torque_coefficient_kq = 0;
    thrust_coefficient_kt = 0;
    efficency = 0;
    tau = 0;
    thrust = 0;
  else
    advance_coefficient_j = advance_velocity_va/(angular_velocity_rotaion_per_second_n*diameter);
    //Get coefficients from polynomials
    torque_coefficient_kq = ((torque_coefficient_kq_4_term_coefficient*(advance_coefficient_j^4))
      +(torque_coefficient_kq_3_term_coefficient*(advance_coefficient_j^3))
      +(torque_coefficient_kq_2_term_coefficient*(advance_coefficient_j^2))
      +(torque_coefficient_kq_1_term_coefficient*(advance_coefficient_j))
      +torque_coefficient_kq_0_term_coefficient);
    thrust_coefficient_kt = ((thrust_coefficient_kq_4_term_coefficient*(advance_coefficient_j^4))
      +(thrust_coefficient_kq_3_term_coefficient*(advance_coefficient_j^3))
      +(thrust_coefficient_kq_2_term_coefficient*(advance_coefficient_j^2))
      +(thrust_coefficient_kq_1_term_coefficient*(advance_coefficient_j))
      +thrust_coefficient_kq_0_term_coefficient);
    efficency = ((efficency_4_term_coefficient*(advance_coefficient_j^4))
      +(efficency_3_term_coefficient*(advance_coefficient_j^3))
      +(efficency_2_term_coefficient*(advance_coefficient_j^2))
      +(efficency_1_term_coefficient*(advance_coefficient_j))
      +efficency_0_term_coefficient);
    //Compute the torque and thrust generated
    tau = torque_coefficient_kq*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^5);
    thrust = thrust_coefficient_kt*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^4);
  end if;

  annotation (
    Icon(
      coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100.0,-100.0},{100.0,100.0}},
        initialScale=0.1),
      graphics={
        Line(
          points={{-100.0,-100.0},{-80.0,-98.0},{-60.0,-92.0},
            {-40.0,-82.0},{-20.0,-68.0},{0.0,-50.0},{20.0,-28.0},
            {40.0,-2.0},{60.0,28.0},{80.0,62.0},{100.0,100.0}},
          color={0,0,127}, smooth=Smooth.Bezier)}),
    Documentation(info="<HTML>
<p>
Model of torque, quadratic dependent on angular velocity of flange.<br>
Parameter TorqueDirection chooses whether direction of torque is the same in both directions of rotation or not.
</p>
</HTML>"),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics));
end PropellerRotational;
