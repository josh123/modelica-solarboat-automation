within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Partial;
partial model PropellerMultiBody

  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;

  parameter Modelica.SIunits.Length diameter = partial_PropellerDesign.diameter;
  parameter Modelica.SIunits.Length pitch = partial_PropellerDesign.pitch;
  parameter Modelica.SIunits.Density water_density = partial_PropellerDesign.water_density;

  parameter Real torque_coefficient_kq_4_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_4_term_coefficient;
  parameter Real torque_coefficient_kq_3_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_3_term_coefficient;
  parameter Real torque_coefficient_kq_2_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_2_term_coefficient;
  parameter Real torque_coefficient_kq_1_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_1_term_coefficient;
  parameter Real torque_coefficient_kq_0_term_coefficient = partial_PropellerDesign.torque_coefficient_kq_0_term_coefficient;

  parameter Real thrust_coefficient_kq_4_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_4_term_coefficient;
  parameter Real thrust_coefficient_kq_3_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_3_term_coefficient;
  parameter Real thrust_coefficient_kq_2_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_2_term_coefficient;
  parameter Real thrust_coefficient_kq_1_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_1_term_coefficient;
  parameter Real thrust_coefficient_kq_0_term_coefficient = partial_PropellerDesign.thrust_coefficient_kq_0_term_coefficient;

  parameter Real efficency_4_term_coefficient = partial_PropellerDesign.efficency_4_term_coefficient;
  parameter Real efficency_3_term_coefficient = partial_PropellerDesign.efficency_3_term_coefficient;
  parameter Real efficency_2_term_coefficient = partial_PropellerDesign.efficency_2_term_coefficient;
  parameter Real efficency_1_term_coefficient = partial_PropellerDesign.efficency_1_term_coefficient;
  parameter Real efficency_0_term_coefficient = partial_PropellerDesign.efficency_0_term_coefficient;

  Modelica.SIunits.AngularVelocity w
    "Angular velocity of flange with respect to support (= der(phi))";
  Modelica.SIunits.Torque torque_generated
    "Accelerating torque acting at flange (= -flange.tau)";
  Real angular_velocity_rotaion_per_second_n
    "Angular velocity in the units of rotations per second";
  Modelica.SIunits.Velocity advance_velocity_va;
  Real advance_coefficient_j;
  Real torque_coefficient_kq;
  Real thrust_coefficient_kt;
  Modelica.SIunits.Force thrust_generated;
  Modelica.SIunits.Efficiency efficency;
  Modelica.SIunits.Power power_consumption;

  Modelica.Mechanics.MultiBody.Forces.WorldForce force
    annotation (Placement(transformation(extent={{14,34},{-6,54}})));
  Modelica.Mechanics.MultiBody.Forces.WorldTorque torque
    annotation (Placement(transformation(extent={{12,-24},{-8,-4}})));
  Modelica.Blocks.Sources.RealExpression x_force(y=sign(w)*thrust_generated)
    annotation (Placement(transformation(extent={{84,34},{48,54}})));
  Modelica.Blocks.Sources.RealExpression y_force(y=0)
    annotation (Placement(transformation(extent={{84,14},{64,34}})));
  Modelica.Blocks.Sources.RealExpression z_force(y=0)
    annotation (Placement(transformation(extent={{84,-6},{64,14}})));
  Modelica.Blocks.Sources.RealExpression x_torque(y=-sign(w)*torque_generated)
    annotation (Placement(transformation(extent={{82,-26},{46,-6}})));
  Modelica.Blocks.Sources.RealExpression y_torque(y=0)
    annotation (Placement(transformation(extent={{82,-46},{62,-26}})));
  Modelica.Blocks.Sources.RealExpression z_torque(y=0)
    annotation (Placement(transformation(extent={{82,-66},{62,-46}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{-78,62},{-58,82}})));
  replaceable SpecSheets.Partial_PropellerDesign partial_PropellerDesign
    annotation (Placement(transformation(extent={{32,68},{52,88}})));
  Modelica.Mechanics.MultiBody.Parts.Body propellerMass(sphereDiameter=0.3, m=
        partial_PropellerDesign.mass) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={24,-70})));
equation
  partial_PropellerDesign.mass = mass_computed;
  partial_PropellerDesign.cost_money = cost_money_computed;
  partial_PropellerDesign.cost_manhours = cost_manhours_computed;

  w =  frame_a.R.w[1];
  angular_velocity_rotaion_per_second_n = w/(2*Modelica.Constants.pi);
  advance_velocity_va =  absoluteVelocity.v[1];
  power_consumption = torque_generated * w;
  if (angular_velocity_rotaion_per_second_n == 0) then
    advance_coefficient_j = 0;
    torque_coefficient_kq = 0;
    thrust_coefficient_kt = 0;
    efficency = 0;
    torque_generated = 0;
    thrust_generated = 0;
  else
    advance_coefficient_j = advance_velocity_va/(angular_velocity_rotaion_per_second_n*diameter);
    //Get coefficients from polynomials
    torque_coefficient_kq = ((torque_coefficient_kq_4_term_coefficient*(advance_coefficient_j^4))
      +(torque_coefficient_kq_3_term_coefficient*(advance_coefficient_j^3))
      +(torque_coefficient_kq_2_term_coefficient*(advance_coefficient_j^2))
      +(torque_coefficient_kq_1_term_coefficient*(advance_coefficient_j))
      +torque_coefficient_kq_0_term_coefficient);
    thrust_coefficient_kt = ((thrust_coefficient_kq_4_term_coefficient*(advance_coefficient_j^4))
      +(thrust_coefficient_kq_3_term_coefficient*(advance_coefficient_j^3))
      +(thrust_coefficient_kq_2_term_coefficient*(advance_coefficient_j^2))
      +(thrust_coefficient_kq_1_term_coefficient*(advance_coefficient_j))
      +thrust_coefficient_kq_0_term_coefficient);
    efficency = ((efficency_4_term_coefficient*(advance_coefficient_j^4))
      +(efficency_3_term_coefficient*(advance_coefficient_j^3))
      +(efficency_2_term_coefficient*(advance_coefficient_j^2))
      +(efficency_1_term_coefficient*(advance_coefficient_j))
      +efficency_0_term_coefficient);
    //Compute the torque and thrust generated
    torque_generated = torque_coefficient_kq*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^5);
    thrust_generated = thrust_coefficient_kt*water_density*(angular_velocity_rotaion_per_second_n^2)*(diameter^4);
  end if;

  connect(x_force.y, force.force[1]) annotation (Line(
      points={{46.2,44},{40,44},{40,42.6667},{16,42.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y_force.y, force.force[2]) annotation (Line(
      points={{63,24},{40,24},{40,44},{16,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z_force.y, force.force[3]) annotation (Line(
      points={{63,4},{40,4},{40,45.3333},{16,45.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(x_torque.y, torque.torque[1]) annotation (Line(
      points={{44.2,-16},{38,-16},{38,-15.3333},{14,-15.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(y_torque.y, torque.torque[2]) annotation (Line(
      points={{61,-36},{38,-36},{38,-14},{14,-14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(z_torque.y, torque.torque[3]) annotation (Line(
      points={{61,-56},{38,-56},{38,-12.6667},{14,-12.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(torque.frame_b, frame_a) annotation (Line(
      points={{-8,-14},{-46,-14},{-46,-72},{0,-72},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(force.frame_b, frame_a) annotation (Line(
      points={{-6,44},{-46,44},{-46,-72},{0,-72},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(absoluteVelocity.frame_a, frame_a) annotation (Line(
      points={{-78,72},{-78,10},{-46,10},{-46,-72},{0,-72},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(propellerMass.frame_a, frame_a) annotation (Line(
      points={{24,-80},{24,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PropellerMultiBody;
