within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Complete.Rotational;
model Predicted160mm135mm
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Partial.PropellerRotational(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted160mm135mm
      partial_PropellerDesign);
end Predicted160mm135mm;
