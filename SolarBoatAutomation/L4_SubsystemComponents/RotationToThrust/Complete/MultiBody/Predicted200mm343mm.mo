within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Complete.MultiBody;
model Predicted200mm343mm
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Partial.PropellerMultiBody(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets.Predicted200mm343mm
      partial_PropellerDesign);
end Predicted200mm343mm;
