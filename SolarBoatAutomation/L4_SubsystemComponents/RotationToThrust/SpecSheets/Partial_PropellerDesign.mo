within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets;
partial record Partial_PropellerDesign
  parameter Modelica.SIunits.Length diameter;
  parameter Modelica.SIunits.Length pitch;
  parameter Modelica.SIunits.Density water_density;

  parameter Real torque_coefficient_kq_4_term_coefficient;
  parameter Real torque_coefficient_kq_3_term_coefficient;
  parameter Real torque_coefficient_kq_2_term_coefficient;
  parameter Real torque_coefficient_kq_1_term_coefficient;
  parameter Real torque_coefficient_kq_0_term_coefficient;

  parameter Real thrust_coefficient_kt_4_term_coefficient;
  parameter Real thrust_coefficient_kt_3_term_coefficient;
  parameter Real thrust_coefficient_kt_2_term_coefficient;
  parameter Real thrust_coefficient_kt_1_term_coefficient;
  parameter Real thrust_coefficient_kt_0_term_coefficient;

  parameter Real efficency_4_term_coefficient;
  parameter Real efficency_3_term_coefficient;
  parameter Real efficency_2_term_coefficient;
  parameter Real efficency_1_term_coefficient;
  parameter Real efficency_0_term_coefficient;

  //Mass
  parameter Modelica.SIunits.Mass mass "Mass in kg of the panel";
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";

end Partial_PropellerDesign;
