within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets;
record Predicted160mm135mm
  extends SpecSheets.Partial_PropellerDesign(
                                  diameter = 0.16,
    pitch = 0.135,
    water_density = 1000,
    torque_coefficient_kq_4_term_coefficient = 0,
    torque_coefficient_kq_3_term_coefficient = -0.0112,
    torque_coefficient_kq_2_term_coefficient = -0.0071,
    torque_coefficient_kq_1_term_coefficient = 0.0047,
    torque_coefficient_kq_0_term_coefficient = 0.0082,
    thrust_coefficient_kt_4_term_coefficient = 0,
    thrust_coefficient_kt_3_term_coefficient = 0.0235,
    thrust_coefficient_kt_2_term_coefficient = -0.1101,
    thrust_coefficient_kt_1_term_coefficient = -0.1026,
    thrust_coefficient_kt_0_term_coefficient = 0.1504,
    efficency_4_term_coefficient = -7.1391,
    efficency_3_term_coefficient = 11.2650,
    efficency_2_term_coefficient = -7.7412,
    efficency_1_term_coefficient = 3.6160,
    efficency_0_term_coefficient = -0.0325,
    mass = 0.02,
    cost_money = 0,
    cost_manhours = 1);
    //TODO Water density should come from the constants

end Predicted160mm135mm;
