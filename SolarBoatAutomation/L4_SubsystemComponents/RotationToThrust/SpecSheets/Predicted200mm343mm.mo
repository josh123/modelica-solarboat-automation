within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets;
record Predicted200mm343mm
  extends SpecSheets.Partial_PropellerDesign(
                                  diameter = 0.2,
    pitch = 0.343,
    water_density = 1000,
    torque_coefficient_kq_4_term_coefficient = 0,
    torque_coefficient_kq_3_term_coefficient = -0.005,
    torque_coefficient_kq_2_term_coefficient = -0.0057,
    torque_coefficient_kq_1_term_coefficient = 0.0142,
    torque_coefficient_kq_0_term_coefficient = 0.0180,
    thrust_coefficient_kt_4_term_coefficient = 0,
    thrust_coefficient_kt_3_term_coefficient = 0.0092,
    thrust_coefficient_kt_2_term_coefficient = -0.0493,
    thrust_coefficient_kt_1_term_coefficient = -0.0865,
    thrust_coefficient_kt_0_term_coefficient = 0.2460,
    efficency_4_term_coefficient = -0.4181,
    efficency_3_term_coefficient = 1.6291,
    efficency_2_term_coefficient = -2.6383,
    efficency_1_term_coefficient = 2.3264,
    efficency_0_term_coefficient = -0.0164,
    mass = 0.02,
    cost_money = 0,
    cost_manhours = 1);

end Predicted200mm343mm;
