within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets;
record Predicted220mm343mm
  extends SpecSheets.Partial_PropellerDesign(
    diameter = 0.220,
    pitch = 0.343,
    water_density = 1000,
    torque_coefficient_kq_4_term_coefficient = 0,
    torque_coefficient_kq_3_term_coefficient = -0.0052,
    torque_coefficient_kq_2_term_coefficient = -0.0053,
    torque_coefficient_kq_1_term_coefficient = 0.0119,
    torque_coefficient_kq_0_term_coefficient = 0.0142,
    thrust_coefficient_kt_4_term_coefficient = 0,
    thrust_coefficient_kt_3_term_coefficient = 0.0107,
    thrust_coefficient_kt_2_term_coefficient = -0.0519,
    thrust_coefficient_kt_1_term_coefficient = -0.0811,
    thrust_coefficient_kt_0_term_coefficient = 0.2112,
    efficency_4_term_coefficient = -0.6098,
    efficency_3_term_coefficient = 2.1460,
    efficency_2_term_coefficient = -3.1532,
    efficency_1_term_coefficient = 2.5396,
    efficency_0_term_coefficient = -0.0173,
    mass = 0.02,
    cost_money = 0,
    cost_manhours = 1);
    //TODO Water density should come from the constants

end Predicted220mm343mm;
