within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.SpecSheets;
record Predicted155mm135mm4Blade
  extends SpecSheets.Partial_PropellerDesign(
                                  diameter = 0.155,
    pitch = 0.135,
    water_density = 1000,
    torque_coefficient_kq_4_term_coefficient = 0,
    torque_coefficient_kq_3_term_coefficient = -0.0222,
    torque_coefficient_kq_2_term_coefficient = -0.0147,
    torque_coefficient_kq_1_term_coefficient = 0.0101,
    torque_coefficient_kq_0_term_coefficient = 0.0179,
    thrust_coefficient_kt_4_term_coefficient = 0,
    thrust_coefficient_kt_3_term_coefficient = 0.0451,
    thrust_coefficient_kt_2_term_coefficient = -0.2188,
    thrust_coefficient_kt_1_term_coefficient = -0.2102,
    thrust_coefficient_kt_0_term_coefficient = 0.3184,
    efficency_4_term_coefficient = -6.1530,
    efficency_3_term_coefficient = 10.0680,
    efficency_2_term_coefficient = -7.1949,
    efficency_1_term_coefficient = 3.4967,
    efficency_0_term_coefficient = -0.0318,
    mass = 0.02,
    cost_money = 0,
    cost_manhours = 1);
    //TODO Water density should come from the constants

end Predicted155mm135mm4Blade;
