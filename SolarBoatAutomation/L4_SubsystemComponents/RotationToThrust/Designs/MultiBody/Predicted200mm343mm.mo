within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.MultiBody;
model Predicted200mm343mm
  import TestPackage;
  import SolarBoatAutomation;
  extends Partial_PropellerMultiBody(redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Predicted200mm343mm
      partial_PropellerDesign);
end Predicted200mm343mm;
