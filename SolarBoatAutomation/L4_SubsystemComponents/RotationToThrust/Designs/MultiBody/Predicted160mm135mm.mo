within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.MultiBody;
model Predicted160mm135mm
  import TestPackage;
  import SolarBoatAutomation;
  extends Partial_PropellerMultiBody(redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Predicted160mm135mm
      partial_PropellerDesign);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Polygon(
          points={{-40,60},{40,-60},{20,-80},{0,0},{-20,60},{-40,60}},
          lineColor={0,0,0},
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Predicted160mm135mm;
