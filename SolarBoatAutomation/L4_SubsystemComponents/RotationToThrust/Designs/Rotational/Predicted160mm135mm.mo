within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Rotational;
model Predicted160mm135mm
  import TestPackage;
  import SolarBoatAutomation;
  extends Partial_PropellerRotational(redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Predicted160mm135mm
      partial_PropellerDesign);
end Predicted160mm135mm;
