within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Rotational;
model Predicted200mm343mm
  import TestPackage;
  import SolarBoatAutomation;
  extends Partial_PropellerRotational(redeclare
      SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs.Predicted200mm343mm
      partial_PropellerDesign);
end Predicted200mm343mm;
