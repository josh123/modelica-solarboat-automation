within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.Designs;
partial record Partial_PropellerDesign
  parameter Modelica.SIunits.Length diameter;
  parameter Modelica.SIunits.Length pitch;
  parameter Modelica.SIunits.Density water_density;

  parameter Real torque_coefficient_kq_4_term_coefficient;
  parameter Real torque_coefficient_kq_3_term_coefficient;
  parameter Real torque_coefficient_kq_2_term_coefficient;
  parameter Real torque_coefficient_kq_1_term_coefficient;
  parameter Real torque_coefficient_kq_0_term_coefficient;

  parameter Real thrust_coefficient_kq_4_term_coefficient;
  parameter Real thrust_coefficient_kq_3_term_coefficient;
  parameter Real thrust_coefficient_kq_2_term_coefficient;
  parameter Real thrust_coefficient_kq_1_term_coefficient;
  parameter Real thrust_coefficient_kq_0_term_coefficient;

  parameter Real efficency_4_term_coefficient;
  parameter Real efficency_3_term_coefficient;
  parameter Real efficency_2_term_coefficient;
  parameter Real efficency_1_term_coefficient;
  parameter Real efficency_0_term_coefficient;
end Partial_PropellerDesign;
