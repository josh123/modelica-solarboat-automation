within SolarBoatAutomation.L4_SubsystemComponents.RotationToThrust.PartialModels;
partial model PartialRotationToThrust
  "Changes mechanical rotation into thrust. e.g. a propeller"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.L3_Subsystems.PartialModels.PartialMassAttributes;
  extends
    SolarBoatAutomation.L3_Subsystems.PartialModels.PartialProcurementAttributes;
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange "Shaft"
    annotation (Placement(transformation(extent={{-108,-8},{-88,12}},
          rotation=0), iconTransformation(extent={{-108,-10},{-88,10}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialRotationToThrust-OPM.png\"/></p>
</html>"));
end PartialRotationToThrust;
