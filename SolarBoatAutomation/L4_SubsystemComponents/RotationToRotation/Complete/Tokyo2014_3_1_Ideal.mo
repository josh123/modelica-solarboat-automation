within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Complete;
model Tokyo2014_3_1_Ideal
  extends Partial.IdealGearbox(redeclare SpecSheets.Tokyo2013_3_1_Specs
      gearboxSpecs);
end Tokyo2014_3_1_Ideal;
