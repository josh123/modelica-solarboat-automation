within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Complete;
model Tokyo2014_5_1_Ideal
  extends Partial.IdealGearbox(redeclare SpecSheets.Tokyo2015_5_1_Specs
      gearboxSpecs);
end Tokyo2014_5_1_Ideal;
