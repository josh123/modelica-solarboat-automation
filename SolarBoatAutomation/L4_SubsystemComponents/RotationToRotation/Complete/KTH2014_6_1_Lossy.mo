within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Complete;
model KTH2014_6_1_Lossy
  extends Partial.LossyGearbox(redeclare SpecSheets.KTH2014_6_1_Specs
      gearboxSpecs);
end KTH2014_6_1_Lossy;
