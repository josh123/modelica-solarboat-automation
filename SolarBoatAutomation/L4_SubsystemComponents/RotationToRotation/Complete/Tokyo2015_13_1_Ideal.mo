within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Complete;
model Tokyo2015_13_1_Ideal
  extends Partial.IdealGearbox(redeclare SpecSheets.Tokyo2015_13_1_Specs
      gearboxSpecs);
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Tokyo2015_13_1_Ideal;
