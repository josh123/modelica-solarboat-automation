within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Complete;
model Tokyo2015_13_1_Lossy
  extends Partial.LossyGearbox(redeclare SpecSheets.Tokyo2015_13_1_Specs
      gearboxSpecs);
end Tokyo2015_13_1_Lossy;
