within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Partial;
partial model IdealGearbox
  extends PartialRotationToRotation;
  Modelica.Mechanics.Rotational.Components.IdealGear idealGear(ratio=
        gearboxSpecs.ratio)
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
  Modelica.Mechanics.MultiBody.Parts.Body gearboxMass(sphereDiameter=0.3, m=
        gearboxSpecs.mass)
              annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={34,-60})));
  replaceable SpecSheets.Partial_GearboxSpecs gearboxSpecs
    annotation (Placement(transformation(extent={{-66,44},{-30,64}})));
  Helpers.Sensors.RotationSensor_MultiUnits
    multiSensor_MultiUnits
    annotation (Placement(transformation(extent={{48,-10},{68,10}})));
equation
  gearboxSpecs.mass = mass_computed;
  gearboxSpecs.cost_money = cost_money_computed;
  gearboxSpecs.cost_manhours = cost_manhours_computed;
  connect(idealGear.flange_a, flange_a) annotation (Line(
      points={{-12,0},{-56,0},{-56,2},{-100,2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(gearboxMass.frame_a, frame_a) annotation (Line(
      points={{34,-70},{34,-78},{0,-78},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(frame_a, frame_a) annotation (Line(
      points={{0,-98},{4,-98},{4,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(idealGear.flange_b, multiSensor_MultiUnits.flange_a) annotation (Line(
      points={{8,0},{48,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(flange_b, multiSensor_MultiUnits.flange_b) annotation (Line(
      points={{100,2},{84,2},{84,0},{68,0}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
      Rectangle(
        origin={-35,62},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-40.0},{15.0,40.0}}),
      Rectangle(
        origin={-35,2},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-21.0},{15.0,21.0}}),
      Line(
        points={{-80,22},{-60,22}}),
      Line(
        points={{-80,-18},{-60,-18}}),
      Line(
        points={{-70,-18},{-70,-84}}),
      Line(
        points={{0,42},{0,-84}}),
      Line(
        points={{-10,42},{10,42}}),
      Line(
        points={{-10,82},{10,82}}),
      Line(
        points={{60,-18},{80,-18}}),
      Line(
        points={{60,22},{80,22}}),
      Line(
        points={{70,-18},{70,-84}}),
      Line(
        points={{70,-84},{-70,-84}}),
      Rectangle(
        origin={-75,2},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-25.0,-10.0},{25.0,10.0}}),
      Rectangle(
        origin={75,2},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-25.0,-10.0},{25.0,10.0}}),
      Rectangle(
        origin={-35,-17},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-35,-6},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-35,21},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-35,10},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={0,62},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-20.0,-10.0},{20.0,10.0}}),
      Rectangle(
        origin={-35,100},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-35,89},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-35,52},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={-35,24},
        fillColor={102,102,102},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-35,35},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-35,72},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={35,62},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-21.0},{15.0,21.0}}),
      Rectangle(
        origin={35,43},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={35,54},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={35,81},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={35,70},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={35,2},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-40.0},{15.0,40.0}}),
      Rectangle(
        origin={35,40},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={35,29},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={35,-8},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={35,-36},
        fillColor={102,102,102},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={35,-25},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={35,12},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={-35,42},
        fillColor=  {255,255,255},
        extent=  {{-15,-61},{15,60}}),
      Rectangle(
        origin={35,23},
        fillColor=  {255,255,255},
        extent=  {{-15,-61},{15,60}}),
                                  Line(
              visible=not useSupport,
              points={{-50,-118},{-30,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{-30,-118},{-10,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{-10,-118},{10,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{10,-118},{30,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{-30,-98},{30,-98}},
              color={0,0,0})}));
end IdealGearbox;
