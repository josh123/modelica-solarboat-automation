within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Partial;
partial model LossyGearbox
  extends PartialRotationToRotation;
  Modelica.Mechanics.MultiBody.Parts.Body gearboxMass(sphereDiameter=0.3, m=
        gearboxSpecs.mass)
              annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={34,-60})));
  replaceable SpecSheets.Partial_GearboxSpecs gearboxSpecs
    annotation (Placement(transformation(extent={{-66,44},{-30,64}})));
  Modelica.Mechanics.Rotational.Components.LossyGear lossyGear(ratio=
        gearboxSpecs.ratio, lossTable=[0,gearboxSpecs.efficiency,gearboxSpecs.efficiency,
        0,0])
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Helpers.Sensors.RotationSensor_MultiUnits
    multiSensor_MultiUnits
    annotation (Placement(transformation(extent={{46,-10},{66,10}})));
equation
  gearboxSpecs.mass = mass_computed;
  gearboxSpecs.cost_money = cost_money_computed;
  gearboxSpecs.cost_manhours = cost_manhours_computed;
  connect(gearboxMass.frame_a, frame_a) annotation (Line(
      points={{34,-70},{34,-78},{0,-78},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(lossyGear.flange_a, flange_a) annotation (Line(
      points={{-10,0},{-56,0},{-56,2},{-100,2}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(lossyGear.flange_b, multiSensor_MultiUnits.flange_a) annotation (Line(
      points={{10,0},{46,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(flange_b, multiSensor_MultiUnits.flange_b) annotation (Line(
      points={{100,2},{84,2},{84,0},{66,0}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
    Polygon(fillColor={161,35,41},
      pattern=LinePattern.None,
      fillPattern=FillPattern.Solid,
      points={{-111,40},{-82,40},{-82,80},{-92,80},{-72,100},{-52,80},{-62,80},
              {-62,20},{-111,20},{-111,40}}),
      Rectangle(
        origin={-33,62},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-40.0},{15.0,40.0}}),
      Rectangle(
        origin={-33,2},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-21.0},{15.0,21.0}}),
      Line(
        points={{-78,22},{-58,22}}),
      Line(
        points={{-78,-18},{-58,-18}}),
      Line(
        points={{-68,-18},{-68,-84}}),
      Line(
        points={{2,42},{2,-84}}),
      Line(
        points={{-8,42},{12,42}}),
      Line(
        points={{-8,82},{12,82}}),
      Line(
        points={{62,-18},{82,-18}}),
      Line(
        points={{62,22},{82,22}}),
      Line(
        points={{72,-18},{72,-84}}),
      Line(
        points={{72,-84},{-68,-84}}),
      Rectangle(
        origin={-73,2},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-25.0,-10.0},{25.0,10.0}}),
      Rectangle(
        origin={77,2},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-25.0,-10.0},{25.0,10.0}}),
      Rectangle(
        origin={-33,-17},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-33,-6},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-33,21},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-33,10},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={2,62},
        lineColor={64,64,64},
        fillColor={191,191,191},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-20.0,-10.0},{20.0,10.0}}),
      Rectangle(
        origin={-33,100},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-33,89},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-33,52},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={-33,24},
        fillColor={102,102,102},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={-33,35},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={-33,72},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={37,62},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-21.0},{15.0,21.0}}),
      Rectangle(
        origin={37,43},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={37,54},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={37,81},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={37,70},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={37,2},
        fillColor={255,255,255},
        fillPattern=FillPattern.HorizontalCylinder,
        extent={{-15.0,-40.0},{15.0,40.0}}),
      Rectangle(
        origin={37,40},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={37,29},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={37,-8},
        fillColor={204,204,204},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={37,-36},
        fillColor={102,102,102},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-2.0},{15.0,2.0}}),
      Rectangle(
        origin={37,-25},
        fillColor={153,153,153},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-3.0},{15.0,3.0}}),
      Rectangle(
        origin={37,12},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid,
        extent={{-15.0,-4.0},{15.0,4.0}}),
      Rectangle(
        origin={-33,42},
        fillColor=  {255,255,255},
        extent=  {{-15,-61},{15,60}}),
      Rectangle(
        origin={37,23},
        fillColor=  {255,255,255},
        extent=  {{-15,-61},{15,60}}),
                                  Line(
              visible=not useSupport,
              points={{2,-118},{22,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{22,-118},{42,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{42,-118},{62,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{62,-118},{82,-98}},
              color={0,0,0}),Line(
              visible=not useSupport,
              points={{22,-98},{82,-98}},
              color={0,0,0}),
      Line(
        points={{50,-84},{50,-98}})}));
end LossyGearbox;
