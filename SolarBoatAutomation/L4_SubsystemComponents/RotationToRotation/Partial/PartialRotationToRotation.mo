within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.Partial;
partial model PartialRotationToRotation
  "Changes the speed and torque of mechanical rotation. e.g. a gearbox"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Mechanics.Rotational.Interfaces.Flange_a
           flange_a "Flange of left shaft" annotation (Placement(
        transformation(extent={{-110,-8},{-90,12}},  rotation=0)));
  Modelica.Mechanics.Rotational.Interfaces.Flange_b
           flange_b "Flange of right shaft" annotation (Placement(
        transformation(extent={{90,-8},{110,12}},  rotation=0)));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialRotationToRotation-OPM.png\"/></p>
</html>"));
end PartialRotationToRotation;
