within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.SpecSheets;
record Tokyo2013_3_1_Specs
  extends Partial_GearboxSpecs(     ratio = 3,
    mass = 0.200,
    cost_money = 0,
    cost_manhours = 1,
    efficiency = 0.8);
end Tokyo2013_3_1_Specs;
