within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.SpecSheets;
record Tokyo2015_13_1_Specs
  extends Partial_GearboxSpecs(     ratio = 13,
    mass = 0.029,
    cost_money = 17300,
    cost_manhours = 1,
    efficiency = 0.8);
end Tokyo2015_13_1_Specs;
