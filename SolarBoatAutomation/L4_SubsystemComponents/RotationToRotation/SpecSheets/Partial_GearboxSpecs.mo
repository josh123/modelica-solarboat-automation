within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.SpecSheets;
partial record Partial_GearboxSpecs
  parameter Real ratio;
  //Mass
  parameter Modelica.SIunits.Mass mass "Mass in kg of the panel";
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";

  parameter Modelica.SIunits.Efficiency efficiency "Gearbox efficency";
end Partial_GearboxSpecs;
