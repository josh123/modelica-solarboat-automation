within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.SpecSheets;
record KTH2014_6_1_Specs
  extends Partial_GearboxSpecs(     ratio = 6,
    mass = 0.082,
    cost_money = 1,
    cost_manhours = 1,
    efficiency = 0.8);
end KTH2014_6_1_Specs;
