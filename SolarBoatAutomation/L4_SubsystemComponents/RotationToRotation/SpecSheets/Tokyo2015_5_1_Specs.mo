within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.SpecSheets;
record Tokyo2015_5_1_Specs
  extends Partial_GearboxSpecs(     ratio = 5,
    mass = 0.042,
    cost_money = 1,
    cost_manhours = 1,
    efficiency = 0.8);
end Tokyo2015_5_1_Specs;
