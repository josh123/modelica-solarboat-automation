within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.CompletedModels;
model GearBox2014A
  extends PartialModels.PartialRotationToRotation;
  extends Modelica.Mechanics.Rotational.Icons.Gear;
  Modelica.Mechanics.Rotational.Components.LossyGear lossyGear
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
  connect(flange_a, lossyGear.flange_a) annotation (Line(
      points={{-100,0},{-10,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(lossyGear.flange_b, flange_b) annotation (Line(
      points={{10,0},{100,0}},
      color={0,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end GearBox2014A;
