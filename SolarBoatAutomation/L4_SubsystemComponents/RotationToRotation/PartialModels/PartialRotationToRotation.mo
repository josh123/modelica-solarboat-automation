within SolarBoatAutomation.L4_SubsystemComponents.RotationToRotation.PartialModels;
partial model PartialRotationToRotation
  "Changes the speed and torque of mechanical rotation. e.g. a gearbox"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.L3_Subsystems.PartialModels.PartialMassAttributes;
  extends
    SolarBoatAutomation.L3_Subsystems.PartialModels.PartialProcurementAttributes;
  extends
    Modelica.Mechanics.Rotational.Interfaces.PartialElementaryTwoFlangesAndSupport2;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialRotationToRotation-OPM.png\"/></p>
</html>"));
end PartialRotationToRotation;
