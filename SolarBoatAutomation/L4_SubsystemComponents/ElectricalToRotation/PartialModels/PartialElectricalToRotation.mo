within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.PartialModels;
partial model PartialElectricalToRotation
  "Converts electrical power to rotation. e.g an electric motor"
  import SolarBoatAutomation;
  extends SolarBoatAutomation.L3_Subsystems.PartialModels.PartialMassAttributes;
  extends
    SolarBoatAutomation.L3_Subsystems.PartialModels.PartialProcurementAttributes;
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange "Shaft"
    annotation (Placement(transformation(extent={{90,-10},{110,10}},
          rotation=0), iconTransformation(extent={{84,-6},{104,14}})));
  Modelica.Blocks.Interfaces.RealInput electricalPowerInput(final unit="W")
    "The power delivered by the MPPT" annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={0,102}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={0,108})));
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialElectricalToRotation-OPM.png\"/></p>
</html>"), Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics));
end PartialElectricalToRotation;
