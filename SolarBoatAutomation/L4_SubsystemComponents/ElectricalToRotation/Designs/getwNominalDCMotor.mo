within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs;
function getwNominalDCMotor
  input Real Kv "Motor speed constant in RPM/Volt";
  input Modelica.SIunits.Voltage VaNominal "Nominal armature voltage";
  output Modelica.SIunits.AngularVelocity wNominal "Nominal speed";
algorithm
  wNominal :=((VaNominal*Kv)*0.85*2*Modelica.Constants.pi)/60;
end getwNominalDCMotor;
