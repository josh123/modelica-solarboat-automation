within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs;
record MaxonEC_max_60W_24V_272763
  extends Partial_DCMotorDesign(Jr=2.19e-06,
    VaNominal=24,
    IaNominal=2.66,
    Ra=1.27,
    La=0.143e-3,
    wNominal=(8040*2*Modelica.Constants.pi)/60,
    IaStall=18.8,
    Kv=393,
    mass = 0.30,
    cost_money = 1,
    cost_manhours = 1);

end MaxonEC_max_60W_24V_272763;
