within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.Rotational;
model MaxonEC_max_60W_24V_272763
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Partial.DCMotor(
     redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.MaxonEC_max_60W_24V_272763
      partial_DCMotorDesign);
end MaxonEC_max_60W_24V_272763;
