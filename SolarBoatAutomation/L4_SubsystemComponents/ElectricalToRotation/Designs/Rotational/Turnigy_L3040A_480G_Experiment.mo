within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.Rotational;
model Turnigy_L3040A_480G_Experiment
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Partial.DCMotor(
     redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.Turnigy_L3040A_480G_Experiment
      partial_DCMotorDesign, dcpm(frictionParameters(
        PRef=45,
        wRef=968.65773485685,
        power_w=2)));
end Turnigy_L3040A_480G_Experiment;
