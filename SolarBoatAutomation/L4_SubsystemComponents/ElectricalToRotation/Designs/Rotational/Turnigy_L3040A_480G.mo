within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.Rotational;
model Turnigy_L3040A_480G
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Partial.DCMotor(
     redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs.Turnigy_L3040A_480G
      partial_DCMotorDesign);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Rectangle(
          extent={{-82,52},{80,-48}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder), Rectangle(
          extent={{80,10},{96,-10}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Turnigy_L3040A_480G;
