within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs;
record Turnigy_L3040A_480G_Experiment
  extends Partial_DCMotorDesign(Jr=2.19e-06,
    VaNominal=22.2,
    IaNominal=2.66,
    Ra=1.27*1.2,
    La=0.143e-3*1.2,
    wNominal=getwNominalDCMotor(           Kv, VaNominal),
    IaStall=52,
    Kv=480,
    mass = 0.194,
    cost_money = 1,
    cost_manhours = 1);

end Turnigy_L3040A_480G_Experiment;
