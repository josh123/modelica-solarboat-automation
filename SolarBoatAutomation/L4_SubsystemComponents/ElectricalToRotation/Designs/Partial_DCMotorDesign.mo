within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Designs;
partial record Partial_DCMotorDesign
  parameter Modelica.SIunits.Inertia Jr "Rotor's moment of inertia";
  parameter Modelica.SIunits.Voltage VaNominal "Nominal armature voltage";
  parameter Modelica.SIunits.Current IaNominal
    "Nominal armature current (>0..Motor, <0..Generator)";
  parameter Modelica.SIunits.AngularVelocity wNominal "Nominal speed";
  parameter Modelica.SIunits.Resistance Ra "Armature resistance at TRef";
  parameter Modelica.SIunits.Inductance La "Armature inductance";
  parameter Real IaStall "Maximum current motor can take";
  parameter Real Kv "Motor speed constant in RPM/Volt";
  //Mass
  parameter Modelica.SIunits.Mass mass "Mass in kg of the panel";
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";
end Partial_DCMotorDesign;
