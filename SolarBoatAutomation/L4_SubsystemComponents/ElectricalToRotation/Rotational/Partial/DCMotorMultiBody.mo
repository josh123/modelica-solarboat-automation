within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial;
partial model DCMotorMultiBody

  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;

  Modelica.Electrical.Machines.BasicMachines.DCMachines.DC_PermanentMagnet dcpm(
    Jr=partial_DCMotorDesign.Jr,
    useThermalPort=false,
    VaNominal=partial_DCMotorDesign.VaNominal,
    IaNominal=partial_DCMotorDesign.IaNominal,
    Ra=partial_DCMotorDesign.Ra,
    La=partial_DCMotorDesign.La,
    useSupport=false,
    wNominal=partial_DCMotorDesign.wNominal,
    frictionParameters(
      PRef=partial_DCMotorDesign.PRef,
      wRef=partial_DCMotorDesign.wRef,
      power_w=partial_DCMotorDesign.power_w))
    annotation (Placement(transformation(extent={{-24,-10},{-4,10}})));
  SolarBoatAutomation.Helpers.Sensors.RotationSensor_MultiUnits
    multiSensor_MultiUnits
    annotation (Placement(transformation(extent={{16,-10},{36,10}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n "Negative pin"
    annotation (Placement(transformation(extent={{-78,110},{-58,90}}, rotation=0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p "Positive pin"
    annotation (Placement(transformation(extent={{42,110},{62,90}}, rotation=0)));
  replaceable SpecSheets.Partial_DCMotorDesign partial_DCMotorDesign
    annotation (Placement(transformation(extent={{-78,-54},{-58,-34}})));
  Modelica.Mechanics.MultiBody.Parts.Body DCMotorMass(sphereDiameter=0.3, m=
        partial_DCMotorDesign.mass) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={34,-60})));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(useAxisFlange=true, n={1,0,0})
    annotation (Placement(transformation(extent={{54,-30},{74,-10}})));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a rotatingInterface
    "To attach the component rigidly to other components" annotation (Placement(
        transformation(
        extent={{-16,-16},{16,16}},
        rotation=0,
        origin={100,0})));
equation
  partial_DCMotorDesign.mass = mass_computed;
  partial_DCMotorDesign.cost_money = cost_money_computed;
  partial_DCMotorDesign.cost_manhours = cost_manhours_computed;

  connect(dcpm.flange, multiSensor_MultiUnits.flange_a) annotation (Line(
      points={{-4,0},{16,0}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(pin_n, dcpm.pin_an) annotation (Line(
      points={{-68,100},{-68,40},{-20,40},{-20,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(pin_p, dcpm.pin_ap) annotation (Line(
      points={{52,100},{56,100},{56,34},{-8,34},{-8,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(DCMotorMass.frame_a, frame_a) annotation (Line(
      points={{34,-70},{34,-86},{0,-86},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(multiSensor_MultiUnits.flange_b, revolute.axis) annotation (Line(
      points={{36,0},{64,0},{64,-10}},
      color={0,0,0},
      smooth=Smooth.None));
  connect(revolute.frame_a, frame_a) annotation (Line(
      points={{54,-20},{0,-20},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(revolute.frame_b, rotatingInterface) annotation (Line(
      points={{74,-20},{78,-20},{78,0},{100,0}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end DCMotorMultiBody;
