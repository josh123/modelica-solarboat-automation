within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete;
model Turnigy_L3040A_480G_Experiment
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets.Turnigy_L3040A_480G_Experiment
      partial_DCMotorDesign);
  annotation (Icon(graphics={            Rectangle(
          extent={{-80,52},{82,-48}},
          lineColor={0,0,0},
          fillColor={255,128,0},
          fillPattern=FillPattern.HorizontalCylinder), Rectangle(
          extent={{82,10},{98,-10}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end Turnigy_L3040A_480G_Experiment;
