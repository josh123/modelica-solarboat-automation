within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete;
model S13560_260R
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets.S13560_260R
      partial_DCMotorDesign);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Rectangle(
          extent={{4,66},{80,-48}},
          lineColor={0,0,0},
          fillColor={175,175,175},
          fillPattern=FillPattern.HorizontalCylinder), Rectangle(
          extent={{80,10},{96,-10}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end S13560_260R;
