within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Complete;
model MaxonEC_max_60W_24V_272763
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.Rotational.Partial.DCMotor(
      redeclare
      SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets.MaxonEC_max_60W_24V_272763
      partial_DCMotorDesign);
end MaxonEC_max_60W_24V_272763;
