within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets;
record Turnigy_L3040A_480G_Experiment
  extends SpecSheets.Partial_DCMotorDesign(
                                Jr=2.19e-06,
    VaNominal=22.2,
    IaNominal=2.66,
    Ra=1.27*1.2,
    La=0.143e-3*1.2,
    wNominal=Helpers.getwNominalDCMotor(   Kv, VaNominal),
    IaStall=52,
    Kv=480,
    mass = 0.194,
    cost_money = 2100,
    cost_manhours = 1,
    IaNoLoad = 2.3,
    PRef=45,
    wRef=968.65773485685,
    power_w=2);

end Turnigy_L3040A_480G_Experiment;
