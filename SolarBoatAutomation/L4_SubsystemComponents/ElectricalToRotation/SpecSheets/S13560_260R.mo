within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets;
record S13560_260R
  //http://shop.zdpshop.com/shopdetail/000000000058/
  //Cost 202500
  extends SpecSheets.Partial_DCMotorDesign(
                                Jr=2.19e-06,
    VaNominal=27,
    IaNominal=4.5,
    Ra=1.27*1.2,
    La=0.143e-3*1.2,
    wNominal=(2600/60)*2*Modelica.Constants.pi,
    IaStall=52,
    Kv=69,
    mass = 1.75,
    cost_money = 0,
    cost_manhours = 1,
    IaNoLoad = 0.2,
    PRef=45,
    wRef=wNominal,
    power_w=2);

end S13560_260R;
