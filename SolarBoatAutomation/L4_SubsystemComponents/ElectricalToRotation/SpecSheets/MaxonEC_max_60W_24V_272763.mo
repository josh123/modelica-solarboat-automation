within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToRotation.SpecSheets;
record MaxonEC_max_60W_24V_272763
  extends SpecSheets.Partial_DCMotorDesign(
                                Jr=2.19e-06,
    VaNominal=24,
    IaNominal=2.66,
    Ra=1.27,
    La=0.143e-3,
    wNominal=(8040*2*Modelica.Constants.pi)/60,
    IaStall=18.8,
    Kv=393,
    mass = 0.30,
    cost_money = 1,
    cost_manhours = 1,
    IaNoLoad = 0.231,
    PRef=VaNominal*IaNoLoad,
    wRef=wNominal,
    power_w=2);

end MaxonEC_max_60W_24V_272763;
