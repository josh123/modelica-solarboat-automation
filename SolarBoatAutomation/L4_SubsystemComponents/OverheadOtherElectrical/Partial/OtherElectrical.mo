within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherElectrical.Partial;
partial model OtherElectrical
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents;
  parameter Modelica.SIunits.Mass mass = partial_OverheadElectircalComponents.mass
    "Mass structural components not accounted for";
  parameter Real cost_money(unit="yen") = partial_OverheadElectircalComponents.cost_money "Cost";
  parameter Real cost_manhours(unit="hours") = partial_OverheadElectircalComponents.cost_manhours
    "Time to build";
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=mass)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-18,-22})));
  replaceable
    SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherElectrical.SpecSheets.Partial_OverheadElectricalComponents
    partial_OverheadElectircalComponents
    annotation (Placement(transformation(extent={{-82,48},{-28,72}})));
equation
  cost_manhours_computed = cost_manhours;
  cost_money_computed = cost_money;
  mass_computed = mass;
  connect(lumpedMass.frame_a, frame_a) annotation (Line(
      points={{-8,-22},{0,-22},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end OtherElectrical;
