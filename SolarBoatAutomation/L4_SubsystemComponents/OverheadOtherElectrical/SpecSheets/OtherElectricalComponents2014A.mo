within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherElectrical.SpecSheets;
record OtherElectricalComponents2014A
  extends Partial_OverheadElectricalComponents(mass = 0.5,
    cost_money = 1,
    cost_manhours = 1);
end OtherElectricalComponents2014A;
