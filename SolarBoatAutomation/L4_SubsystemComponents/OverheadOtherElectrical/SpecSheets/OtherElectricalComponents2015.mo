within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherElectrical.SpecSheets;
record OtherElectricalComponents2015
  extends Partial_OverheadElectricalComponents(mass = 0.5+2.2,
    cost_money = 1,
    cost_manhours = 1);
end OtherElectricalComponents2015;
