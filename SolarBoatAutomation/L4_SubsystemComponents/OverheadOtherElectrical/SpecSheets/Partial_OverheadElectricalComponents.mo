within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherElectrical.SpecSheets;
partial record Partial_OverheadElectricalComponents
  //Mass
  parameter Modelica.SIunits.Mass mass "Mass in kg of the panel";
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";

end Partial_OverheadElectricalComponents;
