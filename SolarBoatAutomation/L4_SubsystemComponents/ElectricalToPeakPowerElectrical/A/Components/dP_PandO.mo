within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Components;
model dP_PandO "dP_PandO MPPT block"
  //From Esram, T. (2010). Modeling and control of an alternating-current photovoltaic module. University of Illinois at Urbana-Champaign. Retrieved from https://www.ideals.illinois.edu/handle/2142/15501

  extends Modelica.Blocks.Interfaces.DiscreteBlockIcon;
  parameter Modelica.SIunits.Time samplePeriod=1/15e3;
  parameter Modelica.SIunits.Time startTime=0;
  parameter Integer nsamples=250
    "samples between updates (sampling frequency / update frequency)";
  parameter Integer half_nsamples=125 "number samples half way between updates";
  parameter Real delta_coarse=0.25 "coarse perturbation step";
  parameter Real delta_fine=0.05 "fine perturbation step";
  parameter Real ThP(unit="W") = 0 "positive threshold for dP";
  parameter Real ThN(unit="W") = 0.5 "negative threshold for dP";
  parameter Real y_start=0.05 "initial condition for y";

protected
  parameter Integer nstore=3 "number of averaged points to store";
  Real p_store[nstore] "power samples";
  Real v_store[nstore] "voltage samples";
  Real p_sam[half_nsamples] "power samples";
  Real v_sam[half_nsamples] "voltage samples";
  Integer n(start=-1) "sample counter";
  Real p_ave "average power at current update";
  Real v_ave "average voltage at current update";
  Real delta_V "change in voltage between updates";
  Real delta_P "change in power between updates";
  Real delta_P2 "intermediate change in power";

public
  Modelica.Blocks.Interfaces.RealInput v annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={-100,60})));
public
  Modelica.Blocks.Interfaces.RealInput i annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={-100,-56})));
  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
algorithm
  // at every sample, run the following:
  when sample(startTime, samplePeriod) then
    for m in 1:(half_nsamples - 1) loop
      v_sam[m] := pre(v_sam[m + 1]);
      p_sam[m] := pre(p_sam[m + 1]);
    end for;
    v_sam[half_nsamples] := v;
    p_sam[half_nsamples] := v*i;
    n := pre(n) + 1;
  end when;

  // at every half_nsamples, run the following:
  when mod(n, half_nsamples) == 0 then
    p_ave := sum(p_sam[:])/(half_nsamples);
    v_ave := sum(v_sam[:])/(half_nsamples);

    for m in 1:(nstore - 1) loop
      v_store[m] := pre(v_store[m + 1]);
      p_store[m] := pre(p_store[m + 1]);
    end for;

    v_store[nstore] := v_ave;
    p_store[nstore] := p_ave;
  end when;

  // at every nsamples, run the following:
  when mod(n, nsamples) == 0 then
    delta_V := v_store[3] - v_store[1];
    delta_P := (p_store[2] - p_store[1]) - (p_store[3] - p_store[2]);
    delta_P2 := p_store[3] - p_store[2];

    if abs(delta_P2) < abs(delta_P) then
      // run basic dP-P&0 with fine delta
      if delta_P < 1e-6 and delta_P > -1e-6 then
        y := pre(y);
      else
        if delta_P > 0 then
          if delta_V > 0 then
            y := pre(y) + delta_fine;
          else
            y := pre(y) - delta_fine;
          end if;
        else
          if delta_V > 0 then
            y := pre(y) - delta_fine;
          else
            y := pre(y) + delta_fine;
          end if;
        end if;
      end if;
    else
      // run optimized dP-P&O with coarse delta
      if delta_P2 < 0 then
        if delta_P >= ThP then
          y := pre(y) - delta_coarse;
        else
          if delta_P <= ThN then
            if delta_V >= 0 then
              y := pre(y) - delta_coarse;
            else
              y := pre(y) + delta_coarse;
            end if;
          else
            if delta_V < 1e-6 and delta_V > -1e-6 then
              y := pre(y) - delta_coarse;
            else
              y := pre(y);
            end if;
          end if;
        end if;
      else
        if delta_P >= ThP then
          y := pre(y) + delta_coarse;
        else
          if delta_P <= ThN then
            if delta_V <= 0 then
              y := pre(y) + delta_coarse;
            else
              y := pre(y) - delta_coarse;
            end if;
          else
            if delta_V < 1e-6 and delta_V > -1e-6 then
              y := pre(y) + delta_coarse;
            else
              y := pre(y);
            end if;
          end if;
        end if;
      end if;
    end if;
  end when;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Line(
          points={{-78,-70},{-32,-70},{-32,-22},{14,-22},{14,16},{56,16},{56,52},
              {90,52},{90,82}},
          color={0,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-112,88},{-12,44}},
          lineColor={0,0,255},
          textString="v"),
        Text(
          extent={{-112,-30},{-12,-74}},
          lineColor={0,0,255},
          textString="i")}));
end dP_PandO;
