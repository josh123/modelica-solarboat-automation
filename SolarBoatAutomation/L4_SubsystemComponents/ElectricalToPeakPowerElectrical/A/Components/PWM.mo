within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Components;
model PWM
  parameter Modelica.SIunits.Frequency frequency=100e3;
  Modelica.Blocks.Interfaces.BooleanOutput q
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput d
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Sources.SawTooth ramp(period=1/frequency)
    annotation (Placement(transformation(extent={{-92,-48},{-72,-28}})));
  Modelica.Blocks.Logical.Greater greater
    annotation (Placement(transformation(extent={{-34,-2},{-14,18}})));
equation

  connect(d, greater.u1) annotation (Line(
      points={{-120,0},{-80,0},{-80,8},{-36,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ramp.y, greater.u2) annotation (Line(
      points={{-71,-38},{-54,-38},{-54,0},{-36,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(greater.y, q) annotation (Line(
      points={{-13,8},{44,8},{44,0},{110,0}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end PWM;
