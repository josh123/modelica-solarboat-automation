within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Complete;
model MPPT
  extends Partial.MPPT(redeclare SpecSheets.MPPT2014A partial_MPPTDesign);

  Modelica.Electrical.Analog.Interfaces.PositivePin
              p
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{-106,50},{-86,70}},  rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n "Negative pin" annotation (Placement(transformation(extent={{-86,-60},
            {-106,-40}},       rotation=0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin
              p1
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{88,48},{108,68}},    rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n1 "Negative pin"
                               annotation (Placement(transformation(extent={{108,-62},
            {88,-42}},         rotation=0)));
  Components.dP_PandO dP_PandO1
    annotation (Placement(transformation(extent={{-32,-32},{-12,-12}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-56,18},{-36,38}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-14,14})));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor(C=3.3e-6) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={8,12})));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L=100e-6)
    annotation (Placement(transformation(extent={{14,18},{34,38}})));
  Modelica.Electrical.Analog.Ideal.IdealClosingSwitch idealClosingSwitch
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,12})));
  Modelica.Electrical.Analog.Ideal.IdealDiode idealDiode
    annotation (Placement(transformation(extent={{46,18},{66,38}})));
  Components.PWM pWM
    annotation (Placement(transformation(extent={{6,-32},{26,-12}})));
equation
  connect(currentSensor.n,voltageSensor. p) annotation (Line(
      points={{-36,28},{-14,28},{-14,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.v,dP_PandO1. v) annotation (Line(
      points={{-24,14},{-38,14},{-38,-16},{-32,-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(currentSensor.i,dP_PandO1. i) annotation (Line(
      points={{-46,18},{-46,-27.6},{-32,-27.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(capacitor.p,voltageSensor. p) annotation (Line(
      points={{8,22},{8,28},{-14,28},{-14,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(capacitor.n,voltageSensor. n) annotation (Line(
      points={{8,2},{8,-4},{-14,-4},{-14,4}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(inductor.p,voltageSensor. p) annotation (Line(
      points={{14,28},{-14,28},{-14,24}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(inductor.n,idealClosingSwitch. n) annotation (Line(
      points={{34,28},{40,28},{40,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealClosingSwitch.p,voltageSensor. n) annotation (Line(
      points={{40,2},{40,-4},{-14,-4},{-14,4}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealDiode.p,idealClosingSwitch. n) annotation (Line(
      points={{46,28},{40,28},{40,22}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(dP_PandO1.y,pWM. d) annotation (Line(
      points={{-11,-22},{4,-22}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pWM.q,idealClosingSwitch. control) annotation (Line(
      points={{27,-22},{30,-22},{30,12},{33,12}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(n, voltageSensor.n) annotation (Line(
      points={{-96,-50},{-96,-4},{-14,-4},{-14,4}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(p, currentSensor.p) annotation (Line(
      points={{-96,60},{-94,60},{-94,28},{-56,28}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(idealDiode.n, p1) annotation (Line(
      points={{66,28},{80,28},{80,58},{98,58}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(n1, voltageSensor.n) annotation (Line(
      points={{98,-52},{98,-4},{-14,-4},{-14,4}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end MPPT;
