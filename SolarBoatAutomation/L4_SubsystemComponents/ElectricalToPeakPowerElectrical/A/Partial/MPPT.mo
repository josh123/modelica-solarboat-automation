within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.A.Partial;
partial model MPPT
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;

  Modelica.Electrical.Analog.Interfaces.PositivePin
              p
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{-106,50},{-86,70}},  rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n "Negative pin" annotation (Placement(transformation(extent={{-86,-60},
            {-106,-40}},       rotation=0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin
              p1
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{88,48},{108,68}},    rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n1 "Negative pin"
                               annotation (Placement(transformation(extent={{108,-62},
            {88,-42}},         rotation=0)));
  replaceable SpecSheets.Partial_MPPTDesign partial_MPPTDesign
    annotation (Placement(transformation(extent={{-64,68},{-32,92}})));
equation
  partial_MPPTDesign.mass = mass_computed;
  partial_MPPTDesign.cost_money = cost_money_computed;
  partial_MPPTDesign.cost_manhours = cost_manhours_computed;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
          Text(
          extent={{-22,40},{-94,-36}},
          lineColor={0,0,255},
          textString="Solar panel
<--"), Text(
          extent={{80,30},{34,-28}},
          lineColor={0,0,255},
          textString="Load
-->")}));
end MPPT;
