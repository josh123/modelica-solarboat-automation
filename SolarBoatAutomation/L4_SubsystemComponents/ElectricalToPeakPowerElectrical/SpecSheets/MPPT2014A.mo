within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.SpecSheets;
record MPPT2014A
  extends Partial_MPPTDesign(mass = 0.5,
    cost_money = 1,
    cost_manhours = 1);
end MPPT2014A;
