within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.C.Complete;
model MPPT2014A
  extends Partial.PartialUnstableVoltageToStableVoltage(redeclare
      SpecSheets.MPPT2014A partial_MPPTDesign);
  parameter Real conversion_percentage_efficency(final unit="") = 94
    "Proportion energy converted";
  Modelica.Mechanics.MultiBody.Parts.Body mpptMass(
      animation=true, m=partial_MPPTDesign.mass)
    "Model the mass of the component" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-2})));
equation
  powerDelivered = powerFromSolarPanel * (conversion_percentage_efficency/100);
  connect(mpptMass.frame_a, frame_a) annotation (Line(
      points={{0,-12},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Icon(graphics={
        Rectangle(
          extent={{-34,22},{44,-8}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-46,30},{-34,22},{-34,-8},{-46,4},{-46,30}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-46,30},{32,30},{44,22},{-34,22},{-46,30}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-32,26},{-32,26}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-34,28},{-26,22},{20,22},{8,28},{-34,28}},
          lineColor={0,0,0},
          lineThickness=1,
          smooth=Smooth.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-22,16},{-22,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-12,16},{-12,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{-2,16},{-2,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{8,16},{8,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{16,16},{16,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{26,16},{26,2}},
          color={165,165,165},
          thickness=1,
          smooth=Smooth.None)}), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics));
end MPPT2014A;
