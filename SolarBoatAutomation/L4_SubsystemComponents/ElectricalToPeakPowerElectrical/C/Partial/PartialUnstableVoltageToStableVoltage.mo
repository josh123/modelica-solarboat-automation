within SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.C.Partial;
partial model PartialUnstableVoltageToStableVoltage
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput powerFromSolarPanel(final unit="W")
    "The power delivered by the solarpanel"
    annotation (Placement(transformation(extent={{-122,-22},{-82,18}})));
  Modelica.Blocks.Interfaces.RealOutput powerDelivered(final unit="W")
    "The power delivered by the solarpanel"
    annotation (Placement(transformation(extent={{94,-10},{114,10}})));
  replaceable
    SolarBoatAutomation.L4_SubsystemComponents.ElectricalToPeakPowerElectrical.SpecSheets.Partial_MPPTDesign
    partial_MPPTDesign
    annotation (Placement(transformation(extent={{-80,50},{-46,70}})));
equation
  partial_MPPTDesign.mass = mass_computed;
  partial_MPPTDesign.cost_money = cost_money_computed;
  partial_MPPTDesign.cost_manhours = cost_manhours_computed;
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialUnstableVoltageToStableVoltage-OPM.png\"/></p>
</html>"));
end PartialUnstableVoltageToStableVoltage;
