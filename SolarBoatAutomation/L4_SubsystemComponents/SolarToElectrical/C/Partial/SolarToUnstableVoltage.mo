within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.C.Partial;
partial model SolarToUnstableVoltage
  import SolarBoatAutomation;
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2") annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={0,100}),                              iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=-90,
        origin={1,103})));
  Modelica.Blocks.Interfaces.RealOutput powerDeliveredBySolarArray(final unit="W")
    "The power from the whole solar panel assembly"                                                                                annotation (
      Placement(transformation(extent={{88,-16},{118,14}}), iconTransformation(
          extent={{98,-6},{118,14}})));
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">OPM</span></h4>
<p><img src=\"modelica://SolarBoatAutomation/Images/opm/PartialSolarToUnstableVoltage-OPM.png\"/></p>
</html>"));
end SolarToUnstableVoltage;
