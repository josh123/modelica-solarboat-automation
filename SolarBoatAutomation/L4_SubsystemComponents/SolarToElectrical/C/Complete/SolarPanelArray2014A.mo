within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.C.Complete;
model SolarPanelArray2014A
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.C.Partial.SolarToUnstableVoltage;
  parameter Real solar_panel_quantity(final unit="") = 6
    "The number of solar panels on the craft";
  parameter Modelica.SIunits.Length length = 0.796 "Length of one solar panel";
  parameter Modelica.SIunits.Length width = 0.443 "Width of one solar panel";
  parameter Real conversion_percentage_efficency(final unit="") = 12.45
    "The percentage of solar insolation converted into usable electricity";
  parameter Modelica.SIunits.Mass solar_panel_mass_per_panel = 0.628
    "Mass in kg of one 2014 solar panel";
  parameter Modelica.SIunits.Area area = length * width
    "Area of one solar panel";
  parameter Modelica.SIunits.Mass mass = solar_panel_mass_per_panel * solar_panel_quantity
    "Mass in kg of the solar panels";
  parameter Real cost_money(unit="yen") = 1 "Cost";
  parameter Real cost_manhours(unit="hours") = 1 "Time to build";
  Modelica.SIunits.Power power_one_solar_panel "The power from one solar panel";
  Modelica.SIunits.Power max_power_one_solar_panel
    "The power from one solar panel with no efficency loss";
  Modelica.SIunits.Power max_power_solar_array
    "The power from the array with not efficency loss";
  Modelica.Mechanics.MultiBody.Parts.Body panelMass(animation=false, m=mass)
    "Model the mass of the solar panels"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-44})));
equation
  max_power_one_solar_panel = solarInsolation*area;
  max_power_solar_array = solar_panel_quantity * max_power_one_solar_panel;

  power_one_solar_panel = max_power_one_solar_panel*(conversion_percentage_efficency/100);
  powerDeliveredBySolarArray = power_one_solar_panel * solar_panel_quantity;
  mass_computed = mass;
  cost_money_computed = cost_money;
  cost_manhours_computed = cost_manhours;
  connect(panelMass.frame_a, frame_a) annotation (Line(
      points={{0,-54},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={
        Polygon(
          points={{-90,42},{-32,8},{8,38},{-46,62},{-90,42}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-90,-24},{-20,-70},{20,-26},{-44,2},{-90,-24}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-4,10},{68,-24},{96,22},{38,42},{-4,10}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-22,16},{22,-20},{76,-38},{100,6}},
          color={0,0,0},
          thickness=1,
          smooth=Smooth.None),
        Line(
          points={{34,-8},{22,-20},{6,-20}},
          color={0,0,0},
          thickness=1,
          smooth=Smooth.None),
        Text(
          extent={{-196,-84},{204,-164}},
          lineColor={0,0,255},
          textString="%name")}),    Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>Make the array auto generated with replaceable components</li>
</ul>
</html>"));
end SolarPanelArray2014A;
