within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.SpecSheets;
record BP_7185
  extends Partial_SolarPanelSpecSheet(
    T_STC = 298,
    G_STC = 1000,
    V_oc = 44.8,
    I_sc = 5.5,
    V_mpp = 36.5,
    I_mpp = 5.1,
    k_i = 0.065,
    k_v = -0.16,
    N=72,
    n_d=1.4061,
    R_s = 0.2614,
    R_sh = 1474,
    a = 2.3e-3,
    V_br = -18,
    m=2,
    mass = 15.4,
    cost_money = 1,
    cost_manhours = 1,
    length = 1.593,
    width = 0.790);
end BP_7185;
