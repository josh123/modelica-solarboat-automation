within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.SpecSheets;
record SP50f_L_LOXX
  extends Partial_SolarPanelSpecSheet(
    T_STC = 298,
    G_STC = 1000,
    V_oc = 21.2,
    I_sc = 3.05,
    V_mpp = 17.6,
    I_mpp = 2.84,
    k_i = 0.05,
    k_v = -0.27,
    N=16,
    n_d=2.9175,
    R_s = 0.1113,
    R_sh = 1259,
    a = 2.3e-3,
    V_br = -18,
    m=2,
    mass = 0.9,
    cost_money = 68580,
    cost_manhours = 1,
    length = 0.555,
    width = 0.535);
end SP50f_L_LOXX;
