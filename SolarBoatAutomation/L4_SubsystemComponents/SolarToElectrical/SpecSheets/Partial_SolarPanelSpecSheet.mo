within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.SpecSheets;
partial record Partial_SolarPanelSpecSheet
  parameter Real T_STC(unit="K") "STC temperature";
  parameter Real G_STC(unit="W/m^2") "STC insolation";

  // Datasheet Parameters
  parameter Real V_oc(unit="V") "module open-circuit voltage at STC";
  parameter Real I_sc(unit="A") "module short-circuit current at STC";
  parameter Real V_mpp(unit="V") "module voltage at MPP at STC";
  parameter Real I_mpp(unit="A") "module current at MPP at STC";
  parameter Real k_i(unit="%/K") "temperature coefficient of I_sc";
  parameter Real k_v(unit="V/K") "temperature coefficient of V_oc";
  parameter Real N "number of cells";

  // Extracted Parameters (using MATLAB)
  parameter Real n_d "diode quality (ideality) factor";
  parameter Real R_s(unit="ohm") "module series resistance";
  parameter Real R_sh(unit="ohm") "module shunt resistance";

  // Breakdown Parameters
  parameter Real a(unit="1/ohm")
    "fraction of ohmic current in avalanche breakdown";
  parameter Real V_br(unit="V") "breakdown voltage of one cell";
  parameter Real m "exponent for avalanche breakdown";

  //Mass
  parameter Modelica.SIunits.Mass mass "Mass in kg of the panel";
  //Cost
  parameter Real cost_money(unit="yen") "Cost";
  parameter Real cost_manhours(unit="hours") "Time to build";

  //Size
  parameter Modelica.SIunits.Length length "Length of the panel in meters";
  parameter Modelica.SIunits.Length width "Width of the panel in meters";
end Partial_SolarPanelSpecSheet;
