within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.SpecSheets;
record FT136SE
  extends Partial_SolarPanelSpecSheet(
    T_STC = 298,
    G_STC = 1000,
    V_oc = 21.8,
    I_sc = 2.7,
    V_mpp = 17.8,
    I_mpp = 2.52,
    k_i = 0.065,
    k_v = -0.16,
    N=12,
    n_d=3.813,
    R_s = 0.3088,
    R_sh = 2.7484e3,
    a = 2.3e-3,
    V_br = -18,
    m=2,
    mass = 0.54,
    cost_money = 0,
    cost_manhours = 1,
    length = 0.774,
    width = 0.43);
end FT136SE;
