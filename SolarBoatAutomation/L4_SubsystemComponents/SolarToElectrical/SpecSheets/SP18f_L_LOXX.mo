within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.SpecSheets;
record SP18f_L_LOXX
  extends Partial_SolarPanelSpecSheet(
    T_STC = 298,
    G_STC = 1000,
    V_oc = 23.7,
    I_sc = 0.99,
    V_mpp = 19.4,
    I_mpp = 0.92,
    k_i = 0.05,
    k_v = -0.27,
    N=12,
    n_d=3.9948,
    R_s = 0.9495,
    R_sh = 2019,
    a = 2.3e-3,
    V_br = -18,
    m=2,
    mass = 0.4,
    cost_money = 1,
    cost_manhours = 1,
    length = 0.434,
    width = 0.277);
end SP18f_L_LOXX;
