within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Components.Complete;
model FT136SE
  extends Components.Partial.ParameterizedPanel(redeclare SpecSheets.FT136SE
      partial_SolarPanelSpecSheet);
  annotation (Icon(graphics));
end FT136SE;
