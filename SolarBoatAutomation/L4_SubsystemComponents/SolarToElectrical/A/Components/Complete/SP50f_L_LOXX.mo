within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Components.Complete;
model SP50f_L_LOXX
  extends Components.Partial.ParameterizedPanel(redeclare
      SpecSheets.SP50f_L_LOXX
      partial_SolarPanelSpecSheet);
  annotation (Icon(graphics));
end SP50f_L_LOXX;
