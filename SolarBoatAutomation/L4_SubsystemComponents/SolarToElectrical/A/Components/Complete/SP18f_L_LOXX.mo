within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Components.Complete;
model SP18f_L_LOXX
  extends Components.Partial.ParameterizedPanel(redeclare
      SpecSheets.SP18f_L_LOXX
      partial_SolarPanelSpecSheet);
  annotation (Icon(graphics));
end SP18f_L_LOXX;
