within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Components.Complete;
model BP_7185
  extends Components.Partial.ParameterizedPanel(redeclare SpecSheets.BP_7185
      partial_SolarPanelSpecSheet);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end BP_7185;
