within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Components.Partial;
partial model ParameterizedPanel
  //Based on: Esram, T. (2010). Modeling and control of an alternating-current photovoltaic module. University of Illinois at Urbana-Champaign. Retrieved from https://www.ideals.illinois.edu/handle/2142/15501

  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  extends Modelica.Electrical.Analog.Interfaces.OnePort;

  //Constants
protected
  parameter Real T_STC(unit="K") = partial_SolarPanelSpecSheet.T_STC
    "STC temperature";
  parameter Real G_STC(unit="W/m^2") = partial_SolarPanelSpecSheet.G_STC
    "STC insolation";

  // Datasheet Parameters
  parameter Real V_oc(unit="V") = partial_SolarPanelSpecSheet.V_oc
    "module open-circuit voltage at STC";
  parameter Real I_sc(unit="A") = partial_SolarPanelSpecSheet.I_sc
    "module short-circuit current at STC";
  parameter Real V_mpp(unit="V") = partial_SolarPanelSpecSheet.V_mpp
    "module voltage at MPP at STC";
  parameter Real I_mpp(unit="A") = partial_SolarPanelSpecSheet.I_mpp
    "module current at MPP at STC";
  parameter Real k_i(unit="%/K") = partial_SolarPanelSpecSheet.k_i
    "temperature coefficient of I_sc";
  parameter Real k_v(unit="V/K") = partial_SolarPanelSpecSheet.k_v
    "temperature coefficient of V_oc";
  parameter Real N=partial_SolarPanelSpecSheet.N "number of cells";

  // Extracted Parameters (using MATLAB)
  parameter Real n_d=partial_SolarPanelSpecSheet.n_d
    "diode quality (ideality) factor";
  parameter Real R_s(unit="ohm") = partial_SolarPanelSpecSheet.R_s
    "module series resistance";
  parameter Real R_sh(unit="ohm") = partial_SolarPanelSpecSheet.R_sh
    "module shunt resistance";

  // Breakdown Parameters
  parameter Real a(unit="1/ohm") = partial_SolarPanelSpecSheet.a
    "fraction of ohmic current in avalanche breakdown";
  parameter Real V_br(unit="V") = partial_SolarPanelSpecSheet.V_br
    "breakdown voltage of one cell";
  parameter Real m=partial_SolarPanelSpecSheet.m
    "exponent for avalanche breakdown";

  //Cost and mass
  parameter Modelica.SIunits.Mass mass = partial_SolarPanelSpecSheet.mass
    "Mass in kg of the panel";
  parameter Real cost_money(unit="yen") = partial_SolarPanelSpecSheet.cost_money "Cost";
  parameter Real cost_manhours(unit="hours") = partial_SolarPanelSpecSheet.cost_manhours
    "Time to build";

  //Size
  parameter Modelica.SIunits.Length length = partial_SolarPanelSpecSheet.length
    "Length of the panel in meters";
  parameter Modelica.SIunits.Length width = partial_SolarPanelSpecSheet.width
    "Width of the panel in meters";
  Modelica.SIunits.Area area "Area of the panel";

  // Variables
  Real I(unit="A") "module current";
  Real V(unit="V") "module voltage";

  // Insolation/Temperature-Dependent Variables
  Real V_T(unit="V") "temperature-dependent junction thermal voltage";
  Real I_sc_T(unit="A") "temperature-dependent short-circuit current";
  Real V_oc_T(unit="V") "temperature-dependent open-circuit voltage";
  Real I_s_T(unit="A") "temperature-dependent dark saturation current";
  Real I_ph_T(unit="A") "temperature-dependent photo-generated current";

  Real I_sc_GT(unit="A")
    "insolation/temperature-dependent short-circuit current";
  Real V_oc_GT(unit="V")
    "insolation/temperature-dependent open-circuit voltage";
  Real I_ph_star(unit="A")
    "insolation/temperature-dependent photo-generated current";
  Real I_ph_GT(unit="A")
    "insolation/temperature-dependent photo-generated current";
  Real I_s_GT(unit="A")
    "insolation/temperature-dependent dark saturation current";
public
  replaceable SpecSheets.Partial_SolarPanelSpecSheet
    partial_SolarPanelSpecSheet
    annotation (Placement(transformation(extent={{-84,54},{-64,74}})));
  Modelica.Mechanics.MultiBody.Parts.Body panelMass(                 m=mass,
      animation=true) "Model the mass of the component"
                                      annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-34,-48})));
  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2")
    "Input solar insolation"                                                                        annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-44,110}),                            iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=-90,
        origin={-49,103})));
  Modelica.Blocks.Interfaces.RealInput temp_kelvin annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={38,112}), iconTransformation(
        extent={{-13,-13},{13,13}},
        rotation=-90,
        origin={59,101})));
equation
  //Cost and mass
  mass_computed = mass;
  cost_money_computed = cost_money;
  cost_manhours_computed = cost_manhours;

  //Surface area
  area = length * width;

  // Temperature dependence:
  V_T =SolarBoatAutomation.Helpers.Constants.k*temp_kelvin/SolarBoatAutomation.Helpers.Constants.q;
  I_sc_T = I_sc*(1 + k_i*(temp_kelvin - T_STC)/100);
  V_oc_T = V_oc + k_v*(temp_kelvin - T_STC);
  I_s_T = (I_sc_T - (V_oc_T - I_sc_T*R_s)/R_sh)*exp(-
    V_oc_T/(n_d*N*V_T));
  I_ph_T = I_s_T*exp(V_oc_T/(n_d*N*V_T)) + V_oc_T/R_sh;

  // Followed by insolation dependence:
  I_sc_GT = I_sc_T*solarInsolation/G_STC;
  I_ph_star = I_ph_T*solarInsolation/G_STC;
  V_oc_GT = ln((I_ph_star*R_sh - V_oc_GT)/(I_s_T*R_sh))*n_d*N*V_T;

  // Using all of the above:
  I_s_GT = (I_sc_GT - (V_oc_GT - I_sc_GT*R_s)/R_sh)*exp(-
    V_oc_GT/(n_d*N*V_T));
  I_ph_GT = I_s_GT*exp(V_oc_GT/(n_d*N*V_T)) + V_oc_GT/R_sh;

  // Module single-diode equation, including breakdown:
  I = I_ph_GT - I_s_GT*(exp((V + I*R_s)/(n_d*N*V_T)) - 1) - ((V +
    I*R_s)/R_sh)*(1 + a*(1 - (V + I*R_s)/(N*V_br))^(-m));

  // External pins assignment; current goes out of positive pin!
  i = -I;
  v = V;
  connect(frame_a, panelMass.frame_a) annotation (Line(
      points={{0,-98},{-18,-98},{-18,-58},{-34,-58}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(graphics), Icon(graphics={Polygon(
          points={{-74,24},{24,-32},{88,46},{-20,84},{-74,24}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255})}));
end ParameterizedPanel;
