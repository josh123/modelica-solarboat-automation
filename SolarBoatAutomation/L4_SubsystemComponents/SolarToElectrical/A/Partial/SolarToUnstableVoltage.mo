within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Partial;
partial model SolarToUnstableVoltage
  extends SolarBoatAutomation.Helpers.Partial.PartialMassAttributes;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  Modelica.Blocks.Interfaces.RealInput solarInsolation(final unit="W.m-2")
    "Input solar insolation"                                                                        annotation (Placement(
        transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-70,112}),                            iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=-90,
        origin={-49,103})));
  Modelica.Blocks.Interfaces.RealInput temp_kelvin annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={12,114}), iconTransformation(
        extent={{-13,-13},{13,13}},
        rotation=-90,
        origin={59,101})));
  Modelica.Electrical.Analog.Interfaces.PositivePin
              p
    "Positive pin (potential p.v > n.v for positive voltage drop v)" annotation (Placement(
        transformation(extent={{-108,-10},{-88,10}}, rotation=0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin
              n "Negative pin" annotation (Placement(transformation(extent={{110,-10},
            {90,10}},          rotation=0)));
equation

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end SolarToUnstableVoltage;
