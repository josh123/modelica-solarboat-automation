within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Partial;
partial model SysArch_1Px1S_Array
  extends SolarToUnstableVoltage;
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  parameter Modelica.SIunits.Voltage total_V_oc = parameterizedPanel.partial_SolarPanelSpecSheet.V_oc;
equation
  mass_computed = parameterizedPanel.mass_computed;
  cost_money_computed = parameterizedPanel.cost_money_computed;
  cost_manhours_computed = parameterizedPanel.cost_manhours_computed;
  connect(solarInsolation, parameterizedPanel.solarInsolation) annotation (Line(
      points={{-70,112},{-70,66},{-4.9,66},{-4.9,10.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel.temp_kelvin) annotation (Line(
      points={{12,114},{12,30},{5.9,30},{5.9,10.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel.frame_a, frame_a) annotation (Line(
      points={{0,-9.8},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel.n, n) annotation (Line(
      points={{10,0},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel.p, p) annotation (Line(
      points={{-10,0},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(graphics={Polygon(
          points={{-84,18},{14,-38},{78,40},{-30,78},{-84,18}},
          lineColor={0,0,255},
          smooth=Smooth.None,
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255})}));
end SysArch_1Px1S_Array;
