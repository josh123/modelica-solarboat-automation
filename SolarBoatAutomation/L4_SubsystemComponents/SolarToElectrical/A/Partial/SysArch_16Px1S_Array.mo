within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Partial;
partial model SysArch_16Px1S_Array
  extends SolarToUnstableVoltage;
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel
    annotation (Placement(transformation(extent={{-96,-130},{-82,-120}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel1
    annotation (Placement(transformation(extent={{36,-70},{50,-60}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel2
    annotation (Placement(transformation(extent={{36,48},{50,58}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel3
    annotation (Placement(transformation(extent={{4,-248},{20,-238}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel4
    annotation (Placement(transformation(extent={{44,74},{60,86}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel5
    annotation (Placement(transformation(extent={{24,-280},{40,-270}})));
  parameter Modelica.SIunits.Voltage total_V_oc = parameterizedPanel.partial_SolarPanelSpecSheet.V_oc;

  replaceable Components.Partial.ParameterizedPanel parameterizedPanel6
    annotation (Placement(transformation(extent={{34,22},{48,32}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel7
    annotation (Placement(transformation(extent={{-18,-222},{-2,-212}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel8
    annotation (Placement(transformation(extent={{34,-16},{48,-6}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel9
    annotation (Placement(transformation(extent={{-58,-178},{-42,-168}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel10
    annotation (Placement(transformation(extent={{34,-40},{48,-30}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel11
    annotation (Placement(transformation(extent={{-76,-156},{-60,-146}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel12
    annotation (Placement(transformation(extent={{34,4},{48,14}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel13
    annotation (Placement(transformation(extent={{-38,-198},{-22,-188}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel14
    annotation (Placement(transformation(extent={{26,-308},{42,-298}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel15
    annotation (Placement(transformation(extent={{28,-336},{44,-326}})));
equation
  mass_computed = parameterizedPanel.mass_computed + parameterizedPanel1.mass_computed + parameterizedPanel2.mass_computed + parameterizedPanel3.mass_computed + parameterizedPanel4.mass_computed + parameterizedPanel5.mass_computed + parameterizedPanel6.mass_computed + parameterizedPanel7.mass_computed + parameterizedPanel8.mass_computed + parameterizedPanel9.mass_computed + parameterizedPanel10.mass_computed + parameterizedPanel11.mass_computed + parameterizedPanel12.mass_computed + parameterizedPanel13.mass_computed + parameterizedPanel14.mass_computed + parameterizedPanel15.mass_computed;
  cost_money_computed = parameterizedPanel.cost_money_computed + parameterizedPanel1.cost_money_computed + parameterizedPanel2.cost_money_computed + parameterizedPanel3.cost_money_computed + parameterizedPanel4.cost_money_computed + parameterizedPanel5.cost_money_computed + parameterizedPanel6.cost_money_computed + parameterizedPanel7.cost_money_computed + parameterizedPanel8.cost_money_computed + parameterizedPanel9.cost_money_computed + parameterizedPanel10.cost_money_computed + parameterizedPanel11.cost_money_computed + parameterizedPanel12.cost_money_computed + parameterizedPanel13.cost_money_computed + parameterizedPanel14.cost_money_computed + parameterizedPanel15.cost_money_computed;
  cost_manhours_computed = parameterizedPanel.cost_manhours_computed + parameterizedPanel1.cost_manhours_computed + parameterizedPanel2.cost_manhours_computed + parameterizedPanel3.cost_manhours_computed + parameterizedPanel4.cost_manhours_computed + parameterizedPanel5.cost_manhours_computed + parameterizedPanel6.cost_manhours_computed + parameterizedPanel7.cost_manhours_computed + parameterizedPanel8.cost_manhours_computed + parameterizedPanel9.cost_manhours_computed + parameterizedPanel10.cost_manhours_computed + parameterizedPanel11.cost_manhours_computed + parameterizedPanel12.cost_manhours_computed + parameterizedPanel13.cost_manhours_computed + parameterizedPanel14.cost_manhours_computed + parameterizedPanel15.cost_manhours_computed;
  connect(parameterizedPanel1.frame_a, frame_a) annotation (Line(
      points={{43,-69.9},{43,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel.frame_a, frame_a) annotation (Line(
      points={{-89,-129.9},{-89,-82},{38,-82},{38,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(p, p) annotation (Line(
      points={{-98,0},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(n, n) annotation (Line(
      points={{100,0},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));

  connect(parameterizedPanel4.n, n) annotation (Line(
      points={{60,80},{76,80},{76,86},{100,86},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel2.n, n) annotation (Line(
      points={{50,53},{76,53},{76,58},{100,58},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel1.n, n) annotation (Line(
      points={{50,-65},{76,-65},{76,-60},{100,-60},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel6.n, n) annotation (Line(
      points={{48,27},{76,27},{76,32},{100,32},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel8.n, n) annotation (Line(
      points={{48,-11},{76,-11},{76,-6},{100,-6},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel10.n, n) annotation (Line(
      points={{48,-35},{76,-35},{76,-30},{100,-30},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel12.n, n) annotation (Line(
      points={{48,9},{76,9},{76,14},{100,14},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.frame_a, frame_a) annotation (Line(
      points={{-68,-155.9},{-68,-48},{54,-48},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel10.frame_a, frame_a) annotation (Line(
      points={{41,-39.9},{41,-48},{54,-48},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel9.frame_a, frame_a) annotation (Line(
      points={{-50,-177.9},{-50,-22},{54,-22},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel8.frame_a, frame_a) annotation (Line(
      points={{41,-15.9},{41,-22},{54,-22},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel13.frame_a, frame_a) annotation (Line(
      points={{-30,-197.9},{-30,0},{54,0},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel12.frame_a, frame_a) annotation (Line(
      points={{41,4.1},{41,0},{54,0},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel7.frame_a, frame_a) annotation (Line(
      points={{-10,-221.9},{-10,18},{54,18},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel6.frame_a, frame_a) annotation (Line(
      points={{41,22.1},{41,18},{54,18},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel3.frame_a, frame_a) annotation (Line(
      points={{12,-247.9},{12,42},{54,42},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel2.frame_a, frame_a) annotation (Line(
      points={{43,48.1},{43,42},{54,42},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel5.frame_a, frame_a) annotation (Line(
      points={{32,-279.9},{32,68},{54,68},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel4.frame_a, frame_a) annotation (Line(
      points={{52,74.12},{52,68},{54,68},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel4.temp_kelvin) annotation (Line(
      points={{12,114},{16,114},{16,94},{56.72,94},{56.72,86.06}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel2.temp_kelvin) annotation (Line(
      points={{12,114},{14,114},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel6.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{45.13,32.05},{45.13,38},{14,38},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel12.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{45.13,14.05},{45.13,18},{14,18},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel8.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{45.13,-5.95},{45.13,-2},{14,-2},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel10.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{45.13,-29.95},{45.13,-26},{14,-26},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel1.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{47.13,-59.95},{47.13,-50},{14,-50},{14,64},{47.13,64},{47.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(temp_kelvin, parameterizedPanel5.temp_kelvin) annotation (Line(
      points={{12,114},{8,114},{8,92},{36.72,92},{36.72,-269.95}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel3.temp_kelvin) annotation (Line(
      points={{12,114},{6,114},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel7.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-5.28,-211.95},{-5.28,40},{6,40},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel13.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-25.28,-187.95},{-25.28,18},{6,18},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel9.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-45.28,-167.95},{-45.28,-2},{6,-2},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel11.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-63.28,-145.95},{-63.28,-24},{6,-24},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-84.87,-119.95},{-84.87,-52},{6,-52},{6,64},{16.72,64},{16.72,-237.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarInsolation, parameterizedPanel4.solarInsolation) annotation (
      Line(
      points={{-70,112},{-66,112},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel2.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{39.57,58.15},{39.57,66},{-66,66},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel5.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{28.08,-269.85},{28.08,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel3.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{8.08,-237.85},{8.08,66},{-66,66},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel6.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{37.57,32.15},{37.57,38},{-66,38},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel12.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{37.57,14.15},{-66,14.15},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel13.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-33.92,-187.85},{-33.92,18},{-66,18},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel9.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-53.92,-167.85},{-53.92,-2},{-66,-2},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel8.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{37.57,-5.85},{-66,-5.85},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel11.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-71.92,-145.85},{-71.92,-24},{-66,-24},{-66,90},{48.08,90},{48.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel10.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{37.57,-29.85},{-16,-29.85},{-16,-32},{-66,-32},{-66,90},{48.08,90},
          {48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-92.43,-119.85},{-92.43,-54},{-66,-54},{-66,90},{48.08,90},{48.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel1.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{39.57,-59.85},{39.57,-60},{-66,-60},{-66,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel4.p, p) annotation (Line(
      points={{44,80},{-98,80},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel2.p, p) annotation (Line(
      points={{36,53},{-38,53},{-38,52},{-98,52},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel6.p, p) annotation (Line(
      points={{34,27},{-38,27},{-38,26},{-98,26},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel12.p, p) annotation (Line(
      points={{34,9},{-38,9},{-38,0},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel8.p, p) annotation (Line(
      points={{34,-11},{-36,-11},{-36,-14},{-98,-14},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel10.p, p) annotation (Line(
      points={{34,-35},{-38,-35},{-38,-38},{-98,-38},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel1.p, p) annotation (Line(
      points={{36,-65},{-38,-65},{-38,-64},{-98,-64},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel.p, p) annotation (Line(
      points={{-96,-125},{-104,-125},{-104,-64},{-98,-64},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel.n, n) annotation (Line(
      points={{-82,-125},{8,-125},{8,-124},{100,-124},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.p, p) annotation (Line(
      points={{-76,-151},{-90,-151},{-90,-148},{-104,-148},{-104,-64},{-98,-64},
          {-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel9.p, p) annotation (Line(
      points={{-58,-173},{-82,-173},{-82,-174},{-104,-174},{-104,-64},{-98,-64},
          {-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel13.p, p) annotation (Line(
      points={{-38,-193},{-70,-193},{-70,-190},{-104,-190},{-104,-64},{-98,-64},
          {-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel7.p, p) annotation (Line(
      points={{-18,-217},{-62,-217},{-62,-212},{-104,-212},{-104,-64},{-98,-64},
          {-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel3.p, p) annotation (Line(
      points={{4,-243},{-52,-243},{-52,-234},{-104,-234},{-104,-64},{-98,-64},{-98,
          0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel5.p, p) annotation (Line(
      points={{24,-275},{-40,-275},{-40,-256},{-104,-256},{-104,-64},{-98,-64},{
          -98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel5.n, n) annotation (Line(
      points={{40,-275},{70,-275},{70,-262},{100,-262},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel3.n, n) annotation (Line(
      points={{20,-243},{58,-243},{58,-236},{100,-236},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel7.n, n) annotation (Line(
      points={{-2,-217},{48,-217},{48,-214},{100,-214},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel13.n, n) annotation (Line(
      points={{-22,-193},{40,-193},{40,-194},{100,-194},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel9.n, n) annotation (Line(
      points={{-42,-173},{28,-173},{28,-172},{100,-172},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.n, n) annotation (Line(
      points={{-60,-151},{20,-151},{20,-152},{100,-152},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel7.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-13.92,-211.85},{-13.92,90},{48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel14.p, p) annotation (Line(
      points={{26,-303},{-38,-303},{-38,-302},{-104,-302},{-104,-64},{-98,-64},{
          -98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel15.p, p) annotation (Line(
      points={{28,-331},{-36,-331},{-36,-330},{-104,-330},{-104,-64},{-98,-64},{
          -98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel14.n, n) annotation (Line(
      points={{42,-303},{72,-303},{72,-302},{100,-302},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel15.n, n) annotation (Line(
      points={{44,-331},{72,-331},{72,-332},{100,-332},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel14.frame_a, frame_a) annotation (Line(
      points={{34,-307.9},{34,-314},{52,-314},{52,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel15.frame_a, frame_a) annotation (Line(
      points={{36,-335.9},{36,-354},{52,-354},{52,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel14.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{30.08,-297.85},{8,-297.85},{8,-260},{28,-260},{28.08,90},{48.08,90},
          {48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel15.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{32.08,-325.85},{32.08,-318},{8,-318},{8,-260},{28,-260},{28.08,90},
          {48.08,90},{48.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel14.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{38.72,-297.95},{62,-297.95},{62,-252},{36,-252},{36.72,-269.95}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel15.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{40.72,-325.95},{40.72,-318},{62,-318},{62,-252},{36,-252},{36.72,
          -269.95}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-14,58},{20,34}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-14,88},{20,64}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-14,0},{20,-24}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-14,28},{20,4}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid)}));
end SysArch_16Px1S_Array;
