within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Partial;
partial model SysArch_3Px2S_Array
  extends SolarToUnstableVoltage;
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel
    annotation (Placement(transformation(extent={{-42,-38},{-14,-16}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel1
    annotation (Placement(transformation(extent={{22,-38},{50,-16}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel2
    annotation (Placement(transformation(extent={{24,10},{52,32}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel3
    annotation (Placement(transformation(extent={{-40,10},{-12,32}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel4
    annotation (Placement(transformation(extent={{24,48},{52,70}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel5
    annotation (Placement(transformation(extent={{-40,48},{-12,70}})));
  parameter Modelica.SIunits.Voltage total_V_oc = 2*parameterizedPanel.partial_SolarPanelSpecSheet.V_oc;

equation
  mass_computed = parameterizedPanel.mass_computed + parameterizedPanel1.mass_computed + parameterizedPanel2.mass_computed + parameterizedPanel3.mass_computed + parameterizedPanel4.mass_computed + parameterizedPanel5.mass_computed;
  cost_money_computed = parameterizedPanel.cost_money_computed + parameterizedPanel1.cost_money_computed + parameterizedPanel2.cost_money_computed + parameterizedPanel3.cost_money_computed + parameterizedPanel4.cost_money_computed + parameterizedPanel5.cost_money_computed;
  cost_manhours_computed = parameterizedPanel.cost_manhours_computed + parameterizedPanel1.cost_manhours_computed + parameterizedPanel2.cost_manhours_computed + parameterizedPanel3.cost_manhours_computed + parameterizedPanel4.cost_manhours_computed + parameterizedPanel5.cost_manhours_computed;
  connect(parameterizedPanel.n, parameterizedPanel1.p) annotation (Line(
      points={{-14,-27},{22,-27}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel1.frame_a, frame_a) annotation (Line(
      points={{36,-37.78},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel.frame_a, frame_a) annotation (Line(
      points={{-28,-37.78},{-28,-52},{36,-52},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel3.n, parameterizedPanel2.p) annotation (
      Line(
      points={{-12,21},{24,21}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(p, p) annotation (Line(
      points={{-98,0},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(n, n) annotation (Line(
      points={{100,0},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel2.frame_a, frame_a) annotation (Line(
      points={{38,10.22},{38,4},{66,4},{66,-52},{36,-52},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel3.frame_a, frame_a) annotation (Line(
      points={{-26,10.22},{-26,4},{66,4},{66,-52},{36,-52},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel5.n, parameterizedPanel4.p) annotation (
      Line(
      points={{-12,59},{24,59}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel4.frame_a, frame_a) annotation (Line(
      points={{38,48.22},{38,42},{66,42},{66,-52},{36,-52},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel5.frame_a, frame_a) annotation (Line(
      points={{-26,48.22},{-26,42},{66,42},{66,-52},{36,-52},{36,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));

  connect(temp_kelvin, parameterizedPanel4.temp_kelvin) annotation (Line(
      points={{12,114},{12,88},{46.26,88},{46.26,70.11}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel5.temp_kelvin) annotation (Line(
      points={{12,114},{12,88},{-17.74,88},{-17.74,70.11}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel3.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{-17.74,32.11},{-17.74,38},{18,38},{18,88},{-17.74,88},{-17.74,70.11}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel2.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{46.26,32.11},{46.26,38},{18,38},{18,88},{-17.74,88},{-17.74,70.11}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel1.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{44.26,-15.89},{44.26,-4},{18,-4},{18,88},{-17.74,88},{-17.74,70.11}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel.temp_kelvin, parameterizedPanel5.temp_kelvin)
    annotation (Line(
      points={{-19.74,-15.89},{-19.74,-4},{18,-4},{18,88},{-17.74,88},{-17.74,70.11}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarInsolation, parameterizedPanel4.solarInsolation) annotation (
     Line(
      points={{-70,112},{-40,112},{-40,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel5.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-32.86,70.33},{-32.86,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel3.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-32.86,32.33},{-32.86,40},{-4,40},{-4,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-34.86,-15.67},{-34.86,-2},{-4,-2},{-4,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel2.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{31.14,32.33},{31.14,40},{-4,40},{-4,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel1.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{29.14,-15.67},{29.14,-2},{-4,-2},{-4,84},{31.14,84},{31.14,70.33}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel5.p, p) annotation (Line(
      points={{-40,59},{-70,59},{-70,60},{-98,60},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel3.p, p) annotation (Line(
      points={{-40,21},{-70,21},{-70,20},{-98,20},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel.p, p) annotation (Line(
      points={{-42,-27},{-98,-27},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel4.n, n) annotation (Line(
      points={{52,59},{76,59},{76,60},{100,60},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel2.n, n) annotation (Line(
      points={{52,21},{76,21},{76,20},{100,20},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel1.n, n) annotation (Line(
      points={{50,-27},{74,-27},{74,-30},{100,-30},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-64,68},{-30,44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{2,68},{36,44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-64,26},{-30,2}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,26},{38,2}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-64,-14},{-30,-38}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,-14},{38,-38}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid)}));
end SysArch_3Px2S_Array;
