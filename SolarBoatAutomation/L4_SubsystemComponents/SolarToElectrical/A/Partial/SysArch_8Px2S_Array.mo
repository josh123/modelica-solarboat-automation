within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Partial;
partial model SysArch_8Px2S_Array
  extends SolarToUnstableVoltage;
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel
    annotation (Placement(transformation(extent={{-40,-62},{-26,-52}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel1
    annotation (Placement(transformation(extent={{24,-62},{38,-52}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel2
    annotation (Placement(transformation(extent={{24,48},{38,58}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel3
    annotation (Placement(transformation(extent={{-40,48},{-24,58}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel4
    annotation (Placement(transformation(extent={{24,74},{40,86}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel5
    annotation (Placement(transformation(extent={{-40,74},{-24,84}})));
  parameter Modelica.SIunits.Voltage total_V_oc = 2*parameterizedPanel.partial_SolarPanelSpecSheet.V_oc;

  replaceable Components.Partial.ParameterizedPanel parameterizedPanel6
    annotation (Placement(transformation(extent={{24,22},{38,32}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel7
    annotation (Placement(transformation(extent={{-40,22},{-24,32}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel8
    annotation (Placement(transformation(extent={{24,-16},{38,-6}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel9
    annotation (Placement(transformation(extent={{-40,-16},{-24,-6}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel10
    annotation (Placement(transformation(extent={{24,-40},{38,-30}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel11
    annotation (Placement(transformation(extent={{-40,-40},{-24,-30}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel12
    annotation (Placement(transformation(extent={{24,4},{38,14}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel13
    annotation (Placement(transformation(extent={{-40,4},{-24,14}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel14
    annotation (Placement(transformation(extent={{-40,-84},{-24,-74}})));
  replaceable Components.Partial.ParameterizedPanel parameterizedPanel15
    annotation (Placement(transformation(extent={{24,-84},{38,-74}})));
equation
  mass_computed = parameterizedPanel.mass_computed + parameterizedPanel1.mass_computed + parameterizedPanel2.mass_computed + parameterizedPanel3.mass_computed + parameterizedPanel4.mass_computed + parameterizedPanel5.mass_computed + parameterizedPanel6.mass_computed + parameterizedPanel7.mass_computed + parameterizedPanel8.mass_computed + parameterizedPanel9.mass_computed + parameterizedPanel10.mass_computed + parameterizedPanel11.mass_computed + parameterizedPanel12.mass_computed + parameterizedPanel13.mass_computed;
  cost_money_computed = parameterizedPanel.cost_money_computed + parameterizedPanel1.cost_money_computed + parameterizedPanel2.cost_money_computed + parameterizedPanel3.cost_money_computed + parameterizedPanel4.cost_money_computed + parameterizedPanel5.cost_money_computed + parameterizedPanel6.cost_money_computed + parameterizedPanel7.cost_money_computed + parameterizedPanel8.cost_money_computed + parameterizedPanel9.cost_money_computed + parameterizedPanel10.cost_money_computed + parameterizedPanel11.cost_money_computed + parameterizedPanel12.cost_money_computed + parameterizedPanel13.cost_money_computed;
  cost_manhours_computed = parameterizedPanel.cost_manhours_computed + parameterizedPanel1.cost_manhours_computed + parameterizedPanel2.cost_manhours_computed + parameterizedPanel3.cost_manhours_computed + parameterizedPanel4.cost_manhours_computed + parameterizedPanel5.cost_manhours_computed + parameterizedPanel6.cost_manhours_computed + parameterizedPanel7.cost_manhours_computed + parameterizedPanel8.cost_manhours_computed + parameterizedPanel9.cost_manhours_computed + parameterizedPanel10.cost_manhours_computed + parameterizedPanel11.cost_manhours_computed + parameterizedPanel12.cost_manhours_computed + parameterizedPanel13.cost_manhours_computed;
  connect(parameterizedPanel.n, parameterizedPanel1.p) annotation (Line(
      points={{-26,-57},{-14,-57},{-14,-56},{-2,-56},{-2,-57},{24,-57}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel3.n, parameterizedPanel2.p) annotation (
      Line(
      points={{-24,53},{24,53}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(p, p) annotation (Line(
      points={{-98,0},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(n, n) annotation (Line(
      points={{100,0},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel5.n, parameterizedPanel4.p) annotation (
      Line(
      points={{-24,79},{2,79},{2,80},{24,80}},
      color={0,0,255},
      smooth=Smooth.None));

  connect(parameterizedPanel5.p, p) annotation (Line(
      points={{-40,79},{-70,79},{-70,86},{-98,86},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel3.p, p) annotation (Line(
      points={{-40,53},{-70,53},{-70,58},{-98,58},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel.p, p) annotation (Line(
      points={{-40,-57},{-98,-57},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel4.n, n) annotation (Line(
      points={{40,80},{76,80},{76,86},{100,86},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel2.n, n) annotation (Line(
      points={{38,53},{76,53},{76,58},{100,58},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel1.n, n) annotation (Line(
      points={{38,-57},{76,-57},{76,-60},{100,-60},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel7.n,parameterizedPanel6. p) annotation (
      Line(
      points={{-24,27},{24,27}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel7.p, p) annotation (Line(
      points={{-40,27},{-70,27},{-70,32},{-98,32},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel6.n, n) annotation (Line(
      points={{38,27},{76,27},{76,32},{100,32},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel9.n,parameterizedPanel8. p) annotation (
      Line(
      points={{-24,-11},{24,-11}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel9.p, p) annotation (Line(
      points={{-40,-11},{-70,-11},{-70,-6},{-98,-6},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel8.n, n) annotation (Line(
      points={{38,-11},{76,-11},{76,-6},{100,-6},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.n, parameterizedPanel10.p) annotation (Line(
      points={{-24,-35},{24,-35}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.p, p) annotation (Line(
      points={{-40,-35},{-70,-35},{-70,-30},{-98,-30},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel10.n, n) annotation (Line(
      points={{38,-35},{76,-35},{76,-30},{100,-30},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel13.n, parameterizedPanel12.p) annotation (Line(
      points={{-24,9},{24,9}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel13.p, p) annotation (Line(
      points={{-40,9},{-70,9},{-70,14},{-98,14},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel12.n, n) annotation (Line(
      points={{38,9},{76,9},{76,14},{100,14},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel11.frame_a, frame_a) annotation (Line(
      points={{-32,-39.9},{-32,-48},{54,-48},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel10.frame_a, frame_a) annotation (Line(
      points={{31,-39.9},{32,-48},{54,-48},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel9.frame_a, frame_a) annotation (Line(
      points={{-32,-15.9},{-32,-22},{54,-22},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel8.frame_a, frame_a) annotation (Line(
      points={{31,-15.9},{32,-22},{54,-22},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel13.frame_a, frame_a) annotation (Line(
      points={{-32,4.1},{-32,0},{54,0},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel12.frame_a, frame_a) annotation (Line(
      points={{31,4.1},{30,0},{54,0},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel7.frame_a, frame_a) annotation (Line(
      points={{-32,22.1},{-32,18},{54,18},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel6.frame_a, frame_a) annotation (Line(
      points={{31,22.1},{30,18},{54,18},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel3.frame_a, frame_a) annotation (Line(
      points={{-32,48.1},{-32,42},{54,42},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel2.frame_a, frame_a) annotation (Line(
      points={{31,48.1},{30,42},{54,42},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel5.frame_a, frame_a) annotation (Line(
      points={{-32,74.1},{-32,68},{54,68},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel4.frame_a, frame_a) annotation (Line(
      points={{32,74.12},{32,68},{54,68},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel4.temp_kelvin) annotation (Line(
      points={{12,114},{16,114},{16,94},{36.72,94},{36.72,86.06}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel2.temp_kelvin) annotation (Line(
      points={{12,114},{14,114},{14,64},{35.13,64},{35.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel6.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,32.05},{35.13,38},{14,38},{14,64},{35.13,64},{35.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel12.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,14.05},{35.13,18},{14,18},{14,64},{35.13,64},{35.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel8.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,-5.95},{35.13,-2},{14,-2},{14,64},{35.13,64},{35.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel10.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,-29.95},{35.13,-26},{14,-26},{14,64},{35.13,64},{35.13,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel1.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,-51.95},{35.13,-50},{14,-50},{14,64},{35.13,64},{35.13,
          58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(temp_kelvin, parameterizedPanel5.temp_kelvin) annotation (Line(
      points={{12,114},{8,114},{8,92},{-27.28,92},{-27.28,84.05}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(temp_kelvin, parameterizedPanel3.temp_kelvin) annotation (Line(
      points={{12,114},{6,114},{6,64},{-27.28,64},{-27.28,58.05}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel7.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-27.28,32.05},{-27.28,40},{6,40},{6,64},{-27.28,64},{-27.28,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel13.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-27.28,14.05},{-27.28,18},{6,18},{6,64},{-27.28,64},{-27.28,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel9.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-27.28,-5.95},{-27.28,-2},{6,-2},{6,64},{-27.28,64},{-27.28,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel11.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-27.28,-29.95},{-27.28,-24},{6,-24},{6,64},{-27.28,64},{-27.28,58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-28.87,-51.95},{-28.87,-52},{6,-52},{6,64},{-27.28,64},{-27.28,
          58.05}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarInsolation, parameterizedPanel4.solarInsolation) annotation (
      Line(
      points={{-70,112},{-66,112},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel2.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,58.15},{27.57,66},{-66,66},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel5.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,84.15},{-36,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel3.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,58.15},{-36,66},{-66,66},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel6.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,32.15},{27.57,38},{-66,38},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel12.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,14.15},{-66,14.15},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel13.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,14.15},{-35.92,18},{-66,18},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel9.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,-5.85},{-35.92,-2},{-66,-2},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel8.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,-5.85},{-66,-5.85},{-66,90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel11.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,-29.85},{-35.92,-24},{-66,-24},{-66,90},{28.08,90},{28.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel10.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,-29.85},{-16,-29.85},{-16,-32},{-66,-32},{-66,90},{28.08,90},
          {28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-36.43,-51.85},{-36.43,-54},{-66,-54},{-66,90},{28.08,90},{28.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel1.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,-51.85},{27.57,-60},{-66,-60},{-66,90},{28.08,90},{28.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(parameterizedPanel.frame_a, frame_a) annotation (Line(
      points={{-33,-61.9},{-33,-66},{54,-66},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel1.frame_a, frame_a) annotation (Line(
      points={{31,-61.9},{32,-66},{54,-66},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel14.n,parameterizedPanel15. p) annotation (Line(
      points={{-24,-79},{24,-79}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel15.n, n) annotation (Line(
      points={{38,-79},{68,-79},{68,-78},{100,-78},{100,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel14.p, p) annotation (Line(
      points={{-40,-79},{-68,-79},{-68,-80},{-98,-80},{-98,0}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(parameterizedPanel15.frame_a, frame_a) annotation (Line(
      points={{31,-83.9},{31,-88},{54,-88},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel14.frame_a, frame_a) annotation (Line(
      points={{-32,-83.9},{-32,-88},{54,-88},{54,-98},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(parameterizedPanel14.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{-35.92,-73.85},{-35.92,-70},{-66,-70},{-66,90},{28.08,90},{28.08,
          86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel14.temp_kelvin, parameterizedPanel3.temp_kelvin)
    annotation (Line(
      points={{-27.28,-73.95},{-27.28,-68},{6,-68},{6,64},{-27.28,64},{-27.28,
          58.05}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel15.solarInsolation, parameterizedPanel4.solarInsolation)
    annotation (Line(
      points={{27.57,-73.85},{27.57,-72},{-36,-72},{-35.92,-70},{-66,-70},{-66,
          90},{28.08,90},{28.08,86.18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(parameterizedPanel15.temp_kelvin, parameterizedPanel2.temp_kelvin)
    annotation (Line(
      points={{35.13,-73.95},{35.13,-68},{14,-68},{14,64},{35.13,64},{35.13,
          58.05}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-64,68},{-30,44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{2,68},{36,44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-64,26},{-30,2}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,26},{38,2}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-64,-14},{-30,-38}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{4,-14},{38,-38}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid)}));
end SysArch_8Px2S_Array;
