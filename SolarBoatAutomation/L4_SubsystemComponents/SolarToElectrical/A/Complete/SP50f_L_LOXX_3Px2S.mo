within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Complete;
model SP50f_L_LOXX_3Px2S
  extends Partial.SysArch_3Px2S_Array(
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel5,
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel3,
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel,
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel1,
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel2,
    redeclare Components.Complete.SP50f_L_LOXX
                                          parameterizedPanel4);
end SP50f_L_LOXX_3Px2S;
