within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Complete;
model SP18f_L_LOXX_16Px1S
  extends Partial.SysArch_16Px1S_Array(
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel4,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel2,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel6,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel12,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel8,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel10,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel1,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel11,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel9,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel13,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel7,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel3,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel5,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel14,
    redeclare Components.Complete.SP18f_L_LOXX parameterizedPanel15);
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end SP18f_L_LOXX_16Px1S;
