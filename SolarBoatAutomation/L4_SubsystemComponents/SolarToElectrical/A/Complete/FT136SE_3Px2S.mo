within SolarBoatAutomation.L4_SubsystemComponents.SolarToElectrical.A.Complete;
model FT136SE_3Px2S
  extends Partial.SysArch_3Px2S_Array(
    redeclare Components.Complete.FT136SE parameterizedPanel5,
    redeclare Components.Complete.FT136SE parameterizedPanel3,
    redeclare Components.Complete.FT136SE parameterizedPanel,
    redeclare Components.Complete.FT136SE parameterizedPanel1,
    redeclare Components.Complete.FT136SE parameterizedPanel2,
    redeclare Components.Complete.FT136SE parameterizedPanel4);
end FT136SE_3Px2S;
