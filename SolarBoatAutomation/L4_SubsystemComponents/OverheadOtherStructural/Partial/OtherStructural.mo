within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.Partial;
partial model OtherStructural
  import SolarBoatAutomation;
  extends
    SolarBoatAutomation.L3_Subsystems.OverheadSubsystems.Partial.PartialOverheadComponents;
  parameter Modelica.SIunits.Mass mass = partial_OverheadStructuralComponents.mass
    "Mass structural components not accounted for";
  parameter Real cost_money(unit="yen") = partial_OverheadStructuralComponents.cost_money "Cost";
  parameter Real cost_manhours(unit="hours") = partial_OverheadStructuralComponents.cost_manhours
    "Time to build";
  Modelica.Mechanics.MultiBody.Parts.Body lumpedMass(m=mass)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-16,-28})));
  replaceable
    SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.SpecSheets.Partial_OverheadStructuralComponents
    partial_OverheadStructuralComponents
    annotation (Placement(transformation(extent={{-70,46},{-18,72}})));
equation
  cost_manhours_computed = cost_manhours;
  cost_money_computed = cost_money;
  mass_computed = mass;
  connect(lumpedMass.frame_a, frame_a) annotation (Line(
      points={{-6,-28},{0,-28},{0,-98}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end OtherStructural;
