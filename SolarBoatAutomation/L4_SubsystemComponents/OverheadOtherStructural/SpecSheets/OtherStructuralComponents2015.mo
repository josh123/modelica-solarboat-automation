within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.SpecSheets;
record OtherStructuralComponents2015
  extends Partial_OverheadStructuralComponents(mass = 2+0.3,
    cost_money = 0,
    cost_manhours = 1);
end OtherStructuralComponents2015;
