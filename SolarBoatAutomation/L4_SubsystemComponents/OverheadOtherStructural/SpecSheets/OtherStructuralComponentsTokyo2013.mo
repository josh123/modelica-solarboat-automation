within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.SpecSheets;
record OtherStructuralComponentsTokyo2013
  extends Partial_OverheadStructuralComponents(mass = 2+3,
    cost_money = 0,
    cost_manhours = 1);
end OtherStructuralComponentsTokyo2013;
