within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.SpecSheets;
record OtherStructuralComponentsKTH2014
  extends Partial_OverheadStructuralComponents(mass = 2+3,
    cost_money = 0,
    cost_manhours = 1);
end OtherStructuralComponentsKTH2014;
