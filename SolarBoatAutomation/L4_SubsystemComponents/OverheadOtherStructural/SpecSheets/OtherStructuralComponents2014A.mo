within SolarBoatAutomation.L4_SubsystemComponents.OverheadOtherStructural.SpecSheets;
record OtherStructuralComponents2014A
  extends Partial_OverheadStructuralComponents(mass = 2,
    cost_money = 0,
    cost_manhours = 1);
end OtherStructuralComponents2014A;
