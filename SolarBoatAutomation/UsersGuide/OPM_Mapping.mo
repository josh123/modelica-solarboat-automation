within SolarBoatAutomation.UsersGuide;
class OPM_Mapping "Defines the mapping from OPM to Modelica"
  extends Modelica.Icons.Information;

  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Provide some guidance and rules for converting OPM to Modelica models</li>
</ul>
<h4><span style=\"color:#008000\">Mapping summary</span></h4>
<p>The following PowerPoint slides provide a summary of the mapping.</p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide1.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide2.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide3.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide4.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide5.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide6.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide7.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide8.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide9.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide10.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide11.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide12.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide13.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide14.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide15.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide16.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide17.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide18.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide19.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide20.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide21.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide22.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide23.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide24.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide25.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide26.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide27.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide28.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide29.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide30.JPG\"/></p>
<p><img src=\"modelica://SolarBoatAutomation/Images/slides/OPMModelicaLink/Slide31.JPG\"/></p>
</html>"));
end OPM_Mapping;
