within SolarBoatAutomation;
package UsersGuide "User's Guide to this package"
    extends Modelica.Icons.Information;


  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<p>This user&apos;s guide attempts to describe how to use this library of components to develop a SolarBoat design and update the libraray with new components.</p>
</html>"));
end UsersGuide;
