within SolarBoatAutomation.Helpers.TestBeds.Partial;
partial model FixedSpeedTowingTank

  inner Modelica.Mechanics.MultiBody.World world(
    driveTrainMechanics3D=true,
    n={0,0,1},
    g=9.81)    annotation (Placement(transformation(extent={{-90,50},{-70,70}},
          rotation=0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox TestRig(r={0,0,0.3})
    annotation (Placement(transformation(extent={{8,50},{28,70}})));
  Modelica.Mechanics.MultiBody.Sensors.CutForceAndTorque cutForceAndTorque
    annotation (Placement(transformation(extent={{-20,50},{0,70}})));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(n={1,0,0},
      useAxisFlange=true)
    annotation (Placement(transformation(extent={{-58,50},{-38,70}})));
  Modelica.Mechanics.Translational.Sources.Speed speed
    annotation (Placement(transformation(extent={{-66,78},{-46,98}})));
  Modelica.Blocks.Sources.RealExpression x_speed(y=2)
    annotation (Placement(transformation(extent={{-98,76},{-78,96}})));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity
    annotation (Placement(transformation(extent={{38,72},{58,92}})));
  Visualization.CompletedModels.VisualEnvironment visualEnvironment
    annotation (Placement(transformation(extent={{-50,4},{-22,24}})));
equation
  connect(TestRig.frame_a,cutForceAndTorque. frame_b) annotation (Line(
      points={{8,60},{0,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(world.frame_b, prismatic.frame_a) annotation (Line(
      points={{-70,60},{-58,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(prismatic.frame_b, cutForceAndTorque.frame_a) annotation (Line(
      points={{-38,60},{-20,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(speed.flange, prismatic.axis) annotation (Line(
      points={{-46,88},{-40,88},{-40,66}},
      color={0,127,0},
      smooth=Smooth.None));
  connect(x_speed.y, speed.v_ref) annotation (Line(
      points={{-77,86},{-74,86},{-74,88},{-68,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(absoluteVelocity.frame_a, TestRig.frame_b) annotation (Line(
      points={{38,82},{36,82},{36,60},{28,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(visualEnvironment.frame_a, prismatic.frame_a) annotation (Line(
      points={{-50,14},{-62,14},{-62,60},{-58,60}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end FixedSpeedTowingTank;
