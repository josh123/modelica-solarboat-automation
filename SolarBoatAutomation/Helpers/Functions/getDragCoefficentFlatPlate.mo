within SolarBoatAutomation.Helpers.Functions;
function getDragCoefficentFlatPlate
  "Computes flat plate drag coefficent, based on 1957 ITTC line"
  input Modelica.SIunits.Velocity velocity "Fluid relatative velocity";
  input Modelica.SIunits.Length characteristic_length
    "Fluid relatative velocity";
  output Real cd_drag_coefficent "Drag coefficent";
  output Modelica.SIunits.ReynoldsNumber reynolds_number "Reynolds number";
algorithm
  reynolds_number :=getReynoldsNumber(velocity, characteristic_length);
  if reynolds_number == 0 then
    cd_drag_coefficent := 0;
  else
    cd_drag_coefficent :=0.075/((log10(abs(reynolds_number)) - 2)^2);
  end if;
end getDragCoefficentFlatPlate;
