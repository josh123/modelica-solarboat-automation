within SolarBoatAutomation.Helpers.Functions;
function getFroudeNumber
  input Modelica.SIunits.Velocity velocity;
  input Modelica.SIunits.Length wet_length;
  output Real froude_number;
algorithm
    froude_number :=abs(velocity)/sqrt(Modelica.Constants.g_n*wet_length);
end getFroudeNumber;
