within SolarBoatAutomation.Helpers.Functions;
function getReynoldsNumber
  input Modelica.SIunits.Velocity velocity "Fluid relatative velocity";
  input Modelica.SIunits.Length characteristic_length
    "Fluid relatative velocity";
  output Modelica.SIunits.ReynoldsNumber reynolds_number "Reynolds number";
algorithm
  reynolds_number :=(velocity*characteristic_length)/SolarBoatAutomation.Helpers.Constants.water_kinematic_viscosity_10Degrees;
end getReynoldsNumber;
