within SolarBoatAutomation.Helpers.Sensors;
model RotationSensor_MultiUnits
  extends Modelica.Mechanics.Rotational.Sensors.MultiSensor;
  Real rotaion_per_second_n
    "Angular velocity in the units of rotations per second";
  Real rpm "Angular velocity in RPM";
equation
  rotaion_per_second_n = w/(2*Modelica.Constants.pi);
  rpm = rotaion_per_second_n*60;
end RotationSensor_MultiUnits;
