within SolarBoatAutomation.Helpers.Partial.Examples;
model FixedParameterCost
  "Example of extending PartialProcurementAttributes and setting varialbes to values from parameters"
  extends Modelica.Icons.Example;
  extends SolarBoatAutomation.Helpers.Partial.PartialProcurementAttributes;
  parameter Real cost_money(unit="yen") = 1 "Cost";
  parameter Real cost_manhours(unit="hours") = 1 "Time to build";
equation
  cost_money_computed = cost_money;
  cost_manhours_computed = cost_manhours;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Show the assignment to variables from parameters</li>
</ul>
</html>"));
end FixedParameterCost;
