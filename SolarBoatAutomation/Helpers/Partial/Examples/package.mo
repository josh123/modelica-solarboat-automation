within SolarBoatAutomation.Helpers.Partial;
package Examples "Some examples using the PartialModels"
  extends Modelica.Icons.ExamplesPackage;


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Provide examples to show how system works</li>
</ul>
</html>"));
end Examples;
