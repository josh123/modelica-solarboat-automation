within SolarBoatAutomation.Helpers;
package Partial "Provides various common attributes which different subsystems might need"


annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Ensure that components will need to provide certain attributes do so in a consistent way</li>
</ul>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>Group common attributes together such that all relevant ones can be inhereted at once</li>
</ul>
</html>"));
end Partial;
