within SolarBoatAutomation.Helpers.Partial;
partial model PartialInAFluidAttributes
  "Provides attributes which all components which displace fluid must provide"
  Modelica.SIunits.Force drag_force "Force caused by moving through the water";
  Modelica.SIunits.Volume displaced_volume "Volume displaced by the main hull";
  Modelica.SIunits.Force buoyancy_force "Force caused by displaced volume";
equation

  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Provide a common set of attributes which all sub systems which displace a fluid must provide</li>
</ul>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li><code>drag_force&nbsp;</code>is defined as a variable. It is likely a function of velocity.</li>
<li><code>displaced_volume</code> is defined as a variable.</li>
<li><code>buoyancy_force</code> is defined as a variable. It is a function of <code>buoyancy_force</code></li>
</ul>
<h4><span style=\"color:#008000\">TODO</span></h4>
<ul>
<li>Compute <code>buoyancy_force</code>  here</li>
</ul>
</html>"));
end PartialInAFluidAttributes;
