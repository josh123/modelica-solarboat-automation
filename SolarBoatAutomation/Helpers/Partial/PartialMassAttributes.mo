within SolarBoatAutomation.Helpers.Partial;
partial model PartialMassAttributes
  "Provides attributes which all components with mass must provide"
  Modelica.SIunits.Mass mass_computed "Mass of the subsystem";
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a
    "To attach the component rigidly to other components"                                                       annotation (Placement(
        transformation(
        extent={{-16,-16},{16,16}},
        rotation=0,
        origin={0,-98})));
equation

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Text(
          extent={{-50,-72},{52,-34}},
          lineColor={0,0,255},
          textString="%name")}), Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Provide a common set of attributes which all sub systems with mass must provide</li>
</ul>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li>mass_computed is defined as a variable. It can be assigned a value from a parameter or be calculated in simulation (e.g. volume * density)</li>
<li>A frame_a is provided so the component can be connected to other masses rigidly</li>
<li>When building a model by extending this, some sort of massive object from Modelica.Mechanics.MultiBody.Parts must be attached within the component to provide mass</li>
</ul>
</html>"));
end PartialMassAttributes;
