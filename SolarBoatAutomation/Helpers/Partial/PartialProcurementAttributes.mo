within SolarBoatAutomation.Helpers.Partial;
partial model PartialProcurementAttributes
  "Provides attributes which all components with cost must provide"
  Real cost_money_computed(unit="yen") "Predicted cost";
  Real cost_manhours_computed(unit="hours") "Predicted time to build";
  annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Purpose</span></h4>
<ul>
<li>Provide a common set of attributes which all sub systems which have cost must provide</li>
</ul>
<h4><span style=\"color:#008000\">Modeling approach</span></h4>
<ul>
<li><code>cost_money_computed</code> is defined as a variable. It can be assigned a value from a parameter or be calculated in simulation.</li>
<li><code>cost_manhours_computed</code> is defined as a variable. It can be assigned a value from a parameter or be calculated in simulation.</li>
</ul>
</html>"));
end PartialProcurementAttributes;
