within SolarBoatAutomation.Helpers.Visualization.CompletedModels;
model VisualEnvironment

  Modelica.Mechanics.MultiBody.Visualizers.FixedShape Water(
    animation=true,
    height=1,
    width=5,
    color={0,128,255},
    r_shape={-distance_markers_distance_between,0,0.5},
    length=200) "Visualization of the water"
    annotation (Placement(transformation(extent={{-52,78},{-32,98}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_1(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-26,54},{-6,74}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_0(
    shapeType="cylinder",
    r_shape={0,2,1},
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r={0,2,1}) "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-54,54},{-34,74}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_2(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{0,54},{20,74}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_3(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{26,54},{46,74}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_4(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{54,54},{74,74}})));
  parameter Modelica.SIunits.Length distance_markers_distance_between = 5
    "The distance between the distance markers";
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a annotation (Placement(
        transformation(rotation=0, extent={{-110,-10},{-90,10}}),
        iconTransformation(extent={{-110,-10},{-90,10}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_5(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-28,28},{-8,48}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_6(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-2,28},{18,48}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_7(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{24,28},{44,48}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_8(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{52,28},{72,48}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_9(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-28,0},{-8,20}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_10(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-2,0},{18,20}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_11(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{24,0},{44,20}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_12(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{52,0},{72,20}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_13(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-30,-26},{-10,-6}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_14(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-4,-26},{16,-6}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_15(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{22,-26},{42,-6}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_16(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{50,-26},{70,-6}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_17(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-28,-52},{-8,-32}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_18(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-2,-52},{18,-32}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_19(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{24,-52},{44,-32}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_20(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{52,-52},{72,-32}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_21(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-28,-80},{-8,-60}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_22(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-2,-80},{18,-60}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_23(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{24,-80},{44,-60}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_24(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{52,-80},{72,-60}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_25(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-30,-106},{-10,-86}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_26(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{-4,-106},{16,-86}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_27(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{22,-106},{42,-86}})));
  Modelica.Mechanics.MultiBody.Visualizers.FixedShape2 distance_marker_28(
    shapeType="cylinder",
    lengthDirection={0,0,-1},
    length=2,
    width=0.1,
    color={175,175,175},
    specularCoefficient=0.7,
    r_shape={distance_markers_distance_between,0,0},
    r={distance_markers_distance_between,0,0})
    "Post marking the 0 meter distance point"
    annotation (Placement(transformation(extent={{50,-106},{70,-86}})));
equation
  connect(distance_marker_0.frame_b, distance_marker_1.frame_a) annotation (
      Line(
      points={{-34,64},{-26,64}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_1.frame_b, distance_marker_2.frame_a) annotation (
      Line(
      points={{-6,64},{0,64}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_2.frame_b, distance_marker_3.frame_a) annotation (
      Line(
      points={{20,64},{26,64}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_3.frame_b, distance_marker_4.frame_a) annotation (
      Line(
      points={{46,64},{54,64}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(frame_a, Water.frame_a) annotation (Line(points={{-100,0},{-66,0},{
          -66,88},{-52,88}}, color={95,95,95}));
  connect(frame_a, distance_marker_0.frame_a) annotation (Line(points={{-100,0},
          {-66,0},{-66,64},{-54,64}},   color={95,95,95}));
  connect(distance_marker_5.frame_b,distance_marker_6. frame_a) annotation (
      Line(
      points={{-8,38},{-2,38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_6.frame_b,distance_marker_7. frame_a) annotation (
      Line(
      points={{18,38},{24,38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_7.frame_b,distance_marker_8. frame_a) annotation (
      Line(
      points={{44,38},{52,38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_4.frame_b, distance_marker_5.frame_a) annotation (
      Line(
      points={{74,64},{78,64},{78,52},{-32,52},{-32,38},{-28,38}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_9.frame_b, distance_marker_10.frame_a) annotation (
      Line(
      points={{-8,10},{-2,10}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_10.frame_b, distance_marker_11.frame_a) annotation (
      Line(
      points={{18,10},{24,10}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_11.frame_b, distance_marker_12.frame_a) annotation (
      Line(
      points={{44,10},{52,10}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_13.frame_b, distance_marker_14.frame_a) annotation (
      Line(
      points={{-10,-16},{-4,-16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_14.frame_b, distance_marker_15.frame_a) annotation (
      Line(
      points={{16,-16},{22,-16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_15.frame_b, distance_marker_16.frame_a) annotation (
      Line(
      points={{42,-16},{50,-16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_12.frame_b, distance_marker_13.frame_a) annotation (
      Line(
      points={{72,10},{76,10},{76,-2},{-34,-2},{-34,-16},{-30,-16}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_8.frame_b, distance_marker_9.frame_a) annotation (
      Line(
      points={{72,38},{76,38},{76,24},{-40,24},{-40,10},{-28,10}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_17.frame_b, distance_marker_18.frame_a) annotation (
      Line(
      points={{-8,-42},{-2,-42}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_18.frame_b, distance_marker_19.frame_a) annotation (
      Line(
      points={{18,-42},{24,-42}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_19.frame_b, distance_marker_20.frame_a) annotation (
      Line(
      points={{44,-42},{52,-42}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_21.frame_b, distance_marker_22.frame_a) annotation (
      Line(
      points={{-8,-70},{-2,-70}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_22.frame_b, distance_marker_23.frame_a) annotation (
      Line(
      points={{18,-70},{24,-70}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_23.frame_b, distance_marker_24.frame_a) annotation (
      Line(
      points={{44,-70},{52,-70}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_25.frame_b, distance_marker_26.frame_a) annotation (
      Line(
      points={{-10,-96},{-4,-96}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_26.frame_b, distance_marker_27.frame_a) annotation (
      Line(
      points={{16,-96},{22,-96}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_27.frame_b, distance_marker_28.frame_a) annotation (
      Line(
      points={{42,-96},{50,-96}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_24.frame_b, distance_marker_25.frame_a) annotation (
      Line(
      points={{72,-70},{76,-70},{76,-82},{-34,-82},{-34,-96},{-30,-96}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_20.frame_b, distance_marker_21.frame_a) annotation (
      Line(
      points={{72,-42},{76,-42},{76,-56},{-40,-56},{-40,-70},{-28,-70}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  connect(distance_marker_16.frame_b, distance_marker_17.frame_a) annotation (
      Line(
      points={{70,-16},{74,-16},{74,-18},{76,-18},{76,-30},{-44,-30},{-44,-42},
          {-28,-42}},
      color={95,95,95},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end VisualEnvironment;
