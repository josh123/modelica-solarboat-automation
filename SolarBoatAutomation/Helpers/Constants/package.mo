within SolarBoatAutomation.Helpers;
package Constants 
  final constant Modelica.SIunits.KinematicViscosity water_kinematic_viscosity_10Degrees=1.307e-6
  "see http://www.engineeringtoolbox.com/water-dynamic-kinematic-viscosity-d_596.html";
  final constant Modelica.SIunits.Density water_density = 1000;
  final constant Real k(unit="J/K") = 1.38065e-23 "Boltzmann constant";
  final constant Real q(unit="C") = 1.602e-19 "electron charge";
end Constants;
