from dymola.dymola_interface import DymolaInterface
from dymola.dymola_exception import DymolaException

import shutil
import os

#Measure how long it takes to run this sript
import time
start_time = time.time()

#For debugging
import pdb
#Use below as a breakpoint as needed
#pdb.set_trace()

dymola = None
try:
    # Instantiate the Dymola interface and start Dymola
    dymola = DymolaInterface()

    #Create a new empty results directory directory. If we have it already empty it
    if not os.path.exists("_Results"):
            os.makedirs("_Results")
    else:
        print "DELETING the old _Results directory"
        shutil.rmtree("_Results")
        os.makedirs("_Results")

    path = os.getcwd()

    test_name = "Test001"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\OverheadOtherElectrical\Examples\MassCheck2014.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.OverheadOtherElectrical.Examples.MassCheck2014",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    dymola.plot(["otherElectrical.mass_computed","otherElectrical.cost_money_computed","otherElectrical.cost_manhours_computed"])
    dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test002"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\OverheadOtherStructural\Examples\MassCheck2014.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.OverheadOtherStructural.Examples.MassCheck2014",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test003"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToRotation\Examples\KTH2014Gearbox.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToRotation.Examples.KTH2014Gearbox",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test004"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Panel_FT136SE.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Panel_FT136SE",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test005"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test006"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\BuoyancyGeneration\Examples\DisplacementHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.BuoyancyGeneration.Examples.DisplacementHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test007"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\BuoyancyGeneration\Examples\PlaningHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.BuoyancyGeneration.Examples.PlaningHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test008"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\TestBedOpenTest200mm343mm.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.TestBedOpenTest200mm343mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test009"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\MovingOpenTest200mm343mm.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.MovingOpenTest200mm343mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test010"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\TestBedPredicted200mm343mm.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.TestBedPredicted200mm343mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test011"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\TestBedPredicted160mm135mm.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.TestBedPredicted160mm135mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test012"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\LockedRotor_MaxonEC_max_60W_24V_272763.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.LockedRotor_MaxonEC_max_60W_24V_272763",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test013"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\LockedRotor_Turnigy_L3040A_480G.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.LockedRotor_Turnigy_L3040A_480G",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test014"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\FreeRunning_MaxonEC_max_60W_24V_272763.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.FreeRunning_MaxonEC_max_60W_24V_272763",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test015"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\FreeRunning_Turnigy_L3040A_480G.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.FreeRunning_Turnigy_L3040A_480G",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test016"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\BuoyancyGeneration\Examples\TowingTankPlaning.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.Examples.TowingTankPlaning",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test017"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\BuoyancyGeneration\Examples\TowingTankDisplacement.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.Examples.TowingTankDisplacement",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test018"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\BuoyancyGeneration\Examples\FloatingHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.BuoyancyGeneration.Examples.FloatingHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test019"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\KTH2014.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.KTH2014",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test020"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_160mm_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_160mm_18_5V",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test020-L"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_160mm_lossy_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_160mm_lossy_18_5V",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test021"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_200mm_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_200mm_18_5V",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test021-L"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_200mm_lossy_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_200mm_lossy_18_5V",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test022"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\SolarToElectrical\Examples\SubSystems_VoltageSweep_STC.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.SolarToElectrical.Examples.SubSystems_VoltageSweep_STC",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test023"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\SolarToElectrical\Examples\SubSystems_Array_VoltageSweep_STC.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.SolarToElectrical.Examples.SubSystems_Array_VoltageSweep_STC",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test024"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\SolarToElectrical\Examples\MPPT_Operation.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.SolarToElectrical.Examples.MPPT_Operation",startTime=0.0, stopTime=0.01,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test025"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\ConstantSunMonoHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.ConstantSunMonoHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test026"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\ConstantNoSunMonoHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.ConstantNoSunMonoHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test027"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\SinusoidalSunMonoHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.SinusoidalSunMonoHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test028"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\ConstantSunDualHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.ConstantSunDualHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test029"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\ConstantNoSunDualHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.ConstantNoSunDualHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test030"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\C\SinusoidalSunDualHull.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.C.SinusoidalSunDualHull",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test031"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\SolarBoat\Examples\A\ConstantSun_MonoDisplacementHullSB_A.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.SolarBoat.Examples.A.ConstantSun_MonoDisplacementHullSB_A",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test032"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\OverheadSubsystems\Examples\Mass2014A.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.OverheadSubsystems.Examples.Mass2014A",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test033"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\AttachmentPoint\Examples\CompileCheck.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.AttachmentPoint.Examples.CompileCheck",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")

    test_name = "Test034"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results/" + test_name
    os.makedirs(test_dir)
    dymola.openModel("..\SolarBoatAutomation\L1_Assess_L2\Powertrain\Examples\Tokyo2015_AverageInsolation.mo")
    resultFile = path + "/" + test_dir + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess_L2.Powertrain.Examples.Tokyo2015_AverageInsolation",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #dymola.plot(["otherStructural.mass_computed","otherStructural.cost_money_computed","otherStructural.cost_manhours_computed"])
    #dymola.ExportPlotAsImage(path + "/" + test_dir + "/plot.png")
    #dymola.savelog(path + "/" + test_dir + "/log.txt")


except DymolaException as ex:
    print("Error: " + str(ex))


#Measure how long it takes to run
end_time = time.time()

print "Time to run this script in seconds:" + str(end_time - start_time)
print "Time to run this script in min:" + str((end_time - start_time)/60)

print "Completed running tests"


#Exit Dymola
if dymola is not None:
    dymola.close()
    dymola = None