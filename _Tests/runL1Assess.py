from dymola.dymola_interface import DymolaInterface
from dymola.dymola_exception import DymolaException

import os

dymola = None
try:
    # Instantiate the Dymola interface and start Dymola
    dymola = DymolaInterface()

    path = os.getcwd()

    # Call a function in Dymola and check its return value
    dymola.openModel("..\SolarBoatAutomation\L1_Assess\CompletedModels\Auto\Auto_StraightLineAvSun_MonoHullSB.mo")
    resultFile = path + "/dsres"
    result = dymola.simulateModel("SolarBoatAutomation.L1_Assess.CompletedModels.Auto.Auto_StraightLineAvSun_MonoHullSB",startTime=0.0, stopTime=10.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)

    dymola.plot(["x_velocity"])
    dymola.ExportPlotAsImage(path + "/Auto_StraightLineAvSun_MonoHullSB.png")
    dymola.savelog(path + "/log.txt")
    print("OK")
except DymolaException as ex:
    print("Error: " + str(ex))
#finally:
#    print("finally")
#    if dymola is not None:
        #dymola.close()
#        dymola = None