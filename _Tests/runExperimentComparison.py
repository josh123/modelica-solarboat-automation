from dymola.dymola_interface import DymolaInterface
from dymola.dymola_exception import DymolaException

import shutil
import os

#For debugging
import pdb
#Use below as a breakpoint as needed
#pdb.set_trace()

#Measure how long it takes to run this sript
import time
start_time = time.time()

#Library to read OpenModelica or Dymola generated mat files
import DyMat

#For plots and charts
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.ticker as mtick

#For reading past experiment data
from numpy import genfromtxt
import csv

#For sorting data
import operator

dymola = None
try:
    # Instantiate the Dymola interface and start Dymola
    dymola = DymolaInterface()

    #Create a new empty results directory directory. If we have it already empty it
    if not os.path.exists("_Results_ExperimentCompare"):
            os.makedirs("_Results_ExperimentCompare")
    else:
        print "DELETING the old _Results_ExperimentCompare directory"
        shutil.rmtree("_Results_ExperimentCompare")
        os.makedirs("_Results_ExperimentCompare")

    path = os.getcwd()

    ####################################################################################################
    test_name = "Propeller-200mm"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results_ExperimentCompare/" + test_name
    os.makedirs(test_dir)
    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\PropellerOpenTest200mm343mm.mo")
    resultFile = path + "/" + test_dir + "/dsres_simulate_experiment"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.PropellerOpenTest200mm343mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_simulate_experiment = DyMat.DyMatFile(resultFile)
    #Simulate predicted
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Examples\PropellerOpenTestPredicted200mm343mm.mo")
    resultFile = path + "/" + test_dir + "/dsres_predicted"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.RotationToThrust.Examples.PropellerOpenTestPredicted200mm343mm",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_predicted = DyMat.DyMatFile(resultFile)

    #Open the spreadsheet of experiment
    experiment_data=np.loadtxt(open("..\SolarBoatAutomation\L3_Assess_L4\RotationToThrust\Experiment\OpenTest200mm343mm.csv","rb"),delimiter=",",skiprows=1)

    fig = plt.figure()

    ax1 = fig.add_subplot(2,1,1)
    plt.title("Thust coefficient comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    thrust_coefficient_kt_simulate_experiment = ax1.plot(mat_file_simulate_experiment.data("x_speed.y"),mat_file_simulate_experiment.data("propellerMultiBody.thrust_coefficient_kt"))
    plots.append(thrust_coefficient_kt_simulate_experiment[0])
    legend_text_list.append("thrust_coefficient_kt_simulate_experiment")
    thrust_coefficient_kt_predicted = ax1.plot(mat_file_predicted.data("x_speed.y"),mat_file_predicted.data("propellerMultiBody.thrust_coefficient_kt"))
    plots.append(thrust_coefficient_kt_predicted[0])
    legend_text_list.append("thrust_coefficient_kt_predicted")
    thrust_coefficient_kt_experiment = ax1.plot(experiment_data[:,1],experiment_data[:,7])
    plots.append(thrust_coefficient_kt_experiment[0])
    legend_text_list.append("thrust_coefficient_kt_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("thrust_coefficient_kt", fontsize='x-small')
    plt.xlabel("Dragging velocity (m/s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    ax1 = fig.add_subplot(2,1,2)
    plt.title("Thust comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    thrust_simulate_experiment = ax1.plot(mat_file_simulate_experiment.data("x_speed.y"),mat_file_simulate_experiment.data("propellerMultiBody.thrust_generated"))
    plots.append(thrust_simulate_experiment[0])
    legend_text_list.append("thrust_simulate_experiment")
    thrust_simulate_predicted = ax1.plot(mat_file_predicted.data("x_speed.y"),mat_file_predicted.data("propellerMultiBody.thrust_generated"))
    plots.append(thrust_simulate_predicted[0])
    legend_text_list.append("thrust_predicted")
    thrust_coefficient_kt_experiment = ax1.plot(experiment_data[:,1],experiment_data[:,4])
    plots.append(thrust_coefficient_kt_experiment[0])
    legend_text_list.append("thrust_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Thrust (N)", fontsize='x-small')
    plt.xlabel("Dragging velocity (m/s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/Thurst.png")


    fig = plt.figure()

    ax1 = fig.add_subplot(2,1,1)
    plt.title("Torque coefficient comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    torque_coefficient_kq_simulate_experiment = ax1.plot(mat_file_simulate_experiment.data("x_speed.y"),mat_file_simulate_experiment.data("propellerMultiBody.torque_coefficient_kq"))
    plots.append(torque_coefficient_kq_simulate_experiment[0])
    legend_text_list.append("torque_coefficient_kq_simulate_experiment")
    torque_coefficient_kq_predicted = ax1.plot(mat_file_predicted.data("x_speed.y"),mat_file_predicted.data("propellerMultiBody.torque_coefficient_kq"))
    plots.append(torque_coefficient_kq_predicted[0])
    legend_text_list.append("torque_coefficient_kq_predicted")
    torque_coefficient_kq_experiment = ax1.plot(experiment_data[:,1],experiment_data[:,8])
    plots.append(torque_coefficient_kq_experiment[0])
    legend_text_list.append("torque_coefficient_kq_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("torque_coefficient_kq", fontsize='x-small')
    plt.xlabel("Dragging velocity (m/s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    ax1 = fig.add_subplot(2,1,2)
    plt.title("Torque comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    torque_simulate_experiment = ax1.plot(mat_file_simulate_experiment.data("x_speed.y"),mat_file_simulate_experiment.data("propellerMultiBody.torque_generated"))
    plots.append(torque_simulate_experiment[0])
    legend_text_list.append("torque_simulate_experiment")
    torque_predicted = ax1.plot(mat_file_predicted.data("x_speed.y"),mat_file_predicted.data("propellerMultiBody.torque_generated"))
    plots.append(torque_predicted[0])
    legend_text_list.append("torque_predicted")
    torque_coefficient_kq_experiment = ax1.plot(experiment_data[:,1],experiment_data[:,6])
    plots.append(torque_coefficient_kq_experiment[0])
    legend_text_list.append("torque_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Torque (Nm)", fontsize='x-small')
    plt.xlabel("Dragging velocity (m/s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/Torque.png")
    ####################################################################################################

    ####################################################################################################
    test_name = "Turnigy_L3040A_480G"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results_ExperimentCompare/" + test_name
    os.makedirs(test_dir)
    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\FreeRunning_Turnigy_L3040A_480G_Experiment.mo")
    resultFile = path + "/" + test_dir + "/dsres_simulate_experiment"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.FreeRunning_Turnigy_L3040A_480G_Experiment",startTime=0.0, stopTime=58.7,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_simulate_experiment = DyMat.DyMatFile(resultFile)
    #Simulate predicted
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Examples\FreeRunning_Turnigy_L3040A_480G.mo")
    resultFile = path + "/" + test_dir + "/dsres_predicted"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.ElectricalToRotation.Examples.FreeRunning_Turnigy_L3040A_480G",startTime=0.0, stopTime=58.7,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_predicted = DyMat.DyMatFile(resultFile)

    #Open the spreadsheet of experiment
    experiment_data=np.loadtxt(open("..\SolarBoatAutomation\L3_Assess_L4\ElectricalToRotation\Experiment\FreeRunningTurnigyL3040A_480G.csv","rb"),delimiter=",",skiprows=1)

    fig = plt.figure()

    ax1 = fig.add_subplot(4,1,1)
    plt.title("Voltage comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    voltage_simulate_experiment = ax1.plot(mat_file_simulate_experiment.abscissa("constantVoltage.V")[0],mat_file_simulate_experiment.data("constantVoltage.V"))
    plots.append(voltage_simulate_experiment[0])
    legend_text_list.append("voltage_simulate_experiment")
    voltage_predicted = ax1.plot(mat_file_predicted.abscissa("constantVoltage.V")[0],mat_file_predicted.data("constantVoltage.V"))
    plots.append(voltage_predicted[0])
    legend_text_list.append("voltage_predicted")
    voltage_experiment = ax1.plot(experiment_data[:,0],experiment_data[:,2])
    plots.append(voltage_experiment[0])
    legend_text_list.append("voltage_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Voltage (V)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(4,1,2)
    plt.title("Current comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    current_simulate_experiment = ax1.plot(mat_file_simulate_experiment.abscissa("constantVoltage.n.i")[0],mat_file_simulate_experiment.data("constantVoltage.n.i"))
    plots.append(current_simulate_experiment[0])
    legend_text_list.append("current_simulate_experiment")
    current_predicted = ax1.plot(mat_file_predicted.abscissa("constantVoltage.n.i")[0],mat_file_predicted.data("constantVoltage.n.i"))
    plots.append(current_predicted[0])
    legend_text_list.append("current_predicted")
    current_experiment = ax1.plot(experiment_data[:,0],experiment_data[:,3])
    plots.append(current_experiment[0])
    legend_text_list.append("current_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Current (A)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(4,1,3)
    plt.title("Power consumption comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    power_simulate_experiment = ax1.plot(mat_file_simulate_experiment.abscissa("power_consumption")[0],mat_file_simulate_experiment.data("power_consumption"))
    plots.append(power_simulate_experiment[0])
    legend_text_list.append("power_simulate_experiment")
    power_predicted = ax1.plot(mat_file_predicted.abscissa("power_consumption")[0],mat_file_predicted.data("power_consumption"))
    plots.append(power_predicted[0])
    legend_text_list.append("power_predicted")
    power_experiment = ax1.plot(experiment_data[:,0],experiment_data[:,4])
    plots.append(voltage_experiment[0])
    legend_text_list.append("power_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Power (W)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(4,1,4)
    plt.title("Angular speed comparison", fontsize='small')
    plots = list()
    legend_text_list = list()
    angular_speed_simulate_experiment = ax1.plot(mat_file_simulate_experiment.abscissa("multiSensor_MultiUnits.rpm")[0],mat_file_simulate_experiment.data("multiSensor_MultiUnits.rpm"))
    plots.append(angular_speed_simulate_experiment[0])
    legend_text_list.append("angular_speed_simulate_experiment")
    angular_speed_predicted = ax1.plot(mat_file_predicted.abscissa("multiSensor_MultiUnits.rpm")[0],mat_file_predicted.data("multiSensor_MultiUnits.rpm"))
    plots.append(angular_speed_predicted[0])
    legend_text_list.append("angular_speed_predicted")
    angular_speed_experiment = ax1.plot(experiment_data[:,0],experiment_data[:,6])
    plots.append(angular_speed_experiment[0])
    legend_text_list.append("angular_speed_experiment")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Angular speed (RPM)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')


    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/Turnigy_L3040A_480G.png")
    ####################################################################################################

    ####################################################################################################
    test_name = "18.5 V thrust test"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results_ExperimentCompare/" + test_name
    os.makedirs(test_dir)
    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_160mm_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_160mm_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_160mm_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_160mm_18_5V = DyMat.DyMatFile(resultFile)

    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_160mm_lossy_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_160mm_lossy_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_160mm_lossy_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_160mm_lossy_18_5V = DyMat.DyMatFile(resultFile)

    #Open the spreadsheet of experiment
    Tokyo2015_160mm_18_5V_experiment_data=np.loadtxt(open("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Experiment\Tokyo2015_160mm_18_5V.csv","rb"),delimiter=",",skiprows=1)

    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_200mm_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_200mm_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_200mm_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_200mm_18_5V = DyMat.DyMatFile(resultFile)

    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_200mm_lossy_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_200mm_lossy_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_200mm_lossy_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_200mm_lossy_18_5V = DyMat.DyMatFile(resultFile)

    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_220mm_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_220mm_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_220mm_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_220mm_18_5V = DyMat.DyMatFile(resultFile)

    #Simulate simulate_experiment
    dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_220mm_lossy_18_5V.mo")
    resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_220mm_lossy_18_5V"
    result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_220mm_lossy_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_Tokyo2015_220mm_lossy_18_5V = DyMat.DyMatFile(resultFile)

    #Open the spreadsheet of experiment
    Tokyo2015_220mm_18_5V_experiment_data=np.loadtxt(open("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Experiment\Tokyo2015_220mm_18_5V.csv","rb"),delimiter=",",skiprows=1)



    # #Simulate simulate_experiment
    # dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_155mm_4Blade_18_5V.mo")
    # resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_155mm_4Blade_18_5V"
    # result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_155mm_4Blade_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    # if not result:
    #     print("Simulation failed. Below is the translation log.")
    #     log = dymola.getLastError()
    #     print(log)
    #     exit(1)
    # #Open the data we just simulated
    # mat_file_Tokyo2015_155mm_4Blade_18_5V = DyMat.DyMatFile(resultFile)

    # #Simulate simulate_experiment
    # dymola.openModel("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Examples\Tokyo2015_155mm_4Blade_lossy_18_5V.mo")
    # resultFile = path + "/" + test_dir + "/dsres_Tokyo2015_155mm_4Blade_lossy_18_5V"
    # result = dymola.simulateModel("SolarBoatAutomation.L2_Assess_L3.ElectricalToThrust.Examples.Tokyo2015_155mm_4Blade_lossy_18_5V",startTime=0.0, stopTime=15,resultFile=resultFile)
    # if not result:
    #     print("Simulation failed. Below is the translation log.")
    #     log = dymola.getLastError()
    #     print(log)
    #     exit(1)
    # #Open the data we just simulated
    # mat_file_Tokyo2015_155mm_4Blade_lossy_18_5V = DyMat.DyMatFile(resultFile)

    # #Open the spreadsheet of experiment
    Tokyo2015_155mm_4Blade_18_5V_experiment_data=np.loadtxt(open("..\SolarBoatAutomation\L2_Assess_L3\ElectricalToThrust\Experiment\Tokyo2015_155mm_4Blade_18_5V.csv","rb"),delimiter=",",skiprows=1)

    fig = plt.figure()

    ax1 = fig.add_subplot(1,1,1)
    plt.title("Thurst vs. Supplied power", fontsize='small')
    plots = list()
    legend_text_list = list()

    thurst_and_power_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.data("supplied_power"),mat_file_Tokyo2015_160mm_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    thurst_and_power_Tokyo2015_160mm_lossy_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_lossy_18_5V.data("supplied_power"),mat_file_Tokyo2015_160mm_lossy_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_160mm_lossy_18_5V[0])
    legend_text_list.append("Tokyo2015_0.8LossyGearbox_160mm")
    thurst_and_power_Tokyo2015_160mm_18_5V_experiment = ax1.plot([0,100],Tokyo2015_160mm_18_5V_experiment_data[:,1])
    plots.append(thurst_and_power_Tokyo2015_160mm_18_5V_experiment[0])
    legend_text_list.append("Tokyo2015_160mm_experiment")

    thurst_and_power_Tokyo2015_200mm_18_5V = ax1.plot(mat_file_Tokyo2015_200mm_18_5V.data("supplied_power"),mat_file_Tokyo2015_200mm_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_200mm_18_5V[0])
    legend_text_list.append("Tokyo2015_200mm")
    thurst_and_power_Tokyo2015_200mm_lossy_18_5V = ax1.plot(mat_file_Tokyo2015_200mm_lossy_18_5V.data("supplied_power"),mat_file_Tokyo2015_200mm_lossy_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_200mm_lossy_18_5V[0])
    legend_text_list.append("Tokyo2015_0.8LossyGearbox_200mm")
    #No experimental data for 200mm

    thurst_and_power_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.data("supplied_power"),mat_file_Tokyo2015_220mm_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    thurst_and_power_Tokyo2015_220mm_lossy_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_lossy_18_5V.data("supplied_power"),mat_file_Tokyo2015_220mm_lossy_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_and_power_Tokyo2015_220mm_lossy_18_5V[0])
    legend_text_list.append("Tokyo2015_0.8LossyGearbox_220mm")
    thurst_and_power_Tokyo2015_220mm_18_5V_experiment = ax1.plot([0,100],Tokyo2015_220mm_18_5V_experiment_data[:,1])
    plots.append(thurst_and_power_Tokyo2015_220mm_18_5V_experiment[0])
    legend_text_list.append("Tokyo2015_220mm_experiment")

    #Not clear on the pitch of the propeller
    # thurst_and_power_Tokyo2015_155mm_4Blade_18_5V = ax1.plot(mat_file_Tokyo2015_155mm_4Blade_18_5V.data("supplied_power"),mat_file_Tokyo2015_155mm_4Blade_18_5V.data("cutForceAndTorque.force[1]"))
    # plots.append(thurst_and_power_Tokyo2015_155mm_4Blade_18_5V[0])
    # legend_text_list.append("Tokyo2015_155mm_4Blade")
    # thurst_and_power_Tokyo2015_155mm_4Blade_lossy_18_5V = ax1.plot(mat_file_Tokyo2015_155mm_4Blade_lossy_18_5V.data("supplied_power"),mat_file_Tokyo2015_155mm_4Blade_lossy_18_5V.data("cutForceAndTorque.force[1]"))
    # plots.append(thurst_and_power_Tokyo2015_155mm_4Blade_lossy_18_5V[0])
    # legend_text_list.append("Tokyo2015_0.8LossyGearbox_155mm_4Blade")
    thurst_and_power_Tokyo2015_155mm_4Blade_18_5V_experiment = ax1.plot([0,100],Tokyo2015_155mm_4Blade_18_5V_experiment_data[:,1])
    plots.append(thurst_and_power_Tokyo2015_155mm_4Blade_18_5V_experiment[0])
    legend_text_list.append("Tokyo2015_155mm_4Blade_experiment")

    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Thurst (N)", fontsize='x-small')
    plt.xlabel("Supplied power (W)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')
    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/SuppliedPower_vs_Thrust.png")


    fig = plt.figure()

    ax1 = fig.add_subplot(3,1,1)
    plt.title("Supplied power", fontsize='small')
    plots = list()
    legend_text_list = list()
    power_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("supplied_power")[0],mat_file_Tokyo2015_220mm_18_5V.data("supplied_power"))
    plots.append(power_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    power_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("supplied_power")[0],mat_file_Tokyo2015_160mm_18_5V.data("supplied_power"))
    plots.append(power_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Power (W)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(3,1,2)
    plt.title("Voltage", fontsize='small')
    plots = list()
    legend_text_list = list()
    voltage_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("rampVoltage.V")[0],mat_file_Tokyo2015_220mm_18_5V.data("rampVoltage.V"))
    plots.append(voltage_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    voltage_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("rampVoltage.V")[0],mat_file_Tokyo2015_160mm_18_5V.data("rampVoltage.V"))
    plots.append(voltage_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Voltage (V)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(3,1,3)
    plt.title("Current", fontsize='small')
    plots = list()
    legend_text_list = list()
    current_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("rampVoltage.i")[0],mat_file_Tokyo2015_220mm_18_5V.data("rampVoltage.i"))
    plots.append(current_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    current_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("rampVoltage.i")[0],mat_file_Tokyo2015_160mm_18_5V.data("rampVoltage.i"))
    plots.append(current_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Current (A)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/Electrical_properties.png")

    fig = plt.figure()

    ax1 = fig.add_subplot(3,1,1)
    plt.title("Thrust", fontsize='small')
    plots = list()
    legend_text_list = list()
    thurst_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("cutForceAndTorque.force[1]")[0],mat_file_Tokyo2015_220mm_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    thurst_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("cutForceAndTorque.force[1]")[0],mat_file_Tokyo2015_160mm_18_5V.data("cutForceAndTorque.force[1]"))
    plots.append(thurst_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Thrust (N)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(3,1,2)
    plt.title("Propeller torque", fontsize='small')
    plots = list()
    legend_text_list = list()
    torque_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("partial_ThrusterMultiBody.partial_PropellerMultiBody.torque_generated")[0],mat_file_Tokyo2015_220mm_18_5V.data("partial_ThrusterMultiBody.partial_PropellerMultiBody.torque_generated"))
    plots.append(torque_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    torque_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("partial_ThrusterMultiBody.partial_PropellerMultiBody.torque_generated")[0],mat_file_Tokyo2015_160mm_18_5V.data("partial_ThrusterMultiBody.partial_PropellerMultiBody.torque_generated"))
    plots.append(torque_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Torque (Nm)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    ax1 = fig.add_subplot(3,1,3)
    plt.title("Propeller angular speed (RPM)", fontsize='small')
    plots = list()
    legend_text_list = list()
    torque_Tokyo2015_220mm_18_5V = ax1.plot(mat_file_Tokyo2015_220mm_18_5V.abscissa("partial_ThrusterMultiBody.idealGearbox.multiSensor_MultiUnits.rpm")[0],mat_file_Tokyo2015_220mm_18_5V.data("partial_ThrusterMultiBody.idealGearbox.multiSensor_MultiUnits.rpm"))
    plots.append(torque_Tokyo2015_220mm_18_5V[0])
    legend_text_list.append("Tokyo2015_220mm")
    torque_Tokyo2015_160mm_18_5V = ax1.plot(mat_file_Tokyo2015_160mm_18_5V.abscissa("partial_ThrusterMultiBody.idealGearbox.multiSensor_MultiUnits.rpm")[0],mat_file_Tokyo2015_160mm_18_5V.data("partial_ThrusterMultiBody.idealGearbox.multiSensor_MultiUnits.rpm"))
    plots.append(torque_Tokyo2015_160mm_18_5V[0])
    legend_text_list.append("Tokyo2015_160mm")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Angular speed (RPM)", fontsize='x-small')
    plt.xlabel("Time (s)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')
    ax1.tick_params(axis='both', which='major', labelsize='x-small')

    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/Propeller_properties.png")


    ####################################################################################################

    ####################################################################################################
    test_name = "SolarPanel-StandardConditions"
    print "Starting test: " + test_name
    current_time = time.time()
    print "Time running script in seconds: " + str(current_time - start_time)
    print "Time to running this script in min: " + str((current_time - start_time)/60)
    test_dir = "_Results_ExperimentCompare/" + test_name
    os.makedirs(test_dir)
    #Simulate VoltageSweep_Array_1Px1S_Array_FT136SE
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_1Px1S_Array_FT136SE.mo")
    resultFile = path + "/" + test_dir + "/dsres_VoltageSweep_Array_1Px1S_Array_FT136SE"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_1Px1S_Array_FT136SE",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_VoltageSweep_Array_1Px1S_Array_FT136SE = DyMat.DyMatFile(resultFile)

    #Simulate VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX.mo")
    resultFile = path + "/" + test_dir + "/dsres_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX = DyMat.DyMatFile(resultFile)

    #Simulate VoltageSweep_Array_3Px2S_Array_FT136SE
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_3Px2S_Array_FT136SE.mo")
    resultFile = path + "/" + test_dir + "/dsres_VoltageSweep_Array_3Px2S_Array_FT136SE"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_3Px2S_Array_FT136SE",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_VoltageSweep_Array_3Px2S_Array_FT136SE = DyMat.DyMatFile(resultFile)

    #Simulate VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.mo")
    resultFile = path + "/" + test_dir + "/dsres_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX = DyMat.DyMatFile(resultFile)

    #Simulate VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX
    dymola.openModel("..\SolarBoatAutomation\L3_Assess_L4\SolarToElectrical\Examples\VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX.mo")
    resultFile = path + "/" + test_dir + "/dsres_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX"
    result = dymola.simulateModel("SolarBoatAutomation.L3_Assess_L4.SolarToElectrical.Examples.VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX",startTime=0.0, stopTime=1.0,resultFile=resultFile)
    if not result:
        print("Simulation failed. Below is the translation log.")
        log = dymola.getLastError()
        print(log)
        exit(1)
    #Open the data we just simulated
    mat_file_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX = DyMat.DyMatFile(resultFile)

    fig = plt.figure()
    ax1 = fig.add_subplot(2,1,1)
    plt.title("Power vs. voltage comparison (standard testing conditions)", fontsize='small')
    plots = list()
    legend_text_list = list()
    VoltageSweep_Array_1Px1S_Array_FT136SE = ax1.plot(mat_file_VoltageSweep_Array_1Px1S_Array_FT136SE.data("voltage"),mat_file_VoltageSweep_Array_1Px1S_Array_FT136SE.data("power"))
    plots.append(VoltageSweep_Array_1Px1S_Array_FT136SE[0])
    legend_text_list.append("1Px1S_Array_FT136SE")
    VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX.data("power"))
    plots.append(VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("1Px1S_Array_SP18f_L_LOXX")
    VoltageSweep_Array_3Px2S_Array_FT136SE = ax1.plot(mat_file_VoltageSweep_Array_3Px2S_Array_FT136SE.data("voltage"),mat_file_VoltageSweep_Array_3Px2S_Array_FT136SE.data("power"))
    plots.append(VoltageSweep_Array_3Px2S_Array_FT136SE[0])
    legend_text_list.append("3Px2S_Array_FT136SE")
    VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.data("power"))
    plots.append(VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("8Px2S_Array_SP18f_L_LOXX")
    VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX.data("power"))
    plots.append(VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("16Px1S_Array_SP18f_L_LOXX")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Power (W)", fontsize='x-small')
    plt.xlabel("Voltage (V)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    ax1 = fig.add_subplot(2,1,2)
    plt.title("Current vs. voltage comparison (standard testing conditions)", fontsize='small')
    plots = list()
    legend_text_list = list()
    VoltageSweep_Array_1Px1S_Array_FT136SE = ax1.plot(mat_file_VoltageSweep_Array_1Px1S_Array_FT136SE.data("voltage"),mat_file_VoltageSweep_Array_1Px1S_Array_FT136SE.data("current"))
    plots.append(VoltageSweep_Array_1Px1S_Array_FT136SE[0])
    legend_text_list.append("1Px1S_Array_FT136SE")
    VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX.data("current"))
    plots.append(VoltageSweep_Array_1Px1S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("1Px1S_Array_SP18f_L_LOXX")
    VoltageSweep_Array_3Px2S_Array_FT136SE = ax1.plot(mat_file_VoltageSweep_Array_3Px2S_Array_FT136SE.data("voltage"),mat_file_VoltageSweep_Array_3Px2S_Array_FT136SE.data("current"))
    plots.append(VoltageSweep_Array_3Px2S_Array_FT136SE[0])
    legend_text_list.append("3Px2S_Array_FT136SE")
    VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX.data("current"))
    plots.append(VoltageSweep_Array_8Px2S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("8Px2S_Array_SP18f_L_LOXX")
    VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX = ax1.plot(mat_file_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX.data("voltage"),mat_file_VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX.data("current"))
    plots.append(VoltageSweep_Array_16Px1S_Array_SP18f_L_LOXX[0])
    legend_text_list.append("16Px1S_Array_SP18f_L_LOXX")
    plt.legend(plots, legend_text_list, fontsize='x-small', loc="best")
    plt.ylabel("Current (A)", fontsize='x-small')
    plt.xlabel("Voltage (V)", fontsize='x-small')
    plt.grid(b=True, which='major', color='k', linestyle='-')
    plt.grid(b=True, which='minor', color='k', linestyle='-')

    #Make everything visable
    plt.tight_layout()
    #Save the figure
    fig.savefig(path + "/" + test_dir + "/SolarPanelCompare.png")


    ####################################################################################################



except DymolaException as ex:
    print("Error: " + str(ex))


#Measure how long it takes to run
end_time = time.time()

print "Time to run this script in seconds:" + str(end_time - start_time)
print "Time to run this script in min:" + str((end_time - start_time)/60)

print "Completed running tests"


#Exit Dymola
if dymola is not None:
    dymola.close()
    dymola = None