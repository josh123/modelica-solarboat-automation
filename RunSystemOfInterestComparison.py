#Runs two ready to go models in testbeds and compares their performance
#Note OpenModelica and Python have different working directories. To avoid confusion must change directory in both domains one after the other

#Input file
# XML file to read (this is the first argument)

#Input flags
# No flags and will try to run with an existing _Results file
# -c Will delete the current _Results file
# -s Will force simulation

#Import the custom library
from lib_classes_system_of_interest_comparison import *
from lib_functions_system_of_interest_comparison import *

#For debugging
import pdb
#Use below as a breakpoint as needed
#pdb.set_trace()

#Measure how long it takes to run this sript
import time
start_time = time.time()

#For file creation and moving around directories
import shutil
import os

#For input arguments
import sys
if ".xml" not in sys.argv[1]:
    print "No path to XML file provided in first input argument"
    exit()
else:
    xml_filename = sys.argv[1]
#Read XML file with package and model definition
import xml.etree.ElementTree as ET
tree = ET.parse(xml_filename)
xml_root = tree.getroot()

print "Argument List:", str(sys.argv)
#Based on the imput arguments request clearing the directory and or running simulation
#Default to not run a new simulation
new_results_dir = False
new_simulation = False
if "-c" in sys.argv:
    print "Requested clearing of the _Results directory"
    new_results_dir = True
if ("-c" or "-s") in sys.argv:
    print "Requested to perform simulation"
    new_simulation = True

print "Starting boat simulation and value extraction process"

#Interface with OpenModelica
from OMPython import OMCSession
OMPython = OMCSession()

#Interface with Dymola
from dymola.dymola_interface import DymolaInterface
# Instantiate the Dymola interface and start Dymola
dymola = DymolaInterface()

#Load the SolarBoat library into OpenModelica
OMPython.execute("clear")
OMPython.execute("loadModel(Modelica)")
OMPython.execute("cd(\"SolarBoatAutomation\")")
os.chdir("SolarBoatAutomation")
OMPython.execute("loadFile(\"package.mo\")")
#Store the top level path of the library
path_library_top_dir = os.getcwd()

#Extract various names from XML
comparison_name = xml_root.get('name')
system_of_interest_name = xml_root.find('system_of_interest').get('name')
testbed_package_dot_notation = xml_root.find('testbed_package_dot_notation').get('name')
testbed_package_slash_notation = xml_root.find('testbed_package_slash_notation').get('name')
alternative_for_replacement_package_dot_notation = xml_root.find('alternative_for_replacement_package_dot_notation').get('name')
alternative_for_replacement = xml_root.find('alternative_for_replacement').get('name')
component_for_replacement_package_dot_notation = xml_root.find('component_for_replacement_package_dot_notation').get('name')
component_for_replacement = xml_root.find('component_for_replacement').get('name')

#Create a new empty results directory directory. If we have it already empty it
if not os.path.exists("_Results/"+comparison_name):
    if not new_simulation:
        print "Requested no simulation, but there are no _Results folder to use. Requesting simulation."
        new_simulation = True
        os.makedirs("_Results/"+comparison_name)
    else:
        print "No _Results/"+comparison_name+" directory exists, making a new one"
        os.makedirs("_Results/"+comparison_name)
elif new_results_dir:
    print "DELETING the old _Results/"+comparison_name+" directory"
    shutil.rmtree("_Results/"+comparison_name)
    os.makedirs("_Results/"+comparison_name)
else:
    print "Use the old _Results/"+comparison_name+" directory"

#Enter the working directory
OMPython.execute("cd(\"_Results\")")
OMPython.execute("cd(\""+comparison_name+"\")")
os.chdir("_Results")
os.chdir(comparison_name)

#Read the components
component_name_list = list()
for component in xml_root.findall('component'):
    component_name_list.append(component.get('name'))
num_components = len(component_name_list)
if num_components == 0:
    #Add a special no component variation component
    component_name_list.append("No_component_variation")
    num_components = num_components + 1


component_count = 0;
for component_name in component_name_list:
    component_count = component_count + 1
    #Make a new component directory and enter it
    OMPython.execute("mkdir(\""+component_name+"\")")
    OMPython.execute("cd(\""+component_name+"\")")
    os.chdir(component_name)

    #Read the scenario descritions and instantiate classes to store extracted simulation results and descriptions
    scenario_descriptions_dict = dict()
    for scenario in xml_root.findall('scenario'):
        scenario_name = scenario.get('name')
        modelica_variable_of_interest = scenario.find('modelica_variable_of_interest').get('name')
        modelica_variable_of_interest_units = scenario.find('modelica_variable_of_interest_units').get('name')
        value_performance_slope_direction = scenario.find('value_performance_slope_direction').get('name')
        minimum_acceptable_performance = float(scenario.find('minimum_acceptable_performance').text)
        stretch_goal = float(scenario.find('stretch_goal').text)
        weight = float(scenario.find('weight').text)
        simulation_length = float(scenario.find('simulation_length').text)
        extract_data_type = scenario.find('extract_data_type').get('name')
        scenario_descriptions_dict[scenario_name] = ScenarioDescription(modelica_variable_of_interest,modelica_variable_of_interest_units,value_performance_slope_direction,minimum_acceptable_performance,stretch_goal,weight,simulation_length,extract_data_type)
    scenario_descriptions = ScenarioDescriptions(scenario_descriptions_dict)
    num_scenarios = scenario_descriptions.getNumberScenarios()
    scenario_descriptions.setEqualWeights()

    #Read the cost scenarios and instantiate classes to store extracted simulation results and descriptions
    cost_scenario_descriptions_dict = dict()
    for scenario in xml_root.findall('cost_scenario'):
        cost_scenario_name = scenario.get('name')
        scenario_name = scenario.get('name')
        modelica_variable_of_interest = scenario.find('modelica_variable_of_interest').get('name')
        modelica_variable_of_interest_units = scenario.find('modelica_variable_of_interest_units').get('name')
        value_performance_slope_direction = scenario.find('value_performance_slope_direction').get('name')
        minimum_acceptable_performance = float(scenario.find('minimum_acceptable_performance').text)
        stretch_goal = float(scenario.find('stretch_goal').text)
        weight = float(scenario.find('weight').text)
        simulation_length = float(scenario.find('simulation_length').text)
        extract_data_type = scenario.find('extract_data_type').get('name')
        cost_scenario_descriptions_dict[scenario_name] = ScenarioDescription(modelica_variable_of_interest,modelica_variable_of_interest_units,value_performance_slope_direction,minimum_acceptable_performance,stretch_goal,weight,simulation_length,extract_data_type)
    cost_scenario_descriptions = ScenarioDescriptions(cost_scenario_descriptions_dict)
    num_cost_scenarios = cost_scenario_descriptions.getNumberScenarios()

    #Read the point design descriptions from XML
    point_design_descriptions_dict = dict()
    for point_design in xml_root.findall('point_design'):
        point_design_name = point_design.get('name')
        point_design_descriptions_dict[point_design_name] = PointDesignDescription()
    point_design_descriptions = PointDesignDescriptions(point_design_descriptions_dict)
    num_point_designs = point_design_descriptions.getNumberPointDesigns()

    simulation_results = SimulationResults(scenario_descriptions.getScenarioNameList(), point_design_descriptions.getPointDesignNameList(), cost_scenario_descriptions.getScenarioNameList())

    #Run the simulations and extract the results
    print "Starting simulation code"
    #For each point design, run each simulation scenario, compute value
    point_design_count = 0;
    for point_design_name in point_design_descriptions.getPointDesignNameList():
        point_design_count = point_design_count + 1
        #Setup the directory for the design
        OMPython.execute("mkdir(\""+point_design_name+"\")")
        OMPython.execute("cd(\""+point_design_name+"\")")
        os.chdir(point_design_name)
        scenario_count = 0;
        #Run the simulations for the scenarios
        for scenario_name in scenario_descriptions.getScenarioNameList():
            scenario_count = scenario_count + 1
            print "component number: " + str(component_count) + " of " + str(num_components)
            print "Design number: " + str(point_design_count) + " of " + str(num_point_designs) + " Name: " + point_design_name
            print "Starting: Scenario number: " + str(scenario_count) + " of " + str(num_scenarios)
            current_time = time.time()
            print "Time running script in seconds: " + str(current_time - start_time)
            print "Time to running this script in min: " + str((current_time - start_time)/60)
            simulation_length = scenario_descriptions.scenario_description_dict[scenario_name].simulation_length
            modelica_variable_of_interest = scenario_descriptions.scenario_description_dict[scenario_name].modelica_variable_of_interest
            extract_data_type = scenario_descriptions.scenario_description_dict[scenario_name].extract_data_type
            stretch_goal = scenario_descriptions.getStretchGoal(scenario_name)
            buildModelRunSimExtractData(point_design_name, scenario_name, scenario_descriptions, cost_scenario_descriptions, "scenario", OMPython, os, simulation_length, modelica_variable_of_interest, extract_data_type, stretch_goal, simulation_results, new_simulation, component_name, dymola, system_of_interest_name, testbed_package_dot_notation, testbed_package_slash_notation, alternative_for_replacement_package_dot_notation, alternative_for_replacement, component_for_replacement_package_dot_notation, component_for_replacement, path_library_top_dir)
        #Run the simulations for the cost_scenarios
        for scenario_name in cost_scenario_descriptions.getScenarioNameList():
            simulation_length = cost_scenario_descriptions.scenario_description_dict[scenario_name].simulation_length
            modelica_variable_of_interest = cost_scenario_descriptions.scenario_description_dict[scenario_name].modelica_variable_of_interest
            extract_data_type = cost_scenario_descriptions.scenario_description_dict[scenario_name].extract_data_type
            stretch_goal = cost_scenario_descriptions.getStretchGoal(scenario_name)
            buildModelRunSimExtractData(point_design_name, scenario_name, scenario_descriptions, cost_scenario_descriptions, "cost", OMPython, os, simulation_length, modelica_variable_of_interest, extract_data_type, stretch_goal, simulation_results, new_simulation, component_name, dymola, system_of_interest_name, testbed_package_dot_notation, testbed_package_slash_notation, alternative_for_replacement_package_dot_notation, alternative_for_replacement, component_for_replacement_package_dot_notation, component_for_replacement, path_library_top_dir)
        OMPython.execute("cd(\"..\")")
        os.chdir("..")
    print "Starting visulization code"
    runAllVisualization(point_design_descriptions, scenario_descriptions, simulation_results, cost_scenario_descriptions,component_name,system_of_interest_name, cost_scenario_name)
    OMPython.execute("cd(\"..\")")
    os.chdir("..")



#Measure how long it takes to run
end_time = time.time()

print "Time to run this script in seconds:" + str(end_time - start_time)
print "Time to run this script in min:" + str((end_time - start_time)/60)

print "Completed simulating and boats and displaying their performance"


#Exit Dymola
if dymola is not None:
    dymola.close()
    dymola = None



##################################################### Later
#Auto generate ready to go test beds
#For each described scenario process
#for scenario_name in scenario_description_dict:
    #Copy the generic test bed (e.g. StraightLineRunning)
        #For each scenario configuration
            #Configure the Modify the test bed to the configuration (e.g. ConstantSun_BestSunEver)
        #end
    #end
#end

#Auto generate ready to go alternative designs (specific for the test bed)
#For each systems architecture
    #Copy the partial model
        #For each subsystem concept
            #Configure the architecture (e.g. V-Model, Podded thruster)
        #end
    #end
#end
##################################################### Later